<?xml version="1.0" encoding="UTF-8" ?>
<%@ Register tagprefix="xhtml" Namespace="web.Tools" Assembly="web" %>
<%@ Page language="c#" Codebehind="catalog.aspx.cs" AutoEventWireup="false" Inherits="web.muggins.catalog" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>
	<head>
		<title>
			<%=title%>
		</title>
		<link rel="stylesheet" href="<%=css%>" type="text/css" />
		<meta forua="true" http-equiv="Cache-Control" content="no-cache, max-age=0, must-revalidate, proxy-revalidate, s-maxage=0" />
	</head>
		<script type="text/javascript">
		function link(Url) {
			document.location.href = Url;
		} 
	</script>
	<body>
		
			<Xhtml:XhtmlTable id="tbTitle" Runat="server" CssClass="normal" />
			<Xhtml:XhtmlTable id="tbPreviews" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2">
				<Xhtml:XhtmlTableRow id="rowPreviews" Runat="server" />
				<Xhtml:XhtmlTableRow id="rowPreviews2" Runat="server" />
			</Xhtml:XhtmlTable>
			<Xhtml:XhtmlTable id="tbCatalog" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2" />
			<Xhtml:XhtmlTable id="tbPages" Runat="server" CssClass="normal" />
			<Xhtml:XhtmlTable id="tbLinks" Runat="server" CssClass="normal" />
			<Xhtml:XhtmlTable id="tbSearch" Runat="server" CssClass="normal" />
	
	</body>
</html>
