<?xml version="1.0" encoding="UTF-8" ?>
<%@ Register tagprefix="xhtml" Namespace="web.Tools" Assembly="web" %>
<%@ Page language="c#" Codebehind="step2.aspx.cs" AutoEventWireup="false" Inherits="web.muggins.step2" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>
  <head>
         <title>Im&aacute;genes y Fondos</title>
         <link rel="stylesheet" href="../3g.css" type="text/css" />
         <meta forua="true" http-equiv="Cache-Control" content="no-cache, max-age=0, must-revalidate, proxy-revalidate, s-maxage=0" />
  </head>
         <body>
                    <a id="start" name="start" />
					 <Xhtml:XhtmlTable id="tbHeader" Runat="server" CssClass="normal" />
					 <form action="download.aspx" method="post" accept-charset="utf-8">
					 
						<Xhtml:XhtmlTable id="tbAttitude" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowTitle" Runat="server" />
                    		<Xhtml:XhtmlTableRow id="rowAttitude" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>			
							<% if (sex == "BOY") {%>
								<td><select name="mood">
										<option value="in_love" selected="selected">enamorado</option>
										<option value="heartbroken">triste</option>
										<option value="cool">guay</option>
										<option value="flirt" >lig&#243;n</option>
										<option value="furious">enfadado</option>
										<option value="happy">contento</option>
									</select></td>										
							<% } else { %>
								<td><select name="mood">
										<option value="in_love" selected="selected">enamorada</option>
										<option value="heartbroken">triste</option>
										<option value="angry">enfadada</option>
										<option value="fun">divertida</option>
										<option value="happy">contenta</option>
										<option value="sexy">sexy</option>
										<option value="standard">normal</option>										
									</select></td>																	
							<% } %>
							</tr>
						</table>
						<Xhtml:XhtmlTable id="tbHairType" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowHairType" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
							<% if (sex == "BOY") { %>	
								<td><select name="hairStyle">
										<option value="dread" selected="selected">rasta</option>
										<option value="long">largo</option>
										<option value="posh">corto con flequillo</option>
										<option value="short">corto</option>
										<option value="soldier">rapado</option>
										<option value="spiky">de punta</option>
										<option value="">sin pelo</option>
									</select></td>						
							<% } else { %>
								<td><select name="hairStyle">										
										<option value="curly" selected="selected">largo ondulado</option>
										<option value="flat">largo liso</option>
										<option value="fringe">largo con flequillo</option>
										<option value="medium">media melena</option>
										<option value="short">corto</option>					
									</select></td>										
							<% } %>								
							</tr>
						</table>
						<Xhtml:XhtmlTable id="tbHair" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowHair" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
							<% if (sex == "BOY") {%>	
								<td><select name="hairColor">
										<option value="brown" selected="selected">marr&oacute;n oscuro</option>
										<option value="lbrown">marr&oacute;n claro</option>
										<option value="black">negro</option>
										<option value="blond">rubio</option>
										<option value="red">pelirrojo</option>										
									</select></td>				
							<% } else { %>
								<td><select name="hairColor">
										<option value="brown" selected="selected">marr&oacute;n oscuro</option>
										<option value="black">negro</option>
										<option value="red">pelirrojo</option>
										<option value="blond">rubio</option>
										<option value="violet">violeta</option>										
									</select></td>		
							<% } %>
							</tr>
						</table>						
						<Xhtml:XhtmlTable id="tbMuggin" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowJacket" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
							<% if (sex == "BOY") {%>	
								<td><select name="clothes">
										<option value="striped_blouse" selected="selected">camiseta rayas</option>
										<option value="polo_tshirt">polo</option>										
										<option value="classic_jacket">chaqueta cl&aacute;sica</option>
										<option value="motor_jacket">chupa de motero</option>
										<option value="hoodie">sudadera</option>
										<option value="coat">abrigo</option>										
										<option value="shirt">camisa</option>
										<option value="sweatshirt">sudadera capucha</option>
										<option value="tshirt">camiseta</option>
										<option value="nadal">camiseta de tirantes</option>										
										<option value="tank_top">chaleco</option>
										<option value="spain_equipment_t_shirt">selecci&oacute;n Espa&#241;ola</option>										
										<option value="spain_tshirt">fan de Espa&#241;a</option>					
									</select></td>
							<% } else { %>
								<td><select name="clothes">										
										<option value="50s_dress" selected="selected">vestido de los 50</option>
										<option value="corset_tshirt">corset</option>
										<option value="skirt_overall">peto</option>
										<option value="evening_gown">vestido de noche</option>										
										<option value="short_dress_stars">vestido corto</option>										
										<option value="tanktop">top tirantes</option>										
										<option value="top">top</option>										
										<option value="waistcoat">chaleco</option>										
										<option value="trench">gabardina</option>
										<option value="spain_equipment_t_shirt">selecci&oacute;n Espa&#241;ola</option>											
									</select></td>								
							<% } %>
							</tr>
						</table>
						
						<Xhtml:XhtmlTable id="colorClothes" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowColorClothes" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
								<td><select name="colorClothes">
										<option value="black" selected="selected">negro</option>																		
										<option value="blue">azul</option>
										<option value="green">verde</option>		
										<option value="red">rojo</option>											
										<option value="yellow">amarillo</option>																				
								<% if (sex == "GIRL") {%>	
										<option value="white">blanco</option>
										<option value="purple">p&uacute;rpura</option>										
										<option value="brown">marr&oacute;n</option>								
								<% } %>		
								</select></td>																							
							</tr>
						</table>
						
						<Xhtml:XhtmlTable id="tbTrouser" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowTrouser" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
							<% if (sex == "BOY") {%>	
								<td><select name="trousers">
										<option value="beach_long" selected="selected">piratas</option>
										<option value="beach_shorts">ba&#241;ador</option>
										<option value="biker_trousers">bermudas</option>
										<option value="baggies">con bolsos</option>
										<option value="classics">cl&aacute;sicos</option>
										<option value="elegant">elegantes</option>
										<option value="scottish">escoceses</option>
										<option value="skater_low">skater</option>
										<option value="velvet_striped">terciopelo</option>
										<option value="velvet_striped">terciopelo</option>
										<option value="jeans">vaqueros</option>
										<option value="pleated">pinzas</option>
										<option value="sport">chandal</option>
										<option value="nadal_shorts">pantal&oacute;n corto</option>										
										<option value="underwear">gallumbos</option>								
									</select></td>								
							<% } else { %>
								<td><select name="trousers">
										<option value="high_waisted_skirt" selected="selected">falda rodilla</option>
										<option value="pleated_skirt">falda corta</option>
										<option value="tartan">falda de cuadros</option>
										<option value="mini_skirt_red">minifalda roja</option>
										<option value="skinny_jeans" >pitillo</option>
										<option value="bell_jeans">vaqueros campana</option>
										<option value="spain_equipment_short">selecci&oacute;n Espa&#241;ola</option>							
									</select></td>	
							<% } %>
								
							</tr>
						</table>
						<Xhtml:XhtmlTable id="tbColorTrousers" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowColorTrousers" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
									<td><select name="colorTrousers">
										<option value="black" selected="selected">negro</option>
										<option value="blue">azul</option>
										<option value="brown">marr&oacute;n</option>
										<option value="green">verde</option>
										<option value="white">blanco</option>																					
										<option value="red">rojo</option>										
							<% if (sex == "GIRL") {%>	
										<option value="yellow">amarillo</option>
										<option value="khaki">kaki</option>
										<option value="purple">p&uacute;rpura</option>
							<% } %>
									</select></td>	
							</tr>
						</table>
						<% if (sex == "GIRL") {%>	
						<Xhtml:XhtmlTable id="tbUnderwear" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowUnderwear" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
								<td><select name="underwear">
										<option value="leggins" selected="selected">medias</option>
										<option value="stripedleggins">medias rayadas</option>
										<option value="">sin nada</option>										
									</select></td>	
							</tr>
						</table>
						<Xhtml:XhtmlTable id="tbColorUnderwear" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowColorUnderwear" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
								<td><select name="colorUnderwear">
										<option value="yellow" selected="selected">amarillo</option>
										<option value="blue">azul</option>
										<option value="white">blanco</option>										
										<option value="khaki">kaki</option>										
										<option value="brown">marr&oacute;n</option>										
										<option value="black">negro</option>										
										<option value="purple">p&uacute;rpura</option>										
										<option value="red">rojo</option>										
										<option value="green">verde</option>										
									</select></td>	
							</tr>
						</table>
						<% } %>
						<Xhtml:XhtmlTable id="tbShoes" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowShoes" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
							<% if (sex == "BOY") {%>	
								<td><select name="shoes">
										<option value="beach_slippers1" selected="selected">hawaianas</option>
										<option value="beach_slippers">chanclas</option>
										<option value="classie">antiguos</option>
										<option value="sneakers2">deportivos</option>
										<option value="sporty_boots">botas de f&uacute;tbol</option>
										<option value="all_stars">all stars</option>
										<option value="casual">cl&aacute;sicos</option>
										<option value="sport_shoes">deportivos</option>																				
									</select></td>													
							<% } else { %>								
								<td><select name="shoes">
										<option value="ankle_belt" selected="selected">zapatos con cintas</option>
										<option value="roman_sandals">sandalias romanas</option>
										<option value="wedges">zapatos de plataforma</option>
										<option value="all_stars">all stars</option>																		
										<option value="ballerina">bailarinas</option>																		
										<option value="beach_slippers">chanclas</option>																		
										<option value="booties">botines</option>																		
										<option value="high_heels">zapatos tac&oacute;n</option>										
									</select></td>		
							<% } %>	
							</tr>
						</table>
						<Xhtml:XhtmlTable id="tbColorShoes" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowColorShoes" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>								
								<td><select name="colorShoes">
										<option value="yellow" selected="selected">amarillo</option>
										<option value="blue">azul</option>
										<option value="white">blanco</option>										
										<option value="brown">marr&oacute;n</option>										
										<option value="black">negro</option>																		
							<% if (sex == "GIRL") {%>	
										<option value="gold">dorado</option>
										<option value="silver">plateado</option>
										<option value="red">rojo</option>										
										<option value="green">verde</option>										
							<% } %>		
								</select></td>										
							</tr>
						</table>
						
						<input type="hidden" name="sex" value="<%=sex%>" />
						<input type="hidden" name="skin" value="<%=skin%>" />
						<input type="hidden" name="eyes" value="<%=eyes%>" />
						<input type="hidden" name="c" value="<%=c%>" />
						<br/><input type="submit" value="DALE VIDA" class="caja" />
					</form>
					<hr />
                   <table width="100%">
						<tr>
							<td align="center" class="LongCell"><a href="./catalog.aspx" style="color: #696969">Volver</a></td>
						</tr>
					</table>
          </body>
</html>
