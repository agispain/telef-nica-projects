using System;
using KMobile.Catalog.Services;
using web.Tools;

namespace web.sexy
{
	public class linkto : System.Web.UI.Page
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			MobileCaps _mobile = (MobileCaps)Request.Browser;
			int idSite = (Request.QueryString["id"] != null && Request.QueryString["id"] != "") ? Convert.ToInt32(Request.QueryString["id"]) : 0;
			if (idSite > 0)
			{
				try
				{ 
					WapTools.SetHeader(this.Context);
					//WapTools.AddUIDatLog(Request, Response);
				}
				catch{}
				try
				{
					WapTools.LogUser(this.Request, idSite, _mobile.MobileType);
					Response.Redirect(String.Format("../catalog.aspx?cg={0}&cs={1}&p={2}&t={3}&s=1", Request.QueryString["cg"], idSite.ToString(), Request.QueryString["p"], Request.QueryString["t"]), false);
				}
				catch{Response.Redirect(String.Format("../catalog.aspx?cg={0}&cs={1}&p={2}&t={3}&s=1", Request.QueryString["cg"], idSite.ToString(), Request.QueryString["p"], Request.QueryString["t"]), true);}
			}
			else
				Response.Redirect("../default.aspx", false);
		}
		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
