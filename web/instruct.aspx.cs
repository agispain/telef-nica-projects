using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using KMobile.Catalog.Services;
using web.Tools;

namespace web
{
	public class instruct : System.Web.UI.Page
	{
		protected XhtmlTable tbHeader, tbTitle, tbPreviews;
		protected XhtmlTableRow rowPreviews, rowPreviews2;
		public string volver, subir;
		protected int ms;
		protected string text = "", page = "";
		public bool is3g = false;

		private void Page_Load(object sender, System.EventArgs e)
		{
			MobileCaps _mobile = (MobileCaps)Request.Browser;
			try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType));}
			catch{is3g = false;}

			try
			{
				WapTools.SetHeader(this.Context);
				WapTools.AddUIDatLog(Request, Response, this.Trace);				
			} 
			catch{}

			try 
			{
				text = "<b>Ap&uacute;ntate a la Zona Vip y participa al Sorteo de 2 Nokia 6110 NAVIGATOR</b><br />"+
						"<b>MODO DE PARTICIPACI&Oacute;N</b><br />"+
						"Participar&aacute;n en el sorteo todos aquellos usuarios que se subscriban al Club de Fondos ZONA VIP (precio subscripci&oacute;n 4 euros, 4,64 con iva.) del sitio de im&aacute;genes de MoviStar emoci&oacute;n durante el per&iacute;odo de la promoci&oacute;n.<br />"+
						"La participaci&oacute;n en esta acci&oacute;n promocional es gratuita, no implica para el consumidor incremento alguno en el precio de los productos adquiridos, salvo el precio habitual de mercado del servicio prestado.<br />"+
						"Con cada suscripci&oacute;n el titular de la l&iacute;nea telef&oacute;nica obtendr&aacute; una participaci&oacute;n para el sorteo.<br />"+
						"<br /><br />"+
						"<b>PREMIOS</b><br />"+
						"2 M&oacute;viles Nokia 6110 NAVIGATOR<br />"+
						"Bandas de operaci&oacute;n: HSDPA / 3G<br />" +
						"Otras caracter&iacute;sticas: GPS incorporado, 101 x 49 x 20 mm, 120 gr, Cliente Correo Electr&oacute;nico POP3, SMTP, IMAP4 C&aacute;mara 2 MP, v&iacute;deo, mp3<br />" +
						"La empresa AG Interactive Ib&eacute;rica, S.L., empresa responsable de esta promoci&oacute;n, ser&aacute; la encargada de realizar el sorteo y entregar los premios a los ganadores.<br />" +
						"<br /><br />"+
						"<b>PERIODO DE PROMOCI&Oacute;N</b><br />"+
						"Fecha de Inicio: mi&eacute;rcoles 25 Septiembre 2008<br />"+
						"Fecha de Fin: s&aacute;bado 1 de Noviembre 2008<br />"+
						"<br /><br />"+
						"<b>FINALIDAD</b><br />"+
						"La finalidad del concurso la promoci&oacute;n es la potenciaci&oacute;n de la suscripci&oacute;n al Club de Fondos denominado Zona Vip, as&iacute; como la fidelizaci&oacute;n y recompensa de los usuarios de emoci&oacute;n, mediante una promoci&oacute;n que se celebrar&aacute; en todo el territorio nacional de Espa&ntilde;a desde el 25 Septiembre 2008 al 1 de Noviembre de 2008 ambos inclusive<br />"+
						"<br /><br />"+
						"<b>GANADORES</b><br />"+
						"El sorteo se llevar&aacute; a cabo mediante un programa inform&aacute;tico \"Instant Win\" que de forma aleatoria seleccionar&aacute; dos momentos ganadores durante el periodo promocional<br />"+
						"A cada usuario que participe en la promoci&oacute;n se le notificar&aacute; inmediatamente al pago del servicio si ha sido afortunado o no mediante una pantalla informativa. Al ganador le aparecer&aacute; un mensaje personalizado con un n&uacute;mero de tel&eacute;fono de contacto para tramitar el premio, as&iacute; como una clave de identificaci&oacute;n.<br />" + 
						"Por el hecho de aceptar el premio, el usuario premiado quedar� obligado a ceder la comunicaci�n publica de sus datos personales de nombre, lugar de residencia o provincia con el fin de a�adir transparencia a esta promoci�n.<br/>" +
						"Quedan excluidos de esta promoci&oacute;n expresamente los empleados de Telefonica M&oacute;viles S.A.U y AG Interactive Ib&eacute;rica, S.L.<br />" + 
						"AG Interactive Ib&eacute;rica, S.L., se reserva el derecho de interpretaci&oacute;n de las presentes bases en caso de reclamaci&oacute;n<br />";
						
				
				XhtmlTools.AddTextTableRow("", rowPreviews2, "", text, Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall);
				
				XhtmlImage bmimg1 = new XhtmlImage();
				bmimg1.ImageUrl = WapTools.GetImage(this.Request, "vip");
				XhtmlTools.AddImgTableRow(rowPreviews, bmimg1);
				tbPreviews.Controls.Add(rowPreviews);
				XhtmlTools.AddLinkTable("IMG", tbPreviews, "ENTRA en la Zona Vip de Fondos haciendo clic AQU�", "./linkto.aspx?id=11036", Color.Empty, Color.Empty, 2, HorizontalAlign.Center, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "club"));
				XhtmlTools.AddLinkTable("IMG", tbPreviews, "Volver", "./club2.aspx", Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "club"));
				
				bmimg1 = null;
				
			}
			catch(Exception caught) 
			{
					WapTools.SendMail(HttpContext.Current, caught);
					Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\xhtml\\instruct.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
					Response.Redirect("./error.aspx");	
			}
			
			#region HEADER
			XhtmlImage img = new XhtmlImage();
			img.ImageUrl = WapTools.GetImage(this.Request, "bannervip",  _mobile.ScreenPixelsWidth, is3g);
			XhtmlTools.AddImgTable(tbHeader, img); 
		
			#endregion

			#region PICTOS
			if(is3g)
			{
				volver = WapTools.getPicto3g(this.Request.ApplicationPath, "volver_club", _mobile.ScreenPixelsWidth);
				subir = WapTools.getPicto3g(this.Request.ApplicationPath, "subir_club", _mobile.ScreenPixelsWidth);
			}
			
			_mobile = null;
			#endregion
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
