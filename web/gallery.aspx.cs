using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using KMobile.Catalog.Services;
using web.Tools;

namespace web
{
	public class gallery : System.Web.UI.Page
	{
		protected XhtmlTable tbHeader;
		protected XhtmlTableRow rowTitle, rowError, rowError2;
		protected web.Tools.XhtmlTable tbDescription;
		public bool is3g = false;

		private void Page_Load(object sender, System.EventArgs e)
		{
			MobileCaps _mobile = (MobileCaps)Request.Browser;
			try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType));}
			catch{is3g = false;}

			try
			{
				WapTools.SetHeader(this.Context);
				WapTools.AddUIDatLog(Request, Response, this.Trace);		
			}
			catch{}
			try
			{
				#region HEADER
			XhtmlImage img = new XhtmlImage();
			img.ImageUrl = WapTools.GetImage(this.Request, "imagenes",  _mobile.ScreenPixelsWidth, is3g);
			XhtmlTools.AddImgTable(tbHeader, img);
			#endregion
			
				if (Request.QueryString["id"] != null && Request.QueryString["id"] != "")
				{
					XhtmlTools.AddTextTableRow("IMG", rowTitle, "", WapTools.GetText("TitleGallery" + Request.QueryString["id"]), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall);
					XhtmlTools.AddTextTableRow("IMG", rowError, "", WapTools.GetText("DescGallery" + Request.QueryString["id"]), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall);
					XhtmlTools.AddLinkTableRow("", rowError2, WapTools.GetText("LnkGallery"), "./linkto.aspx?id=" + Request.QueryString["id"], Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
				}
				else
				{
					XhtmlTools.AddTextTableRow("IMG", rowTitle, "", WapTools.GetText("TitleGallery"), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall);
					XhtmlTools.AddTextTableRow("", rowError, "", WapTools.GetText("DescGallery"), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall);
					rowError2.Visible = false;
					foreach (string id in WapTools.getNodes("Gallerys"))
					{
						XhtmlTableRow row = new XhtmlTableRow();
						XhtmlTools.AddLinkTableRow("", row, WapTools.GetText("TitleGallery" + id), "./linkto.aspx?id=" + id, Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						tbDescription.Rows.Add(row);
						row = null;
					}
//					XhtmlTableRow row = new XhtmlTableRow();
//					XhtmlTools.AddLinkTableRow("", row, WapTools.GetText("TitleGallery11014"), "./linkto.aspx?id=11014", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
//					tbDescription.Rows.Add(row);
//					row = new XhtmlTableRow();
//					XhtmlTools.AddLinkTableRow("", row, WapTools.GetText("TitleGallery11021"), "./linkto.aspx?id=11021", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
//					tbDescription.Rows.Add(row);
//					row = new XhtmlTableRow();
//					XhtmlTools.AddLinkTableRow("", row, WapTools.GetText("TitleGallery11022"), "./linkto.aspx?id=11022", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
//					tbDescription.Rows.Add(row);

					//XhtmlTools.AddLinkTableRow("", rowError2, WapTools.GetText("LnkGallery"), "./linkto.aspx?id=" + Request.QueryString["id"], Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));				
				}
			}
			catch(Exception caught)
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\xhtml\\gallery.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
				Response.Redirect("./error.aspx");	
			}

		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}