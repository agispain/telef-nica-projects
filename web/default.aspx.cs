using System;
using System.Collections;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Presentation;
using KMobile.Catalog.Services;
using web.Tools;

namespace web
{	
	public class _default : XCatalogBrowsing  
	{
		private ArrayList _contentCollImg = new ArrayList(); 
		private ArrayList _contentCollContentSet = new ArrayList();  
		protected XhtmlTable tbPub, tbPub2;  
		protected XhtmlTableRow row1;
		
		int init = 0, day;
		public bool isIE = false, is2x1 = false;
		public string live_wp, group;
		public String[] destacados = new string[14];
		public String[] destacadosLnks = new string[14];

		private void Page_Load(object sender, System.EventArgs e)
		{ 
			try
			{
				_mobile = (MobileCaps)Request.Browser;
				group = (_mobile.ScreenPixelsWidth > 240) ? "web" : "web16";
				
				try{isIE = (_mobile.MobileType == "_IE");}
				catch{isIE = false;}

				try
				{
					WapTools.SetHeader(this.Context);
					WapTools.LogUser(this.Request, 197, _mobile.MobileType);
					WapTools.AddUIDatLog(Request, Response, this.Trace);		
				} 
				catch{} 

				if (_mobile.MobileType != null) 
				{	 
					_displayKey = WapTools.GetXmlValue("DisplayKey");
					_idContentSet = Convert.ToInt32(WapTools.GetXmlValue("Home/Composite"));					
					day = (Request.QueryString["day"] != null) ? Convert.ToInt32(Request.QueryString["day"]) : 0;
					_contentGroup = "IMG";
					BrowseContentSetExtended(null, -1, -1); 

					if( _mobile.IsCompatible("IMG_COLOR") )
					{
						#region PUB
						try
						{
							//	WapTools.CallNewPubTEF2(this.Trace, this.Request, this.Request.Headers["TM_user-id"], tbPub2, (is3g) ? "es.emocion_20.imagenesyfondos.home.bottom" : "es.emocion_10.imagenesyfondos.home.bottom");
							WapTools.CallNewPubTEF2(this.Trace, this.Request, this.Request.Headers["TM_user-id"], tbPub, "es.m_touch_avanzado.imagenes.banner.top");
							WapTools.CallNewPubTEF2(this.Trace, this.Request, this.Request.Headers["TM_user-id"], tbPub2, "es.m_touch_avanzado.imagenes.banner.bottom");
						}
						catch{}
						#endregion   
    
						try      
						{ 
							init = DateTime.Now.Millisecond;			
							int value = Convert.ToInt32(WapTools.GetXmlValue("Home/Subcomposite"));
							if (_contentCollImg.Count > 0) {
								DisplayImages(row1, "IMG", "IMG_COLOR", (init % value), 10);
							}  
						}
						catch{}

						
					}
				}
			}
			catch(Exception caught)
			{  
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\xhtml\\default.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
				Response.Redirect("./error.aspx");				
			}
			finally
			{
				_contentCollImg = null;
				_contentCollContentSet = null;
			}
		}


		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
		
		#region Override
		protected override void DisplayContentSet(Content content, System.Web.UI.MobileControls.Panel pnl)
		{
			_contentCollContentSet.Add(content);
		}

		protected override void DisplayImg(Content content, System.Web.UI.MobileControls.Panel pnl)
		{
			//if (WapTools.isJunior(_mobile) && content.ContentRatings[0].Value > 0) return;
			/*if (cc != null && cc.Count > 0)
			{
				foreach (Command c in cc)
					if (c.Item.ContentId == content.IDContent)
						return;
			}*/
			if (content.ContentGroup.Name == "IMG") _contentCollImg.Add(content);
		}
		#endregion

		#region Display
		public void DisplayImages(TableRow row, string contentGroup, string contentType, int start, int nb)
		{			
			Content content = null;
			_imgDisplayInst = new ImgDisplayInstructions(_mobile);

			_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(WapTools.PreviewUrl(contentGroup, _mobile));

			_imgDisplayInst.TextDwld = WapTools.GetText("Download");
			_imgDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "Img");

			if (Convert.ToBoolean(WapTools.GetXmlValue("Home/Show_view")))
				_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, contentGroup, contentType, HttpUtility.UrlEncode("web|HOME"), "", "0");
			else
				_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(contentGroup, contentType, HttpUtility.UrlEncode("web|HOME"), "", "0", _mobile.MobileType, 0); 			
               
			TableItemStyle tableStyle = new TableItemStyle();
			tableStyle.CssClass="cell";
			//tableStyle.HorizontalAlign = HorizontalAlign.Center;
			int j = 0;
			destacadosLnks[j] = "./linkto.aspx?cg=COMPOSITE&amp;id=3619&amp;cl=1";
			destacados[j++] = WapTools.GetImage(this.Request, "banner_099");						
			destacadosLnks[j] = "./linkto.aspx?cg=COMPOSITE&amp;id=5862&amp;cl=1";
			destacados[j++] = WapTools.GetImage(this.Request, "banner_199");						
			destacadosLnks[j] = "./linkto.aspx?id=11039";
			destacados[j++] = WapTools.GetImage(this.Request, "banner_ava");		
			//destacadosLnks[j] = "./linkto.aspx?id=5793&cg=IMG";
			//destacados[j++] = WapTools.GetImage(this.Request, "banner_pascua");			
	
												
			for(int i = start; i < start + nb; i++ )  
			{
				if (contentGroup == "IMG")
					content = (Content)_contentCollImg[ (i) % _contentCollImg.Count];

				if (content != null)  
				{
					//					if (content.ContentRatings[0].Value > 0)
					//						_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(this.Request, contentGroup, contentType, (is3g) ? HttpUtility.UrlEncode("3g|HOME") : HttpUtility.UrlEncode("xhtml|HOME"), "", "0", _mobile.MobileType);
					//					else
					//						_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, contentGroup, contentType, (is3g) ? HttpUtility.UrlEncode("3g|HOME") : HttpUtility.UrlEncode("xhtml|HOME"), "", "0");
			
					destacadosLnks[j] = String.Format(_imgDisplayInst.UrlDwld, WapTools.isBranded(content) ? "branded" : "", WapTools.GetDefaultContentType(content.ContentGroup.Name), content.IDContent );
					destacados[j++] = String.Format(_imgDisplayInst.PreviewMaskUrl, content.ContentName.Substring(0, 1), content.ContentName);
							
					
/*
					XhtmlTableCell tempCell = new XhtmlTableCell();
					ImgDisplay imgDisplay = new ImgDisplay(_imgDisplayInst);
					imgDisplay.Display(tempCell, content);
					imgDisplay = null;
					tempCell.ApplyStyle(tableStyle);
					row.Cells.Add(tempCell);
					tempCell = null;*/
				}
			}  
			content = null;
			_imgDisplayInst = null;
			tableStyle = null;
		}          
        
		public void DisplayContentSets(XhtmlTable t, string cg, int rangeInf, int rangeSup)
		{
			_contentSetDisplayInst = new ContentSetDisplayInstructions(_mobile);
			_contentSetDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "bullet");
			_contentSetDisplayInst.UrlDwld = "./linkto.aspx?id={0}&cg={1}";
			XhtmlTableCell cell = new XhtmlTableCell();
			XhtmlTableRow row = new XhtmlTableRow();
			if (rangeSup == -1) rangeSup = _contentCollContentSet.Count;
			if (rangeSup > _contentCollContentSet.Count) rangeSup = _contentCollContentSet.Count;
			for( int i = rangeInf; i < rangeSup; i++ )
			{
				Content content = (Content)_contentCollContentSet[i];
				//if (WapTools.FindProperty(content.PropertyCollection, "CompositeContentGroup") != cg) continue;
				ContentSetDisplay contentSetDisplay = new ContentSetDisplay(_contentSetDisplayInst);
				contentSetDisplay.Display(cell, content, true);			
				contentSetDisplay = null;
				row.Controls.Add(cell);
				cell = new XhtmlTableCell();
				t.Controls.Add(row);
				row = new XhtmlTableRow();
				content = null;
			}
			//t.Controls.Add(row);
			_contentSetDisplayInst = null;
			cell = null; row = null;
		}
		#endregion
	}
}
