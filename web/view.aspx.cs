using System;
using System.Configuration;
using System.Web;
using System.Drawing;
using System.Web.UI.WebControls;
using KMobile.Catalog.Services;
using KMobile.Catalog.Presentation;
using web.Tools;

namespace web
{
	public class view : XCatalogBrowsing
	{
		protected XhtmlTable tbTitle, tbLinks;
		protected XhtmlTableRow rowLinkPPD, rowPreview, rowClub1, rowClub2;
		protected Panel pnlFooter;
		protected string urlBillingPPD, urlBillingClub;
		protected XhtmlTable tbHeader, tbPreviews, tbContentset;
		private Content content = null;
		//public string buscar, emocion, atras, up, fondo, back, css;
		public bool is3g = false;

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				_mobile = (MobileCaps)Request.Browser;
				try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType));}
				catch{is3g = false;}

				WapTools.SetHeader(this.Context);
				WapTools.AddUIDatLog(Request, Response, this.Trace);				
					
				int idContent = (Request.QueryString["c"] != null) ? Convert.ToInt32(Request.QueryString["c"]) : 0;
				_idContentSet = (Request.QueryString["cs"] != null && Request.QueryString["cs"] != "") ? Convert.ToInt32(Request.QueryString["cs"]) : 0;
				_displayKey = WapTools.GetXmlValue("DisplayKey");
				string referer = (Request.QueryString["ref"] != null) ? Request.QueryString["ref"].ToString() : "";
				try{content = StaticCatalogService.GetContentInfos(_displayKey, idContent, _mobile.MobileType, _contentType);}
				catch {content = null;} 
				
				if (content != null)
				{
					_contentGroup = content.ContentGroup.Name;
					_contentType = WapTools.GetDefaultContentType(_contentGroup);
					
					XhtmlTableRow row = new XhtmlTableRow();
					XhtmlTools.AddTextTableRow("IMG", row, "", "Descarga " + content.Name.ToUpper(), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall);
					tbTitle.Controls.Add(row); 
					row = null; 

//					XhtmlImage img = new XhtmlImage();
//					img.ImageAlign = ImageAlign.Middle;
//					if (content.Preview.URL != null)
//						img.ImageUrl = content.Preview.URL; 
//					else
//						img.ImageUrl = String.Format(WapTools.GetXmlValue("Url_" + _contentGroup), content.ContentName.Substring(0,1), content.ContentName);						
//					XhtmlTools.AddImgTableRow(rowPreview, img);
//					img = null;
					//rowPreview.Visible = false;

					
					if (_mobile.IsCompatible(_contentType))
					{
						urlBillingPPD = String.Format(WapTools.GetUrlBilling(_contentGroup, _contentType, HttpUtility.UrlEncode(referer), null, _idContentSet.ToString(), _mobile.MobileType),  WapTools.isBranded(content) ? "branded" : "", _contentType, content.IDContent, _mobile.MobileType, false);
						XhtmlTools.AddLinkTableRow(_contentGroup, rowLinkPPD, Server.HtmlEncode(WapTools.GetText("BillingPPD")), urlBillingPPD, Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
					
						if (referer.IndexOf("xhtml") >= 0)
							referer = referer.Replace("xhtml", "CLUB_xHTML");
						else if (referer.IndexOf("3g") >= 0)
							referer = referer.Replace("3g", "CLUB_xHTML");
						
						urlBillingClub = String.Format(WapTools.GetUrlBillingClub099(this.Request, _contentGroup, _contentType, HttpUtility.UrlEncode(referer), null, "0"), "", _contentType, content.IDContent);
						//XhtmlTools.AddLinkTableRow(_contentGroup, rowClub1, Server.HtmlEncode(WapTools.GetText("BillingClub1")), urlBillingClub, Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						XhtmlTools.AddLinkTableRow(_contentGroup, rowClub2, Server.HtmlEncode(WapTools.GetText("BillingClub2")), urlBillingClub, Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
					}
					else
						XhtmlTools.AddTextTableRow(_contentGroup, rowLinkPPD, WapTools.GetImage(this.Request, "bullet"), Server.HtmlEncode(WapTools.GetText("Compatibility")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall);
				}			
				
				#region HEADER
				XhtmlImage img2 = new XhtmlImage();
				img2.ImageUrl = WapTools.GetImage(this.Request, "imagenes",  _mobile.ScreenPixelsWidth, is3g, WapTools.isSomething(_mobile.MobileType, "TouchWeb"));
				XhtmlTools.AddImgTable(tbHeader, img2);
				#endregion
                                     
				_mobile = null;

				// Search
				content = null;
			}
			catch(Exception caught)
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\xhtml\\view.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
				Response.Redirect("./error.aspx");				
			}
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
