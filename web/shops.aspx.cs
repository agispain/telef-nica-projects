using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using KMobile.Catalog.Services;
using web.Tools;

namespace web
{
	public class shops : System.Web.UI.Page
	{
		protected XhtmlTable tbHeader, tbShops;
		private int idShop;
		public string css = "xhtml.css", volver, subir;
		public bool is3g = false, isWeb = false;
	
		private void Page_Load(object sender, System.EventArgs e) 
		{
			try
			{
				MobileCaps _mobile = (MobileCaps)Request.Browser;
				idShop = (Request.QueryString["id"] != null) ? Convert.ToInt32(Request.QueryString["id"]) : 0;
				try{isWeb = WapTools.isSomething(_mobile.MobileType, "TouchWeb");}
				catch{isWeb = false;}
				
				if (!isWeb) 
				{
					try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType));}
					catch{is3g = false;}
				} 
				else
				{
					css = "web.css";
				}

				try
				{ 
					WapTools.SetHeader(this.Context);
					WapTools.AddUIDatLog(Request, Response, this.Trace);				
				}
				catch{}
				
				
				#region PUB
				if (idShop != 27)
				{
					try
					{
						//WapTools.CallNewPubTEF2(this.Trace, this.Request, this.Request.Headers["TM_user-id"], tbShops, "es.m_touch_avanzado.imagenes.banner.top");
					}
					catch{}					
				}
				#endregion  

				XhtmlImage img = new XhtmlImage();
				if(idShop == 27)
				{
					img.ImageUrl = WapTools.GetImage(this.Request, "felicitaciones",  _mobile.ScreenPixelsWidth, is3g, WapTools.isSomething(_mobile.MobileType, "TouchWeb"));
					//XhtmlTools.AddImgTable(tbHeader, img);
					XhtmlTools.AddLinkTable("", tbShops, WapTools.GetText("AnimaNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/ANIMANOMBRES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
					if (_mobile.IsCompatible("VIDEO_DWL"))
					{
						XhtmlTools.AddLinkTable("", tbShops, WapTools.GetText("VideoNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEONOMBRES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
					//	if (_mobile.IsCompatible("VIDEO_DWL"))
						XhtmlTools.AddLinkTable("", tbShops, WapTools.GetText("VideoAnimaciones"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEOANIMACIONES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
					}
				}
				else if (idShop == 34)
				{
					img.ImageUrl = WapTools.GetImage(this.Request, "imagenes",  _mobile.ScreenPixelsWidth, is3g, WapTools.isSomething(_mobile.MobileType, "TouchWeb"));
					XhtmlTools.AddImgTable(tbHeader, img);
/*				
1 Settings - Display - Wallpaper or Screen Saver.
2 Then, select Image - Theme, choose the file to set it as the screensaver.

Series 60 phones:
1 Tools - Settings - General - Personalisation - Themes - power saver
2 Then select Animation - Select Animation File, then browse the .swf file from �Others� folder of both Phone and Memory Card, choose the file to set as screensaver.
*/
					string text ="<br/><b>Nokia Serie 40</b>:<br/>Ajustes - Display - Wallpaper o Screensaver.<br/>Selecciona Imagen - Tema, y escoge el fichero SWF que quieras usar.<br/><br/>" +
						"<b>Nokia Serie 60</b>:<br/>Herramientas - Ajustes - Generales - Personalizaci&oacute;n - Temas - Ahorro Energ&iacute;<br>Selecciona Animaci&oacute;n y busca el fichero SWF que quieras usar como salvapantallas.";
					XhtmlTools.AddTextTable(tbHeader, text, Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XSmall);
					XhtmlTools.AddLinkTable("", tbHeader, "Volver a FLASH", String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}", WapTools.GetXmlValue("Home/FLASH")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));					
					
					
				}
				else
				{
					img.ImageUrl = WapTools.GetImage(this.Request, "portales",  _mobile.ScreenPixelsWidth, is3g, WapTools.isSomething(_mobile.MobileType, "TouchWeb"));
					//XhtmlTools.AddImgTable(tbHeader, img);			
					XhtmlTools.AddLinkTable("LongCell", tbShops, WapTools.GetText("Shop18"), "./linkto.aspx?id=18", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
					//if (!(WapTools.isSomething(_mobile.MobileType, "Android")))
					//{
					XhtmlTools.AddLinkTable("LongCell", tbShops, WapTools.GetText("Shop1"), "./linkto.aspx?id=1", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, "");				
					XhtmlTools.AddLinkTable("LongCell", tbShops, WapTools.GetText("Shop4"), "./linkto.aspx?id=4", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, "");											
					//}
					XhtmlTools.AddLinkTable("LongCell", tbShops, WapTools.GetText("Shop7"), "./linkto.aspx?id=7", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
					//XhtmlTools.AddLinkTable("", tbShops, WapTools.GetText("Shop2"), "./linkto.aspx?id=2", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
					//XhtmlTools.AddLinkTable("", tbShops, WapTools.GetText("Shop10"), "./linkto.aspx?id=10", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
					XhtmlTools.AddLinkTable("LongCell", tbShops, WapTools.GetText("Shop12"), "./linkto.aspx?id=12", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
					//XhtmlTools.AddLinkTable("", tbShops, WapTools.GetText("Shop8"), "./linkto.aspx?id=8", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
					//if (!(WapTools.isSomething(_mobile.MobileType, "Android")))
					//{
					XhtmlTools.AddLinkTable("LongCell", tbShops, WapTools.GetText("Shop3"), "./linkto.aspx?id=3", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
					//	XhtmlTools.AddLinkTable("", tbShops, WapTools.GetText("Shop4"), "./linkto.aspx?id=4", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));											
					//}
					
					//XhtmlTools.AddLinkTable("", tbShops, WapTools.GetText("Shop5"), "./linkto.aspx?id=5", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
					
				//	XhtmlTools.AddLinkTable("", tbShops, WapTools.GetText("Shop13"), "./linkto.aspx?id=13", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
				//	XhtmlTools.AddLinkTable("", tbShops, WapTools.GetText("Shop16"), "./linkto.aspx?id=16", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
				//	XhtmlTools.AddLinkTable("", tbShops, WapTools.GetText("Shop15"), "./linkto.aspx?id=15", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
					XhtmlTools.AddLinkTable("LongCell", tbShops, WapTools.GetText("Shop17"), "./linkto.aspx?id=17", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
				}
				#region 3G
				if (is3g)
				{
					css = "3g.css";
					volver = WapTools.getPicto3g(this.Request.ApplicationPath, "volver", _mobile.ScreenPixelsWidth);
					subir = WapTools.getPicto3g(this.Request.ApplicationPath, "subir", _mobile.ScreenPixelsWidth);
				}
				#endregion

				#region PUB2
				if (idShop != 27)
				{					
					try
					{
						//WapTools.CallNewPubTEF2(this.Trace, this.Request, this.Request.Headers["TM_user-id"], tbShops, "es.m_touch_avanzado.imagenes.banner.bottom");
					}
					catch{}
				}
				#endregion  

				#region PICTOS
				_mobile = null;
				#endregion
			}
			catch(Exception caught)
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\xhtml\\shops.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
				Response.Redirect("./error.aspx");				
			}
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}