
using System;
//using System.Collections.Generic;
//using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;

namespace usingparameterwebservice2
{
	/// <summary>
	/// Summary description for mywebservice
	/// </summary>
	[WebService(Namespace = "mystoredprocedure2.org")]
	//[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
	//[System.Web.Script.Services.ScriptService]
	public class mywebservice : System.Web.Services.WebService
	{
		string constring = "Database=AGI_IC;server=184.72.243.100;user=ServiceDev;password=Dev*826";
		SqlConnection conn;
		SqlCommand comm;
       
		[WebMethod(Description="new video")]
		public string newvideo(string id, string mobiletype)
		{
			conn = new SqlConnection(constring);
			conn.Open();
          
			comm = new SqlCommand("[dbo].[PNewVideo]",conn);
			comm.Connection=conn;
			comm.CommandType = System.Data.CommandType.StoredProcedure;
			comm.Parameters.Add(
				new SqlParameter("@idFacebook", id));
			comm.Parameters.Add(
				new SqlParameter("@osType", mobiletype));
			try
			{
				comm.ExecuteReader();
				return "Record Saved";
			}
			catch (Exception)
			{
				return "Not Saved";
			}
			finally
			{
				conn.Close();
			}
		}
	}
}