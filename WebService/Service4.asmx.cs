
using System;
//using System.Collections.Generic;
//using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;

namespace usingparameterwebservice4
{
	/// <summary>
	/// Summary description for mywebservice
	/// </summary>
	[WebService(Namespace = "mystoredprocedure4.org")]
	//[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
	//[System.Web.Script.Services.ScriptService]
	public class mywebservice : System.Web.Services.WebService
	{
		string constring = "Database=AGI_IC;server=184.72.243.100;user=ServiceDev;password=Dev*826";
		SqlConnection conn;
		SqlCommand comm;
       
		[WebMethod(Description="updatevideo")]
		public void updatevideo(string id,int points,string urlvideo,string urlthumb)
		{
			conn = new SqlConnection(constring);
			conn.Open();
          
			comm = new SqlCommand("[dbo].[PUpdateVideo]",conn);
			comm.Connection=conn;
			comm.CommandType = System.Data.CommandType.StoredProcedure;
			comm.Parameters.Add(
				new SqlParameter("@idVideo", id));
			comm.Parameters.Add(
				new SqlParameter("@points", points));
			comm.Parameters.Add(
				new SqlParameter("@urlVideo", urlvideo));
			comm.Parameters.Add(
				new SqlParameter("@urlThumb", urlthumb));
			try
			{
				comm.ExecuteReader();
			}
			catch (Exception)
			{
		
			}
			finally
			{
				conn.Close();
			}
		}
	}
}