
using System;
using System.Web;
using System.Web.Services;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using Microsoft.Win32;
using System.Configuration;
using KMobile.BasicTools;
using System.Text;
using System.Data;
using System.Collections;
using System.IO;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Net;








namespace usingparameterwebservice
{  
	/// <summary>
	/// Summary description for mywebservice
	/// </summary>
	[WebService(Namespace = "mywebserviceorg")]
	//[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
	//[System.Web.Script.Services.ScriptService]
	
	public class icecream: System.Web.Services.WebService
	{

		[Serializable]
			public class Video 
		{
			private string _idFacebook;
			private int _idVideo;
			private int _points;
		
			public string IdFacebook
			{
				get {return _idFacebook;}
				set {_idFacebook=value;}
			}
			public int IdVideo
			{
				get {return _idVideo;}
				set {_idVideo=value;}
			}
			public int Points
			{
				get {return _points;}
				set {_points=value;}
			}
		}
	
		public class VideoCollection : IEnumerable
		{
		
			private ArrayList _list=new ArrayList();
		

			public Video this[int index]
			{
				get { return (Video) _list[index]; }
			}

			public int Count
			{
				get { return _list.Count; }
			}
			#region Constructor
			public VideoCollection()
			{
					
				_list = new ArrayList();
			}

			~VideoCollection()
			{
				_list = null;			
			}
			public void Add(Video vdt)
			{
				_list.Add(vdt);
			}
			public void Remove (Video vdt)
			{
				_list.Remove(vdt);
			}
			#endregion
			#region IEnumerable Members

			public VideoEnumerator GetEnumerator() 
			{
				return new VideoEnumerator(this);
			}

			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			#endregion
			public class VideoEnumerator : IEnumerator
			{
				private int _intIndex = -1;
				private VideoCollection _objColl=new VideoCollection();

				public VideoEnumerator( VideoCollection objColl )
				{
					this._objColl = objColl;
				}
				public bool MoveNext()
				{
					if (_intIndex < _objColl._list.Count - 1)
					{
						_intIndex++;
						return true;
					}
					else
					{
						return false;
					}
				}
				public void Reset()
				{
					_intIndex = -1;
				}
				public Video Current
				{
					get { return (Video)_objColl._list[_intIndex]; }
				}
				object IEnumerator.Current
				{
					get{ return Current; }
				}
			}
		
		}



		[Serializable]
			public class Challenge 
		{
			private int _idChallenge;
			private string _idFacebookUser1;
			private string _idVideoUser1;
			private string _idFacebookUser2;
			private string _idVideoUser2;
			
		
			public int IdChallenge
			{
				get {return _idChallenge;}
				set {_idChallenge=value;}
			}
			public string IdFacebookUser1
			{
				get {return _idFacebookUser1;}
				set {_idFacebookUser1=value;}
			}

			public string IdVideoUser1
			{
				get {return _idVideoUser1;}
				set {_idVideoUser1=value;}
			
			}
			public string IdFacebookUser2
			{
				get {return _idFacebookUser2;}
				set {_idFacebookUser2=value;}
			}
			
			public string IdVideoUser2
			
			{
				get {return _idVideoUser2;}
				set {_idVideoUser2=value;}
			}
		}
	
		public class ChallengeCollection : IEnumerable
		{
		
			private ArrayList _list=new ArrayList();
		

			public Challenge this[int index]
			{
				get { return (Challenge) _list[index]; }
			}

			public int Count
			{
				get { return _list.Count; }
			}
			#region Constructor
			public ChallengeCollection()
			{
					
				_list = new ArrayList();
			}

			~ChallengeCollection()
			{
				_list = null;			
			}
			public void Add(Challenge challenge)
			{
				_list.Add(challenge);
			}
			public void Remove (Challenge challenge)
			{
				_list.Remove(challenge);
			}
			#endregion
			#region IEnumerable Members

			public ChallengeEnumerator GetEnumerator() 
			{
				return new ChallengeEnumerator(this);
			}

			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			#endregion
			public class ChallengeEnumerator : IEnumerator
			{
				private int _intIndex = -1;
				private ChallengeCollection _objColl=new ChallengeCollection();

				public ChallengeEnumerator( ChallengeCollection objColl )
				{
					this._objColl = objColl;
				}
				public bool MoveNext()
				{
					if (_intIndex < _objColl._list.Count - 1)
					{
						_intIndex++;
						return true;
					}
					else
					{
						return false;
					}
				}
				public void Reset()
				{
					_intIndex = -1;
				}
				public Challenge Current
				{
					get { return (Challenge)_objColl._list[_intIndex]; }
				}
				object IEnumerator.Current
				{
					get{ return Current; }
				}
			}
		
		}


		[Serializable]
			public class VideoInfo 
		{
			
			private int _idVideo;
			private int _points;
			private string _urlThumb;
			private string _urlVideo;
			private int _position;
		
			
			public int IdVideo
			{
				get {return _idVideo;}
				set {_idVideo=value;}
			}
			public int Points
			{
				get {return _points;}
				set {_points=value;}
			}

			public string UrlThumb
			{
				get {return _urlThumb;}
				set {_urlThumb=value;}
			}

			public string UrlVideo
			{
				get {return _urlVideo;}
				set {_urlVideo=value;}
			}
			public int Position
			{
			    get {return _position;}
				set {_position=value;}

			}
		}
		public class VideoInfoCollection : IEnumerable
		{
		
			private ArrayList _list=new ArrayList();
		

			public VideoInfo this[int index]
			{
				get { return (VideoInfo) _list[index]; }
			}

			public int Count
			{
				get { return _list.Count; }
			}
			#region Constructor
			public VideoInfoCollection()
			{
					
				_list = new ArrayList();
			}

			~VideoInfoCollection()
			{
				_list = null;			
			}
			public void Add(VideoInfo vdt)
			{
				_list.Add(vdt);
			}
			public void Remove (VideoInfo vdt)
			{
				_list.Remove(vdt);
			}
			#endregion
			#region IEnumerable Members

			public VideoInfoEnumerator GetEnumerator() 
			{
				return new VideoInfoEnumerator(this);
			}

			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			#endregion
			public class VideoInfoEnumerator : IEnumerator
			{
				private int _intIndex = -1;
				private VideoInfoCollection _objColl=new VideoInfoCollection();

				public VideoInfoEnumerator( VideoInfoCollection objColl )
				{
					this._objColl = objColl;
				}
				public bool MoveNext()
				{
					if (_intIndex < _objColl._list.Count - 1)
					{
						_intIndex++;
						return true;
					}
					else
					{
						return false;
					}
				}
				public void Reset()
				{
					_intIndex = -1;
				}
				public VideoInfo Current
				{
					get { return (VideoInfo)_objColl._list[_intIndex]; }
				}
				object IEnumerator.Current
				{
					get{ return Current; }
				}
			}
		}


			[Serializable]
				public class KnowNickVideoInfo 
			{
			
				private int _idVideo;
				private int _points;
				private string _urlThumb;
				private string _urlVideo;
				private string _urlYoutube;
		
			
				public int IdVideo
				{
					get {return _idVideo;}
					set {_idVideo=value;}
				}
				public int Points
				{
					get {return _points;}
					set {_points=value;}
				}

				public string UrlThumb
				{
					get {return _urlThumb;}
					set {_urlThumb=value;}
				}

				public string UrlVideo
				{
					get {return _urlVideo;}
					set {_urlVideo=value;}
				}
				
				public string UrlYoutube
				{
					get {return _urlYoutube;}
					set {_urlYoutube=value;}
				
				}
			}
		public class KnowNickVideoInfoCollection : IEnumerable
		{
		
			private ArrayList _list=new ArrayList();
		

			public KnowNickVideoInfo this[int index]
			{
				get { return (KnowNickVideoInfo) _list[index]; }
			}

			public int Count
			{
				get { return _list.Count; }
			}
			#region Constructor
			public KnowNickVideoInfoCollection()
			{
					
				_list = new ArrayList();
			}

			~KnowNickVideoInfoCollection()
			{
				_list = null;			
			}
			public void Add(KnowNickVideoInfo vdt)
			{
				_list.Add(vdt);
			}
			public void Remove (KnowNickVideoInfo vdt)
			{
				_list.Remove(vdt);
			}
			#endregion
			#region IEnumerable Members

			public KnowNickVideoInfoEnumerator GetEnumerator() 
			{
				return new KnowNickVideoInfoEnumerator(this);
			}

			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			#endregion
			public class KnowNickVideoInfoEnumerator : IEnumerator
			{
				private int _intIndex = -1;
				private KnowNickVideoInfoCollection _objColl=new KnowNickVideoInfoCollection();

				public KnowNickVideoInfoEnumerator( KnowNickVideoInfoCollection objColl )
				{
					this._objColl = objColl;
				}
				public bool MoveNext()
				{
					if (_intIndex < _objColl._list.Count - 1)
					{
						_intIndex++;
						return true;
					}
					else
					{
						return false;
					}
				}
				public void Reset()
				{
					_intIndex = -1;
				}
				public KnowNickVideoInfo Current
				{
					get { return (KnowNickVideoInfo)_objColl._list[_intIndex]; }
				}
				object IEnumerator.Current
				{
					get{ return Current; }
				}
			}
		}


			[Serializable]
				public class videoRanking 
			{
				private int _ranking;
				
				public int Ranking
				{
					get {return _ranking;}
					set {_ranking=value;}
				}
			}
		
		
		
		
		// string constring=Regedit.GetConnectionString("IceCream");
		 string  constring ="Database=appcornettolickchallengecom;Server=db-www.app.cornettolickchallenge.com;user=appcornettolickchallengecomDBA;Password=1k8875Q2o8D18iS";
		
		SqlConnection conn;
		SqlCommand comm;
		
		[WebMethod(Description="Create a new user in the database sending idFacebook and iOS/Android OS type")]
		public bool new_user(string idFacebook, string osType)
		{
		try
			{
		
			conn = new SqlConnection(constring);
			conn.Open();
			
         
			comm = new SqlCommand("[dbo].[PNewUser]",conn);
			comm.Connection=conn;
			comm.CommandType = System.Data.CommandType.StoredProcedure;
			comm.Parameters.Add(
				new SqlParameter("@idFacebook", idFacebook.ToLower()));
			comm.Parameters.Add(
				new SqlParameter("@osType", osType.ToLower()));
			
				comm.ExecuteReader();
				return true;
			}
			catch (Exception)
			{
				return false;
			}
			finally
			{
				conn.Close();
			}
		}

		[WebMethod(Description="Insert a new video")]
		public int new_video(string idFacebook, string osType)
		{
		try
			{
			int idVideo = -1;
			conn = new SqlConnection(constring);
			conn.Open();
          
			comm = new SqlCommand("[dbo].[PNewVideo]",conn);
			comm.Connection=conn;
			comm.CommandType = System.Data.CommandType.StoredProcedure;
			comm.Parameters.Add(
				new SqlParameter("@idFacebook", idFacebook.ToLower()));
			comm.Parameters.Add(
				new SqlParameter("@osType", osType.ToLower()));

			SqlParameter outputParameter = new SqlParameter("@idVideo", System.Data.SqlDbType.Int);
			outputParameter.Direction = System.Data.ParameterDirection.Output;
			comm.Parameters.Add(outputParameter);
			comm.ExecuteReader();
			idVideo=Int32.Parse(outputParameter.Value.ToString());
			
				return idVideo;
			}
			catch (Exception)
			{
				return -1;
			}
			finally
			{
				conn.Close();
			}
	
		}

		[WebMethod(Description="login user")]
		public bool login_user(string idFacebook)
		{
		try
			{
				int user=-1;
				conn = new SqlConnection(constring);
				conn.Open();         
				comm = new SqlCommand("[dbo].[PLoginUser]",conn);
				comm.Connection=conn;
				comm.CommandType = System.Data.CommandType.StoredProcedure;
				comm.Parameters.Add(
					new SqlParameter("@idFacebook", idFacebook.ToLower()));
				SqlParameter outputParameter = new SqlParameter("@user", System.Data.SqlDbType.Int);
				outputParameter.Direction = System.Data.ParameterDirection.Output;
				comm.Parameters.Add(outputParameter);
				comm.ExecuteReader();
				user=Int32.Parse(outputParameter.Value.ToString());
				if (user==1)
					return true;
				else return false;
			}
				
			     
				
			catch (Exception)
			{
				return false;
			}
			finally
			{
				conn.Close();
			}
		}

		[WebMethod(Description="update video with the following parameters:idVideo/points/urlVideo/urlthumb")]
		public bool update_video(string idVideo,int points,string urlvideo,string urlthumb)
		{
		try
			{
				int eroare=-1;
				conn = new SqlConnection(constring);
				conn.Open();
          
				comm = new SqlCommand("[dbo].[PUpdateVideo]",conn);
				comm.Connection=conn;
				comm.CommandType = System.Data.CommandType.StoredProcedure;
				comm.Parameters.Add(
					new SqlParameter("@idVideo", idVideo));
				comm.Parameters.Add(
					new SqlParameter("@points", points));
				comm.Parameters.Add(
					new SqlParameter("@urlVideo", urlvideo));
				comm.Parameters.Add(
					new SqlParameter("@urlThumb", urlthumb));
				SqlParameter outputParameter = new SqlParameter("@eroare", System.Data.SqlDbType.Int);
				outputParameter.Direction = System.Data.ParameterDirection.Output;
				comm.Parameters.Add(outputParameter);
				comm.ExecuteReader();
				eroare=Int32.Parse(outputParameter.Value.ToString());
				if (eroare==1) return true;
				else return false;
			}
			catch (Exception)
			{
				return false;
			}
			finally
			{  
				conn.Close();
							
			}
		}
		[WebMethod(Description="Receive data of the video")]
		public bool UploadFile ( byte[] f, string fileName) 

		{ 
              
			// the byte array argument contains the content of the file 

			// the string argument contains the name and extension 

			// of the file passed in the byte array 

		try 

			{ 

				// instance a memory stream and pass the 

				// byte array to its constructor 

				MemoryStream ms = new MemoryStream(f); 

                

				// instance a filestream pointing to the 

				// storage folder, use the original file name 

				// to name the resulting file 

				FileStream fs = new FileStream(System.Web.HttpContext.Current.Server.MapPath("./VIDEOS/")+fileName, System.IO.FileMode.Create);


				// write the memory stream containing the original 

				// file as a byte array to the filestream 

				ms.WriteTo(fs);  


				// clean up 
				fs.Flush();
				ms.Close(); 

				fs.Close(); 
                
				

				// return OK if we made it this far 

				return true; 

			} 

			catch 

			{ 

				// return the error message if the operation fails 

				return false; 

			} 

		}
		[WebMethod(Description="get complete ranking")]
		public VideoCollection Ranking(int nr)
		{
		
		VideoCollection videos=new VideoCollection();
		try
			{
			conn = new SqlConnection(constring);
			conn.Open();  
			comm = new SqlCommand("[dbo].[GetCompleteRanking]",conn);
			comm.Connection=conn;
			comm.CommandType = System.Data.CommandType.StoredProcedure;
			comm.Parameters.Add(
				new SqlParameter("@nr", nr));	
			Video video=new Video();
			SqlDataReader dr = comm.ExecuteReader();
			while (dr.Read())
			{
				//read from database to video object
				video=new Video();
				string points=dr["points"].ToString();
				video.Points=Int32.Parse(points);
				string	idVideo=dr["idVideo"].ToString();
				video.IdVideo=Int32.Parse(idVideo);
				string idFacebook=dr["idFacebook"].ToString();
				video.IdFacebook=idFacebook;
				
				//adding the video to the videos collection
				videos.Add(video);
			}
			
		 	return videos ;
		
			
			}
			catch (Exception)
			{
				return videos;
			}
			finally
			{
				conn.Close();
			}
	
		}

		[WebMethod(Description="new challenge")]
		public int newChallenge(string idFacebook, string idVideo, string idFacebookFriend)
		{
		try
			{ 
				//output parameter from sql procedure that tells us if the user exists or not
				int idChallenge=-1;
				conn=new SqlConnection(constring);
				conn.Open();
				comm = new SqlCommand("[dbo].[NewChallenge]",conn);
				comm.Connection=conn;
				comm.CommandType = System.Data.CommandType.StoredProcedure;

				comm.Parameters.Add(
					new SqlParameter("@idFacebookUser1", idFacebook.ToLower()));
				comm.Parameters.Add(
					new SqlParameter("@idFacebookUser2", idFacebookFriend.ToLower()));
				comm.Parameters.Add(
					new SqlParameter("@idVideoUser1", idVideo.ToLower()));
				SqlParameter outputParameter = new SqlParameter("@idChallenge", System.Data.SqlDbType.Int);
				outputParameter.Direction = System.Data.ParameterDirection.Output;
				comm.Parameters.Add(outputParameter);
				comm.ExecuteReader();
				idChallenge=Int32.Parse(outputParameter.Value.ToString());
				return idChallenge;
			}
			catch (Exception)
			{
				return -1;
			}
			finally
			{  
				conn.Close();
						
			}
				
		}


		[WebMethod(Description="challenge_modification")]
		public bool updateChallenge(string idChallenge, string idVideoUser2)
		{
		 
		try
			{
				int eroare=-1;
				conn=new SqlConnection(constring);
				conn.Open();
				comm = new SqlCommand("[dbo].[UpdateChallenge]",conn);
				comm.Connection=conn;
				comm.CommandType = System.Data.CommandType.StoredProcedure;

				comm.Parameters.Add(
					new SqlParameter("@idChallenge", idChallenge.ToLower()));
				comm.Parameters.Add(
					new SqlParameter("@idVideoUser2", idVideoUser2.ToLower()));
				SqlParameter outputParameter = new SqlParameter("@eroare", System.Data.SqlDbType.Int);
				outputParameter.Direction = System.Data.ParameterDirection.Output;
				comm.Parameters.Add(outputParameter);
				comm.ExecuteReader();
				eroare=Int32.Parse(outputParameter.Value.ToString());
				if (eroare==1) return true;
				else return false;
			   
			}
			catch (Exception)
			{
				return false;
			}
			finally
			{  
				conn.Close();
				
				
			
			}

	
				
		}
		[WebMethod(Description="get challenges for user")]
		public ChallengeCollection Challengeinbox(string idFacebook)
		{
			ChallengeCollection challenges=new ChallengeCollection();
		
		try
			{
			conn = new SqlConnection(constring);
			conn.Open();  
			comm = new SqlCommand("[dbo].[GetChallengesUser]",conn);
			comm.Connection=conn;
			comm.CommandType = System.Data.CommandType.StoredProcedure;
			comm.Parameters.Add(
				new SqlParameter("@idFacebook", idFacebook.ToLower()));		
			Challenge challenge=new Challenge();
			SqlDataReader dr = comm.ExecuteReader();
			while (dr.Read())
			{
				//read from database to video object
				challenge=new Challenge();
				string idChallenge=dr["idChallenge"].ToString();
				challenge.IdChallenge=Int32.Parse(idChallenge);
				string idFacebookUser1=dr["idFacebookUser1"].ToString();
				challenge.IdFacebookUser1=idFacebookUser1;
				string idVideoUser1=dr["idVideoUser1"].ToString();
				challenge.IdVideoUser1=idVideoUser1;
				string idFacebookUser2=dr["idFacebookUser2"].ToString();
				challenge.IdFacebookUser2=idFacebookUser2;
				string idVideoUser2=dr["idVideoUser2"].ToString();
				challenge.IdVideoUser2=idVideoUser2;

				
				
				//adding the video to the videos collection
				challenges.Add(challenge);
			}
			
					
	    	return challenges ;
				
			
			}
			catch (Exception)
			{
				return challenges;
			}
			finally
			{
				conn.Close();
			}
		}

		[WebMethod(Description="get complete video info")]
		public VideoInfoCollection GetVideoInfo(int idVideoUser)
		{
		VideoInfoCollection videos=new VideoInfoCollection();
		try
			{

			conn = new SqlConnection(constring);
			conn.Open();  
			comm = new SqlCommand("[dbo].[GetCompleteVideoInfo]",conn);
			comm.Connection=conn;
			comm.CommandType = System.Data.CommandType.StoredProcedure;
			comm.Parameters.Add(
				new SqlParameter("@idVideoUser", idVideoUser));			
			VideoInfo videoInfo=new VideoInfo();
			SqlDataReader dr = comm.ExecuteReader();
			while (dr.Read())
			{
				//read from database to video object
				videoInfo=new VideoInfo();
				string points=dr["points"].ToString();
				videoInfo.Points=Int32.Parse(points);
				string	idVideo=dr["idVideo"].ToString();
				videoInfo.IdVideo=Int32.Parse(idVideo);
				string urlThumb=dr["urlThumb"].ToString();
				videoInfo.UrlThumb=urlThumb;
				string urlVideo=dr["urlVideo"].ToString();
				videoInfo.UrlVideo=urlVideo;
				string position=dr["position"].ToString();
				videoInfo.Position=Int32.Parse(position);
				
				//adding the video to the videos collection
				videos.Add(videoInfo);
			}
							
				return videos ;
				
			
			}
			catch (Exception)
			{
				return videos;
			}
			finally
			{
				conn.Close();
			}
	

		}

		[WebMethod(Description="increase counter")]
		public bool IncrementCounter(int idVideo, string language)
		{
		try
			{ 
				int eroare=-1;
				conn=new SqlConnection(constring);
				conn.Open();
				comm = new SqlCommand("[dbo].[IncreaseCounter]",conn);
				comm.Connection=conn;
				comm.CommandType = System.Data.CommandType.StoredProcedure;
				comm.Parameters.Add(
					new SqlParameter("@idVideo", idVideo));
			comm.Parameters.Add(
				new SqlParameter("@language", language.ToLower()));
				SqlParameter outputParameter = new SqlParameter("@eroare", System.Data.SqlDbType.Int);
				outputParameter.Direction = System.Data.ParameterDirection.Output;
				comm.Parameters.Add(outputParameter);
				comm.ExecuteReader();
				eroare=Int32.Parse(outputParameter.Value.ToString());
				if (eroare==1) return true;
				else return false;
				
			}
			catch (Exception)
			{
				return false;
			}
			finally
			{  
				conn.Close();
											
			}
					
		}


		[WebMethod(Description="get ranking")]
		public int GetRanking(int points)
		{
		
		try
			{ 
                int rank=-1;
				conn=new SqlConnection(constring);
				conn.Open();
				comm = new SqlCommand("[dbo].[GetUserRanking]",conn);
				comm.Connection=conn;
				comm.CommandType = System.Data.CommandType.StoredProcedure;
				comm.Parameters.Add(
					new SqlParameter("@points",points));
				SqlDataReader dr = comm.ExecuteReader();
				while (dr.Read())
				{
					string position=dr["position"].ToString();
					rank=Int32.Parse(position);
				}
				return rank;
				
			}
			catch (Exception)
			{
				return -1;
			}
			finally
			{  
				conn.Close();					
			
			}

		}



		[WebMethod(Description="get know nick video info")]
		public KnowNickVideoInfoCollection GetKnowNickInfo(int idVideoUser, string lang)
		{
		
        KnowNickVideoInfoCollection videos=new KnowNickVideoInfoCollection();
		try
			{
			conn = new SqlConnection(constring);
			conn.Open();  
			comm = new SqlCommand("[dbo].[GetKnowNickVideoInfo]",conn);
			comm.Connection=conn;
			comm.CommandType = System.Data.CommandType.StoredProcedure;
			comm.Parameters.Add(
				new SqlParameter("@idVideoUser", idVideoUser));
			comm.Parameters.Add(
				new SqlParameter("@lang", lang.ToLower()));		
			KnowNickVideoInfo videoInfo=new KnowNickVideoInfo();
			SqlDataReader dr = comm.ExecuteReader();
			while (dr.Read())
			{
				//read from database to video object
				videoInfo=new KnowNickVideoInfo();
				string points=dr["points"].ToString();
				videoInfo.Points=Int32.Parse(points);
				string	idVideo=dr["idVideo"].ToString();
				videoInfo.IdVideo=Int32.Parse(idVideo);
				string urlThumb=dr["urlThumb"].ToString();
				videoInfo.UrlThumb=urlThumb;
				string urlVideo=dr["urlVideo"].ToString();
				videoInfo.UrlVideo=urlVideo;
				string urlYoutube=dr["urlYoutube"].ToString();
				videoInfo.UrlYoutube=urlYoutube;
		
				
				//adding the video to the videos collection
				videos.Add(videoInfo);
			}
									
				return videos ;
							
			}
			catch (Exception)
			{
				return videos;
			}
			finally
			{
				conn.Close();
			}
	

		}
		

	}
}

