using System;
using System.Collections;
using System.Configuration;
using System.Web;
using KMobile.Catalog.Presentation;
using KMobile.Catalog.Services;
using wap.Tools;
using System.Web.UI.MobileControls;

namespace wap 
{
	public class _default : CatalogBrowsing 
	{
		private ArrayList _contentCollImg = new ArrayList();
		private ArrayList _contentCollContentSet = new ArrayList();
		protected Panel pnlTemas, pnlPreview, pnlShops, pnlFooter, pnlEnd;
		protected System.Web.UI.MobileControls.Panel Panel1;
		protected System.Web.UI.MobileControls.Form frmDefault;
		protected System.Web.UI.WebControls.Literal  htmlcode;
		protected Image imgLogo;
		protected bool is3g = false;
		private Especial esp; 
		private int day;
		public int promo = -1;
		
		private void Page_Load(object sender, System.EventArgs e)
		{ 
			try 
			{ 
				_mobile = (MobileCaps)Request.Browser; 
				try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(this.Request, _mobile));}
				catch{is3g = false;}
				try{if (Request["id"]!=null) WapTools.LogUser(this.Request, Convert.ToInt32(Request["id"]), _mobile.MobileType);}
				catch{}
				if (Request["id"]!=null)
				{
					try{Response.Redirect(WapTools.GetText("Lnk" + Request["id"]), false);}
					catch{Response.Redirect(WapTools.GetText("Lnk" + Request["id"]), true);}
				}
				else if (_mobile.PreferredRenderingType == "chtml10")
				{
					try{if (WapTools.isTestSite(this.Request)) Response.Redirect(ConfigurationSettings.AppSettings["UrlImodeDev"], false); else Response.Redirect(ConfigurationSettings.AppSettings["UrlImode"], false);}
					catch{if (WapTools.isTestSite(this.Request)) Response.Redirect(ConfigurationSettings.AppSettings["UrlImodeDev"], true); else Response.Redirect(ConfigurationSettings.AppSettings["UrlImode"], true);}
				}				
				// Samsung J400 --> TOP
				else if (WapTools.isHomeTop(_mobile.MobileType))//  && ! WapTools.isTestSite(this.Request))
				{
					try{Response.Redirect("./linkto.aspx?cg=IMG&id=5745", false);}
					catch{Response.Redirect(".linkto.aspx?cg=IMG&id=5745", true);}				
				}
				// redirection xhtml
				else if (Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_Xhtml"]) && (_mobile.IsXHTML || WapTools.isXhtml(this.Request, _mobile) || is3g))
				{
					try{if (WapTools.isTestSite(this.Request)) Response.Redirect(ConfigurationSettings.AppSettings["UrlXhtmlDev"], false); else Response.Redirect(ConfigurationSettings.AppSettings["UrlXhtml"], false);}
					catch{if (WapTools.isTestSite(this.Request)) Response.Redirect(ConfigurationSettings.AppSettings["UrlXhtmlDev"], true); else Response.Redirect(ConfigurationSettings.AppSettings["UrlXhtml"], true);}
				} 
				else 
				{
					if (_mobile.MobileType != null &&  _mobile.IsCompatible("IMG_COLOR"))
					{      
						try{WapTools.AddUIDatLog(Request, Response);}
						catch{}      
						if(_mobile.IsXHTML || WapTools.isXhtml(this.Request, _mobile) || is3g)
						{
							string errMsg = "Xhtml handsets are being redirected to Wap site. Please take immediate action.";
							WapTools.SendMail(HttpContext.Current, errMsg);
						}
						WapTools.LogUser(this.Request, 101, _mobile.MobileType);
						_displayKey = WapTools.GetXmlValue("DisplayKey"); 
						_contentGroup = "IMG";
						day = (Request.QueryString["day"] != null) ? Convert.ToInt32(Request.QueryString["day"]) : 0;
						_idContentSet = Convert.ToInt32(WapTools.GetXmlValue("Home/Composite"));
						BrowseContentSetExtended( pnlPreview, -1, -1 );
						if (_mobile.IsAdvanced)
							imgLogo.ImageUrl = String.Format(WapTools.GetImage(this.Request, "imagenes"), WapTools.GetFolderImg(_mobile));
						
						#region PUB
						//if (WapTools.isTestSite(this.Request))
							WapTools.CallNewPubTEF2(this.Trace, this.Request, pnlPreview,"183955");
						//else
						//	WapTools.CallNewPubTEF(this.Trace, this.Request, this.pnlPreview, 1188);
						
//							WapTools.CallNewPub(this.Request, WapTools.SearchType.ts_ImagenesFondos_text_top.ToString(), "xml" , 1188, this.pnlPreview); // ts_text, ts_mixed, ts_imag
						//else
						//	WapTools.CallPub(this.Request, WapTools.SearchType.ts_ImagenesFondos_text_top.ToString(), "xml" , this.pnlPreview); // ts_text, ts_mixed, ts_imag
						#endregion
					
						#region PROMO
						promo = WapTools.isPromo(this.Request);
						Trace.Warn(promo.ToString());
						if (promo >= 0)
							WapTools.AddLabelCenter(pnlPreview, WapTools.GetXmlValue("Promo/Message" + promo.ToString()), "", _mobile, BooleanOption.True);
						#endregion
						
						#region AHORA EN IMAGENES
						WapTools.AddLabelCenter(pnlPreview, WapTools.GetText("Ahora"), "", _mobile, BooleanOption.True);
						int dayCounter = Convert.ToInt32(WapTools.GetText("DayCounter"));
						int dia = WapTools.isTestSite(this.Request) ? (day != 0 && day <= 31 && day >= 1) ? day : DateTime.Now.AddDays(dayCounter).Day : DateTime.Now.Day;
					//	int dia = WapTools.isTestSite(this.Request) ? DateTime.Now.AddDays(Convert.ToInt32(WapTools.GetText("DayCounter"))).Day : DateTime.Now.Day;
											
						for (int i=1; i<=3; i++)
						{
							esp = new Especial();
							esp = WapTools.getEspecial(i, dia.ToString());
							
							while (esp.name == "" || !_mobile.IsCompatible(esp.filter))
								esp = WapTools.GetCompatibleEspecial(esp);
							dia = WapTools.isTestSite(this.Request) ? (day != 0 && day <= 31 && day >= 1) ? day : DateTime.Now.AddDays(dayCounter).Day : DateTime.Now.Day;
							WapTools.AddLink(pnlPreview, WapTools.GetText(esp.name), WapTools.GetText("Link" + esp.name), WapTools.GetImage(this.Request, "bullet"), _mobile);
						}
						//DisplayContentSets(pnlPreview, 0, 1);
						#endregion

						#region DESTACADOS
						WapTools.AddLabelCenter(pnlPreview, WapTools.GetText("Destacados"), "", _mobile, BooleanOption.True);
						DisplayImages(pnlPreview, "IMG", "IMG_COLOR");
						WapTools.AddLink(pnlTemas, "Zona VIP - Descarga Ilimitada", "./linkto.aspx?id=11032", WapTools.GetImage(this.Request, "club"), _mobile);
						
						WapTools.AddLink(pnlTemas, WapTools.GetText("avatar"), "./linkto.aspx?id=11040", WapTools.GetImage(this.Request, "bullet"), _mobile);
						//WapTools.AddLink(pnlTemas, "Halloween", "./linkto.aspx?id=11015", WapTools.GetImage(this.Request, "halloween"), _mobile);
						
						//WapTools.AddLink(pnlTemas, WapTools.GetText("TopNew"), String.Format("./linkto.aspx?cg=IMG&id={0}", WapTools.GetXmlValue("Home/TopNew")), WapTools.GetImage(this.Request, "bullet"), _mobile);
						//WapTools.AddLink(pnlTemas, "Viva San Ferm�n!!!", "./linkto.aspx?id=11031", WapTools.GetImage(this.Request, "bullet"), _mobile);
						WapTools.AddLink(pnlTemas, WapTools.GetText("TopImg"), String.Format("./linkto.aspx?cg=IMG&id={0}", WapTools.GetXmlValue("Home/Top")), WapTools.GetImage(this.Request, "bullet"), _mobile);
						WapTools.AddLink(pnlTemas, WapTools.GetText("AmorImg"), String.Format("./linkto.aspx?cg=IMG&id={0}", WapTools.GetXmlValue("Home/Amor")), WapTools.GetImage(this.Request, "bullet"), _mobile);
						WapTools.AddLink(pnlTemas, WapTools.GetText("ModelsImg"), String.Format("./linkto.aspx?cg=IMG&id={0}", WapTools.GetXmlValue("Home/Models")), WapTools.GetImage(this.Request, "bullet"), _mobile);
						WapTools.AddLink(pnlTemas, WapTools.GetText("Gigigo"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/GIGIGO")), WapTools.GetImage(this.Request, "bullet"), _mobile);
						//		DisplayContentSets(pnlTemas, _contentCollContentSet.Count-2, -1);
						WapTools.AddLink(pnlTemas, WapTools.GetText("Categorias"), "./linkto.aspx?cg=COMPOSITE&id=3619", WapTools.GetImage(this.Request, "bullet"), _mobile);
						//WapTools.AddLink(pnlTemas, WapTools.GetText("EnviaPostales"), String.Format("./linkto.aspx?d=1&cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/POSTALES")), WapTools.GetImage(this.Request, "bullet"), _mobile);					
						
						#endregion

						#region FELICITACIONES
						WapTools.AddLabelCenter(pnlFooter, WapTools.GetText("Felicitaciones"), "", _mobile, BooleanOption.True);
						WapTools.AddLink(pnlFooter, WapTools.GetText("FondoDedicatorias"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FONDODEDICATORIAS")), WapTools.GetImage(this.Request, "bullet"), _mobile);
						if (_mobile.IsCompatible("ANIM_COLOR"))
							WapTools.AddLink(pnlFooter, WapTools.GetText("PostalesAnimadas"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/POSTALESANIMADAS")), WapTools.GetImage(this.Request, "bullet"), _mobile);
						if (_mobile.IsCompatible("VIDEO_DWL"))
							WapTools.AddLink(pnlFooter, WapTools.GetText("VideoFelicitaciones"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEOFELICITACIONES")), WapTools.GetImage(this.Request, "bullet"), _mobile);
						//WapTools.AddLink(pnlFooter, WapTools.GetText("ZonaVipFelicitaciones"), "./linkto.aspx?cg=COMPOSITE&id=10093", WapTools.GetImage(this.Request, "club"), _mobile);
							
						#endregion

						#region DESCARGATE  
						WapTools.AddLabelCenter(pnlFooter, WapTools.GetText("Descargate"), "", _mobile, BooleanOption.True);
//						if (WapTools.isTestSite(this.Request))
//							WapTools.AddLink(pnlFooter, "Fondos Din�micos", "./linkto.aspx?cg=COMPOSITE&id=6579", WapTools.GetImage(this.Request, "bullet"), _mobile);
						
						if (_mobile.IsCompatible("VIDEO_DWL"))
							WapTools.AddLink(pnlFooter, WapTools.GetText("VIDEO"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEO")), WapTools.GetImage(this.Request, "bullet"), _mobile);
						if (_mobile.IsCompatible("ANIM_COLOR"))
							WapTools.AddLink(pnlFooter, WapTools.GetText("ANIM"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/ANIM")), WapTools.GetImage(this.Request, "bullet"), _mobile);					
						if (WapTools.isCompatibleThemes(_mobile))
							WapTools.AddLink(pnlFooter, WapTools.GetText("Temas"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/TEMAS")), WapTools.GetImage(this.Request, "bullet"), _mobile);					
						WapTools.AddLink(pnlFooter, WapTools.GetText("FondoNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FONDONOMBRES")), WapTools.GetImage(this.Request, "bullet"), _mobile);
//						WapTools.AddLink(pnlFooter, WapTools.GetText("AnimaNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/ANIMANOMBRES")), WapTools.GetImage(this.Request, "bullet"), _mobile);
//						if (_mobile.IsCompatible("VIDEO_DWL"))
//						{
//							WapTools.AddLink(pnlFooter, WapTools.GetText("VideoNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEONOMBRES")), WapTools.GetImage(this.Request, "bullet"), _mobile);
//							WapTools.AddLink(pnlFooter, WapTools.GetText("VideoAnimaciones"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEOANIMACIONES")), WapTools.GetImage(this.Request, "bullet"), _mobile);
//						}
						WapTools.AddLink(pnlFooter,  WapTools.GetText("MasDESCARGATE"), "./linkto.aspx?id=27", WapTools.GetImage(this.Request, "bullet"), _mobile);
						
						#endregion
	
						#region SEARCH
						//if (_mobile.MobileDeviceManufacturer.ToUpper() != "SIEMENS" && _mobile.MobileDeviceManufacturer.ToUpper() != "SAGEM")						
						//	WapTools.AddNewSearchBlock(this, pnlFooter, WapTools.GetText("SearchCmd"), WapTools.GetText("SearchLbl"), "","", "", "", _mobile);
						//WapTools.AddLink(pnlFooter, WapTools.GetText("TitleGallery"), String.Format("./linkto.aspx?id={0}", WapTools.GetXmlValue("Home/Gallery")), WapTools.GetImage(this.Request, "bullet"), _mobile);
						
						#endregion 
	
						#region SHOPS
						WapTools.AddLabelCenter(pnlShops, WapTools.GetText("Shops"), "", _mobile, BooleanOption.True);
						WapTools.AddLink(pnlShops, WapTools.GetText("Shop18"), "./linkto.aspx?id=18", WapTools.GetImage(this.Request, "bullet"), _mobile);
						WapTools.AddLink(pnlShops, WapTools.GetText("Shop33"), "./linkto.aspx?id=33", WapTools.GetImage(this.Request, "bullet"), _mobile);
						//WapTools.AddLink(pnlShops, WapTools.GetText("Shop6"), "./linkto.aspx?id=6", WapTools.GetImage(this.Request, "bullet"), _mobile);
						//WapTools.AddLink(pnlShops, WapTools.GetText("Shop19"), "./linkto.aspx?id=19", WapTools.GetImage(this.Request, "bullet"), _mobile);
						//WapTools.AddLink(pnlShops, WapTools.GetText("Shop30"), "./linkto.aspx?id=30", WapTools.GetImage(this.Request, "bullet"), _mobile);
						//	WapTools.AddLink(pnlShops, WapTools.GetText("Shop11"), "./linkto.aspx?id=11", WapTools.GetImage(this.Request, "bullet"), _mobile);
						//WapTools.AddLink(pnlShops, WapTools.GetText("Cateto"), "./linkto.aspx?id=10002&cg=COMPOSITE", WapTools.GetImage(this.Request, "bullet"), _mobile);
						WapTools.AddLink(pnlShops, WapTools.GetText("moreshops"), "./linkto.aspx?id=20", WapTools.GetImage(this.Request, "bullet"), _mobile);
						#endregion

						#region ALERTAS
						WapTools.AddLabelCenter(pnlShops, WapTools.GetText("Apuntate"), "", _mobile, BooleanOption.True);
						WapTools.AddLabel(pnlShops, "Pru�balas GRATIS si no las conoces", "", _mobile);
						//WapTools.AddLink(pnlShops, WapTools.GetText("Alerta3"), "./linkto.aspx?id=23", WapTools.GetImage(this.Request, "bullet"), _mobile);
						//WapTools.AddLink(pnlShops, WapTools.GetText("Alerta4"), "./linkto.aspx?id=24", WapTools.GetImage(this.Request, "bullet"), _mobile);
						WapTools.AddLink(pnlShops, WapTools.GetText("Alerta6"), "./linkto.aspx?id=28", WapTools.GetImage(this.Request, "bullet"), _mobile);
						WapTools.AddLink(pnlShops, WapTools.GetText("Alerta7"), "./linkto.aspx?id=29", WapTools.GetImage(this.Request, "bullet"), _mobile);
						WapTools.AddLink(pnlShops, WapTools.GetText("Alerta1"), "./linkto.aspx?id=21", WapTools.GetImage(this.Request, "bullet"), _mobile);
						WapTools.AddLink(pnlShops, WapTools.GetText("Alerta2"), "./linkto.aspx?id=22", WapTools.GetImage(this.Request, "bullet"), _mobile);
						//WapTools.AddLink(pnlShops, WapTools.GetText("Alerta5"), "./linkto.aspx?id=25", WapTools.GetImage(this.Request, "bullet"), _mobile);
						WapTools.AddLink(pnlShops, "M�s Alertas", "./linkto.aspx?id=26", WapTools.GetImage(this.Request, "bullet"), _mobile);
						#endregion						

						#region PUB2
						//if (WapTools.isTestSite(this.Request))
//						if (WapTools.isTestSite(this.Request))
						//if (WapTools.isTestSite(this.Request))
							WapTools.CallNewPubTEF2(this.Trace, this.Request, this.pnlShops, "183953");
						//else
						//	WapTools.CallNewPubTEF(this.Trace, this.Request, this.pnlShops, 1651);													
//						else
//							WapTools.CallNewPub(this.Request, WapTools.SearchType.ts_ImagenesFondos_text_top.ToString(), "xml" , 1651, this.pnlShops); // ts_text, ts_mixed, ts_imag
						//else
						//	WapTools.CallPub(this.Request, WapTools.SearchType.ts_ImagenesFondos_text_top.ToString(), "xml" , this.pnlPreview); // ts_text, ts_mixed, ts_imag
						#endregion
					}
					else
						WapTools.AddLabel(pnlTemas, WapTools.GetText("Compatibility"), "", _mobile);

					#region FOOTER						
					WapTools.AddLink(pnlEnd, "Buscar", "http://msf.movistar.es/index.jsp",WapTools.GetImage(this.Request, "buscar"), _mobile);
					WapTools.AddLink(pnlEnd, "emoci�n", "http://wap.movistar.com", WapTools.GetImage(this.Request, "home"), _mobile);
					WapTools.AddLink(pnlEnd, "Atr�s", "http://wap.movistar.com", WapTools.GetImage(this.Request, "back"), _mobile);
					WapTools.AddLink(pnlEnd, "Arriba", "default.aspx", WapTools.GetImage(this.Request, "up"), _mobile);
					#endregion
				}
			}		
			catch(Exception caught)
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format(" Unexpected exception in emocion\\wap\\default.aspx - UA : {0}", Request.UserAgent), caught);
				this.RedirectToMobilePage("./error.aspx");				
			}
		}


		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		
		#region Display
		protected override void DisplayContentSet(Content content, System.Web.UI.MobileControls.Panel pnl)
		{
			_contentCollContentSet.Add(content);
		}

		protected override void DisplayImg(Content content, System.Web.UI.MobileControls.Panel pnl, bool preview)
		{
			_contentCollImg.Add(content);
		}

		public void DisplayImages(Panel pnl, string contentGroup, string contentType)
		{
			_imgDisplayInst = new ImgDisplayInstructions(_mobile);
			_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(String.Format("Url_{0}", contentGroup));
			_imgDisplayInst.TextDwld = WapTools.GetText("Download");
			//_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, contentGroup, contentType, HttpUtility.UrlEncode("wap|HOME"), "", "0");
			_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(this.Request, contentGroup, contentType, HttpUtility.UrlEncode("wap|HOME"), "", "0", promo);
			_imgDisplayInst.DisplayDescription = false;
			int init = DateTime.Now.Millisecond; 
			int value = Convert.ToInt32(WapTools.GetXmlValue("Home/Subcomposite"));
			Content content;
			content = (Content)_contentCollImg[ (value + (init % (_contentCollImg.Count - 2 - value))) ];
			ImgDisplay imgDisplay = new ImgDisplay(_imgDisplayInst);
			imgDisplay.Display(pnl, content, true);
			imgDisplay = null;
			if (_mobile.IsAdvanced)
			{
				content = (Content)_contentCollImg[ init % value ];
				imgDisplay = new ImgDisplay(_imgDisplayInst);
				imgDisplay.Display(pnl, content, true);
				imgDisplay = null;
			}
		}

		public void DisplayContentSets(Panel pnl, int rangeInf, int rangeSup)
		{
			if (rangeInf == -1) rangeInf = 0;
			if (rangeSup == -1) rangeSup = _contentCollContentSet.Count;
			_contentSetDisplayInst = new ContentSetDisplayInstructions(_mobile);
			_contentSetDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "bullet");
			_contentSetDisplayInst.UrlDwld = "./linkto.aspx?id={0}&cg={1}";

			for( int i = rangeInf; i < rangeSup; i++ )
			{
				Content content = (Content)_contentCollContentSet[i];
				//if (WapTools.FindProperty(content.PropertyCollection, "CompositeContentGroup") != "IMG") continue;
				ContentSetDisplay contentSetDisplay = new ContentSetDisplay(_contentSetDisplayInst);
				contentSetDisplay.Display(pnl, content);
				contentSetDisplay = null;
			} 
		}
		#endregion
	}
}
