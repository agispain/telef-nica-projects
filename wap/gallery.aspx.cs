using System;
using System.Web.UI.MobileControls;
using KMobile.Catalog.Services;
using wap.Tools;

namespace wap
{
	public class gallery : System.Web.UI.MobileControls.MobilePage
	{
		protected System.Web.UI.MobileControls.Form Form1;
		protected Panel pnlGallery, pnlEnd;
		protected System.Web.UI.MobileControls.Panel pnlPreview;
		protected Image imgLogo;

		private void Page_Load(object sender, System.EventArgs e)
		{
			MobileCaps _mobile = (MobileCaps)Request.Browser; 

			
			if (_mobile.IsAdvanced)
				imgLogo.ImageUrl = String.Format(WapTools.GetImage(this.Request, "imagenes"), WapTools.GetFolderImg(_mobile));
			
			WapTools.AddLabelCenter(pnlGallery, WapTools.GetText("TitleGallery"), "", _mobile, BooleanOption.True);
			WapTools.AddLabel(pnlGallery, WapTools.GetText("DescGallery"), "", _mobile);
			foreach (string id in WapTools.getNodes("Gallerys"))
				WapTools.AddLink(pnlGallery, WapTools.GetText("TitleGallery" + id), "linkto.aspx?id=" + id, "", _mobile);
			//WapTools.AddLink(pnlShops, WapTools.GetText("Back"), "./default.aspx", WapTools.GetImage(this.Request, "bullet"), _mobile);
			WapTools.AddLink(pnlEnd, "Buscar", "http://10.132.67.244/buscador2/searcher.initsearch.do",WapTools.GetImage(this.Request, "buscar"), _mobile);
			WapTools.AddLink(pnlEnd, "Home", "http://wap.movistar.com", WapTools.GetImage(this.Request, "home"), _mobile);
			WapTools.AddLink(pnlEnd, "Atr�s", "default.aspx", WapTools.GetImage(this.Request, "back"), _mobile);
			WapTools.AddLink(pnlEnd, "Arriba", "error.aspx", WapTools.GetImage(this.Request, "up"), _mobile);
			WapTools.AddLinkCenter(pnlGallery, WapTools.GetText("ImagenesFondos"), "./default.aspx", "", _mobile);
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
