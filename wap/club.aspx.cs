using System;
using System.Configuration;
using System.Web;
using KMobile.Catalog.Services;
using wap.Tools;

namespace wap 
{
	public class _club : System.Web.UI.Page 
	{
		protected bool is3g = false;
		
		private void Page_Load(object sender, System.EventArgs e)
		{ 
			try 
			{ 
				MobileCaps _mobile = (MobileCaps)Request.Browser; 
				try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(this.Request, _mobile));}
				catch{is3g = false;}
				
				WapTools.LogUser(this.Request, 11037, _mobile.MobileType);						
				
				// redirection xhtml
				if (Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_Xhtml"]) && (_mobile.IsXHTML || WapTools.isXhtml(this.Request, _mobile) || is3g))
				{
					try{if (WapTools.isTestSite(this.Request)) Response.Redirect("http://emocion.dev.kiwee.com/xhtml_v6/club2.aspx", false); else Response.Redirect("http://emocion.kiwee.com/xhtml/club2.aspx", false);}
					catch{if (WapTools.isTestSite(this.Request)) Response.Redirect("http://emocion.dev.kiwee.com/xhtml_v6/club2.aspx", true); else Response.Redirect("http://emocion.kiwee.com/xhtml/club2.aspx", true);}
				} 
				else 
				{
					try{if (WapTools.isTestSite(this.Request)) Response.Redirect("http://emocion.dev.kiwee.com/wap/club2.aspx", false); else Response.Redirect("http://emocion.kiwee.com/wap/club2.aspx", false);}
					catch{if (WapTools.isTestSite(this.Request)) Response.Redirect("http://emocion.dev.kiwee.com/wap/club2.aspx", true); else Response.Redirect("http://emocion.kiwee.com/wap/club2.aspx", true);}
				} 
			}		
			catch(Exception caught)
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format(" Unexpected exception in emocion\\wap\\club.aspx - UA : {0}", Request.UserAgent), caught);
				Response.Redirect("./error.aspx");				
			}
		}


		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
