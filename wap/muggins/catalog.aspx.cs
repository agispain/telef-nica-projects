using System;
using System.Web;
using KMobile.Catalog.Presentation;
using KMobile.Catalog.Services;
using wap.Tools;

namespace wap.muggins
{
	public class catalog : CatalogBrowsing
	{
		protected System.Web.UI.MobileControls.Form frmCatalog; 
		protected System.Web.UI.MobileControls.Panel pnlCatalog, pnlEnd;
		protected System.Web.UI.MobileControls.Panel pnlPreview;
		protected System.Web.UI.MobileControls.Image imgLogo;
		protected int promo = -1;
		
		private void Page_Load(object sender, System.EventArgs e)
		{ 
			try
			{
				_mobile = (MobileCaps)Request.Browser;
				
				if (_mobile.MobileType != null &&  _mobile.IsCompatible("IMG_COLOR"))
				{ 
					WapTools.LogUser(this.Request, 202, _mobile.MobileType);
					_idContentSet = Convert.ToInt32(WapTools.GetXmlValue("Muggins/Backgrounds"));					

					int page = (Request.QueryString["n"] != null) ? Convert.ToInt32(Request.QueryString["n"]) : 1;
					_contentGroup = "IMG"; _contentType = "IMG_COLOR";
					_displayKey = WapTools.GetXmlValue("DisplayKey"); ;
				
					ContentSet contentSet = BrowseContentSetExtended();
				
					int nbPreview = (_mobile.IsAdvanced) ? 6 : 4;
					if (nbPreview > contentSet.ContentCount) nbPreview = contentSet.ContentCount;
					
					_imgDisplayInst = new ImgDisplayInstructions(_mobile);
					_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(String.Format("Url_{0}", _contentGroup));
					_imgDisplayInst.UrlDwld = "./step1.aspx?c={1}";
					bool noPreviews = false; 
					ReadContentSet(contentSet, pnlCatalog, page, nbPreview, !noPreviews);
				
					string txtPrevious = WapTools.GetText("Previous");
					string txtNext = WapTools.GetText("Next"); 

					if(_hasNextPage)
						WapTools.AddLink(pnlCatalog, txtNext, String.Format("./catalog.aspx?cg={0}&cs={1}&ms={2}&n={3}&p={4}&t={5}&s=1&{6}", _contentGroup, _idContentSet, Request.QueryString["ms"], page + 1, Request.QueryString["p"], Server.UrlEncode(Request.QueryString["t"]), Request.QueryString["p"], WapTools.GetParamBack(Request, false)), WapTools.GetImage(this.Request, "bullet"), _mobile);
					if(_hasPreviousPage)
						WapTools.AddLink(pnlCatalog, txtPrevious, String.Format("./catalog.aspx?cg={0}&cs={1}&ms={2}&n={3}&p={4}&t={5}&s=1&{6}", _contentGroup, _idContentSet, Request.QueryString["ms"], page - 1, Request.QueryString["p"], Server.UrlEncode(Request.QueryString["t"]), Request.QueryString["p"], WapTools.GetParamBack(Request, false)), WapTools.GetImage(this.Request, "bullet"), _mobile);								

					contentSet = null;
				
					#region HEADER
					if (_mobile.IsAdvanced)
						imgLogo.ImageUrl = String.Format(WapTools.GetImage(this.Request, "muggins"), WapTools.GetFolderImg(_mobile));
					else pnlPreview.Visible = false;
					#endregion
				}
				else
					WapTools.AddLabel(pnlCatalog, WapTools.GetText("Compatibility"), "", _mobile);
					
				WapTools.AddLink(pnlEnd, "Im�genes y Fondos", "../default.aspx",WapTools.GetImage(this.Request, "bullet"), _mobile);
			}
			catch(Exception caught)
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\wap\\muggins\\catalog.aspx  - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
				this.RedirectToMobilePage("../error.aspx");				
			}
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
