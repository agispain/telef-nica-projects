using System;
using System.Web;
using KMobile.Catalog.Services;
using wap.Tools;
using System.Web.UI.MobileControls;

namespace wap.muggins
{
	public class step2 : CatalogBrowsing 
	{
		protected Panel pnlText, pnlRopaInterior, pnlClotherColor;
		protected Image imgLogo;
		protected SelectionList mood, hairStyle, hairColor, clothes, colorClothes, trousers, colorTrousers, underwear, colorUnderwear, shoes, colorShoes, glasses, earrings, hat, necklace;
		protected System.Web.UI.MobileControls.Panel pnlPreview;
		protected System.Web.UI.MobileControls.Command Command1;
		protected System.Web.UI.MobileControls.Form frmDefault;
		
		private void Page_Load(object sender, System.EventArgs e)
		{ 
			try 
			{ 
				_mobile = (MobileCaps)Request.Browser; 
				
				string sex = Request.Form["sex"].ToUpper();
				string skin = Request.Form["skin"].ToUpper();
				string eyes = Request.Form["eyes"].ToUpper();

				string idContent = Request.Form["__V_idContent"];


				if (_mobile.MobileType != null &&  _mobile.IsCompatible("IMG_COLOR"))
				{      
					WapTools.LogUser(this.Request, 205, _mobile.MobileType);	
					
					try{WapTools.AddUIDatLog(Request, Response);}
					catch{}      
						
					if (_mobile.IsAdvanced)
						imgLogo.ImageUrl = String.Format(WapTools.GetImage(this.Request, "muggins"), WapTools.GetFolderImg(_mobile));

					#region VALUES 
					if (sex == "BOY")
					{
						mood.Items.Add(new MobileListItem("enamorado", "in_love"));
						mood.Items.Add(new MobileListItem("coraz�n roto", "heartbroken"));
						mood.Items.Add(new MobileListItem("guay", "cool"));
						mood.Items.Add(new MobileListItem("lig�n", "flirt"));
						mood.Items.Add(new MobileListItem("enfadado", "furious"));
						mood.Items.Add(new MobileListItem("contento", "happy"));
						mood.Items.Add(new MobileListItem("esqueleto", "skeleton"));
						
						hairStyle.Items.Add(new MobileListItem("rapado", "Soldier"));
						hairStyle.Items.Add(new MobileListItem("rizado", "Dread"));
						hairStyle.Items.Add(new MobileListItem("corto", "Short"));
						hairStyle.Items.Add(new MobileListItem("con flequillo", "Posh"));
						hairStyle.Items.Add(new MobileListItem("largo", "Long"));
						hairStyle.Items.Add(new MobileListItem("de punta", "Spiky"));
						hairStyle.Items.Add(new MobileListItem("sin pelo", ""));	
	
						hairColor.Items.Add(new MobileListItem("marr�n oscuro", "brown"));
						hairColor.Items.Add(new MobileListItem("marr�n claro", "lbrown"));
						hairColor.Items.Add(new MobileListItem("negro", "black"));
						hairColor.Items.Add(new MobileListItem("rubio", "blond"));
						hairColor.Items.Add(new MobileListItem("pelirrojo", "red"));

						clothes.Items.Add(new MobileListItem("camiseta s.valent�n", "tshirt_v"));
						clothes.Items.Add(new MobileListItem("jersey s.valent�n", "jersey_long_neck"));
						clothes.Items.Add(new MobileListItem("chaqueta cl�sica", "classic_jacket"));
						clothes.Items.Add(new MobileListItem("borreguillo", "furry_hoodie"));
						clothes.Items.Add(new MobileListItem("sudadera", "hoodie"));
						clothes.Items.Add(new MobileListItem("chupa motero", "motor_jacket"));
						clothes.Items.Add(new MobileListItem("jersey", "pullover"));
						clothes.Items.Add(new MobileListItem("gabardina", "trench_coat"));
						clothes.Items.Add(new MobileListItem("jersey cisne", "turtleneck_jersey"));
						clothes.Items.Add(new MobileListItem("chupa", "zipped_hoodie"));
						clothes.Items.Add(new MobileListItem("camiseta", "tshirt"));
						clothes.Items.Add(new MobileListItem("chaquet�n", "vampire_blouse"));
						clothes.Items.Add(new MobileListItem("camiseta rota", "frankenstein_blouse"));
						clothes.Items.Add(new MobileListItem("camisa", "shirt"));
						clothes.Items.Add(new MobileListItem("sudadera", "sweatshirt"));
						clothes.Items.Add(new MobileListItem("abrigo", "coat"));
						clothes.Items.Add(new MobileListItem("camiseta de Nadal", "nadal"));
						clothes.Items.Add(new MobileListItem("sin ropa", ""));
						
						colorClothes.Items.Add(new MobileListItem("amarillo", "yellow"));
						colorClothes.Items.Add(new MobileListItem("azul", "blue"));
						colorClothes.Items.Add(new MobileListItem("negro", "black"));
						colorClothes.Items.Add(new MobileListItem("rojo", "red"));
						colorClothes.Items.Add(new MobileListItem("verde", "green"));

						trousers.Items.Add(new MobileListItem("s.valent�n", "camo"));
						trousers.Items.Add(new MobileListItem("amorosos", "casual_v"));
						trousers.Items.Add(new MobileListItem("skater", "skater_low"));
						trousers.Items.Add(new MobileListItem("escoceses", "scottish"));
						trousers.Items.Add(new MobileListItem("gallumbos", "underwear"));
						trousers.Items.Add(new MobileListItem("con bolsos", "baggies"));
						trousers.Items.Add(new MobileListItem("cl�sicos", "classic"));
						trousers.Items.Add(new MobileListItem("elegantes", "elegant"));
						trousers.Items.Add(new MobileListItem("terciopelo", "velvet_stripped"));
						trousers.Items.Add(new MobileListItem("vaqueros", "jeans"));
						trousers.Items.Add(new MobileListItem("pinzas", "pleated"));
						trousers.Items.Add(new MobileListItem("chandal", "sport"));
						trousers.Items.Add(new MobileListItem("cl�sicos", "casual"));
						trousers.Items.Add(new MobileListItem("camuflaje", "army"));
						trousers.Items.Add(new MobileListItem("pantal�n corto", "nadal_shorts"));
						trousers.Items.Add(new MobileListItem("sin pantal�n", ""));
						
						colorTrousers.Items.Add(new MobileListItem("negro", "black"));
						colorTrousers.Items.Add(new MobileListItem("azul", "blue"));
						colorTrousers.Items.Add(new MobileListItem("marr�n", "brown"));
						colorTrousers.Items.Add(new MobileListItem("rojo", "red"));
						colorTrousers.Items.Add(new MobileListItem("verde", "green"));
						colorTrousers.Items.Add(new MobileListItem("blanco", "white"));

						shoes.Items.Add(new MobileListItem("botas", "boots"));
						shoes.Items.Add(new MobileListItem("antiguos", "classie"));
						shoes.Items.Add(new MobileListItem("deportivos", "sneakers2"));
						shoes.Items.Add(new MobileListItem("botas de nieve", "snowboard_boots"));
						shoes.Items.Add(new MobileListItem("botas de f�tbol", "sporty_boots"));
						shoes.Items.Add(new MobileListItem("all stars", "all_stars"));
						shoes.Items.Add(new MobileListItem("cl�sicos", "casual"));
						shoes.Items.Add(new MobileListItem("camper", "sneakers"));
						shoes.Items.Add(new MobileListItem("deportivos", "sport_shoes"));
						shoes.Items.Add(new MobileListItem("descalzo", ""));
						
						colorShoes.Items.Add(new MobileListItem("amarillo", "yellow"));
						colorShoes.Items.Add(new MobileListItem("azul", "blue"));
						colorShoes.Items.Add(new MobileListItem("blanco", "white"));
						colorShoes.Items.Add(new MobileListItem("marr�n", "brown"));
						colorShoes.Items.Add(new MobileListItem("negro", "black"));
						
						glasses.Items.Add(new MobileListItem("sin gafas", "noglasses"));
						glasses.Items.Add(new MobileListItem("cl�sicas", "glasses"));
						glasses.Items.Add(new MobileListItem("modernas", "glassessport"));
						glasses.Items.Add(new MobileListItem("ray-ban", "sunglasses"));
						
						pnlRopaInterior.Visible = false;
						
					}
					else
					{
						mood.Items.Add(new MobileListItem("enamorada", "in_love"));
						mood.Items.Add(new MobileListItem("coraz�n roto", "heartbroken"));
						mood.Items.Add(new MobileListItem("contenta", "happy"));
						mood.Items.Add(new MobileListItem("divertida", "fun"));
						mood.Items.Add(new MobileListItem("normal", "standard"));
						mood.Items.Add(new MobileListItem("sexy", "sexy"));
						mood.Items.Add(new MobileListItem("enfadada", "angry"));
						mood.Items.Add(new MobileListItem("diablesa", "devil"));
						//mood.Items.Add(new MobileListItem("esqueleto", "skeleton"));
						//mood.Items.Add(new MobileListItem("vampiresa", "vampire"));
						//mood.Items.Add(new MobileListItem("zombi", "zombie"));
						
						//hairStyle.Items.Add(new MobileListItem("mam� noel", "xmas"));
						hairStyle.Items.Add(new MobileListItem("corto", "short"));
						hairStyle.Items.Add(new MobileListItem("largo con flequillo", "fringe"));
						hairStyle.Items.Add(new MobileListItem("largo ondulado", "curly"));
						hairStyle.Items.Add(new MobileListItem("largo liso", "flat"));
						hairStyle.Items.Add(new MobileListItem("media melena", "medium"));
						hairStyle.Items.Add(new MobileListItem("bruja", "witch"));
						hairStyle.Items.Add(new MobileListItem("diablesa", "devil"));
//						hairStyle.Items.Add(new MobileListItem("frankenstein", "frankenstein"));
//						hairStyle.Items.Add(new MobileListItem("esqueleto", "skeleton"));
//						hairStyle.Items.Add(new MobileListItem("zombie", "zombie"));
						hairStyle.Items.Add(new MobileListItem("sin pelo", ""));

						hairColor.Items.Add(new MobileListItem("marr�n oscuro", "brown"));
						hairColor.Items.Add(new MobileListItem("negro", "black"));
						hairColor.Items.Add(new MobileListItem("pelirrojo", "red"));
						hairColor.Items.Add(new MobileListItem("rubio", "blond"));
						hairColor.Items.Add(new MobileListItem("violeta", "violet"));
						

						//clothes.Items.Add(new MobileListItem("mam� noel", "xmas_costume"));
						//clothes.Items.Add(new MobileListItem("vestido �lfico", "elf_costume"));
						clothes.Items.Add(new MobileListItem("camiseta s.valent�n", "valentines_tshirt"));
						clothes.Items.Add(new MobileListItem("jersey s.valent�n", "valentines_jersey"));
						clothes.Items.Add(new MobileListItem("vestido s.valent�n", "valentine_dress"));
						clothes.Items.Add(new MobileListItem("gabardina cool", "trench"));
						clothes.Items.Add(new MobileListItem("abrigo corto corto", "abrigo_borne"));
						clothes.Items.Add(new MobileListItem("abrigo largo", "abrigo_largo"));
						clothes.Items.Add(new MobileListItem("chaqueta cuero", "chaqueta_cuero"));
						clothes.Items.Add(new MobileListItem("chaqueta esqu�", "snowboard_jacket"));
						clothes.Items.Add(new MobileListItem("vestido de noche", "evening_gown"));
						clothes.Items.Add(new MobileListItem("top", "top"));
						clothes.Items.Add(new MobileListItem("chaleco", "waistcoat"));
						clothes.Items.Add(new MobileListItem("top tirantes", "tanktop"));
						clothes.Items.Add(new MobileListItem("sin ropa", ""));
						
						colorClothes.Items.Add(new MobileListItem("amarillo", "yellow"));
						colorClothes.Items.Add(new MobileListItem("azul", "blue"));
						colorClothes.Items.Add(new MobileListItem("blanco", "white"));
						colorClothes.Items.Add(new MobileListItem("marr�n", "brown"));
						colorClothes.Items.Add(new MobileListItem("negro", "black"));
						colorClothes.Items.Add(new MobileListItem("p�rpura", "purple"));
						colorClothes.Items.Add(new MobileListItem("rojo", "red"));
						colorClothes.Items.Add(new MobileListItem("verde", "green"));

						trousers.Items.Add(new MobileListItem("vaqueros s.valent�n", "valentine_jeans"));
						trousers.Items.Add(new MobileListItem("falda rodilla", "high_waisted_skirt"));
						trousers.Items.Add(new MobileListItem("falda corta", "pleated_skirt"));
						trousers.Items.Add(new MobileListItem("falda de cuadros", "tartan"));
						trousers.Items.Add(new MobileListItem("minifalda roja", "mini_skirt_red"));
						trousers.Items.Add(new MobileListItem("pitillo", "skinny_jeans"));
						trousers.Items.Add(new MobileListItem("minifalda", "mini_leggings"));
						trousers.Items.Add(new MobileListItem("vaqueros campana", "bell_jeans"));
						trousers.Items.Add(new MobileListItem("sin pantal�n", ""));

						colorTrousers.Items.Add(new MobileListItem("negro", "black"));
						colorTrousers.Items.Add(new MobileListItem("azul", "blue"));
						colorTrousers.Items.Add(new MobileListItem("marr�n", "brown"));
						colorTrousers.Items.Add(new MobileListItem("verde", "green"));
						colorTrousers.Items.Add(new MobileListItem("blanco", "white"));
						colorTrousers.Items.Add(new MobileListItem("amarillo", "yellow"));
						colorTrousers.Items.Add(new MobileListItem("kaki", "khaki"));
						colorTrousers.Items.Add(new MobileListItem("p�rpura", "purple"));
						colorTrousers.Items.Add(new MobileListItem("rojo", "red"));

						underwear.Items.Add(new MobileListItem("medias", "leggins"));
						underwear.Items.Add(new MobileListItem("medias rayadas", "stripedleggins"));
						underwear.Items.Add(new MobileListItem("sin medias", ""));

						colorUnderwear.Items.Add(new MobileListItem("amarillo", "yellow"));
						colorUnderwear.Items.Add(new MobileListItem("azul", "blue"));
						colorUnderwear.Items.Add(new MobileListItem("blanco", "white"));
						colorUnderwear.Items.Add(new MobileListItem("kaki", "khaki"));
						colorUnderwear.Items.Add(new MobileListItem("marr�n", "brown"));
						colorUnderwear.Items.Add(new MobileListItem("negro", "black"));
						colorUnderwear.Items.Add(new MobileListItem("p�rpura", "purple"));
						colorUnderwear.Items.Add(new MobileListItem("rojo", "red"));
						colorUnderwear.Items.Add(new MobileListItem("verde", "green"));

						earrings.Items.Add(new MobileListItem("sin pendientes", ""));
						earrings.Items.Add(new MobileListItem("negros", "earrings_black"));
						earrings.Items.Add(new MobileListItem("amarillos", "earrings_yellow"));
						//earrings.Items.Add(new MobileListItem("navide�os", "earrings_xmas"));
						//earrings.Items.Add(new MobileListItem("�lficos", "earrings_elf"));
						
						hat.Items.Add(new MobileListItem("sin sombrero", ""));
						//hat.Items.Add(new MobileListItem("gorro �lfico", "elf_hat"));
						//hat.Items.Add(new MobileListItem("navide�o", "xmas_hat"));
						hat.Items.Add(new MobileListItem("gorra", "pink_hat"));
						hat.Items.Add(new MobileListItem("de bruja", "hat"));
						hat.Items.Add(new MobileListItem("de lana", "woolcap"));
						
						necklace.Items.Add(new MobileListItem("sin colgante", ""));
						necklace.Items.Add(new MobileListItem("en cruz", "neck_cross"));
						necklace.Items.Add(new MobileListItem("coraz�n", "neck_heart"));
						//necklace.Items.Add(new MobileListItem("�lfico", "elf_necklace"));
						necklace.Items.Add(new MobileListItem("nieve", "snow_necklace"));
						
						//shoes.Items.Add(new MobileListItem("botas �lficas", "elf_boots"));
						//shoes.Items.Add(new MobileListItem("botas navide�as", "xmas_boots"));
						shoes.Items.Add(new MobileListItem("botas invierno", "frayed_boots"));
						shoes.Items.Add(new MobileListItem("botas tac�n", "high_heeled_boots"));
						shoes.Items.Add(new MobileListItem("botas nieve", "snow_boots"));
						shoes.Items.Add(new MobileListItem("tac�n de plataforma", "wedges"));
						shoes.Items.Add(new MobileListItem("all stars", "all_stars"));
						shoes.Items.Add(new MobileListItem("bailarinas", "ballerina"));
						shoes.Items.Add(new MobileListItem("botines", "booties"));
						shoes.Items.Add(new MobileListItem("chanclas", "beach_slippers"));
						shoes.Items.Add(new MobileListItem("zapatos tac�n", "high_heels"));
						shoes.Items.Add(new MobileListItem("descalza", ""));
						
						colorShoes.Items.Add(new MobileListItem("amarillo", "yellow"));
						colorShoes.Items.Add(new MobileListItem("azul", "blue"));
						colorShoes.Items.Add(new MobileListItem("blanco", "white"));
						colorShoes.Items.Add(new MobileListItem("marr�n", "brown"));
						colorShoes.Items.Add(new MobileListItem("negro", "black"));
						colorShoes.Items.Add(new MobileListItem("dorado", "gold"));
						colorShoes.Items.Add(new MobileListItem("plateado", "silver"));
						colorShoes.Items.Add(new MobileListItem("rojo", "red"));
						colorShoes.Items.Add(new MobileListItem("verde", "green"));
						
						glasses.Items.Add(new MobileListItem("sin gafas", "noglasses"));
						glasses.Items.Add(new MobileListItem("azul", "gl_sun_glasses_blue"));
						glasses.Items.Add(new MobileListItem("naranja", "gl_sun_glasses_orange"));
						glasses.Items.Add(new MobileListItem("oscura", "gl_sun_glasses_dark"));
						
					}
					#endregion
					
					this.HiddenVariables.Add("idContent", idContent);
					this.HiddenVariables.Add("sex", sex);
					this.HiddenVariables.Add("skin", skin);
					this.HiddenVariables.Add("eyes", eyes);
				}
				else
					WapTools.AddLabel(pnlText, WapTools.GetText("Compatibility"), "", _mobile);
				WapTools.AddLinkCenter(pnlText, "Volver", "./catalog.aspx", "", _mobile);

				
			}		
			catch(Exception caught)
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format(" Unexpected exception in emocion\\wap\\muggins\\step2.aspx - UA : {0}", Request.UserAgent), caught);
				this.RedirectToMobilePage("../error.aspx");				
			}
		}


		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
