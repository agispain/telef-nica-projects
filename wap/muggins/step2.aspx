<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Page language="c#" Codebehind="step2.aspx.cs" Inherits="wap.muggins.step2" AutoEventWireup="false" %>
<body Xmlns:mobile="http://schemas.microsoft.com/Mobile/WebForm">
	<mobile:form id="frmDefault" runat="server" EnableViewState="false" Action="download.aspx" Method="Post"
		Title="Crea tu AVATAR">
<mobile:Panel id="pnlPreview" runat="server">
			<mobile:Image id="imgLogo" Alignment="Center" Runat="server"></mobile:Image>
		</mobile:Panel>
		Actitud: <mobile:SelectionList id="mood" Runat="server" /><br/>
		Peinado: <mobile:SelectionList id="hairStyle" Runat="server" /><br/>
		Color de Pelo: <mobile:SelectionList id="hairColor" Runat="server" /><br/>
		Ropa: <mobile:SelectionList id="clothes" Runat="server" /><br/>
		<mobile:Panel id="pnlClotherColor" Runat="server">
				Color de ropa: <mobile:SelectionList id="colorClothes" Runat="server" /><br/>
		</mobile:Panel>
		Pantalones: <mobile:SelectionList id="trousers" Runat="server" /><br/>
		Color de pantalones: <mobile:SelectionList id="colorTrousers" Runat="server" /><br/>
		<mobile:Panel id="pnlRopaInterior" Runat="server">
			Ropa Interior: <mobile:SelectionList id="underwear" Runat="server" /><br/>
			Color de ropa interior: <mobile:SelectionList id="colorUnderwear" Runat="server" /><br/>
			Pendientes: <mobile:SelectionList id="earrings" Runat="server" /><br/>
			Sombreros: <mobile:SelectionList id="hat" Runat="server" /><br/>
			Collares: <mobile:SelectionList id="necklace" Runat="server" /><br/>
		</mobile:Panel>
		Zapatos: <mobile:SelectionList id="shoes" Runat="server" /><br/>
		Color de zapatos: <mobile:SelectionList id="colorShoes" Runat="server" /><br/>
		Gafas de sol<mobile:SelectionList id="glasses" Runat="server" /><br/>
		
			
		<mobile:Command id="Command1" Runat="server">DALE VIDA</mobile:Command>
<mobile:Panel id="pnlText" runat="server"></mobile:Panel>
	</mobile:form>
</body>
