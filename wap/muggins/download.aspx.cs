using System;
using System.Web;
using System.Web.UI.MobileControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using wap.Tools;

namespace wap.muggins
{
	public class download : MobilePage
	{
		protected System.Web.UI.MobileControls.Form Form1;
		protected System.Web.UI.MobileControls.Panel pnlContent;
		protected int _idContent = 0;
		
		private void Page_Load(object sender, System.EventArgs e)
		{					
			MobileCaps _mobile = (MobileCaps)Request.Browser;
			
			string sex = Request.Form["__V_sex"].ToUpper();
			string skin = Request.Form["__V_skin"].ToUpper();
			string eyes = Request.Form["__V_eyes"].ToUpper();
			string mood = (Request.Form["mood"] != null) ? Request.Form["mood"].ToUpper() : "";
			string hairStyle = (Request.Form["hairStyle"] != null) ? Request.Form["hairStyle"].ToUpper() : "";
			string hairColor = (Request.Form["hairColor"] != null) ? Request.Form["hairColor"].ToUpper() : "";
			string clothes = (Request.Form["clothes"] != null) ? Request.Form["clothes"].ToUpper() : "";
			string colorClothes = (Request.Form["colorClothes"] != null) ? Request.Form["colorClothes"].ToUpper() : "";
			string trousers = (Request.Form["trousers"] != null) ? Request.Form["trousers"].ToUpper() : "";
			string colorTrousers = (Request.Form["colorTrousers"] != null) ? Request.Form["colorTrousers"].ToUpper() : "";
			string shoes = (Request.Form["shoes"] != null) ? Request.Form["shoes"].ToUpper() : "";
			string colorShoes = (Request.Form["colorShoes"] != null) ? Request.Form["colorShoes"].ToUpper() : "";
			string underwear = (Request.Form["underwear"] != null) ? Request.Form["underwear"].ToUpper() : "";
			string colorUnderwear = (Request.Form["colorUnderwear"] != null) ? Request.Form["colorUnderwear"].ToUpper() : "";
			string glasses = (Request.Form["glasses"] != null) ? Request.Form["glasses"].ToUpper() : "";
			string others = (Request.Form["others"] != null) ? Request.Form["others"].ToUpper() : "";
			string necklace = (Request.Form["necklace"] != null) ? Request.Form["necklace"].ToUpper() : "";
			string hat = (Request.Form["hat"] != null) ? Request.Form["hat"].ToUpper() : "";
			string earrings = (Request.Form["earrings"] != null) ? Request.Form["earrings"].ToUpper() : "";

			try{_idContent = (Request.Form["__V_idContent"] != null && Request.Form["__V_idContent"] != "") ? Convert.ToInt32(Request.Form["__V_idContent"]) : 0;}
			catch{_idContent = 0;}

			if(_idContent > 0)
			{
				try 
				{
					string _displayKey = WapTools.GetXmlValue("DisplayKeyAvatars");

					this.ActiveForm.Title = "Avatares";
					Operator op = new Operator(Request.UserHostAddress);
					if (op.OperatorName!= null && op.OperatorName == "MOVISTAR")
					{
						ParameterList pl = new ParameterList();
						pl.Add("MUGGIN", String.Format("{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}-{8}-{9}-{10}-{11}-{12}-{13}-{14}-{15}-{16}-{17}-{18}", sex, skin, mood, eyes, hairColor, hairStyle, clothes, colorClothes, trousers, colorTrousers, shoes, colorShoes, underwear, colorUnderwear, others, glasses, earrings, hat, necklace)); 

						DownloadInfo downloadInfo = null;
						BillingRequest billingRequest = null;

						CommandItem commandItem = new CommandItem(new Guid(_displayKey), _idContent, "IMG_COLOR", null, "WAP", _mobile.MobileType, "IMG");
						BillingManager billingManager = new BillingManager(); 
	
						billingRequest = billingManager.CreateCommand(Request, WapTools.GetXmlValue("Billing/Provider_CLUB"), commandItem);
						downloadInfo = billingManager.DeliverCommand(Request, billingRequest.GUIDCommand, null, pl, WrapperType.DescriptorWrapper | WrapperType.ForwardLockWrapper);
					
						string dwldUrl = downloadInfo.Uri;
						Trace.Warn( "Uri : " + dwldUrl );

						WapTools.AddLabel(pnlContent, "Para finalizar tu descarga, pulsa en el siguiente enlace UNA SOLA VEZ:", "", _mobile);
						WapTools.AddLabel(pnlContent, " ", "", _mobile);
						WapTools.AddLink(pnlContent, "Descargar aqu�", dwldUrl, "", _mobile);
						WapTools.AddLabel(pnlContent, " ", "", _mobile);
						WapTools.AddLabel(pnlContent, "Puede durar varios segundos", "", _mobile);
				
					}
					else
						WapTools.AddLabel(pnlContent, WapTools.GetText("Operateur"), "", _mobile);

					WapTools.AddLink(pnlContent, "Avatares", "./catalog.aspx", "", _mobile);
				}
				catch(Exception caught)
				{
					WapTools.SendMail(HttpContext.Current, caught);
					Log.LogError(String.Format(" Unexpected exception in EMOCION --> emocion.kiwee.com\\wap\\muggins\\download.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
					this.RedirectToMobilePage("http://emocion.kiwee.com/wap/error.aspx");	
				}
			}
			
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
