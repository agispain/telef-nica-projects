using System;
using System.Configuration;
using System.Web;
using System.Web.UI.MobileControls;
using KMobile.Catalog.Presentation;
using KMobile.Catalog.Services;
using wap.Tools;

namespace wap
{
	public class viewdwld : CatalogBrowsing
	{
		protected string description, url, site = "wap";
		protected Panel pnlView;
		protected System.Web.UI.MobileControls.Form frmSearch;
		protected Image imgLogo;
		public bool is3g = false;

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				Content content = null;;
				_mobile = (MobileCaps)Request.Browser;
				string teforig = null;

				try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(this.Request, _mobile));}
				catch{is3g = false;}

				try
				{
					if (Request["ref"]!=null && Request.QueryString["ref"]=="SEARCH") WapTools.LogUser(this.Request, 104, _mobile.MobileType);
					else if (Request["ref"]!=null && Request.QueryString["ref"]=="JUMPTAP") WapTools.LogUser(this.Request, 105, _mobile.MobileType);
					else if (Request["ref"]!=null && Request.QueryString["ref"]=="YAHOO") WapTools.LogUser(this.Request, 150, _mobile.MobileType);
				}
				catch{}		

				try
				{
					if (Request["teforig"]!=null && Request.QueryString["teforig"]!="") 
						teforig = Request.QueryString["teforig"].ToString();
				}
				catch{teforig = null;}	
				
				_displayKey = WapTools.GetXmlValue("DisplayKey");
				_idContent = (Request.QueryString["c"] != null) ? Convert.ToInt32(Request.QueryString["c"]) : 24957;
				string referer = (Request.QueryString["ref"] != null) ? Request.QueryString["ref"].ToString() : "";

				try{content = StaticCatalogService.GetAllContentDetails(_displayKey, _idContent, _mobile.MobileType, null, null);}
				catch {content = null;}

				try{_idContentSet = (Request.QueryString["cs"] != null) ? Convert.ToInt32(Request.QueryString["cs"]) : 0;}
				catch{_idContentSet = 0;}

				if (is3g) site = "3g";
				else if (_mobile.IsXHTML || WapTools.isXhtml(this.Request, _mobile)) site = "xhtml";
							
				if (content != null )
				{
					_contentGroup = content.ContentGroup.Name;
					_contentType = WapTools.GetDefaultContentType(_contentGroup);
					
					if (_mobile.IsCompatible(_contentType))
					{
						//if (_contentType == "IMG_COLOR" && (is3g || _mobile.IsXHTML || WapTools.isXhtml(this.Request, _mobile)))
						//	url = String.Format("http://emocion.kiwee.com/xhtml/view.aspx?ref={0}&ct={1}&cs=0&c={2}", HttpUtility.UrlEncode(site + "|" + referer), _contentType, _idContent.ToString());
						//else if (_contentType == "IMG_COLOR")
						//	url = String.Format("./view.aspx?ref={0}&ct={1}&cs=0&c={2}", HttpUtility.UrlEncode(site + "|" + referer), _contentType, _idContent.ToString());						
						//else
						url = String.Format(WapTools.GetUrlBilling(this.Request, _contentGroup, _contentType, HttpUtility.UrlEncode(site + "|" + referer), "", _idContentSet.ToString()), WapTools.isBranded(content) ? "branded" : "", _idContent.ToString(), _contentType);
						if (teforig != null && teforig != "")
							url += "&teforig=" + teforig;
						try{this.RedirectToMobilePage(url, false);}
						catch{this.RedirectToMobilePage(url, true);}
					}
					else
					{
						imgLogo.ImageUrl = String.Format(WapTools.GetImage(this.Request, "imagenes"), WapTools.GetFolderImg(_mobile));
						WapTools.AddLabel(pnlView, WapTools.GetText("ContentCompatibility"), "", _mobile);
						WapTools.AddLinkCenter(pnlView, WapTools.GetText("ImagenesFondos"), "./default.aspx", "", _mobile);
					}
				}
				else
				{	
					try{this.RedirectToMobilePage("./error.aspx", false);}
					catch{this.RedirectToMobilePage("./error.aspx", true);}
				}
			}
			catch(Exception caught)
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format(" Unexpected exception in wap\\emocion\\view.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
				this.RedirectToMobilePage("./error.aspx");	
			}
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
