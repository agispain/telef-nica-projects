using System;
using System.Configuration;
using KMobile.Catalog.Services;
using wap.Tools;

namespace wap
{
	public class linkto : System.Web.UI.Page
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			MobileCaps _mobile = (MobileCaps)Request.Browser;
			string idSite = Request.QueryString["id"];

			if (_mobile.Family == "Android") // || WapTools.isSomething(_mobile.MobileType, "Android") || WapTools.isSomething(_mobile.MobileType, "Android16B"))
			{
				string cg = Request.QueryString["cg"];
				Trace.Write("Redirecting to Android site");

				try{Response.Redirect(String.Format("http://emocion.kiwee.com/web/default.aspx?id={0}&cg={1}&cl={2}", idSite, cg, Request.QueryString["cl"]), false);}
				catch{Response.Redirect(String.Format("http://emocion.kiwee.com/web/default.aspx?id={0}&cg={1}&cl={2}", idSite, cg, Request.QueryString["cl"]),  true);}
			} 
			else 
			{

				if (idSite == "5987") idSite = "6045";
				if (idSite == "5747") idSite = "7176";
				if (idSite == "7347") idSite = "3059"; //chicas con motos
				if (idSite == "7344") idSite = "3967"; //cartoon networks
				//if (idSite == "7322") idSite = "4124"; 
			
				//	if (idSite == "2090") idSite = "6389";
				//	if (idSite == "5972" && (DateTime.Now.Day == 8 || DateTime.Now.Day == 6)) idSite = "2604";
				//	if (idSite == "5972") idSite = "2604";
				int site = 0;
				try {site = Convert.ToInt32(idSite);}
				catch {site = 0;}
	
				try{WapTools.AddUIDatLog(Request, Response);}
				catch{}

				if (site == 0)
				{
					idSite = WapTools.GetXmlValue("Home/CsError");
					string cg = WapTools.GetXmlValue("Home/CgError");
					string link = String.Format("./catalog.aspx?cg={0}&cs={1}", cg, idSite);
					try
					{
						WapTools.LogUser(this.Request, Convert.ToInt32(idSite), _mobile.MobileType);
						Response.Redirect(link, false);
					}
					catch{Response.Redirect(link, true);}
				}
				else if (site <= 50)
				{
					try
					{
						int log = 110;
						log += site;
						WapTools.LogUser(this.Request, log, _mobile.MobileType);
						if (WapTools.GetText("LnkShop" + idSite) != "")
						{
							try{Response.Redirect(WapTools.GetText("LnkShop" + idSite), false);}
							catch{Response.Redirect(WapTools.GetText("LnkShop" + idSite), true);}
						}				
						else
							Response.Redirect("./default.aspx");
					}
					catch{Response.Redirect("./default.aspx");}
				}
				else if (Convert.ToInt32(idSite) >= 10000)
				{
					try
					{
						if (WapTools.GetText("LnkGenera" + idSite) != "")
						{
							if(Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_Xhtml"]) && (_mobile.IsXHTML || WapTools.isXhtml(this.Request, _mobile)) && idSite == "11017")
							{
								try{Response.Redirect(ConfigurationSettings.AppSettings["UrlXhtmlLinkto"] + Request.ServerVariables["QUERY_STRING"], false);}
								catch{Response.Redirect(ConfigurationSettings.AppSettings["UrlXhtmlLinkto"] + Request.ServerVariables["QUERY_STRING"], true);}
							}
							else
							{
								WapTools.LogUser(this.Request, site, _mobile.MobileType);
								try{Response.Redirect(WapTools.GetText("LnkGenera" + idSite), false);}
								catch{Response.Redirect(WapTools.GetText("LnkGenera" + idSite), true);}	
							}
						}
						else
							Response.Redirect("./default.aspx");
					}
					catch{Response.Redirect("./default.aspx");}
				}
				else
				{
					string link = String.Format("./catalog.aspx?cg={0}&cs={1}&p={2}&t={3}", Request.QueryString["cg"], idSite, Request.QueryString["p"], Request.QueryString["t"]);
					if (Request.QueryString["a1"] != null && Request.QueryString["a1"] != "" )
						link += String.Format("&a1={0}&a2={1}&a3={2}&a4={3}&a5={4}&a6={5}", Request.QueryString["a1"], Request.QueryString["a2"], Request.QueryString["a3"], 
							Request.QueryString["a4"], Request.QueryString["a5"], Request.QueryString["a6"]);
					if (Request.QueryString["h"] != null && Request.QueryString["h"] != "" )
						link += "&h=1";
					if (Request.QueryString["cl"] != null && Request.QueryString["cl"] != "" )
						link += "&cl=1";
					if (Request.QueryString["ms"] != null && Request.QueryString["ms"] != "" )
						link += "&ms=" + Request.QueryString["ms"];
		
					try
					{
						WapTools.LogUser(this.Request, site, _mobile.MobileType);
						Response.Redirect(link, false);
					}
					catch{Response.Redirect(link, true);}
				}
			}
		}
		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
