using System;
using KMobile.Catalog.Services;
using wap.Tools;

namespace wap.sexy
{
	public class linkto : System.Web.UI.Page
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			MobileCaps _mobile = (MobileCaps)Request.Browser;
			string idSite = Request.QueryString["id"];
			int site = 0;
			try {site = Convert.ToInt32(idSite);}
			catch {site = 0;}
	
			try{WapTools.AddUIDatLog(Request, Response);}
			catch{}

			if (site == 0)
			{
				idSite = WapTools.GetXmlValue("Home/CsError");
				string cg = WapTools.GetXmlValue("Home/CgError");
				string link = String.Format("../catalog.aspx?cg={0}&cs={1}&s=1", cg, idSite);
				try
				{
					WapTools.LogUser(this.Request, Convert.ToInt32(idSite), _mobile.MobileType);
					Response.Redirect(link, false);
				}
				catch{Response.Redirect(link, true);}
			}			
			else
			{
				string link = String.Format("../catalog.aspx?cg={0}&cs={1}&p={2}&t={3}&s=1", Request.QueryString["cg"], idSite, Request.QueryString["p"], Request.QueryString["t"]);
				if (Request.QueryString["a1"] != null && Request.QueryString["a1"] != "" )
					link += String.Format("&a1={0}&a2={1}&a3={2}&a4={3}&a5={4}&a6={5}", Request.QueryString["a1"], Request.QueryString["a2"], Request.QueryString["a3"], 
						Request.QueryString["a4"], Request.QueryString["a5"], Request.QueryString["a6"]);
		
				try
				{
					WapTools.LogUser(this.Request, site, _mobile.MobileType);
					Response.Redirect(link, false);
				}
				catch{Response.Redirect(link, true);}
			}
		}
		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
