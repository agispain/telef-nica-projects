using System;
using System.Web;
using System.Web.UI.MobileControls;
using KMobile.Catalog.Presentation;
using KMobile.Catalog.Services;
using wap.Tools;

namespace wap.club
{
	public class catalog : CatalogBrowsing
	{
		protected System.Web.UI.MobileControls.Panel pnlCatalog, pnlEnd;
		protected System.Web.UI.MobileControls.Panel pnlPreview;
		protected System.Web.UI.MobileControls.Image imgLogo;
		
		private void Page_Load(object sender, System.EventArgs e)
		{ 
			try
			{
				_mobile = (MobileCaps)Request.Browser;

				if (_mobile.MobileType != null && _mobile.IsCompatible("IMG_COLOR"))
				{
					_idContentSet = (Request.QueryString["cs"] != null) ? Convert.ToInt32(Request.QueryString["cs"]) : 6664;
					int page = (Request.QueryString["n"] != null) ? Convert.ToInt32(Request.QueryString["n"]) : 1;
					_contentGroup = (Request.QueryString["cg"] != null) ? Request.QueryString["cg"].ToString() : "COMPOSITE";
					_contentType = WapTools.GetDefaultContentType(_contentGroup);
					_displayKey = WapTools.GetXmlValue("DisplayKeyClub");
									
					string paramBack = String.Format("a1=n&a2={0}&a3=cg&a4={1}&a5=cs&a6={2}",
						page, _contentGroup, _idContentSet);

					ContentSet contentSet = BrowseContentSetExtended();
					int nbPreview = (_mobile.PreferredRenderingType == "chtml10") ? 2 : (_mobile.IsAdvanced) ? 6 : 4;
					if (nbPreview > contentSet.ContentCount) nbPreview = contentSet.ContentCount;
					int nbPages = (contentSet.Count % nbPreview == 0) ? contentSet.Count / nbPreview : contentSet.Count / nbPreview + 1;

					if (_idContentSet == 6664)
					{
						WapTools.LogUser(this.Request, 110, _mobile.MobileType);		
						// INSTANT WIN
//						if (Request.Headers["TM_user-id"] != null && 
//							((Request.Headers["TM_user-id"].ToString() == "0343500081583261738") 
//							|| (Request.Headers["TM_user-id"].ToString() == "0340308234027839328")
//							|| (Request.Headers["TM_user-id"].ToString() == "0349627515117387490")))
//							WapTools.AddLabelCenter(pnlCatalog, "ENHORABUENA! Has ganado un Nokia 6110. Para recoger tu premio llama al  91 524 57 64 indicando el codigo " + Request.Headers["TM_user-id"].Substring(17), "", _mobile, BooleanOption.True);
					}

					if( _contentGroup == "COMPOSITE" )
					{
						nbPreview = (_mobile.IsAdvanced) ? 15 : 10;
						_contentSetDisplayInst = new ContentSetDisplayInstructions(_mobile);
						_contentSetDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "bullet");
						_contentSetDisplayInst.UrlDwld = String.Format("./catalog.aspx?cs={0}&cg={1}&p={2}&t={3}&{4}", "{0}", "{1}", _idContentSet.ToString(), Server.UrlEncode(contentSet.Name), paramBack); 
					}
					else if (_contentGroup == "VIDEO" || _contentGroup == "VIDEO_RGT" || _contentGroup == "") 
					{
						if (_contentGroup=="")
							_contentType = WapTools.GetDefaultContentType(_contentGroup);
						
						_videoDisplayInst = new VideoDisplayInstructions(_mobile);
						//_videoDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, _contentGroup, _contentType, HttpUtility.UrlEncode(String.Format("WAP|CONTENTSET|{0}|{1}", contentSet.Name, page)), "", _idContentSet.ToString()); 
						_videoDisplayInst.UrlDwld = WapTools.GetUrlBillingClub(this.Request, _contentGroup, _contentType, HttpUtility.UrlEncode(String.Format("CLUB_WAP|CONTENTSET|{0}|{1}", contentSet.Name, page)), "", _idContentSet.ToString()); 
					}				
					else
					{
						_imgDisplayInst = new ImgDisplayInstructions(_mobile);
						_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(String.Format("Url_{0}", _contentGroup));
						//_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, _contentGroup, _contentType, HttpUtility.UrlEncode(String.Format("{0}|CONTENTSET|{1}|{2}", _referer != "" ? _referer : "WAP", contentSet.Name, page)), "", _idContentSet.ToString()); 
						_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingClub(this.Request, _contentGroup, _contentType, HttpUtility.UrlEncode(String.Format("CLUB_WAP|CONTENTSET|{0}|{1}", contentSet.Name, page)), "", _idContentSet.ToString()); 
					}
 
					if (_contentGroup=="COMPOSITE")
						ReadContentSet(contentSet, pnlCatalog, page, nbPreview, true);
					else
					{
						WapTools.AddLabelCenter(pnlCatalog, contentSet.Name + " (" + page.ToString() + "/" + nbPages.ToString() + ")", "", _mobile, BooleanOption.True);
						ReadContentSet(contentSet, pnlCatalog, page, nbPreview, true);
					}
					string txtPrevious = WapTools.GetText("Previous");
					string txtNext = WapTools.GetText("Next"); 

					if(_hasNextPage)
						WapTools.AddLink(pnlCatalog, txtNext, String.Format("./catalog.aspx?cg={0}&cs={1}&ms={2}&n={3}&p={4}&t={5}&s=1&{6}", _contentGroup, _idContentSet, Request.QueryString["ms"], page + 1, Request.QueryString["p"], Server.UrlEncode(Request.QueryString["t"]), Request.QueryString["p"], WapTools.GetParamBack(Request, false)), WapTools.GetImage(this.Request, "bullet"), _mobile);
					if(_hasPreviousPage)
						WapTools.AddLink(pnlCatalog, txtPrevious, String.Format("./catalog.aspx?cg={0}&cs={1}&ms={2}&n={3}&p={4}&t={5}&s=1&{6}", _contentGroup, _idContentSet, Request.QueryString["ms"], page - 1, Request.QueryString["p"], Server.UrlEncode(Request.QueryString["t"]), Request.QueryString["p"], WapTools.GetParamBack(Request, false)), WapTools.GetImage(this.Request, "bullet"), _mobile);					

					#region LINKS
					try
					{
						WapTools.AddLabel(pnlCatalog, " ", "", _mobile);
						int isAlerta = WapTools.isAlerta(_idContentSet);
						if (isAlerta > 0)
							WapTools.AddLink(pnlCatalog, "Alerta de " + WapTools.GetText("Alerta" + isAlerta.ToString()), "../linkto.aspx?id=" + (20 + isAlerta).ToString(), WapTools.GetImage(this.Request, "bullet"), _mobile);
						
						if (Request["p"] != null && Request["t"] != null && Request["p"] != "" && Request["t"] != "")
							WapTools.AddLink(pnlCatalog, "Volver a " + Server.UrlDecode(Request.QueryString["t"]), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}", Request.QueryString["p"]), WapTools.GetImage(this.Request, "bullet"), _mobile);
						
						if (_idContentSet != 6664 && Request.QueryString["a6"] != "6664" && Request.QueryString["p"] != "6664")
							WapTools.AddLinkCenter(pnlCatalog, "Zona VIP de Fondos", "./catalog.aspx?cg=COMPOSITE&cs=6664", "", _mobile);
					
						WapTools.AddLinkCenter(pnlCatalog, "Salir de la Zona VIP de Fondos", "../default.aspx", "", _mobile);
					}
					catch{}
					#endregion

					contentSet = null;
				}
				else
					WapTools.AddLabel(pnlCatalog, WapTools.GetText("Compatibility"), "", _mobile);
				string atras = WapTools.UpdateFooter(_mobile, this.Context, null); 				

				#region HEADER
				if (_mobile.IsAdvanced)
				{ 
					imgLogo.ImageUrl = String.Format(WapTools.GetImage(this.Request, "bannervip"), WapTools.GetFolderImg(_mobile));
				}
				else pnlPreview.Visible = false;
				#endregion
					
				WapTools.AddLink(pnlEnd, "Buscar", "http://10.132.67.244/buscador2/searcher.initsearch.do",WapTools.GetImage(this.Request, "buscar"), _mobile);
				WapTools.AddLink(pnlEnd, "emoci�n", "http://wap.movistar.com", WapTools.GetImage(this.Request, "home"), _mobile);
				WapTools.AddLink(pnlEnd, "Atr�s", atras, WapTools.GetImage(this.Request, "back"), _mobile);
				WapTools.AddLink(pnlEnd, "Arriba", String.Format("catalog.aspx?{0}", Request.ServerVariables["QUERY_STRING"]), WapTools.GetImage(this.Request, "up"), _mobile);
				//Search
				//WapTools.AddLink(pnlCatalog, WapTools.GetText("SearchLink"), String.Format("./search.aspx?cg={0}", contentGroupDisplay), "", _mobile);
				//WapTools.AddLink(pnlCatalog, WapTools.GetText("Back"), "./default.aspx", WapTools.GetImage(this.Request, "bullet"), _mobile);
			}
			catch(Exception caught)
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\wap\\club\\catalog.aspx  - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
				this.RedirectToMobilePage("../error.aspx");				
			}
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
