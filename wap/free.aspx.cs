using System;
using System.Web;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using wap.Tools;

namespace wap
{
	public class free : CatalogBrowsing
	{
		protected System.Web.UI.MobileControls.Panel pnlContent;
		protected int ms;
		protected string theme = "", page = "";
		protected System.Web.UI.MobileControls.Form Form1;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (WapTools.isPromo(this.Request) == 0)
			{					
			_mobile = (MobileCaps)Request.Browser;
			_idContent = (Request.QueryString["c"] != null) ? Convert.ToInt32(Request.QueryString["c"]) : 0;
				if(_idContent > 0)
				{
					try 
					{
						_contentType = (Request.QueryString["ct"] != null) ? Request.QueryString["ct"] : "";
						_contentGroup = WapTools.GetDefaultContentGroup(_contentType);
						try{_idContentSet = (Request.QueryString["cs"] != null) ? Convert.ToInt32(Request.QueryString["cs"]) : 0;}
						catch{_idContentSet = 0;}
						try{ms = (Request.QueryString["ms"] != null) ? Convert.ToInt32(Request.QueryString["ms"]) : 0;}
						catch{ms = 0;}
						_displayKey = WapTools.GetXmlValue("DisplayKey");

						this.ActiveForm.Title = "Im�genes y Fondos";
						Operator op = new Operator(Request.UserHostAddress);
						Trace.Warn(op.OperatorName);
						if (op.OperatorName!= null && op.OperatorName == "MOVISTAR")
						{
							DownloadInfo downloadInfo = null;
							BillingRequest billingRequest = null;

							CommandItem commandItem = new CommandItem(new Guid(_displayKey), _idContent, _contentType, null, "FREE", _mobile.MobileType, _contentGroup);
							BillingManager billingManager = new BillingManager(); 
	
							billingRequest = billingManager.CreateCommand(Request, WapTools.GetXmlValue("Billing/Provider_FREE"), commandItem);
							downloadInfo = billingManager.DeliverCommand(Request, billingRequest.GUIDCommand, null, null, WrapperType.DescriptorWrapper | WrapperType.ForwardLockWrapper);
					
							string dwldUrl = downloadInfo.Uri;
							Trace.Warn( "Uri : " + dwldUrl );

							WapTools.AddLabel(pnlContent, "Para finalizar tu descarga, pulsa en el siguiente enlace UNA SOLA VEZ:", "", _mobile);
							WapTools.AddLabel(pnlContent, " ", "", _mobile);
							WapTools.AddLink(pnlContent, "Descargar aqu�", dwldUrl, "", _mobile);
							WapTools.AddLabel(pnlContent, " ", "", _mobile);
							WapTools.AddLabel(pnlContent, "Puede durar varios segundos", "", _mobile);
							try
							{
								if(theme != "")
								{
									if (page == "") page = "1";
									if (theme.ToUpper().IndexOf("M�S ") < 0)
										WapTools.AddLink(pnlContent, "M�s " + theme, String.Format("http://emocion.kiwee.com/wap/catalog.aspx?cs={0}&cg={1}", _idContentSet.ToString(), _contentGroup), "", _mobile);
									else 
										WapTools.AddLink(pnlContent, theme, String.Format("http://emocion.kiwee.com/wap/catalog.aspx?cs={0}&cg={1}", _idContentSet.ToString(), _contentGroup), "", _mobile);
								}
							}
							catch{}
											
						}
						else
							WapTools.AddLabel(pnlContent, WapTools.GetText("Operateur"), "", _mobile);

						WapTools.AddLink(pnlContent, "Im�genes y Fondos", "http://emocion.kiwee.com/wap", "", _mobile);
					}
					catch(Exception caught)
					{
						WapTools.SendMail(HttpContext.Current, caught);
						Log.LogError(String.Format(" Unexpected exception in EMOCION --> emocion.kiwee.com\\wap\\free.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
						this.RedirectToMobilePage("http://emocion.kiwee.com/wap/error.aspx");	
					}
				}
				else
					this.RedirectToMobilePage("http://emocion.kiwee.com/wap/imagebranded.aspx?" + Request.ServerVariables["QUERY_STRING"]);	
			}
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
