using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Mail;
using System.Web.UI.MobileControls;
using System.Xml;
using AGInteractive.Business;
using KMobile.BasicTools;
using KMobile.Catalog.Presentation;
using KMobile.Catalog.Services;


namespace wap.Tools
{
	public struct Especial
	{
		public string name;
		public string filter;
	}

	public class WapCommand : System.Web.UI.MobileControls.Command
	{
		// Render IMG --> Alt + Src
		protected override void Render(System.Web.UI.HtmlTextWriter output)
		{
			this.Format = CommandFormat.Link;
			this.CausesValidation = true;
			output.Write("<small><anchor>Buscar<go method=\"get\" href=\"" + this.MobilePage.ActiveForm.Action + "\">");
			output.Write("<postfield name=\"ct\" value=\"image\" />");
			output.Write("<postfield name='q' value='$(q)' />");
			//output.Write("<postfield name='categoryName' value='image' />");
			output.WriteLine("</go></anchor></small><br/>");

//			output.Write("<small><anchor title='Ir'>" + this.Text + "<go href='" + this.MobilePage.ActiveForm.Action + "'>");
//			output.Write("<postfield name='q' value='$(q)' />");
//			output.Write("<postfield name='db' value='vimages' />");
//			output.Write("<postfield name='jtsp' value='vimages' />");
//			output.WriteLine("</go></anchor></small><br/>");
		}
	}


	public class WapTools
	{
		private static XmlDocument _xmlDoc; 
		private static string TEFUri = "http://213.4.216.20:1280/AdCLProxy/GetAds?";
		private static string newTEFUri = "http://213.4.216.20:1280/AdCLProxy/GetAdvertisements?";
		private static string newTEFUri2 = "http://80.169.208.62/adc/adc?";
		private static string newTEFUri3 = "http://es-acl.telefonica.com/adc/adc?";
		//private static string TEFUriTest = "http://195.235.160.209:1280/AdCLProxy/GetAds?";
		private static string newTEFUri4 = "http://tfes-prod.amobee.com/upsteed/gaprequest?";
		
		public static bool isTestSite(HttpRequest req)
		{
			if (req.ServerVariables["SERVER_NAME"].ToUpper().IndexOf(".DEV.")>=0 || req.ServerVariables["SERVER_NAME"].ToUpper().IndexOf("LOCALHOST")>=0)
				return true;
			else
				return false;
		}

		public static string GetFolderImg(MobileCaps mobile)
		{
			string size = "118";
			if(mobile.ScreenPixelsWidth >= 164 && mobile.ScreenPixelsWidth < 229)
				size = "164";
			if(mobile.ScreenPixelsWidth >= 229 && mobile.ScreenPixelsWidth < 315)
				size = "229";
			if(mobile.ScreenPixelsWidth >= 315 && mobile.ScreenPixelsWidth < 339)
				size = "315";
			if(mobile.ScreenPixelsWidth >= 339)
				size = "339";
			if (ConfigurationSettings.AppSettings["IsChristmas"] == "true")
				size =  "christmas_" + size;
			return size;
		}

		public static string UpdateFooter(MobileCaps mobile, HttpContext context, string urlBackDefault )
		{
			if( context.Request.QueryString["a1"] != null )
				urlBackDefault = String.Format("{0}{1}", context.Request.ApplicationPath, WapTools.GetXmlValue("Back/CatalogGraphic"));
			try
			{
				if( urlBackDefault != null )
				{
					string param = GetParamBack(context.Request, true);

					return String.Format("{0}?{1}", urlBackDefault, param);
				}
				else
				{
					return "./default.aspx";
				}
			} 
			catch{return "./default.aspx";}
		}

		public static XmlNode GetXmlNode( string path )
		{
			string pathXmlFile = HttpContext.Current.Server.MapPath("~/ConfigSite.xml"); // Gets Physical path of the "ConfigSite.xml" on server
			Cache cache = HttpContext.Current.Cache;
		
			try
			{
				_xmlDoc = (XmlDocument)cache[pathXmlFile];
				if (_xmlDoc == null)
				{
					_xmlDoc = new XmlDocument();
					_xmlDoc.Load(pathXmlFile);  // loads "ConfigSite.xml file 
					cache.Add(pathXmlFile, _xmlDoc, new CacheDependency(pathXmlFile), DateTime.Now.AddHours(6),TimeSpan.Zero, CacheItemPriority.High, null);
				}
				XmlNode root =_xmlDoc.DocumentElement;
				return root.SelectSingleNode(path);
			}
			catch(Exception ex)
			{
				throw ex;
				
			}
		}

		public static string GetXmlValue( string path )
		{
			string xmlValue = null;
			try 
			{
				xmlValue = GetXmlNode(path).Attributes["value"].Value;
			}
			catch  
			{
				xmlValue = "";
			//	SendMail(HttpContext.Current, ex);
			}
			return xmlValue;
		} 

		public static string GetText( string text )
		{
			return GetXmlValue(String.Format("Texts/data[@name='{0}']", text));
		}

		public static string GetImage( HttpRequest req, string img )
		{
			return String.Format("{0}/Images/{1}", req.ApplicationPath, GetXmlValue(String.Format("Images/data[@name='{0}']", img)));
		} 

		public static string GetDefaultContentType( string contentGroup )
		{
			return GetXmlValue(String.Format("DefaultContentType/data[@name='{0}']", contentGroup));
		}

		public static string GetDefaultContentGroup( string contentType )
		{
			return GetXmlValue(String.Format("DefaultContentGroup/data[@name='{0}']", contentType));
		}

		public static void AddUIDatLog(HttpRequest req, HttpResponse rep)
		{
			try
			{
				string sUID = req.Headers["TM_user-id"];
				if (sUID != null && sUID != "")				
					rep.AppendToLog("uid={" + sUID + "}" );
			}
			catch{}
		}

		public static void LogUser(HttpRequest req, int idSite, string mobiletype)
		{
			try
			{
				string sUID = req.Headers["TM_user-id"]; 
				if(sUID != null && sUID != "") 
				{
					User u = new User();
					u.Visit(sUID, idSite, mobiletype);
					u = null;
				}
			}
			catch{}
		}

		public static void CallPub(HttpRequest req, string type, string format, Panel pnl)
		{
			string banner = "";
			try
			{
				string postData = string.Format("spot={0}&pub=ts&ver=1.5&u={1}&format={2}&ua={3}", type, req.Headers["TM_user-id"], format, req.UserAgent);
				HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(ConfigurationSettings.AppSettings["UrlPub"] + postData);
				myRequest.Method = "GET";
				myRequest.Timeout = Convert.ToInt32(ConfigurationSettings.AppSettings["TimeOut"]) ;
				HttpWebResponse myHttpWebResponse= (HttpWebResponse)myRequest.GetResponse();
				Stream streamResponse=myHttpWebResponse.GetResponseStream();
				StreamReader streamRead = new StreamReader( streamResponse );
				banner = streamRead.ReadToEnd();
				streamRead.Close();
				streamResponse.Close();
				myHttpWebResponse.Close();
			}
			catch{banner="";}
			
			if (banner != "") // Not TIMEOUT								
			{
				string link="", line1="", line2="", img="";
				XmlDocument _xmlBanner = new XmlDocument();	
				_xmlBanner.InnerXml = banner;
				// link
				try
				{
					link = (_xmlBanner.SelectSingleNode("ads/banner/link") != null) ?  _xmlBanner.SelectSingleNode("ads/banner/link").Attributes["href"].Value : "";
					img = (_xmlBanner.SelectSingleNode("ads/banner/img") != null) ?  _xmlBanner.SelectSingleNode("ads/banner/img").Attributes["src"].Value : "";
					line1 = (_xmlBanner.SelectSingleNode("ads/banner/line1") != null) ? _xmlBanner.SelectSingleNode("ads/banner/line1").InnerText : "";
					line2 = (_xmlBanner.SelectSingleNode("ads/banner/line2") != null) ? _xmlBanner.SelectSingleNode("ads/banner/line2").InnerText : "";
				}
				catch{}
				// IMAGE
				if (link != "" && img != "")
				{
					Image imagen = new Image();
					imagen.NavigateUrl = link;
					imagen.ImageUrl = img;
					imagen.AlternateText = "";
					imagen.Alignment = Alignment.Left;
					pnl.Controls.Add(imagen);
				}
				if (link != "" && line1 != "")
				{
					Link lnk = new Link();
					lnk.NavigateUrl = link;
					lnk.Text = line1;
					lnk.Font.Size = FontSize.Small;
					lnk.Alignment = Alignment.Left;
					pnl.Controls.Add(lnk);			
				}
				if (line2 != "")
				{
					Label lbl = new Label();
					lbl.Text = line2;
					lbl.Alignment = Alignment.Left;
					lbl.Font.Size = FontSize.Small;
					pnl.Controls.Add(lbl);	
				}
			}
		}

		public static void CallNewPubTEF(TraceContext t, HttpRequest req, Panel pnl, int slot)
		{
			string banner = "";
			string postData = "";

			try
			{
				if (!req.UserAgent.StartsWith("SIE-A"))
				{
					HttpWebRequest myRequest;

					string uid = req.Headers["TM_user-id"];
					if (uid == "" || uid == null) uid = "0";

					Object[] obj_params = {slot, req.UserAgent, GetAdServerCounter().ToString(), uid};
					postData = string.Format("ch=010&as={0}&ua={1}&ai={2}&ve=1&ui={3}&ac=2&pi=1100&pr=0104", obj_params);
					myRequest = (HttpWebRequest)WebRequest.Create(newTEFUri + postData);
				
					myRequest.Method = "GET";
					myRequest.Timeout = Convert.ToInt32(WapTools.GetXmlValue("JumpTap/Timeout")); 
					HttpWebResponse myHttpWebResponse= (HttpWebResponse)myRequest.GetResponse();
					Stream streamResponse=myHttpWebResponse.GetResponseStream();
					StreamReader streamRead = new StreamReader( streamResponse );
					banner = streamRead.ReadToEnd();
					streamRead.Close();
					streamResponse.Close();
					myHttpWebResponse.Close();
					if (Convert.ToBoolean(WapTools.GetXmlValue("JumpTap/SendEmail")))
						WapTools.SendMail(HttpContext.Current, newTEFUri + postData, banner);	
				}
			}
			catch(Exception e)
			{
				t.Write(e.ToString()); banner="";
				if (Convert.ToBoolean(WapTools.GetXmlValue("JumpTap/SendEmail")))
					WapTools.SendMail(HttpContext.Current, newTEFUri + postData, e.ToString());
			}
			if (banner != "") 								
			{
				string link="", text="";
				XmlDocument _xmlBanner = new XmlDocument();	
				_xmlBanner.InnerXml = banner;
				t.Write("Reading link and banner: " + _xmlBanner.InnerXml);
				try
				{
					link = _xmlBanner.SelectSingleNode("adresponse/ad/resource/creative_element/interaction/attribute[@type='URL']").InnerText;
					t.Write("link: " + link);
					text = _xmlBanner.SelectSingleNode("adresponse/ad/resource/creative_element/attribute[@type='adtext']").InnerText;
					t.Write("text: " + text);
				}
				catch{}
				
				if (link != "" && text != "")
				{
					Link lnk = new Link();
					lnk.NavigateUrl = link;
					lnk.Text = text;
					lnk.Font.Size = FontSize.Small;
					lnk.Alignment = Alignment.Left;
					pnl.Controls.Add(lnk);	
				}
			}
		}

		public static void CallNewPubTEF2(TraceContext t, HttpRequest req, Panel pnl, string slot)
		{
			string banner = "";
			string postData = "";

			try
			{
				if (!req.UserAgent.StartsWith("SIE-A"))
				{
					HttpWebRequest myRequest;

					string uid = req.Headers["TM_user-id"];
					if (uid == "" || uid == null) uid = "0";

					Object[] obj_params = {slot, req.UserAgent, GetAdServerCounter().ToString(), uid};
					//postData = string.Format("ch=010&as={0}&ua={1}&ai={2}&ve=1&ui={3}&ac=2&pi=1100&pr=0104", obj_params);
					postData = string.Format("ch=010&as={0}&ua={1}&ai={2}&ve=1&ui={3}&ac=2&pi=1100", obj_params);
					//myRequest = (HttpWebRequest)WebRequest.Create(newTEFUri3 + postData);
				    myRequest = (HttpWebRequest)WebRequest.Create(newTEFUri4 + postData);
					myRequest.Method = "GET";
					myRequest.Timeout = Convert.ToInt32(WapTools.GetXmlValue("JumpTap/Timeout")); 
					HttpWebResponse myHttpWebResponse= (HttpWebResponse)myRequest.GetResponse();
					Stream streamResponse=myHttpWebResponse.GetResponseStream();
					StreamReader streamRead = new StreamReader( streamResponse );
					banner = streamRead.ReadToEnd();
					streamRead.Close();
					streamResponse.Close();
					myHttpWebResponse.Close();
					if (Convert.ToBoolean(WapTools.GetXmlValue("JumpTap/SendEmail")))
						WapTools.SendMail(HttpContext.Current, newTEFUri2 + postData, banner);	
				}
			}
			catch(Exception e)
			{
				t.Write(e.ToString()); banner="";
				if (Convert.ToBoolean(WapTools.GetXmlValue("JumpTap/SendEmail")))
					WapTools.SendMail(HttpContext.Current, newTEFUri2 + postData, e.ToString());
			}
			if (banner != "") 								
			{
				string link="", text="";
				XmlDocument _xmlBanner = new XmlDocument();	
				_xmlBanner.InnerXml = banner;
				t.Write("Reading link and banner: " + _xmlBanner.InnerXml);
				try
				{
					link = _xmlBanner.SelectSingleNode("adresponse/ad/resource/creative_element/interaction/attribute[@type='URL']").InnerText;
					t.Write("link: " + link);
					text = _xmlBanner.SelectSingleNode("adresponse/ad/resource/creative_element/attribute[@type='adtext']").InnerText;
					t.Write("text: " + text);
				}
				catch{}
				
				if (link != "" && text != "")
				{
					Link lnk = new Link();
					lnk.NavigateUrl = link;
					lnk.Text = text;
					lnk.Font.Size = FontSize.Small;
					lnk.Alignment = Alignment.Left;
					pnl.Controls.Add(lnk);	
				}
			}
		}

		
		private static int GetAdServerCounter()
		{
			SqlDataReader reader = null;
			Int32 value = -1;
			
			try
			{
				reader = SqlHelper.ExecuteReader(
					Regedit.GetConnectionString( "IMODE" ),
					"getAdserverCounter"
					);

				if (reader.Read())
				{
					value = Convert.ToInt32(reader[0]);
				}
				reader.Close();
				return value;
			}
			catch (Exception caught)
			{
				throw new Exception("Unable to get user's info", caught);
			}
			finally
			{
				if (reader != null) reader.Close();
				reader = null;
			}
		}

		public static void CallNewPub(HttpRequest req, string type, string format, int slot, Panel pnl)
		{
			string banner = "";
			string postData = "";

			try
			{
				if (!req.UserAgent.StartsWith("SIE-A"))
				{
					string uid = req.Headers["TM_user-id"];
					if (uid == "" || uid == null) uid = "34000000000";
					Object[] obj_params = {req.UserAgent, DateTime.Now.AddHours(6).ToString("yyyy-MM-ddTHH:mm:ss") + ".000", uid, GetAdServerCounter().ToString(), slot};
					//postData = string.Format("pepid=emocion&adserverid=JumpTap&adultcontrol=SAFE&version=1.0&ua={0}&time={1}&uid={2}&id={3}&slot={4}&adtype=banner", obj_params);
					postData = string.Format("pepid=1100&adserverid=Amobee&adultcontrol=SAFE&version=1.0&ua={0}&time={1}&uid={2}&id={3}&slot={4}&adtype=text", obj_params);
				
					HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(TEFUri + postData);
				
					//string postData = string.Format("spot={0}&pub=ts&ver=1.5&u={1}&format={2}&ua={3}", type, req.Headers["TM_user-id"], format, req.UserAgent);
					//HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(ConfigurationSettings.AppSettings["UrlPub"] + postData);
					myRequest.Method = "GET";
					//myRequest.Timeout = 1000;
					myRequest.Timeout = Convert.ToInt32(WapTools.GetXmlValue("JumpTap/Timeout")); 
				
					HttpWebResponse myHttpWebResponse= (HttpWebResponse)myRequest.GetResponse();
					Stream streamResponse=myHttpWebResponse.GetResponseStream();
					StreamReader streamRead = new StreamReader( streamResponse );
					banner = streamRead.ReadToEnd();
					streamRead.Close();
					streamResponse.Close();
					myHttpWebResponse.Close();
					if (Convert.ToBoolean(WapTools.GetXmlValue("JumpTap/SendEmail")))
						WapTools.SendMail(HttpContext.Current, TEFUri + postData, banner);
				}
			}	
			catch(Exception e)
			{
				banner="";
				if (Convert.ToBoolean(WapTools.GetXmlValue("JumpTap/SendEmail")))
					WapTools.SendMail(HttpContext.Current, TEFUri + postData, e.ToString());
			}
			
			if (banner != "") // Not TIMEOUT								
			{
				string link="", text="";
				XmlDocument _xmlBanner = new XmlDocument();	
				_xmlBanner.InnerXml = banner;
				// link
				try
				{
					link = _xmlBanner.SelectSingleNode("adresponse/adres/interaction/locator").InnerText;
					text = _xmlBanner.SelectSingleNode("adresponse/adres/adsource/text").InnerText;
				}
				catch{}
				if (link != "" && text != "")
				{
					Link lnk = new Link();
					lnk.NavigateUrl = link;
					lnk.Text = text;
					lnk.Font.Size = FontSize.Small;
					lnk.Alignment = Alignment.Left;
					pnl.Controls.Add(lnk);			
				}
			}			
		}

		public static void TratarBanner(Panel pnl, string banner)
		{
			string link="", line1="", line2="", img="";
			XmlDocument _xmlBanner = new XmlDocument();	
			_xmlBanner.InnerXml = banner;
			// link
			try
			{
				link = (_xmlBanner.SelectSingleNode("ads/banner/link") != null) ?  _xmlBanner.SelectSingleNode("ads/banner/link").Attributes["href"].Value : "";
				img = (_xmlBanner.SelectSingleNode("ads/banner/img") != null) ?  _xmlBanner.SelectSingleNode("ads/banner/img").Attributes["src"].Value : "";
				line1 = (_xmlBanner.SelectSingleNode("ads/banner/line1") != null) ? _xmlBanner.SelectSingleNode("ads/banner/line1").InnerText : "";
				line2 = (_xmlBanner.SelectSingleNode("ads/banner/line2") != null) ? _xmlBanner.SelectSingleNode("ads/banner/line2").InnerText : "";
			}
			catch{}
			// IMAGE
			if (link != "" && img != "")
			{
				Image imagen = new Image();
				imagen.NavigateUrl = link;
				imagen.ImageUrl = img;
				imagen.AlternateText = "";
				imagen.Alignment = Alignment.Center;
				pnl.Controls.Add(imagen);
			}
			if (link != "" && line1 != "")
			{
				Link lnk = new Link();
				lnk.NavigateUrl = link;
				lnk.Text = line1;
				lnk.Font.Size = FontSize.Small;
				lnk.Alignment = Alignment.Center;
				pnl.Controls.Add(lnk);			
			}
			if (line2 != "")
			{
				Label lbl = new Label();
				lbl.Text = line2;
				lbl.Alignment = Alignment.Center;
				lbl.Font.Size = FontSize.Small;
				pnl.Controls.Add(lbl);	
			}
		}

		public static System.Web.UI.MobileControls.Link BuildLink( string text, string navigateUrl )
		{
			System.Web.UI.MobileControls.Link lnk = new System.Web.UI.MobileControls.Link();
			lnk.Font.Size = FontSize.Small;
			lnk.BreakAfter = true;
			lnk.Alignment = Alignment.Left;
			lnk.Text = text;
			lnk.SoftkeyLabel = text;
			lnk.NavigateUrl = navigateUrl;
			return lnk;
		}

		public static string FindProperty(PropertyCollection colProperty, string name)
		{
			try{return colProperty[name].Value.ToString();}
			catch{return "";}			
		}

		public static void AddPicto( MobileCaps mobile, System.Web.UI.MobileControls.Panel pnl, string imageUrl )
		{
			if (mobile.IsAdvanced) // || mobile.IsColor)
			{
				System.Web.UI.MobileControls.Image img = new System.Web.UI.MobileControls.Image();
				img.ImageUrl = imageUrl;
				img.BreakAfter = false;
				pnl.Controls.Add(img);
				img = null;
			}
		}
 
		public static string GetUrlBilling( System.Web.HttpRequest req, string contentGroup, string contentType, string referer, string version, string contentSet, int sexy, int promo )
		{
			string urlBilling; 
			if (promo == 0)
				urlBilling = String.Format("{0}{1}?ref={2}&c={3}&cs={4}&ct={5}",
					WapTools.GetXmlValue("Billing/Url"),
					WapTools.GetXmlValue("Billing/UrlBilling_Free_" + contentType),
					referer, "{1}", contentSet, "{2}"); 			
			else
				urlBilling = String.Format("{0}{1}?ref={2}&c={3}&cs={4}&ct={5}",
				(sexy > 0) ? WapTools.GetXmlValue("Billing/UrlSexy") : (contentGroup == "FLASH") ? WapTools.GetXmlValue("Billing/UrlX") : WapTools.GetXmlValue("Billing/Url"),
				(sexy > 0) ? WapTools.GetXmlValue("Billing/UrlSexyBilling_" + contentType) : WapTools.GetXmlValue("Billing/UrlBilling_" + contentType),
				referer, "{1}", contentSet, "{2}"); 
			
			return urlBilling;
		}

		public static string GetUrlBilling( System.Web.HttpRequest req, string contentGroup, string contentType, string referer, string version, string contentSet, int promo )
		{
			return GetUrlBilling(req, contentGroup, contentType, referer, version, contentSet, 0, promo);
		}

		public static string GetUrlBilling( System.Web.HttpRequest req, string contentGroup, string contentType, string referer, string version, string contentSet )
		{
			return GetUrlBilling(req, contentGroup, contentType, referer, version, contentSet, 0, -1);
		}

		public static string GetUrlBillingClub( System.Web.HttpRequest req, string contentGroup, string contentType, string referer, string version, string contentSet )
		{
			return String.Format("{0}{1}?ref={2}&c={3}&cs={4}&ct={5}", 
				WapTools.GetXmlValue("Billing/UrlClub"),
				WapTools.GetXmlValue("Billing/UrlClubBilling"),
				referer, "{1}", contentSet, "{2}");
		}	

		public static string GetUrlXBillingClub( System.Web.HttpRequest req, string contentGroup, string contentType, string referer, string version, string contentSet )
		{
			return String.Format("{0}{1}?ref={2}&ct={3}&cs={4}&c={5}", 
				WapTools.GetXmlValue("Billing/UrlXClub"),
				WapTools.GetXmlValue("Billing/UrlClubBilling"),
				referer, "{1}", contentSet, "{2}");
		}		
		

		public static string GetUrlXView( System.Web.HttpRequest req, string contentGroup, string contentType, string referer, string version, string contentSet )
		{
			return String.Format("{0}{1}?ref={2}&c={3}&cs={4}&ct={5}", 
				req.ApplicationPath,
				WapTools.GetXmlValue("Billing/UrlXView"),
				referer, "{1}", contentSet, "{2}");
		}

		public static string GetParamBack(HttpRequest req, bool decrypt)
		{
			int index = 1;
			string param = "";
			
			while( req.QueryString[String.Format("a{0}", index)] != null )
			{
				if(decrypt)
				{
					if( param == "" )
						param = String.Format("{0}={1}", req.QueryString[String.Format("a{0}", index)], req.QueryString[String.Format("a{0}", (index + 1))]);
					else
						param = String.Format("{0}&{1}={2}", param, req.QueryString[String.Format("a{0}", index)], req.QueryString[String.Format("a{0}", (index + 1))]);
				}
				else
				{
					if( param == "" )
						param = String.Format("a{0}={1}&a{2}={3}", index, req.QueryString[String.Format("a{0}", index)], index + 1, req.QueryString[String.Format("a{0}", index + 1)]);
					else
						param = String.Format("{0}&a{1}={2}&a{3}={4}", param, index, req.QueryString[String.Format("a{0}", index)], index + 1, req.QueryString[String.Format("a{0}", index + 1)]);
				}
				index = index + 2;
			}
			return param;
		}

		public static void AddLink( Panel pnl, string text, string navigateUrl, string urlPicto, MobileCaps mobile )
		{
			if( urlPicto != "" )
				AddPicto(mobile, pnl, urlPicto);

			Link lnk = BuildLink(text, navigateUrl);

			pnl.Controls.Add(lnk);
		}

		public static void AddLinkCenter( Panel pnl, string text, string navigateUrl, string urlPicto, MobileCaps mobile )
		{
			if( urlPicto != "" )
				AddPicto(mobile, pnl, urlPicto);

			Link lnk = BuildLink(text, navigateUrl);
			lnk.Alignment = Alignment.Center;

			pnl.Controls.Add(lnk);
		}

		public static void AddPictoBreak( MobileCaps mobile, System.Web.UI.MobileControls.Panel pnl, string imageUrl )
		{
			if (mobile.IsAdvanced || mobile.IsColor)
			{
				System.Web.UI.MobileControls.Image img = new System.Web.UI.MobileControls.Image();
				img.ImageUrl = imageUrl;
				img.BreakAfter = true;
				pnl.Controls.Add(img);
				img = null;
			}
		}

		public static void AddLabel( Panel pnl, string text, string urlPicto, MobileCaps mobile )
		{
			if( text != "" )
			{
				if( urlPicto != "" )
					AddPicto(mobile, pnl, urlPicto);

				Label lbl = new Label();
				lbl.Text = text;
				lbl.Alignment = Alignment.Left;
				lbl.Font.Size = FontSize.Small;

				pnl.Controls.Add(lbl);
			}
		}

		public static void AddLabelCenter( Panel pnl, string text, string urlPicto, MobileCaps mobile, BooleanOption bold)
		{
			if( text != "" )
			{
				if( urlPicto != "" )
					AddPicto(mobile, pnl, urlPicto);

				Label lbl = new Label();
				lbl.Alignment = Alignment.Center;
				lbl.Font.Bold = bold;
				lbl.Text = text;
				lbl.Alignment = Alignment.Left;
				lbl.Font.Size = FontSize.Small;

				pnl.Controls.Add(lbl);
			}
		}

		public static void AddSearchBlock( MobilePage page, Panel pnl, string searchCmd, string searchLbl, 
			string pictoSearch, string pictoCmd, string contentGroup, string contentType, MobileCaps mobile )
		{			
			page.ActiveForm.Method = FormMethod.Get;
			page.ActiveForm.Action = ConfigurationSettings.AppSettings["UrlSearch"];

			//page.ActiveForm.Action = "./default.aspx";

			//WapTextBox txtBox = new WapTextBox();
			TextBox txtBox = new TextBox();
			//txtBox.ID = "txtSearch";
			txtBox.ID = "q";

			txtBox.BreakAfter = false;

			WapCommand cmd = new WapCommand();
			cmd.ID = "cmd";
			cmd.Text = searchCmd;
			cmd.Font.Size = FontSize.Small;
			cmd.Format = CommandFormat.Link;
			cmd.CausesValidation = true;

			pnl.Controls.Add(txtBox);
			pnl.Controls.Add(cmd);
		}

		public static void AddNewSearchBlock( MobilePage page, Panel pnl, string searchCmd, string searchLbl, 
			string pictoSearch, string pictoCmd, string contentGroup, string contentType, MobileCaps mobile )
		{
			
			page.ActiveForm.Method = FormMethod.Get;
			//page.ActiveForm.Action = ConfigurationSettings.AppSettings["UrlSearch"];
			page.ActiveForm.Action = GetText("UrlSearch");
			//page.ActiveForm.Action = GetText("UrlSearchTest");
			//page.ActiveForm.Action = "./default.aspx";
			//page.HiddenVariables.Add("ct", "image");

			//WapTextBox txtBox = new WapTextBox();
			TextBox txtBox = new TextBox();
			//txtBox.ID = "txtSearch";
			txtBox.ID = "q";
			txtBox.Size = 6;
			txtBox.BreakAfter = false;

			WapCommand cmd = new WapCommand();
			cmd.ID = "cmd";
			cmd.Text = searchCmd;
			cmd.Font.Size = FontSize.Small;
			cmd.Format = CommandFormat.Link;
			cmd.CausesValidation = true;

			pnl.Controls.Add(txtBox);
			pnl.Controls.Add(cmd);
		}

		public static bool isXhtml(HttpRequest req, MobileCaps mobile)
		{
			bool correct, xswitch;
			try
			{
				xswitch = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_Xhtml"]);
				
				correct = false;
				foreach (string m in WapTools.getNodes("XhtmlConditions/Correct"))
				{
					if (mobile.MobileType == m)
					{
						correct = true;
						break;
					}
				}
				return (xswitch && correct); 
			}
			catch {return false;}
		}

		public static bool is3G(HttpRequest req, MobileCaps mobile)
		{
			bool correct;
			try
			{
			//	xswitch = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]);
				
				correct = false;
				foreach (string m in WapTools.getNodes("Conditions3G/Correct"))
				{
					if (mobile.MobileType == m)
					{
						correct = true;
						break;
					}
				}
				return (correct); 
			}
			catch {return false;}
		}

		public static bool noPreview(int idContentSet)
		{
			try
			{
				bool desc = false;
				foreach (string id in WapTools.getNodes("NoPreviews"))
				{
					if (id == idContentSet.ToString())
					{
						desc = true;
						break;
					}
				}
				return (desc); 
			}
			catch {return false;}
		}

		public static ArrayList getNodes(string node)
		{
			ArrayList h = new ArrayList();
			foreach(XmlNode n in GetXmlNode(node))
			{
				h.Add(n.Attributes["value"].Value);
			}
			return h;       
		}

		public static bool isCompatibleThemes(MobileCaps mobile)
		{
			try
			{
				bool compatible = false;
				foreach (string m in WapTools.getNodes("ThemesCompatibility"))
				{
					if (mobile.MobileType == m)
					{
						compatible = true;
						break;
					}
				}
				return (compatible); 
			}
			catch {return false;}
		}

		public static bool isHomeTop(string mobileType)
		{
			try
			{
				bool isHomeTop = false;
				foreach (string m in WapTools.getNodes("HomeTop"))
				{
					if (mobileType == m)
					{
						isHomeTop = true;
						break;
					}
				}
				return (isHomeTop); 
			}
			catch {return false;}
		}

		public static Especial getEspecial (int nbEspecial, string day)
		{
			Especial esp = new Especial();
			try
			{
				esp.name = GetXmlValue(String.Format("Especial{0}/data[@name='{1}']", nbEspecial, day));
				try
				{
					esp.filter = GetXmlNode(String.Format("Texts/data[@name='{0}']", esp.name)).Attributes["filter"].Value;
					if (esp.filter=="") esp.filter = "IMG_COLOR";
				} 
				catch{esp.filter="IMG_COLOR";}
				return esp;
			}
			catch
			{
				esp.name = "Especial" + nbEspecial;
				esp.filter = "";
				return esp;
			}			
		}
		public static Especial GetCompatibleEspecial (Especial esp)
		{
			Especial nextEsp = new Especial();
			try
			{
				string nextIndex = GetXmlNode(String.Format("Texts/data[@name='{0}']", esp.name)).Attributes["next"].Value;
				try
				{
					if(nextIndex != null && nextIndex != "")
					{	
						nextEsp.name = "Especial" + nextIndex;
						nextEsp.filter = GetXmlNode(String.Format("Texts/data[@name='{0}']", nextEsp.name)).Attributes["filter"].Value;
					}
					if (nextEsp.filter=="" || nextEsp.filter== null) nextEsp.filter = "IMG_COLOR";
				} 
				catch{ nextEsp.filter="IMG_COLOR"; }
				return nextEsp;
			}
			catch
			{
				nextEsp.name = esp.name;
				nextEsp.filter = "IMG_COLOR";
				return nextEsp;
			}		
		}
		public static bool isBranded(Content content)
		{
			string copyright = null;
			try
			{
				copyright = content.PropertyCollection["Copyright"].Value.ToString();
				bool branded = false;
				foreach (string id in WapTools.getNodes("Copyrights"))
				{
					if (id == copyright)
					{
						branded = true;
						break;
					}
				}
				return (branded); 
			}
			catch {return false;}
		}

		public static int isAlerta(int IDContentset)
		{
			int id = 0;
			try
			{
				foreach (string c in WapTools.getNodes("Alertas"))
				{
					if (c == IDContentset.ToString())
					{
						id =  Convert.ToInt32(GetXmlNode(String.Format("Alertas/ContentSet[@value='{0}']", c)).Attributes["link"].Value);
						break;
					}
				}
				return (id); 
			}
			catch {return 0;}
		}

		public static bool isSexy(int IDContentset)
		{
			try
			{
				if (Convert.ToBoolean(GetXmlValue("Home/SexyUrl")))
					foreach (string c in WapTools.getNodes("Sexy"))
						if (c == IDContentset.ToString())
							return true;
				return false;
			}
			catch {return false;}
		}

		public static void SendMail (HttpContext context, Exception error)
		{
			SendMail(context, error, true);
		}

		public static void SendMail (HttpContext context, string request, string response)
		{
			try
			{
				MailMessage mailMessage = new MailMessage();
				mailMessage.From =  "\"Im�genes y Fondos - Request AdServer\" <f.martin@ag.com>";
				mailMessage.To = "f.martin@ag.com"; // adcl_test@yahoo.es; j_arribas_df@hotmail.com"; // arribas@tid.es; amg@tid.es";
				mailMessage.Subject = "Request to ad server";
				mailMessage.BodyFormat = MailFormat.Text;
				mailMessage.BodyEncoding = Encoding.UTF8;
				mailMessage.Body = String.Format("Request: \n\n{0}\n\nResponse: \n\n{1}", request, response);
				mailMessage.Body += SysInfo(context.Request);	
				SmtpMail.SmtpServer = "127.0.0.1";
				SmtpMail.Send(mailMessage);
			}
			catch(Exception ex)
			{
				Log.LogError("Site emocion : Unexpected exception while sending mail in emocion\\wap", ex);
			}
		}

		public static int isPromo(HttpRequest req)
		{
			int promo = -1;
			try
			{
				if (Convert.ToBoolean(GetXmlValue("Promo/Enabled")))
				{
					try
					{
						Customer cst = new Customer(req);
						CommandCollection cc = cst.LoadCommands(CommandStatus.Delivered, DateTime.Now.AddDays(-1), DateTime.Now, new Guid(GetXmlValue("DisplayKey")), null);
						int num = 0;

						if (cc!= null && cc.Count>0 && cc[cc.Count-1].ServiceId != 363 && cc[cc.Count-1].ServiceId != 364 && cc[cc.Count-1].ServiceId != 365 && cc[cc.Count-1].ServiceId != 366)
						{
							foreach (AGInteractive.Business.Command c in cc)
								if (c.ServiceId != 363 && c.ServiceId != 364 && c.ServiceId != 365 && c.ServiceId != 366) num++;
						
							if (num > 0 && num % Convert.ToInt32(GetXmlValue("Promo/Num")) == 0)
								promo = 0;
							else if (num % Convert.ToInt32(GetXmlValue("Promo/Num")) == Convert.ToInt32(GetXmlValue("Promo/Num"))-1)
								promo = 1;					
							cc = null;
							cst = null;
						}
					}
					catch
					{
						return promo;
					}
				}
				return promo;
			}
			catch {return -1;}
		}
		
		public static void SendMail (HttpContext context, Exception error, bool isHandled)
		{
			try
			{
				MailMessage mailMessage = new MailMessage();
				mailMessage.From =  "\"Kiwee Telecom\" <kiweetelecom@ag.com>";
				mailMessage.To = "f.martin@ag.com";
				mailMessage.Subject = "emocion.kiwee.com crashed";
				mailMessage.BodyFormat = MailFormat.Text;
				mailMessage.BodyEncoding = Encoding.UTF8;
				mailMessage.Body = String.Format("{0} exception in {1}\nError : {2}\n", isHandled ? "Handled" : "Unhandled", Environment.MachineName, error.ToString());
				//	for (int i=0; i < v.Count; i++)
				mailMessage.Body += SysInfo(context.Request);	
				mailMessage.Body += AssemblyInfo(error);
				mailMessage.Body += EnhancedStackTrace(error);
				mailMessage.Body += GetASPSettings(context);
				SmtpMail.SmtpServer = "127.0.0.1";
				SmtpMail.Send(mailMessage);
			}
			catch(Exception ex)
			{
				Log.LogError("Site emocion : Unexpected exception while sending mail in emocion\\wap", ex);
			}
		}
		
		public static void SendMail (HttpContext context, string exception)
		{
			try
			{
				MailMessage mailMessage = new MailMessage();
				mailMessage.From =  "\"Kiwee Telecom\" <kiweetelecom@ag.com>";
				mailMessage.To = "f.martin@ag.com";
				mailMessage.Subject = "emocion.kiwee.com crashed";
				mailMessage.BodyFormat = MailFormat.Text;
				mailMessage.Priority = MailPriority.High;
				mailMessage.BodyEncoding = Encoding.UTF8;
				mailMessage.Body = String.Format("{0} exception in {1}\nError : {2}\n", "severe", Environment.MachineName, exception);
				//	for (int i=0; i < v.Count; i++)
				mailMessage.Body += SysInfo(context.Request);	
				mailMessage.Body += AssemblyInfo(null);
				mailMessage.Body += EnhancedStackTrace((Exception)null);
				mailMessage.Body += GetASPSettings(context);
				SmtpMail.SmtpServer = "127.0.0.1";
				SmtpMail.Send(mailMessage);
			}
			catch(Exception ex)
			{
				Log.LogError("Site emocion : Unexpected exception while sending mail in emocion\\xhtml", ex);
			}
		}

		public static string GetASPSettings(HttpContext context)
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append("--- ASP.NET Collections --");
			sb.Append(Environment.NewLine);
			sb.Append(Environment.NewLine);

			sb.Append(HttpVarsToString(context.Request.QueryString, "QueryString"));
			sb.Append(HttpVarsToString(context.Request.QueryString, "Form"));
			sb.Append(HttpVarsToString(context.Request.Cookies));
			sb.Append(HttpVarsToString(context.Session));
			sb.Append(HttpVarsToString(context.Cache));
			sb.Append(HttpVarsToString(context.Application));
			sb.Append(HttpVarsToString(context.Request.ServerVariables, "ServerVariables"));
			
			return sb.ToString();
		}

		public static string HttpVarsToString(HttpCookieCollection cookies)
		{
			if(cookies == null) return "";
			if(cookies.Count == 0) return "";
			
			StringBuilder sb = new StringBuilder();
			
			sb.Append("Cookies");
			sb.Append(Environment.NewLine);
			sb.Append(Environment.NewLine);
			try
			{
				for(int i = 0; i < cookies.Count; i++)
					AppendLine(sb, cookies[i].Name.ToString(), cookies[i].Value.ToString());
			}
			catch{ return "";}
			
			sb.Append(Environment.NewLine);
			return sb.ToString();
		}
		
		public static string HttpVarsToString(HttpApplicationState application)
		{
			if(application == null) return "";
			if(application.Count == 0) return "";
			
			StringBuilder sb = new StringBuilder();
			
			sb.Append("Application");
			sb.Append(Environment.NewLine);
			sb.Append(Environment.NewLine);
			try
			{
				for(int i = 0; i < application.Count; i++)
					AppendLine(sb, application.Keys[i].ToString(), application[i].ToString());
			}
			catch{ return "";}
			
			sb.Append(Environment.NewLine);
			return sb.ToString();
		}
		
		public static string HttpVarsToString(Cache cache)
		{
			if(cache == null) return "";
			if(cache.Count == 0) return "";
			
			StringBuilder sb = new StringBuilder();
			
			sb.Append("Cache");
			sb.Append(Environment.NewLine);
			sb.Append(Environment.NewLine);
			try
			{
				foreach(DictionaryEntry de in cache)
					AppendLine(sb, de.Key.ToString(), de.Value.ToString());
			}
			catch{	return "";}
			
			sb.Append(Environment.NewLine);
			return sb.ToString();
		}

		public static string HttpVarsToString(HttpSessionState session)
		{
			if(session == null) return "";
			if(session.Count == 0) return "";
			
			StringBuilder sb = new StringBuilder();
			
			sb.Append("Session");
			sb.Append(Environment.NewLine);
			sb.Append(Environment.NewLine);
			try
			{
				for(int i = 0; i < session.Count; i++)
					AppendLine(sb, session.Keys[i].ToString(), session[i].ToString());
			}
			catch{ return "";}
			
			sb.Append(Environment.NewLine);
			return sb.ToString();
		}

		public static string HttpVarsToString(NameValueCollection nvc, string title)
		{
			if(nvc == null) return "";
			if(! nvc.HasKeys()) return "";
			
			StringBuilder sb = new StringBuilder();
			
			sb.Append(title);
			sb.Append(Environment.NewLine);
			sb.Append(Environment.NewLine);
			try
			{
				for(int i = 0; i < nvc.Count; i++)
					AppendLine(sb, nvc.GetKey(i).ToString(), nvc.Get(i).ToString());
			}
			catch{ return "";}
			
			sb.Append(Environment.NewLine);
			return sb.ToString();
		}
		
		public static string SysInfo(HttpRequest request)
		{	
			StringBuilder sb = new StringBuilder();
			
			try
			{
				sb.AppendFormat("{0, -30}{1}", "URL:	", WebCurrentUrl(request));
				sb.Append(Environment.NewLine);
				sb.AppendFormat("{0, -30}{1}", "User-Agent:	", request.UserAgent.ToString());
				sb.Append(Environment.NewLine);
				sb.AppendFormat("{0, -30}{1}", "Date and Time:	", DateTime.Now.ToString());
				sb.Append(Environment.NewLine);
				sb.AppendFormat("{0, -30}{1}", ".NET RunTime Version:	", Environment.Version.ToString());
				sb.Append(Environment.NewLine);
				sb.AppendFormat("{0, -30}{1}", "Application Domain:	", AppDomain.CurrentDomain.FriendlyName);
				sb.Append(Environment.NewLine);
			}
			catch
			{ return "";}
			sb.Append(Environment.NewLine);
			return sb.ToString();
		}
		
		public static string WebCurrentUrl(HttpRequest request)
		{	
			StringBuilder sb = new StringBuilder();
			
			try
			{
				sb.Append("http://"+request.ServerVariables["server_name"]);
				if(request.ServerVariables["server_port"] != "80")
					sb.Append(":"+request.ServerVariables["server_port"]);
				sb.Append(request.ServerVariables["url"]);
				if(request.ServerVariables["query_string"].Length > 0)
					sb.Append("?"+request.ServerVariables["query_string"]);

			}
			catch{ return "";}
			
			return sb.ToString();
		}
		public static string AssemblyInfo(Exception ex)
		{	
			try
			{
				Assembly a = GetAssemblyFromName(ex.Source);	
				if(a != null)
					return AssemblyDetails(a);
				else 
					return AllAssemblyDetails();
			}
			catch{ return "";}	
		}
		
		public static Assembly GetAssemblyFromName(string  assemblyName)
		{	
			foreach(Assembly a in AppDomain.CurrentDomain.GetAssemblies())
			{
				if(a.GetName().Name == assemblyName)
					return a;
			}
			return null;
		}
		
		public static string AssemblyDetails(Assembly a)
		{	
			StringBuilder sb = new StringBuilder();
			
			try
			{
				sb.AppendFormat("{0, -30}{1}", "Assembly CodeBase:	", a.CodeBase.Replace("file:///", ""));
				sb.Append(Environment.NewLine);
				sb.AppendFormat("{0, -30}{1}", "Assembly Full Name:	", a.FullName);
				sb.Append(Environment.NewLine);
				sb.AppendFormat("{0, -30}{1}", "Assembly Version:	", a.GetName().Version.ToString());
				sb.Append(Environment.NewLine);
				sb.AppendFormat("{0, -30}{1}", "Assembly BuildDate:	", AssemblyBuildDate(a).ToString());
				sb.Append(Environment.NewLine);
			}
			catch{ return "";}
			sb.Append(Environment.NewLine);
			return sb.ToString();
		}

		public static string AllAssemblyDetails()
		{	
			StringBuilder sb = new StringBuilder();
			
			try
			{
				foreach(Assembly a in AppDomain.CurrentDomain.GetAssemblies())
				{
					sb.AppendFormat("{0, -30}{1}", "Assembly CodeBase:	", a.CodeBase.Replace("file:///", ""));
					sb.Append(Environment.NewLine);
					sb.AppendFormat("{0, -30}{1}", "Assembly Full Name:	", a.FullName);
					sb.Append(Environment.NewLine);
					sb.AppendFormat("{0, -30}{1}", "Assembly Version:	", a.GetName().Version.ToString());
					sb.Append(Environment.NewLine);
					sb.AppendFormat("{0, -30}{1}", "Assembly BuildDate:	", AssemblyBuildDate(a).ToString());
					sb.Append(Environment.NewLine);
				}
				
			}
			catch{ return "";}
			sb.Append(Environment.NewLine);
			return sb.ToString();
		}

		public static DateTime AssemblyBuildDate(Assembly a)
		{	
			try
			{
				return File.GetLastWriteTime(a.Location);
			}
			catch{ return DateTime.MaxValue;}
		}
		
		public static string EnhancedStackTrace(Exception ex)
		{
			if(ex == null) return "";
			return EnhancedStackTrace(new StackTrace(ex, true));
		}

		public static string EnhancedStackTrace(StackTrace st)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(Environment.NewLine);
			sb.Append("--- Stack Trace ---");
			sb.Append(Environment.NewLine);
			
			try
			{
				for(int frame = 0; frame < st.FrameCount; frame++)
				{
					StackFrame sf = st.GetFrame(frame);
					sb.Append(StackFrameToString(sf));
				}
			}
			catch{ return "";}
			sb.Append(Environment.NewLine);
			return sb.ToString();
		}

		public static string StackFrameToString(StackFrame sf)
		{
			StringBuilder sb = new StringBuilder();
			int param = 0;
			MemberInfo mi = sf.GetMethod();
			sb.Append("	");
			sb.Append(mi.DeclaringType.Namespace);
			sb.Append(".");
			sb.Append(mi.DeclaringType.Name);
			sb.Append(".");
			sb.Append(mi.Name);
			sb.Append("(");
			
			foreach(ParameterInfo parameter in sf.GetMethod().GetParameters())
			{
				param += 1;
				if(param > 1) sb.Append(", ");
				sb.Append(parameter.ParameterType.Name +" ");
				sb.Append(parameter.Name);
			}
			
			sb.Append(")");
			sb.Append(Environment.NewLine);

			sb.Append("		");
			if((sf.GetFileName() == null) || (sf.GetFileName().Length == 0))
			{
				sb.Append("(Unknown File)");
				sb.Append(": N ");
				sb.Append(String.Format("{0:#0000}", sf.GetNativeOffset()));
			}
			else
			{
				sb.Append(Path.GetFileName(sf.GetFileName()));
				sb.Append(": line ");
				sb.Append(String.Format("{0:#0000}", sf.GetFileLineNumber()));
				sb.Append(", column ");
				sb.Append(String.Format("{0:#00}", sf.GetFileColumnNumber()));
			}
			sb.Append(Environment.NewLine);
			return sb.ToString();
		}

		public static void AppendLine(StringBuilder sb, string key, string value)
		{
			if(value == null) return;
			sb.Append(String.Format("	{0, -30}{1}", key, value));
			sb.Append(Environment.NewLine);
		}

		#region ContentGroups
		public class xContentGroup
		{
			string name, ct;
			int rows, cols;
			bool free;
			public string Name
			{
				get{ return name; }
				set{ name = value; }
			}
			public string ContentType
			{
				get{ return ct; }
				set{ ct = value; }
			}
			public int  nbRows
			{
				get{ return rows; }
				set{ rows = value; }
			}
			public int nbCols
			{
				get{ return cols; }
				set{ cols = value; }
			}
			public bool Free
			{
				get{ return free; }
				set{ free = value; }
			}
		}

		public class xContentGroupGraphic: xContentGroup
		{
			public xContentGroupGraphic()
			{
				this.nbCols = 2;
				this.nbRows = 4;
				this.Free = false;
			}
		}

		public class xContentGroupSound: xContentGroup
		{
			public xContentGroupSound()
			{
				this.nbCols = 1;
				this.nbRows = 10;
				this.Free = false;
			}
		}

          
		public class IMG : xContentGroupGraphic
		{
			public IMG()
			{
				this.Name = "IMG";
				this.ContentType = "IMG_COLOR";
			}
		}

		public class ANIM : xContentGroupGraphic
		{
			public ANIM()
			{
				this.Name = "ANIM";
				this.ContentType = "ANIM_COLOR";
			}
		}
		public class VIDEO : xContentGroupGraphic
		{
			public VIDEO()
			{
				this.Name = "VIDEO";
				this.ContentType = "VIDEO_DWL";
			}
		}

		public class GAME : xContentGroupGraphic
		{
			public GAME()
			{
				this.Name = "GAME";
				this.ContentType = "GAME_JAVA";
			}
		}

		public class SOUND: xContentGroupGraphic
		{
			public SOUND()
			{
				this.Name = "SOUND";
				this.ContentType = "SOUND_POLY";
			}
		}
		public class SFX: xContentGroupGraphic
		{
			public SFX()
			{
				this.Name = "SFX";
				this.ContentType = "SOUND_FX";
			}
		}
		public class COMPOSITE: xContentGroupGraphic
		{
			public COMPOSITE()
			{
				this.Name = "COMPOSITE";
				this.ContentType = "";
			}
		}
		public class contentgroupFactory
		{
			public xContentGroup Create(string cg)
			{
				switch(cg)
				{
					case "IMG":
					case "IMG_COLOR":
						return new IMG();
					case "ANIM":
					case "ANIM_COLOR":
						return new ANIM();
					case "VIDEO":
					case "VIDEO_DWL":
						return new VIDEO();
					case "GAME":
					case "GAME_JAVA":
						return new GAME();
					case "SOUND":
					case "SOUND_POLY":
						return new SOUND();
					case "SFX":
					case "SOUND_FX":
						return new SFX();
					case "COMPOSITE":
						return new COMPOSITE();
					default:
						return null;
				}
			}
		}

		#endregion
	
		public enum SearchType
		{
			ts_ImagenesFondos_text_top, 
			ts_ImagenesFondos_banner_top
		}
	}
}
