using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using KMobile.Catalog.Services;

namespace wap.Tools
{
	public class getImage : System.Web.UI.Page
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			MobileCaps _mobile = (MobileCaps)Request.Browser;

			string contentName = Request.QueryString["t"];
			
			ImgDisplayInstructions _imgDisplayInst = new ImgDisplayInstructions(_mobile);
			_imgDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "bullet");
			if (_mobile.MobileDeviceManufacturer == "NOKIA")
				_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue("Url_Galeria3_IMG");
			else
			{
				if (_mobile.ScreenPixelsWidth > 176) _imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue("Url_Galeria_IMG");
				else if (_mobile.ScreenPixelsWidth > 101) _imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue("Url_Galeria2_IMG");		
			}
			//	_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue("Url_Galeria2_IMG");		

			int weight = 10000;
			long quality = weight >= 0 ? 100L : 70L;

			EncoderParameters parameters = null;
			ImageCodecInfo jpgCodec = null;
			ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
			for (int i = 0; i < codecs.Length; i++)
				if (codecs[i].MimeType == "image/jpeg")
				{
					jpgCodec = codecs[i];
					break;
				}

			WebRequest webrq = WebRequest.Create(String.Format(_imgDisplayInst.PreviewMaskUrl, contentName.Substring(0, 1), contentName));
			Bitmap bmp = (Bitmap)Bitmap.FromStream(webrq.GetResponse().GetResponseStream());
			bmp = ResizeImg(bmp, Convert.ToInt32(_mobile.ScreenPixelsWidth - (_mobile.ScreenPixelsWidth * 0.2)), Convert.ToInt32(_mobile.ScreenPixelsHeight - (_mobile.ScreenPixelsHeight * 0.2)));

			parameters = new EncoderParameters();
			parameters.Param = new EncoderParameter[1];
			MemoryStream st = new MemoryStream();
			do
			{
				st = new MemoryStream();
				Response.Clear();
				//save bmp to JPEG format				
				parameters.Param[0] = new EncoderParameter(Encoder.Quality, quality);
			
				bmp.Save(st, jpgCodec, parameters);
				//lower quality
				quality -= 5L;
			} while (st.Length > weight && quality > 30L && weight > 0);
			
			// make byte array the same size as the image
			byte[] imageContent = new Byte[st.Length];

			// rewind the memory stream
			st.Position = 0;

			// load the byte array with the image
			st.Read(imageContent, 0, (int)st.Length);
			Response.ContentType = "image/jpeg";
			Response.BinaryWrite(imageContent);
		}

		protected virtual Bitmap ResizeImg( Bitmap source, int width, int height )
		{
			Graphics gc = null;
			Bitmap resultImg = null;
			Bitmap bufferedImg = null;
			
			double srcRatio;
			double destRatio;
			double zoomFactor;

			try
			{
				//init ratios
				srcRatio = (double)source.Width / (double)source.Height;
				destRatio = (double)width / (double)height;

				//init zoom factor
				if( srcRatio > destRatio )
					zoomFactor = (double)height / (double)source.Height;
				else
					zoomFactor = (double)width / (double)source.Width;
				
				//resize bmp
				bufferedImg = new Bitmap(
					source, 
					(int)(source.Width * zoomFactor),
					(int)(source.Height * zoomFactor) );

				//init final resiezd bmp
				resultImg = new Bitmap( width, height, PixelFormat.Format32bppArgb );
				gc = Graphics.FromImage( resultImg );
				
				//crop bmp
				gc.DrawImage(
					bufferedImg,
					new Rectangle( 0, 0, resultImg.Width, resultImg.Height ),
					new Rectangle(
					( bufferedImg.Width - resultImg.Width ) / 2,
					( bufferedImg.Height - resultImg.Height ) / 2,
					resultImg.Width,	
					resultImg.Height ),
					GraphicsUnit.Pixel );

				return resultImg;
			}
			catch{}
			finally
			{
				if( bufferedImg != null ) bufferedImg.Dispose();
				bufferedImg = null;
				if( gc != null ) gc.Dispose();
				gc = null;
				resultImg = null;
			}
			return source;
		}




		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
