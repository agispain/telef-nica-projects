using System;
using System.Web;
using KMobile.Catalog.Services;
using wap.Tools;
using System.Web.UI.MobileControls;

namespace wap 
{
	public class instruct : CatalogBrowsing 
	{
		protected Panel pnlText;
		protected Image imgLogo;
		
		private void Page_Load(object sender, System.EventArgs e)
		{ 
			try 
			{ 
				_mobile = (MobileCaps)Request.Browser; 
				
				if (_mobile.MobileType != null &&  _mobile.IsCompatible("IMG_COLOR"))
				{      
					try{WapTools.AddUIDatLog(Request, Response);}
					catch{}      
						
					if (_mobile.IsAdvanced)
						imgLogo.ImageUrl = String.Format(WapTools.GetImage(this.Request, "bannervip"), WapTools.GetFolderImg(_mobile));

					WapTools.AddLabelCenter(pnlText, "� Ap�ntate a la Zona Vip y participa al Sorteo de 2 Nokia 6110 NAVIGATOR !", "", _mobile, BooleanOption.True);
					WapTools.AddLabel(pnlText, "MODO DE PARTICIPACION", "", _mobile);
					WapTools.AddLabel(pnlText, "Participar�n en el sorteo todos aquellos usuarios que se subscriban al Club de Fondos ZONA VIP (precio subscripci�n 4 euros, 4,64 con iva.) del sitio de im�genes de MoviStar emoci�n durante el periodo de la promoci�n. La participaci�n en esta acci�n promocional es gratuita, no implica para el consumidor incremento alguno en el precio de los productos adquiridos, salvo el el precio habitual de mercado del servicio prestado. Con cada suscripci�n el titular de la l�nea telef�nica obtendr� una participaci�n para el sorteo.", "", _mobile);
					WapTools.AddLabel(pnlText, "PREMIOS", "", _mobile);
					WapTools.AddLabel(pnlText, "2 M�viles Nokia 6110 NAVIGATOR. Bandas de operaci�n : HSDPA / 3G. Otras caracteristicas: GPS incorporado, 101 x 49 x 20 mm, 120 gr, Cliente Correo Electr�nico POP3, SMTP, IMAP4 Camara 2 MP, v�deo, mp3. La empresa AG Interactive Ib�rica, S.L., empresa responsable de esta promoci�n, ser� la encargada de realizar el sorteo y entregar los premios a los ganadores.", "", _mobile);
					WapTools.AddLabel(pnlText, "PERIODO DE PROMOCION", "", _mobile);
					WapTools.AddLabel(pnlText, "Fecha de Inicio: mi�rcoles 25 Septiembre 2008. Fecha de Fin: s�bado 1 de Noviembre 2008", "", _mobile);
					WapTools.AddLabel(pnlText, "FINALIDAD", "", _mobile);
					WapTools.AddLabel(pnlText, "La finalidad de la promoci�n es la potenciaci�n de la suscripci�n al Club de Fondos denominado Zona Vip, as� como la fidelizaci�n y recompensa de los usuarios de emoci�n, mediante una promoci�n que se celebrar� en todo el territorio nacional de Espa�a desde el 25 Septiembre 2008 al 1 de Noviembre de 2008 ambos inclusive.", "", _mobile);
					WapTools.AddLabel(pnlText, "GANADORES", "", _mobile);
					WapTools.AddLabel(pnlText, "El sorteo se llevar� a cabo mediante un programa inform�tico \"Instant Win\" que de forma aleatoria seleccionar� dos momentos ganadores durante el periodo promocional. A cada usuario que participe en la promoci�n  se le notificar� inmediatamente al pago del servicio si ha sido afortunado o no mediante una pantalla informativa. Al ganador le aparecer� un mensaje personalizado con un n�mero de tel�fono de contacto para tramitar el premio, asi como una clave de identificaci�n. Quedan excluidos de esta promoci�n expresamente los empleados de Telefonica Moviles S.A.U  y AG Interactive Ib�rica, S.L. AG Interactive Ib�rica, S.L.,se reserva el derecho de interpretaci�n de las presentes bases en caso de reclamaci�n", "", _mobile);
						
					WapTools.AddLink(pnlText, "Ap�ntate a la Zona Vip haciendo clic aqu�", "linkto.aspx?id=11036", WapTools.GetImage(this.Request, "club"), _mobile);
					WapTools.AddLink(pnlText, "Volver", "club2.aspx", WapTools.GetImage(this.Request, "club"), _mobile);
				}
				else
					WapTools.AddLabel(pnlText, WapTools.GetText("Compatibility"), "", _mobile);
				WapTools.AddLinkCenter(pnlText, WapTools.GetText("ImagenesFondos"), "./default.aspx", "", _mobile);

				
			}		
			catch(Exception caught)
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format(" Unexpected exception in emocion\\wap\\instruct.aspx - UA : {0}", Request.UserAgent), caught);
				this.RedirectToMobilePage("./error.aspx");				
			}
		}


		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
