<%@ Page language="c#" Codebehind="club2.aspx.cs" Inherits="wap.club2" AutoEventWireup="false" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<body Xmlns:mobile="http://schemas.microsoft.com/Mobile/WebForm">
	<mobile:form id="frmDefault" runat="server" EnableViewState="false" Title="Zona VIP">
		<mobile:Panel id="pnlPreview" runat="server">
			<mobile:Image id="imgLogo" Runat="server" Alignment="Center"></mobile:Image>
		</mobile:Panel>
		<mobile:Panel id="pnlText" runat="server"></mobile:Panel>
	</mobile:form>
</body>
