using System;
using System.Web.UI.MobileControls;
using KMobile.Catalog.Services;
using wap.Tools;

namespace wap
{
	public class shops : System.Web.UI.MobileControls.MobilePage
	{
		protected System.Web.UI.MobileControls.Form Form1;
		protected Panel pnlShops, pnlEnd;
		protected System.Web.UI.MobileControls.Panel pnlPreview;
		protected Image imgLogo;

		private void Page_Load(object sender, System.EventArgs e)
		{
			MobileCaps _mobile = (MobileCaps)Request.Browser; 
			int idShop = (Request.QueryString["id"] != null) ? Convert.ToInt32(Request.QueryString["id"]) : 0;
			if(idShop == 27)
			{
				if (_mobile.IsAdvanced)
					imgLogo.ImageUrl = String.Format(WapTools.GetImage(this.Request, "imagenes"), WapTools.GetFolderImg(_mobile));
				WapTools.AddLink(pnlShops, WapTools.GetText("AnimaNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/ANIMANOMBRES")), WapTools.GetImage(this.Request, "bullet"), _mobile);
				
				if (_mobile.IsCompatible("VIDEO_DWL"))
				{
					WapTools.AddLink(pnlShops, WapTools.GetText("VideoNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEONOMBRES")), WapTools.GetImage(this.Request, "bullet"), _mobile);
					WapTools.AddLink(pnlShops, WapTools.GetText("VideoAnimaciones"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEOANIMACIONES")), WapTools.GetImage(this.Request, "bullet"), _mobile);
				}
			}
			else
			{
				if (_mobile.IsAdvanced)
					imgLogo.ImageUrl = String.Format(WapTools.GetImage(this.Request, "portales"), WapTools.GetFolderImg(_mobile));
			
				#region PUB
//				if (WapTools.isTestSite(this.Request))
				WapTools.CallNewPubTEF2(this.Trace, this.Request, pnlPreview, "183955");
				//WapTools.CallNewPubTEF(this.Trace, this.Request, pnlShops, 1189);
//				else
//					WapTools.CallNewPub(this.Request, WapTools.SearchType.ts_ImagenesFondos_text_top.ToString(), "xml" , 1189, pnlShops); // ts_text, ts_mixed, ts_imag
				#endregion
				//WapTools.AddLink(pnlShops, WapTools.GetText("Shop30"), "./linkto.aspx?id=30", WapTools.GetImage(this.Request, "bullet"), _mobile);
				//WapTools.AddLink(pnlShops, WapTools.GetText("Shop4"), "./linkto.aspx?id=4", WapTools.GetImage(this.Request, "bullet"), _mobile);
				WapTools.AddLink(pnlShops, WapTools.GetText("Shop1"), "./linkto.aspx?id=1", WapTools.GetImage(this.Request, "bullet"), _mobile);
				WapTools.AddLink(pnlShops, WapTools.GetText("Shop4"), "./linkto.aspx?id=4", WapTools.GetImage(this.Request, "bullet"), _mobile);
				WapTools.AddLink(pnlShops, WapTools.GetText("Shop7"), "./linkto.aspx?id=7", WapTools.GetImage(this.Request, "bullet"), _mobile);
				//WapTools.AddLink(pnlShops, WapTools.GetText("Shop2"), "./linkto.aspx?id=2", WapTools.GetImage(this.Request, "bullet"), _mobile);
				WapTools.AddLink(pnlShops, WapTools.GetText("Shop10"), "./linkto.aspx?id=10", WapTools.GetImage(this.Request, "bullet"), _mobile);
				WapTools.AddLink(pnlShops, WapTools.GetText("Shop12"), "./linkto.aspx?id=12", WapTools.GetImage(this.Request, "bullet"), _mobile);
				//WapTools.AddLink(pnlShops, WapTools.GetText("Shop8"), "./linkto.aspx?id=8", WapTools.GetImage(this.Request, "bullet"), _mobile);
				WapTools.AddLink(pnlShops, WapTools.GetText("Shop3"), "./linkto.aspx?id=3", WapTools.GetImage(this.Request, "bullet"), _mobile);
				//WapTools.AddLink(pnlShops, WapTools.GetText("Shop5"), "./linkto.aspx?id=5", WapTools.GetImage(this.Request, "bullet"), _mobile);
				WapTools.AddLink(pnlShops, WapTools.GetText("Shop9"), "./linkto.aspx?id=9", WapTools.GetImage(this.Request, "bullet"), _mobile);
				//	WapTools.AddLink(pnlShops, WapTools.GetText("Shop13"), "./linkto.aspx?id=13", WapTools.GetImage(this.Request, "bullet"), _mobile);
				//	WapTools.AddLink(pnlShops, WapTools.GetText("Shop16"), "./linkto.aspx?id=16", WapTools.GetImage(this.Request, "bullet"), _mobile);
				//	WapTools.AddLink(pnlShops, WapTools.GetText("Shop15"), "./linkto.aspx?id=15", WapTools.GetImage(this.Request, "bullet"), _mobile);
				WapTools.AddLink(pnlShops, WapTools.GetText("Shop17"), "./linkto.aspx?id=17", WapTools.GetImage(this.Request, "bullet"), _mobile);
				#region PUB2
//				if (WapTools.isTestSite(this.Request))
				WapTools.CallNewPubTEF2(this.Trace, this.Request, pnlShops, "183953");
				//WapTools.CallNewPubTEF(this.Trace, this.Request, pnlShops, 1652);
//				else
//					WapTools.CallNewPub(this.Request, WapTools.SearchType.ts_ImagenesFondos_text_top.ToString(), "xml" , 1652, pnlShops); // ts_text, ts_mixed, ts_imag
				#endregion
				
			}
			WapTools.AddLink(pnlEnd, "Buscar", "http://10.132.67.244/buscador2/searcher.initsearch.do",WapTools.GetImage(this.Request, "buscar"), _mobile);
			WapTools.AddLink(pnlEnd, "Home", "http://wap.movistar.com", WapTools.GetImage(this.Request, "home"), _mobile);
			WapTools.AddLink(pnlEnd, "Atr�s", "default.aspx", WapTools.GetImage(this.Request, "back"), _mobile);
			WapTools.AddLink(pnlEnd, "Arriba", "shops.aspx", WapTools.GetImage(this.Request, "up"), _mobile);
			WapTools.AddLinkCenter(pnlShops, WapTools.GetText("ImagenesFondos"), "./default.aspx", "", _mobile);
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
