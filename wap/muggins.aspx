<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Page language="c#" Codebehind="muggins.aspx.cs" Inherits="wap.avatars" AutoEventWireup="false" %>
<body Xmlns:mobile="http://schemas.microsoft.com/Mobile/WebForm">
	<mobile:form id="frmDefault" runat="server" EnableViewState="false" Title="Crea tu AVATAR">
		<mobile:Panel id="pnlPreview" runat="server">
			<mobile:Image id="imgLogo" Alignment="Center" Runat="server"></mobile:Image>
		</mobile:Panel>
		<mobile:Panel id="pnlText" runat="server"></mobile:Panel>
	</mobile:form>
</body>
