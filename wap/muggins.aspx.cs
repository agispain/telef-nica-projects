using System;
using System.Configuration;
using System.Web;
using KMobile.Catalog.Services;
using wap.Tools;
using System.Web.UI.MobileControls;

namespace wap 
{
	public class avatars : CatalogBrowsing 
	{
		protected Panel pnlText;
		protected System.Web.UI.MobileControls.Panel pnlPreview;
		protected System.Web.UI.MobileControls.Form frmDefault;
		protected Image imgLogo;
		bool is3g = false;
		
		private void Page_Load(object sender, System.EventArgs e)
		{ 
			try 
			{ 
				_mobile = (MobileCaps)Request.Browser; 
			
				try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(this.Request, _mobile));}
				catch{is3g = false;}
				
				if (_mobile.IsXHTML || WapTools.isXhtml(this.Request, _mobile) || is3g)
				{
					try{Response.Redirect("http://emocion.kiwee.com/xhtml/muggins.aspx", false);}
					catch{Response.Redirect("http://emocion.kiwee.com/xhtml/muggins.aspx", true);}
				} 
				
				if (_mobile.MobileType != null &&  _mobile.IsCompatible("IMG_COLOR"))
				{      
					try{WapTools.AddUIDatLog(Request, Response);}
					catch{}      
						
					if (_mobile.IsAdvanced)
						imgLogo.ImageUrl = String.Format(WapTools.GetImage(this.Request, "muggins"), WapTools.GetFolderImg(_mobile));

					WapTools.AddLink(pnlText, "Crea tu AVATAR - Descarga ILIMITADA", "muggins/catalog.aspx", WapTools.GetImage(this.Request, "club"), _mobile);
					WapTools.AddLabelCenter(pnlText, "�Bienvenido al Incre�ble Mundo de los Avatares de Movistar Emoci�n!", "", _mobile, BooleanOption.True);
					WapTools.AddLabel(pnlText, "Seguro que tienes ganas de renovar tu avatar... �Qu� prefieres ser hoy? Cambia tu actitud, el color de tu ropa, tus zapatillas... Y podr�s hacerte todos los que quieras..." , "", _mobile);
					
					WapTools.AddLink(pnlText, "Crea tu AVATAR - Descarga ILIMITADA", "muggins/catalog.aspx", WapTools.GetImage(this.Request, "club"), _mobile);
				}
				else
					WapTools.AddLabel(pnlText, WapTools.GetText("Compatibility"), "", _mobile);
				WapTools.AddLinkCenter(pnlText, WapTools.GetText("ImagenesFondos"), "./default.aspx", "", _mobile);

				
			}		
			catch(Exception caught)
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format(" Unexpected exception in emocion\\wap\\instruct.aspx - UA : {0}", Request.UserAgent), caught);
				this.RedirectToMobilePage("./error.aspx");				
			}
		}


		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
