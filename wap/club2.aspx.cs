using System;
using System.Configuration;
using System.Web;
using KMobile.Catalog.Services;
using wap.Tools;
using System.Web.UI.MobileControls;

namespace wap 
{
	public class club2 : CatalogBrowsing 
	{
		protected Panel pnlText;
		protected Image imgLogo;
		protected bool is3g = false;
		protected System.Web.UI.MobileControls.Panel pnlPreview;
		protected System.Web.UI.MobileControls.Form frmDefault;
		public int promo = -1;
		
		private void Page_Load(object sender, System.EventArgs e)
		{ 
			try 
			{ 
				_mobile = (MobileCaps)Request.Browser; 
				try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(this.Request, _mobile));}
				catch{is3g = false;}
				//WapTools.LogUser(this.Request, 109, _mobile.MobileType);						
				
				// redirection xhtml
				if (Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_Xhtml"]) && (_mobile.IsXHTML || WapTools.isXhtml(this.Request, _mobile) || is3g))
				{
					try{if (WapTools.isTestSite(this.Request)) Response.Redirect("http://emocion.dev.kiwee.com/xhtml_v6/club2.aspx", false); else Response.Redirect("http://emocion.kiwee.com/xhtml/club2.aspx", false);}
					catch{if (WapTools.isTestSite(this.Request)) Response.Redirect("http://emocion.dev.kiwee.com/xhtml_v6/club2.aspx", true); else Response.Redirect("http://emocion.kiwee.com/xhtml/club2.aspx", true);}
				} 
				else 
				{
					if (_mobile.MobileType != null &&  _mobile.IsCompatible("IMG_COLOR"))
					{      
						try{WapTools.AddUIDatLog(Request, Response);}
						catch{}      
						
						if (_mobile.IsAdvanced)
							imgLogo.ImageUrl = String.Format(WapTools.GetImage(this.Request, "bannervip"), WapTools.GetFolderImg(_mobile));

						WapTools.AddLabelCenter(pnlText, "ZONA VIP", "", _mobile, BooleanOption.True);
						WapTools.AddLink(pnlText, "ENTRA en la Zona Vip de Fondos haciendo clic AQU�", "./linkto.aspx?id=11036", WapTools.GetImage(this.Request, "club"), _mobile);
						WapTools.AddLabel(pnlText, "En la zona Vip de Im�genes y Fondos de emoci�n podr�s disfrutar de barra libre con descargas ilimitadas para la mayor selecci�n de fondos de Warner Bros, Modelos de Sport Illustrated, Amor, Tunning,  Kukuxumuxu y muchas m�s de todo tipo.", "", _mobile);
						WapTools.AddLabel(pnlText, "Por s�lo 4 euros al mes (4,64 con iva) podr�s disfrutar de todos estos contenidos y en el futuro, m�s contenidos en exclusiva antes que otros clientes, y otras ventajas y promociones.", "", _mobile);
						
						WapTools.AddLink(pnlText, "Ap�ntate a la Zona Vip de Fondos haciendo clic aqu�", "./linkto.aspx?id=11036", WapTools.GetImage(this.Request, "club"), _mobile);
						//WapTools.AddLink(pnlText, "Bases de la Promoci�n", "instruct.aspx", WapTools.GetImage(this.Request, "club"), _mobile);
						
						if (_mobile.PreferredRenderingType != "chtml10")
						{
							Image img = new Image();
							img.ImageUrl = WapTools.GetImage(this.Request, "vip");
							img.Alignment = Alignment.Center;
						
							pnlText.Controls.Add(img);
							img = null;
						}
					}
					else
						WapTools.AddLabel(pnlText, WapTools.GetText("Compatibility"), "", _mobile);
					WapTools.AddLinkCenter(pnlText, WapTools.GetText("ImagenesFondos"), "./default.aspx", "", _mobile);

				}
			}		
			catch(Exception caught)
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format(" Unexpected exception in emocion\\wap\\club.aspx - UA : {0}", Request.UserAgent), caught);
				this.RedirectToMobilePage("./error.aspx");				
			}
		}


		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
