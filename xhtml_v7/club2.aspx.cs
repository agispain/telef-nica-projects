using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;

namespace xhtml_v7
{
	public class club2 : XCatalogBrowsing
	{
		protected XhtmlTable tbHeader, tbPreviews;
		public string volver, subir;
		protected int ms;
		protected string text = "", page = "";
		public bool is3g = false;

		private void Page_Load(object sender, System.EventArgs e)
		{
			_mobile = (MobileCaps)Request.Browser;
			try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType));}
			catch{is3g = false;}

			try
			{
				WapTools.SetHeader(this.Context);
				WapTools.AddUIDatLog(Request, Response, this.Trace);				
			} 
			catch{}

			try 
			{
				_contentType = (Request.QueryString["ct"] != null) ? Request.QueryString["ct"] : "";
				_contentGroup = WapTools.GetDefaultContentGroup(_contentType);
				try{_idContentSet = (Request.QueryString["cs"] != null) ? Convert.ToInt32(Request.QueryString["cs"]) : 0;}
				catch{_idContentSet = 0;}
				_displayKey = WapTools.GetXmlValue("DisplayKey");
				
				XhtmlTools.AddLinkTable("IMG", tbPreviews, "ENTRA en la Zona Vip de Fondos haciendo clic AQU�", "./linkto.aspx?id=11036", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, true, FontUnit.XXSmall, "");
																																																																																																													  
				text = "En la zona Vip de Im&aacute;genes y Fondos de emoci&oacute;n podr&aacute;s disfrutar de <b>barra libre con descargas ilimitadas</b> para la mayor selecci&oacute;n de fondos de Warner Bros, Modelos de Sport Illustrated, Amor, Tunning, Kukuxumusu y muchas m&aacute;s de todo tipo.<br/><br/>" +
					"<b>Por s&oacute;lo 4 euros al mes</b> (4,64 con iva) podr&aacute;s disfrutar de todos estos contenidos y en el futuro, m&aacute;s contenidos en exclusiva antes que otros clientes, y otras ventajas y promociones.";
					
				XhtmlTools.AddTextTable(tbPreviews, text, Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall);
				
				XhtmlImage bmimg1 = new XhtmlImage();
				bmimg1.ImageUrl = WapTools.GetImage(this.Request, "vip");
				XhtmlTools.AddImgTable(tbPreviews, bmimg1);
				
				XhtmlTools.AddLinkTable("IMG", tbPreviews, "ENTRA en la Zona Vip de Fondos haciendo clic AQU�", "./linkto.aspx?id=11036", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, true, FontUnit.XXSmall, "");
				//XhtmlTools.AddLinkTable("IMG", tbPreviews, "Bases de la promoci�n", "./instruct.aspx", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, true, FontUnit.XXSmall, "");
				
				bmimg1 = null;
				
			}
			catch(Exception caught) 
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\xhtml\\club2.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
				Response.Redirect("./error.aspx");	
			}
			
			#region HEADER
			XhtmlImage img = new XhtmlImage();
			img.ImageUrl = WapTools.GetImage(this.Request, "bannervip",  _mobile.ScreenPixelsWidth, is3g);
			XhtmlTools.AddImgTable(tbHeader, img); 
		
			#endregion

			#region PICTOS
			if(is3g)
			{
				volver = WapTools.getPicto3g(this.Request.ApplicationPath, "volver_club", _mobile.ScreenPixelsWidth);
				subir = WapTools.getPicto3g(this.Request.ApplicationPath, "subir_club", _mobile.ScreenPixelsWidth);
			}
			
			#endregion
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
