using System;
using System.Configuration;
using System.Web;
using System.Drawing;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using KMobile.Catalog.Presentation;
using xhtml_v7.Tools;

namespace xhtml_v7
{
	public class catalog : XCatalogBrowsing
	{
		protected XhtmlTable tbTitle, tbCatalog, tbLinks, tbSearch, tbPages;
		protected XhtmlTableRow rowPreviews, rowPreviews2;
		private int first = 0, page, ms;
		protected string cg_temp;
		protected XhtmlTable tbHeader;
		protected XhtmlTable tbPreviews; 
		public string buscar, emocion, atras, up, fondo, back, title =  "Im&aacute;genes y Fondos", css = "xhtml.css";
		public string textMarquee, header, p1, p2, p3, p4, f1, f2, f3, f4, f5, banner_footer, volver, subir;
		public bool is3g = false, showHeader = false, showLink = true, isWeb = false, showMarquee = false;
		protected int isSexy = 0, promo = -1;
		private bool isPurchaser = false; 
		private bool isOldUser = false;
		private bool is2x1 = false, isNavidad = false;
		//private CommandCollection cc;
		private int dwldsClubV = -1, dwldsClub099 = -1, club = 0, minisite = -1;
		private Customer customer;

		private void Page_Load(object sender, System.EventArgs e)
		{  
			try 
			{
				_mobile = (MobileCaps)Request.Browser;
				try{isWeb = WapTools.isSomething(_mobile.MobileType, "TouchWeb");}
				catch{isWeb = false;}
				
				if (!isWeb) 
				{
					try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType));}
					catch{is3g = false;}
				} 
				else
				{
					css = "web.css";
				}
				if (_mobile.MobileType.StartsWith("SAMSUNGGT-i9000")) 
				{
					showLink = false;
					is3g = false;
				}
				try
				{
					WapTools.SetHeader(this.Context);
					WapTools.AddUIDatLog(Request, Response, this.Trace);				
					//if (_mobile.MobileType != "SAMSUNGGT-i9000") 
					//	WapTools.AddUIDatLog(Request, Response, this.Trace);					
				} 
				catch{}

				try
				{
					//					int numVisits = -1;
					//						
					customer = new Customer(this.Request);
					dwldsClubV = WapTools.isPurchaserClubV(customer);
					dwldsClub099 = WapTools.isPurchaserClub099(customer);
					minisite = (Request.QueryString["ms"] != null && Request.QueryString["ms"] != "") ? Convert.ToInt32(Request.QueryString["ms"]) : -1;
					isNavidad = (minisite == 7408);
					//					User u = new User();
					//					numVisits = u.countVisits(customer.Alias, DateTime.Now.AddMonths(-6), DateTime.Today.AddDays(-1).AddHours(18), 100, 100000);									
					//					isOldUser = (numVisits > 0);
					//
					//					if (isOldUser)
					//					{
					//						isPurchaser = WapTools.isPurchaser(customer);
					//						if (!isPurchaser)
					//							is2x1 = true;
					//							//is2x1 = WapTools.is2x1(customer);
					//						Trace.Warn("isOldUser: " + isOldUser.ToString() + " - is2x1: " + is2x1.ToString() + Convert.ToInt32(is2x1).ToString());
					//					}
					//	
					//					//Trace.Warn("isPurchaser: " + isPurchaser.ToString() + " - is2x1: " + is2x1.ToString() + Convert.ToInt32(is2x1).ToString());
					//	cc = customer.LoadCommands(CommandStatus.Delivered, DateTime.Now.AddHours(-24), DateTime.Now, new Guid(WapTools.GetXmlValue("DisplayKeyClubV")), _mobile.MobileType, "VIDEO_CLIP");
					//					//cc = customer.LoadCommands(CommandStatus.Delivered, new Guid(_displayKey), "IMG_COLOR");
					//					//Trace.Write("Commands: " + cc.Count.ToString());
					//					customer = null;
				}
				catch
				{
					//cc = null;
					dwldsClubV = -1;	
					dwldsClub099 = -1;
				}

				if (_mobile.MobileType != null &&  _mobile.IsCompatible("IMG_COLOR"))
				{
					DateTime currentTime = DateTime.Now.AddHours(9);
					is2x1 = WapTools.is2x1(customer, currentTime);
					Trace.Write("2x1: ", is2x1.ToString());

					if (((currentTime.Month == 12) && ( currentTime.Day == 24 || currentTime.Day == 25||currentTime.Day==31))||((currentTime.Month == 1 &&  currentTime.Day == 1)))
					{
						showMarquee = true;
						if (is2x1) textMarquee = WapTools.GetText("messagefreecontent");
						else textMarquee = WapTools.GetText("message_navidad");
					} 						
					else if ((currentTime.Month == 1) && ( currentTime.Day) > 1 && (currentTime.Day <= 10))
					{ 
						promo=1;
						showMarquee = true;
						textMarquee=WapTools.GetText("Promociones");
					}
					else 
					{
						showMarquee=false;
					}
					_idContentSet = (Request.QueryString["cs"] != null) ? Convert.ToInt32(Request.QueryString["cs"]) : 0;
					if (_idContentSet == 5747) _idContentSet = 7176;

					/*if (isNavidad) 
					{
						if (((currentTime.Month == 12) && (currentTime.Day == 9 || currentTime.Day == 18 || currentTime.Day == 24))
							|| (currentTime.Month == 1 && (currentTime.Day == 1 || currentTime.Day == 5)))
						{
							showMarquee = true;
							is2x1 = WapTools.is2x1(customer);
							Trace.Write("2x1: ", is2x1.ToString());
							textMarquee = WapTools.GetText("Hay2x1");
						} 
						else if (currentTime.Month == 12
							|| (currentTime.Month == 1 && currentTime.Day <= 8))
						{
							showMarquee = true;
							textMarquee = WapTools.GetText("GanaIpad");
						} 
						else 
						{
							showMarquee = false;
						}
					}*/

					#region PROMO
					//					promo = WapTools.isPromo(this.Request);
					//					Trace.Warn(promo.ToString());
					//					
					//					if (promo >= 0)
					//					{
					//						XhtmlTableRow rowPromo = new XhtmlTableRow();
					//						XhtmlTools.AddTextTableRow(rowPromo, WapTools.GetXmlValue("Promo/Message" + promo.ToString()), Color.Empty, Color.Green, 2, HorizontalAlign.Center,  VerticalAlign.Middle, true, FontUnit.XSmall);
					//						tbCatalog.Rows.Add(rowPromo);
					//					}
					//					else if (Convert.ToBoolean(WapTools.GetXmlValue("Promo/Enabled")))
					//					{
					//						XhtmlTableRow rowPromo = new XhtmlTableRow();
					//						XhtmlTools.AddTextTableRow(rowPromo, WapTools.GetXmlValue("Promo/PromoText"), Color.Empty, Color.Green, 2, HorizontalAlign.Center,  VerticalAlign.Middle, true, FontUnit.XSmall);
					//						tbCatalog.Rows.Add(rowPromo);
					//					}
					if (isOldUser && !isPurchaser)
					{
						WapTools.LogUser(this.Request, 206, _mobile.MobileType);
					
						XhtmlTableRow rowPromo = new XhtmlTableRow();
						XhtmlTools.AddTextTableRow(rowPromo, WapTools.GetText("2x1"), Color.Empty, Color.Black, 1, HorizontalAlign.Center,  VerticalAlign.Middle, true, FontUnit.XSmall);
						tbTitle.Rows.Add(rowPromo);
					}
					#endregion

					page = (Request.QueryString["n"] != null) ? Convert.ToInt32(Request.QueryString["n"]) : 1;
					club = (Request.QueryString["cl"] != null && Request.QueryString["cl"] != "") ? Convert.ToInt32(Request.QueryString["cl"]) : 0;
					ms = (Request.QueryString["ms"] != null && Request.QueryString["ms"] != "") ? Convert.ToInt32(Request.QueryString["ms"]) : 0;
					_contentGroup = (Request.QueryString["cg"] != null) ? Request.QueryString["cg"].ToString() : ""; 
					if (_idContentSet == 3619) _contentGroup = "COMPOSITE";
					if (_idContentSet == 5862) _contentGroup = "COMPOSITE";

					_contentType = WapTools.GetDefaultContentType(_contentGroup);
					_displayKey = WapTools.GetXmlValue("DisplayKey"); 
					isSexy = Convert.ToInt32(WapTools.isSexy(_idContentSet)); //(Request.QueryString["s"] != null && Request.QueryString["s"] != "") ? Convert.ToInt32(Request.QueryString["s"]) : 0;
				
					string paramBack = String.Format("a1=n&a2={0}&a3=cg&a4={1}&a5=cs&a6={2}",
						page, _contentGroup, _idContentSet);
								
					ContentSet contentSet = BrowseContentSetExtended();
			
					if (Request.QueryString["ms"]!=null && Request.QueryString["ms"]!="" && WapTools.GetText(Request.QueryString["ms"]) != "")
						title = WapTools.GetText(Request.QueryString["ms"]);                         

					int nbrows = Convert.ToInt32(WapTools.GetXmlValue("Home/Nb_Rows"));
					int nbcols = (_contentGroup == "ANIM" || _mobile.ScreenPixelsWidth>140) ? 2 : 1;

					if (_contentGroup == "FLASH" && !(_mobile.IsCompatible("FLASH")))
					{
						string url = "./linkto.aspx?id=1159&cg=ANIM";						
						try{Response.Redirect(url, false);}
						catch{Response.Redirect(url, true);}
					}

					if (_contentGroup == "SCHEME" && !(_mobile.IsCompatible("SCHEME")))
					{
						string url = "./linkto.aspx?id=3967&cg=IMG";						
						try{Response.Redirect(url, false);}
						catch{Response.Redirect(url, true);}
					}
                      						
					if( _contentGroup == "COMPOSITE" )    
					{
						#region COMPOSITE
						try
						{
							for (int i=0; i<contentSet.Count - 1; i++)
								try
								{
									if (Convert.ToInt32(WapTools.FindProperty(contentSet.ContentCollection[i].PropertyCollection, "IDComposite")) > 0 && contentSet.ContentCollection[i].PropertyCollection["CompositeContentGroup"].Value.ToString() != "COMPOSITE")
									{
										first = Convert.ToInt32(WapTools.FindProperty(contentSet.ContentCollection[i].PropertyCollection, "IDComposite"));
										cg_temp = contentSet.ContentCollection[i].PropertyCollection["CompositeContentGroup"].Value.ToString();							
										break; 
									}
								}
								catch{}
						}
						catch{first = 0;}
						nbcols = Convert.ToInt32(WapTools.GetXmlValue("Home/Nb_PreviewsComposite"));
						_contentSetDisplayInst = new ContentSetDisplayInstructions(_mobile);
						_contentSetDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "bullet");
						if (_idContentSet != 3619 && _idContentSet.ToString() != WapTools.GetXmlValue("Home/IMG") && _idContentSet.ToString() != WapTools.GetXmlValue("Home/ANIM") && _idContentSet.ToString() != WapTools.GetXmlValue("Home/VIDEO") && _idContentSet.ToString() != WapTools.GetXmlValue("Home/FLASH"))
							_contentSetDisplayInst.UrlDwld = String.Format("./catalog.aspx?cs={0}&cg={1}&p={2}&t={3}&{4}", "{0}", "{1}", _idContentSet.ToString(), Server.UrlEncode(contentSet.Name), paramBack); 
						else
							_contentSetDisplayInst.UrlDwld = String.Format("./catalog.aspx?cs={0}&cg={1}&{2}", "{0}", "{1}",  paramBack); 
						if (Request.QueryString["ref"] != "" && Request.QueryString["ref"] != null) _contentSetDisplayInst.UrlDwld += "&ref=" + Request.QueryString["ref"];
						if (club == 1) _contentSetDisplayInst.UrlDwld += "&cl=1";
						// PREVIEWS?
						try
						{
							if (first > 0 && _mobile.ScreenPixelsWidth>128)
							{
								ContentSet contentSetTemp = StaticCatalogService.GetContentsByRandomize( _displayKey, first, cg_temp, WapTools.GetDefaultContentType(cg_temp), _mobile.MobileType, Convert.ToInt32(WapTools.GetXmlValue("Home/Nb_Previews")) + Convert.ToInt32(WapTools.GetXmlValue("Home/Nb_Links")));
								DisplayContents(rowPreviews, rowPreviews2, contentSetTemp, contentSet.Name, paramBack);
								contentSetTemp = null;
							}
						}
						catch{}
						if(_mobile.MobileType == "NOKIA3220" || _mobile.MobileType == "NOKIA5070" || _mobile.MobileType == "NOKIA2630")
							DisplayContentSets(tbCatalog, contentSet, nbcols);
						else ReadContentSet(contentSet, tbCatalog, page, nbcols);
						page_max = (contentSet.Count / nbcols);
						if (contentSet.Count % nbcols > 0) page_max++;
						_contentSetDisplayInst = null;
						#endregion
					}
					else
					{
						#region CONTENTSET
						_imgDisplayInst = new ImgDisplayInstructions(_mobile);
						_imgDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "bullet");
						_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(WapTools.PreviewUrl(_contentGroup, _mobile));
						//	_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(String.Format("Url_{0}", _contentGroup));
						//_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, _contentGroup, _contentType, HttpUtility.UrlEncode(String.Format("{0}|CONTENTSET|{1}|{2}", referer, contentSet.Name, page)), "", _idContentSet.ToString());
						
						if (club == 1 && Convert.ToBoolean(WapTools.GetXmlValue("Home/Club_video")) && (dwldsClubV < 10) && (_contentGroup.StartsWith("VIDEO") || _contentGroup == ""))
							_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingClub(this.Request, _contentGroup, _contentType, HttpUtility.UrlEncode(String.Format("CLUB_V_{0}|CONTENTSET|{1}|{2}", (is3g) ? "3g" : "xhtml", contentSet.Name, page)), "", _idContentSet.ToString());
						else if (club == 1 && Convert.ToBoolean(WapTools.GetXmlValue("Home/Club_099")) && (dwldsClub099 < 15) && (_contentGroup.StartsWith("IMG")))
							_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingClub099(this.Request, _contentGroup, _contentType, HttpUtility.UrlEncode(String.Format("CLUB099|CONTENTSET|{1}|{2}", (is3g) ? "3g" : "xhtml", contentSet.Name, page)), "", _idContentSet.ToString());
						else if (is2x1) 
						{
							_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingFree(this.Request, _contentGroup, _contentType, HttpUtility.UrlEncode(String.Format("{0}|CONTENTSET|{1}|{2}", (is3g) ? "3g" : "xhtml", contentSet.Name, page)), "", _idContentSet.ToString());
						}
						else if ((promo>0)&&(club==0)) 
						{
							_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(this.Request, _contentGroup, _contentType, (is3g) ? HttpUtility.UrlEncode("3g|HOME_" + WapTools.GetText(_idContentSet.ToString())) : HttpUtility.UrlEncode("xhtml|HOME_" + WapTools.GetText(_idContentSet.ToString())) , "", "0", _mobile.MobileType, promo); 	
						}
						else if (_mobile.Family == "Android" && _contentGroup != "LIVE_WP")
						{
							_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, _contentGroup, _contentType, HttpUtility.UrlEncode(String.Format("{0}|CONTENTSET|{1}|{2}", (is3g) ? "3g" : "xhtml", contentSet.Name, page)), "", _idContentSet.ToString());						
						}
						
							/*if (_idContentSet == 7290 || _idContentSet == 6595 || _idContentSet == 6591 || _idContentSet == 7285 || _idContentSet == 6592 || _idContentSet == 5501 || _idContentSet == 7284 || _idContentSet == 5426 || _idContentSet == 6160 || _idContentSet == 5817 || _idContentSet == 5657 || _idContentSet == 5419 || _idContentSet == 5422 || _idContentSet == 2896 || _idContentSet == 6613 || _idContentSet == 5429 || _idContentSet == 2780 || _idContentSet == 3497)
								_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingFree(this.Request, _contentGroup, _contentType, "FREE_MUNDIAL", "", _idContentSet.ToString());
							else */else if (club == 0 && Convert.ToBoolean(WapTools.GetXmlValue("Home/Club_video")) && Convert.ToBoolean(WapTools.GetXmlValue("Home/Show_viewV")) && (dwldsClubV < 10) && (_contentGroup.StartsWith("VIDEO") || _contentGroup == ""))
							_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, _contentGroup, _contentType, HttpUtility.UrlEncode(String.Format("{0}|CONTENTSET|{1}|{2}", (is3g) ? "3g" : "xhtml", contentSet.Name, page)), "", _idContentSet.ToString());
						else 
							_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(this.Request, _contentGroup, _contentType, (is3g) ? HttpUtility.UrlEncode(String.Format("3g|CONTENTSET|{0}|{1}", contentSet.Name, page)) : HttpUtility.UrlEncode(String.Format("xhtml|CONTENTSET|{0}|{1}", contentSet.Name, page)), "", _idContentSet.ToString(), _mobile.MobileType, Convert.ToInt32(is2x1), isSexy);

						if (Request.QueryString["ms"]!= null && Request.QueryString["ms"]!="")
							_imgDisplayInst.UrlDwld += "&ms=" + Request.QueryString["ms"];
						if (Request["p"] != null && Request["t"] != null && Request["p"] != "" && Request["t"] != "")
							_imgDisplayInst.UrlDwld += "&p=" + Request.QueryString["p"] + "&t=" + Request.QueryString["t"];
						if (_contentGroup == "IMG" && Convert.ToBoolean(WapTools.GetXmlValue("Home/Show_view")))
							_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, _contentGroup, _contentType, (is3g) ? HttpUtility.UrlEncode(String.Format("3g|CONTENTSET|{0}|{1}", contentSet.Name, page)) : HttpUtility.UrlEncode(String.Format("xhtml|CONTENTSET|{0}|{1}", contentSet.Name, page)), "", _idContentSet.ToString());
						if (_mobile.IsXHTML && _mobile.ScreenPixelsWidth > 200) nbrows += 1; 

						// PACK JUNIOR --> FILTER CONTENTS
						//if (WapTools.isJunior(_mobile))
						//	contentSet = WapTools.FilterContentset(contentSet);
						ReadContentSet(contentSet, tbCatalog, page, nbrows*nbcols);
						page_max = contentSet.Count / (nbrows * nbcols);
						if (contentSet.Count % (nbrows * nbcols) > 0) page_max++;
						if (WapTools.noPreview(contentSet.IDContentSet, _mobile.MobileType)) page_max=1;					
						_imgDisplayInst = null;
						#endregion
					}

					#region HEADER
					XhtmlImage img = new XhtmlImage();
					if (_idContentSet == 7290 || _idContentSet == 5817 || _idContentSet == 6592 || _idContentSet == 6613)
						img.ImageUrl = WapTools.GetImage(this.Request, "mundialg",  _mobile.ScreenPixelsWidth, is3g, isWeb);
					else if (_idContentSet == 5745)
						img.ImageUrl = WapTools.GetImage(this.Request, "top",  _mobile.ScreenPixelsWidth, is3g, isWeb);
					else if (_idContentSet == 628)
						img.ImageUrl = WapTools.GetImage(this.Request, "novedades",  _mobile.ScreenPixelsWidth, is3g, isWeb);
					else if (_idContentSet == 3207 || _idContentSet == 5863)
						img.ImageUrl = WapTools.GetImage(this.Request, "animaciones",  _mobile.ScreenPixelsWidth, is3g, isWeb);
					else if (_idContentSet == 4124 || _idContentSet == 5862 )
					{
						img.ImageUrl = WapTools.GetImage(this.Request, "videos",  _mobile.ScreenPixelsWidth, is3g, isWeb);
						if (is3g || _mobile.IsXHTML)
						{
							XhtmlTableRow row = new XhtmlTableRow();
							XhtmlTools.AddLinkTableRow("IMG", row, "TV de Modelos", "./linkto.aspx?id=11055", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							tbTitle.Controls.Add(row);  
						}
					}
					else if (_idContentSet == 1159)
						img.ImageUrl = WapTools.GetImage(this.Request, "top",  _mobile.ScreenPixelsWidth, is3g, isWeb);
					else if (_idContentSet == 1160) 
						img.ImageUrl = WapTools.GetImage(this.Request, "novedades",  _mobile.ScreenPixelsWidth, is3g, isWeb);
					else if (_idContentSet == 3957)
						img.ImageUrl = WapTools.GetImage(this.Request, "top",  _mobile.ScreenPixelsWidth, is3g, isWeb);
					else if (_idContentSet == 3958)
						img.ImageUrl = WapTools.GetImage(this.Request, "novedades",  _mobile.ScreenPixelsWidth, is3g, isWeb);
					else if (_idContentSet == 5747)
						img.ImageUrl = WapTools.GetImage(this.Request, "amor",  _mobile.ScreenPixelsWidth, is3g, isWeb);
					else if (_idContentSet == 5748)
						img.ImageUrl = WapTools.GetImage(this.Request, "modelos",  _mobile.ScreenPixelsWidth, is3g, isWeb);
					else if (_idContentSet == 6637 || _idContentSet == 6638)
						img.ImageUrl = WapTools.GetImage(this.Request, "olympic",  _mobile.ScreenPixelsWidth, is3g, isWeb);
					else if (_idContentSet == 6625 || _idContentSet == 6626)
						img.ImageUrl = WapTools.GetImage(this.Request, "batman",  _mobile.ScreenPixelsWidth, is3g, isWeb);
					else if (_contentGroup == "IMG" || cg_temp == "IMG")
					{
						if(ms == 6123)	img.ImageUrl = WapTools.GetImage(this.Request, "img_" + ms.ToString(),  _mobile.ScreenPixelsWidth, is3g, isWeb);
						else	img.ImageUrl = WapTools.GetImage(this.Request, "categorias",  _mobile.ScreenPixelsWidth, is3g, isWeb);	
						XhtmlTableRow row = new XhtmlTableRow();
						XhtmlTools.AddTextTableRow("IMG", row, "", Server.HtmlEncode(contentSet.Name.ToUpper()) /*contentSet.Name.ToUpper()*/, Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, true, FontUnit.XXSmall);
						tbTitle.Controls.Add(row);  
						if (club == 1 && _mobile.Family != "Android")
						{
							row = new XhtmlTableRow();
							XhtmlTools.AddTextTableRow("IMG", row, "", "Todo a 0.99 euros/semana" /*contentSet.Name.ToUpper()*/, Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, true, FontUnit.XXSmall);
							tbTitle.Controls.Add(row);  					
						}
					}
					else if (_contentGroup == "ANIM" || cg_temp == "ANIM")
					{
						if(ms == 6123)	img.ImageUrl = WapTools.GetImage(this.Request, "anim_" + ms.ToString(),  _mobile.ScreenPixelsWidth, is3g, isWeb);
						else	img.ImageUrl = WapTools.GetImage(this.Request, "animaciones",  _mobile.ScreenPixelsWidth, is3g, isWeb);
						XhtmlTableRow row = new XhtmlTableRow();
						XhtmlTools.AddTextTableRow("IMG", row, "", Server.HtmlEncode(contentSet.Name.ToUpper()), Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, true, FontUnit.XXSmall);
						tbTitle.Controls.Add(row);  
					}
					else if (_contentGroup == "VIDEO" || cg_temp == "VIDEO" || _contentGroup == "VIDEO_RGT" || cg_temp == "VIDEO_RGT" || _contentGroup == "")
					{
						if(ms == 6123)	img.ImageUrl = WapTools.GetImage(this.Request, "video_" + ms.ToString(),  _mobile.ScreenPixelsWidth, is3g, isWeb);
						else	img.ImageUrl = WapTools.GetImage(this.Request, "videos",  _mobile.ScreenPixelsWidth, is3g, isWeb);
						XhtmlTableRow row = new XhtmlTableRow();
						XhtmlTools.AddTextTableRow("IMG", row, "", Server.HtmlEncode(contentSet.Name.ToUpper()), Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, true, FontUnit.XXSmall);
						tbTitle.Controls.Add(row);  
						//						if (is3g || _mobile.IsXHTML)
						//						{
						//							row = new XhtmlTableRow();
						//							XhtmlTools.AddLinkTableRow("IMG", row, "TV de Modelos", "./linkto.aspx?id=11055", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						//							tbTitle.Controls.Add(row);  
						//						}
					}
					else 
					{
						img.ImageUrl = WapTools.GetImage(this.Request, "imagenes",  _mobile.ScreenPixelsWidth, is3g, isWeb);
						XhtmlTableRow row = new XhtmlTableRow();
						XhtmlTools.AddTextTableRow("IMG", row, "", Server.HtmlEncode(contentSet.Name.ToUpper()), Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, true, FontUnit.XXSmall);
						tbTitle.Controls.Add(row);  
					}
					//XhtmlTools.AddImgTable(tbHeader, img);
					#endregion
                                     
					#region PAGES

					if (isWeb)
					{
						TableCell cellTemp = new TableCell();
						XhtmlTableRow rowTemp = new XhtmlTableRow();
								
						string URL_Suivant = String.Format("./catalog.aspx?d={0}&ms={1}&cg={2}&cs={3}&n={4}&p={5}&t={6}&cl={7}&{8}", Request.QueryString["d"], Request.QueryString["ms"], _contentGroup, _idContentSet, page + 1, Request.QueryString["p"], Server.UrlEncode(Request.QueryString["t"]), club, paramBack);
						string URL_Precedent = String.Format("./catalog.aspx?d={0}&ms={1}&cg={2}&cs={3}&n={4}&p={5}&t={6}&cl={7}&{8}", Request.QueryString["d"], Request.QueryString["ms"], _contentGroup, _idContentSet, page - 1, Request.QueryString["p"], Server.UrlEncode(Request.QueryString["t"]), club, paramBack);
						
						if (page > 1)
						{
							XhtmlLink link = new XhtmlLink();				
							
							link.ImageUrl = WapTools.GetImageWeb(this.Request, "previous", _mobile.ScreenPixelsWidth);
							link.NavigateUrl = URL_Precedent;
							cellTemp.HorizontalAlign = HorizontalAlign.Left;
							cellTemp.Controls.Add(link);
							rowTemp.Cells.Add(cellTemp);
							link = null;
						}
						if (page < page_max) 
						{
							cellTemp = new TableCell();
							
							XhtmlLink link = new XhtmlLink();
							link.ImageUrl = WapTools.GetImageWeb(this.Request, "next", _mobile.ScreenPixelsWidth);
							link.NavigateUrl = URL_Suivant;
							cellTemp.HorizontalAlign = HorizontalAlign.Right;
							cellTemp.Controls.Add(link);
							rowTemp.Cells.Add(cellTemp);
							link = null;							
						}
						tbPages.Rows.Add(rowTemp);
					}
					else
					{
						TableCell cellTemp = new TableCell();
						cellTemp.HorizontalAlign = HorizontalAlign.Center;
						XhtmlTableRow rowTemp = new XhtmlTableRow();
						int premiere = 0, derniere = 0, cont = 0;
						int[] limits = new int[page_max/5];
						while (cont<(page_max/5))
							limits[cont]=(++cont)*5;
						for (cont=0;cont<page_max/5;cont++)
							if (page<=limits[cont])
							{
								premiere=limits[cont]-4;
								derniere=limits[cont];
								break;
							}
						if (premiere==0 && derniere==0 && page>0)
						{
							derniere = page_max;
							if (limits.Length==0)
								premiere = 1; 
							else
								premiere = limits[cont-1]+1;
						}				
						string URL_Suivant = String.Format("./catalog.aspx?d={0}&ms={1}&cg={2}&cs={3}&n={4}&p={5}&t={6}&cl={7}&{8}", Request.QueryString["d"], Request.QueryString["ms"], _contentGroup, _idContentSet, derniere + 1, Request.QueryString["p"], Server.UrlEncode(Request.QueryString["t"]), club, paramBack);
						string URL_Precedent = String.Format("./catalog.aspx?d={0}&ms={1}&cg={2}&cs={3}&n={4}&p={5}&t={6}&cl={7}&{8}", Request.QueryString["d"], Request.QueryString["ms"], _contentGroup, _idContentSet, premiere - 1, Request.QueryString["p"], Server.UrlEncode(Request.QueryString["t"]), club, paramBack);
						if (derniere>1)
						{
							XhtmlLink link = new XhtmlLink();				
							if (premiere > 1)
							{
								//link.ImageUrl = WapTools.GetImage(this.Request, "Previous");
								link.Text = "Atr&aacute;s";
								link.NavigateUrl = URL_Precedent;
								cellTemp.Controls.Add(link);
							}
							else
								cellTemp.Text = "&nbsp;";
							rowTemp.Cells.Add(cellTemp);
							link = null;
							cellTemp = new TableCell();
							cellTemp.HorizontalAlign = HorizontalAlign.Center;

							for (cont=premiere;cont<=derniere;cont++)
							{
								if (cont!=page)
								{
									link = new XhtmlLink();
									link.CssClass = _contentGroup;
									link.NavigateUrl = String.Format("./catalog.aspx?d={0}&ms={1}&cg={2}&cs={3}&n={4}&p={5}&t={6}&cl={7}&{8}", Request.QueryString["d"], Request.QueryString["ms"], _contentGroup, _idContentSet, cont, Request.QueryString["p"], Server.UrlEncode(Request.QueryString["t"]), club, paramBack);
									link.Text = cont.ToString();
									cellTemp.Controls.Add(link);
								}
								else
								{
									//cellTemp.ForeColor = Color.FromName(WapTools.GetText("Color_" +  _contentGroup));
									cellTemp.Text = cont.ToString();
								}
								rowTemp.Cells.Add(cellTemp);
								link = null;
								cellTemp = new TableCell();
								cellTemp.HorizontalAlign = HorizontalAlign.Center;
							}
			
							if (derniere < page_max)
							{
								link = new XhtmlLink();
								//link.ImageUrl = WapTools.GetImage(this.Request, "Next");
								link.Text = "M�s";
								link.NavigateUrl = URL_Suivant;
								cellTemp.Controls.Add(link);
							}
							else
								cellTemp.Text = "&nbsp;";
							rowTemp.Cells.Add(cellTemp);
							link = null;
							tbPages.Rows.Add(rowTemp);
						}
						else
							tbPages.Visible = false;	
					}
					
					#endregion

					#region LINKS
					try
					{
						if (_mobile.MobileDeviceManufacturer == "NOKIA" && (_contentGroup == "FLASH" || _idContentSet == 6579)) // && _mobile.MobileDeviceManufacturer == "NOKIA")
							XhtmlTools.AddLinkTable("IMG", tbTitle, "Instrucciones FLASH", "./linkto.aspx?cg=COMPOSITE&id=11056", Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						
						if (_idContentSet == 6697)
							XhtmlTools.AddLinkTable("IMG", tbLinks, "Crea tu avatar Vampiro", "./linkto.aspx?cg=COMPOSITE&id=11042", Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						if (_idContentSet == 6595) 
						{
							XhtmlTools.AddLinkTable("IMG", tbTitle, "Avatares de F�tbol", "./linkto.aspx?id=11039", Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						}
						/*if (_idContentSet == 7290 || _idContentSet == 5817 || _idContentSet == 6592)
						{
							XhtmlTools.AddLinkTable("IMG", tbLinks, "Animaciones del Mundial", "./catalog.aspx?cs=6613&cg=ANIM", Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							XhtmlTools.AddLinkTable("IMG", tbLinks, "Especial Mundial Emoci�n", "./linkto.aspx?id=10102", Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						}
						if (_idContentSet == 6613)
						{
							XhtmlTools.AddLinkTable("IMG", tbLinks, "Fondos del Mundial", "./catalog.aspx?cs=7290&cg=IMG", Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							XhtmlTools.AddLinkTable("IMG", tbLinks, "Especial Mundial Emoci�n", "./linkto.aspx?id=10102", Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						}*/
						if (_idContentSet == 5793)
						{
							XhtmlTools.AddLinkTable("IMG", tbLinks, "Animaciones S.Santa", "./catalog.aspx?cg=ANIM&cs=6922", Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							if (_mobile.IsCompatible("VIDEO_CLIP"))
								XhtmlTools.AddLinkTable("IMG", tbLinks, "Videos S.Santa", "./catalog.aspx?cg=VIDEO_RGT&cs=6956", Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						}
						//						if (_idContentSet == 6427)
						//							XhtmlTools.AddLinkTable("IMG", tbLinks, WapTools.GetText("FondoNombresC"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", "10052"), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						if (_idContentSet == 6339) //NAVIDAD
						{
							if (_mobile.IsCompatible("ANIM_COLOR"))
								XhtmlTools.AddLinkTable("IMG", tbLinks, "Animaciones de Navidad", "./catalog.aspx?cg=ANIM&cs=6340&ms=7408", Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							if (_mobile.IsCompatible("VIDEO_RGT"))
								XhtmlTools.AddLinkTable("IMG", tbLinks, "Videos de Navidad", "./catalog.aspx?cg=VIDEO_RGT&cs=6341&ms=7408", Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						}
						if (_idContentSet == 6625)
						{
							XhtmlTools.AddLinkTable("IMG", tbLinks, WapTools.GetText("BatmanAnim"), String.Format("./linkto.aspx?cg=ANIM&id={0}", "6626"), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							XhtmlTools.AddLinkTable("IMG", tbLinks, WapTools.GetText("BatmanComic"), String.Format("./linkto.aspx?cg=IMG&id={0}", "6138"), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							XhtmlTools.AddLinkTable("IMG", tbLinks, WapTools.GetText("BatmanIntro"), String.Format("./instruct.aspx"), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						}
						if (_idContentSet == 6637)
							XhtmlTools.AddLinkTable("IMG", tbLinks, WapTools.GetText("OlympicAnim"), String.Format("./linkto.aspx?cg=ANIM&id={0}", "6638"), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						if (_idContentSet == 6444)
							XhtmlTools.AddLinkTable("IMG", tbTitle, "Avatares de San Valentin: 100% Amor", String.Format("./linkto.aspx?id={0}", "11039"), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						// GALLERY LINK
						//						int idGallery = WapTools.isGallery(_idContentSet);
						//						if (idGallery > 0)
						//							XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("TitleGallery" + idGallery), String.Format("./gallery.aspx?id={0}", idGallery), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));				

						// AVATAR LINK
						//						int idAvatar = WapTools.isAvatar(_idContentSet);
						//						if (idAvatar > 0)
						//							XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("TitleGallery" + idGallery), String.Format("./gallery.aspx?id={0}", idGallery), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));				

						if (_contentGroup == "FLASH")
						{
							string instr = WapTools.isInstructions(_idContentSet);
							if (instr != null)
								XhtmlTools.AddTextTable(tbTitle, instr, Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall);						
						}

						int isAlerta = WapTools.isAlerta(_idContentSet);
						if (isAlerta > 0)
							XhtmlTools.AddLinkTable("", tbLinks, "Alerta de " + WapTools.GetText("Alerta" + isAlerta.ToString()), "./linkto.aspx?id=" + (20 + isAlerta).ToString(), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						
						if (Request["p"] != null && Request["t"] != null && Request["p"] != "" && Request["t"] != "")
							XhtmlTools.AddLinkTable("", tbLinks, "Volver a " + Server.UrlDecode(Request.QueryString["t"]), "./catalog.aspx?cg=COMPOSITE&cs=" + Request.QueryString["p"] + "&cl=" + club, Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));							
						
						img = new XhtmlImage();
						img.ImageUrl = WapTools.GetImage(this.Request, "descargate",  _mobile.ScreenPixelsWidth, is3g);
						
						if (Request.QueryString["ms"]!=null && Request.QueryString["ms"]!="")
						{
							string picto = "bullet";
							if (Request.QueryString["ms"] == "6123" ) {css = "halloween.css"; picto = "halloween";}
							XhtmlTools.AddLinkTable("", tbLinks, (WapTools.GetText(Request.QueryString["ms"]) != "") ? WapTools.GetText(Request.QueryString["ms"]): "Volver", "./minisite.aspx?id=" + Request.QueryString["ms"], Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, picto));							
						}
							/*	else if (_idContentSet.ToString() == WapTools.GetXmlValue("Home/POSTALES"))
								{
									XhtmlTools.AddImgTable(tbLinks, img);
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("IMG"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/IMG"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									if (_mobile.IsCompatible("VIDEO_DWL"))
										XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("VIDEO"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/VIDEO"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									if (_mobile.IsCompatible("ANIM_COLOR"))
										XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("ANIM"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/ANIM"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									if (WapTools.isCompatibleThemes(_mobile))
										XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("Temas"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/TEMAS")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("FondoNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FONDONOMBRES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("AnimaNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/ANIMANOMBRES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("FondoDedicatorias"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FONDODEDICATORIAS")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									if (_mobile.IsCompatible("VIDEO_DWL")) XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("VideoNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEONOMBRES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									if (_mobile.IsCompatible("VIDEO_DWL")) XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("VideoAnimaciones"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEOANIMACIONES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("Gigigo"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/GIGIGO")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								} */
							//else if (Request.QueryString["d"] == "1")
							//	XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("MasPOSTALES"), String.Format("./catalog.aspx?d=1&cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/POSTALES"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							/*	else if (_idContentSet.ToString() == WapTools.GetXmlValue("Home/IMG") || _idContentSet.ToString() == "3619")
								{
									XhtmlTools.AddImgTable(tbLinks, img);
									//XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("POSTALES"), String.Format("./catalog.aspx?d=1&cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/POSTALES"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									if (_mobile.IsCompatible("VIDEO_DWL"))
										XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("VIDEO"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/VIDEO"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									if (_mobile.IsCompatible("ANIM_COLOR"))
										XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("ANIM"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/ANIM"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								//	if (WapTools.isTestSite(this.Request) && _mobile.IsCompatible("FLASH_SCREENSAVER"))
								//		XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("FLASH"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/FLASH"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									if (WapTools.isCompatibleThemes(_mobile))
										XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("Temas"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/TEMAS")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("FondoNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FONDONOMBRES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("AnimaNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/ANIMANOMBRES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("FondoDedicatorias"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FONDODEDICATORIAS")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									if (_mobile.IsCompatible("VIDEO_DWL")) XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("VideoNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEONOMBRES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									if (_mobile.IsCompatible("VIDEO_DWL")) XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("VideoAnimaciones"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEOANIMACIONES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("Gigigo"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/GIGIGO")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								} */
						else if ((_contentGroup == "IMG" || cg_temp == "IMG") && (_idContentSet.ToString() != WapTools.GetXmlValue("Home/IMG")))
						{
							if(is3g) XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("MasIMG"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/IMG"), paramBack), ColorTranslator.FromHtml("#CDE4FD"), Color.Empty, 1, HorizontalAlign.Right, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							else XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("MasIMG"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/IMG"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						}
							/*	else if (_idContentSet.ToString() == WapTools.GetXmlValue("Home/ANIM"))
								{
									XhtmlTools.AddImgTable(tbLinks, img);
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("IMG"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/IMG"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									//XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("POSTALES"), String.Format("./catalog.aspx?d=1&cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/POSTALES"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									if (_mobile.IsCompatible("VIDEO_DWL"))
										XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("VIDEO"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/VIDEO"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								//	if (WapTools.isTestSite(this.Request) && _mobile.IsCompatible("FLASH_SCREENSAVER"))
								//		XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("FLASH"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/FLASH"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									if (WapTools.isCompatibleThemes(_mobile))
										XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("Temas"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/TEMAS")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("FondoNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FONDONOMBRES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("AnimaNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/ANIMANOMBRES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("FondoDedicatorias"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FONDODEDICATORIAS")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									if (_mobile.IsCompatible("VIDEO_DWL")) XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("VideoNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEONOMBRES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									if (_mobile.IsCompatible("VIDEO_DWL")) XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("VideoAnimaciones"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEOANIMACIONES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("Gigigo"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/GIGIGO")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								} */
						else if ((_idContentSet.ToString() != WapTools.GetXmlValue("Home/ANIM")) && (_idContentSet != 6613) && (_contentGroup == "ANIM" || cg_temp == "ANIM"))
							XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("MasANIM"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/ANIM"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						else if ((_idContentSet.ToString() != WapTools.GetXmlValue("Home/FLASH")) && (_contentGroup == "FLASH" || cg_temp == "FLASH"))
							XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("MasFLASH"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/FLASH"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							/*	else if (_idContentSet.ToString() == WapTools.GetXmlValue("Home/VIDEO"))
								{
									XhtmlTools.AddImgTable(tbLinks, img);
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("IMG"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/IMG"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									//XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("POSTALES"), String.Format("./catalog.aspx?d=1&cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/POSTALES"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									if (_mobile.IsCompatible("ANIM_COLOR"))
										XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("ANIM"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/ANIM"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								//	if (WapTools.isTestSite(this.Request) && _mobile.IsCompatible("FLASH_SCREENSAVER"))
								//		XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("FLASH"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/FLASH"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									if (WapTools.isCompatibleThemes(_mobile))
										XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("Temas"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/TEMAS")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("FondoNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FONDONOMBRES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("AnimaNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/ANIMANOMBRES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("FondoDedicatorias"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FONDODEDICATORIAS")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									if (_mobile.IsCompatible("VIDEO_DWL")) XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("VideoNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEONOMBRES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									if (_mobile.IsCompatible("VIDEO_DWL")) XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("VideoAnimaciones"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEOANIMACIONES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("Gigigo"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/GIGIGO")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								}	*/				
						else if ((_idContentSet.ToString() != WapTools.GetXmlValue("Home/VIDEO")) && (_contentGroup == "" || _contentGroup == "VIDEO" || _contentGroup == "VIDEO_RGT" || cg_temp == "VIDEO" || cg_temp == "VIDEO_RGT"))
							XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("MasVIDEO"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&cl={1}&{2}", WapTools.GetXmlValue("Home/VIDEO"), club, paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));	
						/*		else if (_idContentSet.ToString() == WapTools.GetXmlValue("Home/FLASH"))
							   {
								   XhtmlTools.AddImgTable(tbLinks, img);
								   XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("IMG"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/IMG"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								   //XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("POSTALES"), String.Format("./catalog.aspx?d=1&cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/POSTALES"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								   if (_mobile.IsCompatible("VIDEO_DWL"))
									   XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("VIDEO"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/VIDEO"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								   if (_mobile.IsCompatible("ANIM_COLOR"))
									   XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("ANIM"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/ANIM"), paramBack), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								   if (WapTools.isCompatibleThemes(_mobile))
									   XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("Temas"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/TEMAS")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								   XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("FondoNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FONDONOMBRES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								   XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("AnimaNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/ANIMANOMBRES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								   XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("FondoDedicatorias"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FONDODEDICATORIAS")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								   if (_mobile.IsCompatible("VIDEO_DWL")) XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("VideoNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEONOMBRES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								   if (_mobile.IsCompatible("VIDEO_DWL")) XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("VideoAnimaciones"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEOANIMACIONES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								   XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("Gigigo"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/GIGIGO")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							   } 
						   */
					}
					catch{} 
					#endregion

					#region PICTOS
					if (ms == 6123)
					{
						fondo = WapTools.GetImage(this.Request, "hfondo", _mobile.ScreenPixelsWidth, false);
						buscar = WapTools.getPicto(this.Request, "hbuscar", _mobile);
						emocion = WapTools.getPicto(this.Request, "hemocion", _mobile);
						back = WapTools.getPicto(this.Request, "hback", _mobile);
						up = WapTools.getPicto(this.Request, "hup", _mobile);						
					}
					else
					{
						fondo = WapTools.GetImage(this.Request, "fondo", _mobile.ScreenPixelsWidth, false);
						buscar = WapTools.getPicto(this.Request, "buscar", _mobile);
						emocion = WapTools.getPicto(this.Request, "emocion", _mobile);
						back = WapTools.getPicto(this.Request, "back", _mobile);
						up = WapTools.getPicto(this.Request, "up", _mobile);
					}
					#endregion

					#region 3G
					if (is3g)
					{
						css = "3g.css";
						p1 = WapTools.getPicto3g(this.Request.ApplicationPath, "p1", _mobile.ScreenPixelsWidth);
						p2 = WapTools.getPicto3g(this.Request.ApplicationPath, "p2", _mobile.ScreenPixelsWidth);
						p3 = WapTools.getPicto3g(this.Request.ApplicationPath, "p3", _mobile.ScreenPixelsWidth);
						p4 = WapTools.getPicto3g(this.Request.ApplicationPath, "p4", _mobile.ScreenPixelsWidth);
						//						f1 = WapTools.getPicto3g(this.Request.ApplicationPath, "f1", _mobile.ScreenPixelsWidth);
						//						f2 = WapTools.getPicto3g(this.Request.ApplicationPath, "f2", _mobile.ScreenPixelsWidth);
						//						f3 = WapTools.getPicto3g(this.Request.ApplicationPath, "f3", _mobile.ScreenPixelsWidth);
						//						f4 = WapTools.getPicto3g(this.Request.ApplicationPath, "f4", _mobile.ScreenPixelsWidth);
						//						f5 = WapTools.getPicto3g(this.Request.ApplicationPath, "f5", _mobile.ScreenPixelsWidth);
						//						banner_footer = WapTools.getPicto3g(this.Request.ApplicationPath, "footer",  _mobile.ScreenPixelsWidth);		
						volver = WapTools.getPicto3g(this.Request.ApplicationPath, "volver", _mobile.ScreenPixelsWidth);
						subir = WapTools.getPicto3g(this.Request.ApplicationPath, "subir", _mobile.ScreenPixelsWidth);		
						if (_idContentSet == 3619) showHeader = true;
						//if (_idContentSet == 5862 || _idContentSet == 5863 ||  _idContentSet == 3580) showFooter = true;
					}
					#endregion

					contentSet = null;
					_mobile = null;
					
					atras = WapTools.UpdateFooter(_mobile, this.Context, null); 				
				}
				else
				{
					tbCatalog.Visible = false;
					tbPages.Visible = false;
					tbPreviews.Visible = false;
					tbSearch.Visible = false;
					tbTitle.Visible = false;
					XhtmlTableRow row = new XhtmlTableRow();
					XhtmlTools.AddTextTableRow(row, WapTools.GetText("Compatibility2"), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XSmall);
					tbLinks.Rows.Add(row);
					row = null;
				}
			}
			catch(Exception caught) 
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\xhtml\\catalog.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
				Response.Redirect("./error.aspx");				
			}
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		#region Display
		public void DisplayContents(TableRow row, TableRow row2, ContentSet contentset, string name, string paramBack)
		{
			_imgDisplayInst = new ImgDisplayInstructions(_mobile);
			_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(WapTools.PreviewUrl(contentset.ContentGroup, _mobile));
			//	_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(String.Format("Url_{0}", contentset.ContentGroup));
			_imgDisplayInst.TextDwld = WapTools.GetText("Download");
			_imgDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "download");
			//_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup), HttpUtility.UrlEncode(String.Format("{0}|HOME_{1}",referer, name)), "", _idContentSet.ToString());
			
			if (club == 1 && Convert.ToBoolean(WapTools.GetXmlValue("Home/Club_video")) && (dwldsClubV < 10) && (contentset.ContentGroup.StartsWith("VIDEO") || contentset.ContentGroup == ""))
				_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingClub(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup), HttpUtility.UrlEncode(String.Format("CLUB_V_{0}|HOME_{1}", (is3g) ? "3g" : "xhtml", name)), "", _idContentSet.ToString());
			else if (club == 1 && Convert.ToBoolean(WapTools.GetXmlValue("Home/Club_099")) && (dwldsClub099 < 15) && (_contentGroup.StartsWith("IMG") || cg_temp == "IMG"))
				_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingClub099(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup), HttpUtility.UrlEncode(String.Format("CLUB099|HOME_{1}", (is3g) ? "3g" : "xhtml", name)), "", _idContentSet.ToString());
			else if (is2x1) 
				_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingFree(this.Request, _contentGroup, _contentType, "2x1", "", _idContentSet.ToString());
			else if (club == 0 && Convert.ToBoolean(WapTools.GetXmlValue("Home/Club_video")) && Convert.ToBoolean(WapTools.GetXmlValue("Home/Show_viewV")) && (dwldsClubV < 10) && (contentset.ContentGroup.StartsWith("VIDEO") || contentset.ContentGroup == ""))
				_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup), HttpUtility.UrlEncode(String.Format("{0}|HOME_{1}", (is3g) ? "3g" : "xhtml", name)), "", _idContentSet.ToString());
			else 
				_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup),  (is3g) ? HttpUtility.UrlEncode(String.Format("3g|HOME_{0}", name)) : HttpUtility.UrlEncode(String.Format("xhtml|HOME_{0}", name)), "", _idContentSet.ToString(), _mobile.MobileType, Convert.ToInt32(is2x1), isSexy) + "&p=" + _idContentSet + "&t=" + Server.UrlEncode(name);
                   
			TableItemStyle tableStyle = new TableItemStyle();
			try
			{
				tableStyle.HorizontalAlign = HorizontalAlign.Center; 
				int previews = (_mobile.ScreenPixelsWidth < 140 && contentset.ContentGroup != "ANIM") ? 1 : 2;            
				for( int i = DateTime.Now.Day; i < DateTime.Now.Day + previews; i++ )
				{
					Content content = contentset.ContentCollection[ i % contentset.Count];
					if (content != null)
					{
						if (contentset.ContentGroup == "IMG")
						{
							if (Convert.ToBoolean(WapTools.GetXmlValue("Home/Show_view")))
								_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup), (is3g) ? HttpUtility.UrlEncode(String.Format("3g|HOME_{0}", name)) : HttpUtility.UrlEncode(String.Format("xhtml|HOME_{0}", name)), "", _idContentSet.ToString());
							else //if (content.ContentRatings[0].Value > 0)
							{
								//if (Convert.ToBoolean(WapTools.GetXmlValue("Home/Club_video")) && (_contentGroup.StartsWith("VIDEO") || _contentGroup == ""))
									//if (_contentGroup.StartsWith("VIDEO"))
								//	_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingClub(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup), HttpUtility.UrlEncode(String.Format("CLUB_V_{0}|HOME_{1}", (is3g) ? "3g" : "xhtml", name)), "", _idContentSet.ToString());
								if (club == 1 && Convert.ToBoolean(WapTools.GetXmlValue("Home/Club_099")) && (dwldsClub099 < 15) && (_contentGroup.StartsWith("IMG") || cg_temp == "IMG"))
									_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingClub099(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup), HttpUtility.UrlEncode(String.Format("CLUB099|HOME_{1}", (is3g) ? "3g" : "xhtml", name)), "", _idContentSet.ToString());
								else
									_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup), (is3g) ? HttpUtility.UrlEncode(String.Format("3g|HOME_{0}", name)) : HttpUtility.UrlEncode(String.Format("xhtml|HOME_{0}", name)), "", _idContentSet.ToString(), _mobile.MobileType, Convert.ToInt32(is2x1));
							}							
						
						}

						XhtmlTableCell tempCell = new XhtmlTableCell();
						//XhtmlTableCell textCell = new XhtmlTableCell();
						ImgDisplay imgDisplay = new ImgDisplay(_imgDisplayInst);
						//imgDisplay.Display(tempCell, textCell, content);
						imgDisplay.Display(tempCell, content);
						imgDisplay = null;
						tempCell.ApplyStyle(tableStyle);
						//textCell.ApplyStyle(tableStyle);
						row.Cells.Add(tempCell); 
						//rowTexts.Cells.Add(textCell);		
						tempCell = null;
					} 
					content = null;
				}
			}
			catch{}
			try
			{
				tableStyle.HorizontalAlign = HorizontalAlign.Center; 
				int previews = (_mobile.ScreenPixelsWidth < 140 && contentset.ContentGroup != "ANIM") ? 1 : 2;            
				for( int i = DateTime.Now.Day + 2; i < DateTime.Now.Day + 2 + previews; i++ )
				{
					Content content = contentset.ContentCollection[ i % contentset.Count];
					if (content != null)
					{
						if (contentset.ContentGroup == "IMG")
						{
							//if (content.ContentRatings[0].Value > 0)
							//	_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup), (is3g) ? HttpUtility.UrlEncode(String.Format("3g|HOME_{0}", name)) : HttpUtility.UrlEncode(String.Format("xhtml|HOME_{0}", name)), "", _idContentSet.ToString(), _mobile.MobileType);
							//else
							//	_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup), (is3g) ? HttpUtility.UrlEncode(String.Format("3g|HOME_{0}", name)) : HttpUtility.UrlEncode(String.Format("xhtml|HOME_{0}", name)), "", _idContentSet.ToString());
							
							/*if (Convert.ToBoolean(WapTools.GetXmlValue("Home/Show_view")))
								_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup), (is3g) ? HttpUtility.UrlEncode(String.Format("3g|HOME_{0}", name)) : HttpUtility.UrlEncode(String.Format("xhtml|HOME_{0}", name)), "", _idContentSet.ToString());
							else //if (content.ContentRatings[0].Value > 0)
								_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup), (is3g) ? HttpUtility.UrlEncode(String.Format("3g|HOME_{0}", name)) : HttpUtility.UrlEncode(String.Format("xhtml|HOME_{0}", name)), "", _idContentSet.ToString(), _mobile.MobileType, Convert.ToInt32(is2x1));							
						*/
							if (Convert.ToBoolean(WapTools.GetXmlValue("Home/Club_video")) && Convert.ToBoolean(WapTools.GetXmlValue("Home/Show_viewV")) && (dwldsClubV < 10) && (_contentGroup.StartsWith("VIDEO") || _contentGroup == ""))
								_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup), HttpUtility.UrlEncode(String.Format("{0}|HOME_{1}", (is3g) ? "3g" : "xhtml", name)), "", _idContentSet.ToString());
							else if (club == 1 && Convert.ToBoolean(WapTools.GetXmlValue("Home/Club_video")) && (dwldsClubV < 10) && (_contentGroup.StartsWith("VIDEO") || _contentGroup == ""))
								_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingClub(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup), HttpUtility.UrlEncode(String.Format("CLUB_V_{0}|HOME_{1}", (is3g) ? "3g" : "xhtml", name)), "", _idContentSet.ToString());
							else if (club == 1 && Convert.ToBoolean(WapTools.GetXmlValue("Home/Club_099")) && (dwldsClub099 < 15) && (_contentGroup.StartsWith("IMG") || cg_temp == "IMG"))
								_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingClub099(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup), HttpUtility.UrlEncode(String.Format("CLUB099|HOME_{1}", (is3g) ? "3g" : "xhtml", name)), "", _idContentSet.ToString());
							else
								_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup), (is3g) ? HttpUtility.UrlEncode(String.Format("3g|HOME_{0}", name)) : HttpUtility.UrlEncode(String.Format("xhtml|HOME_{0}", name)), "", _idContentSet.ToString(), _mobile.MobileType, Convert.ToInt32(is2x1));							
								
           
						}

						XhtmlTableCell tempCell = new XhtmlTableCell();
						//XhtmlTableCell textCell = new XhtmlTableCell();
						ImgDisplay imgDisplay = new ImgDisplay(_imgDisplayInst);
						//imgDisplay.Display(tempCell, textCell, content);
						imgDisplay.Display(tempCell, content);
						imgDisplay = null;
						tempCell.ApplyStyle(tableStyle);
						//textCell.ApplyStyle(tableStyle);
						row2.Cells.Add(tempCell); 
						//rowTexts.Cells.Add(textCell);		
						tempCell = null;
					} 
					content = null;
				}
			}
			catch{}
		}


		public void DisplayContents(string cg, Table t, ContentSet contentset, string name, string paramBack)
		{
			TableRow row = new TableRow();
			ContentSet newContentset = new ContentSet();
			newContentset.ContentCollection = new ContentCollection();
			foreach (Content c in contentset.ContentCollection)
				if (c.ContentGroup.Name == cg)
					newContentset.ContentCollection.Add(c);
			_imgDisplayInst = new ImgDisplayInstructions(_mobile);
			_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(WapTools.PreviewUrl(cg, _mobile));
			//	_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(String.Format("Url_{0}", cg));
			_imgDisplayInst.TextDwld = WapTools.GetText("Download");
			_imgDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "download");
			//_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup), HttpUtility.UrlEncode(String.Format("{0}|HOME_{1}",referer, name)), "", _idContentSet.ToString());
			
			/*if (Convert.ToBoolean(WapTools.GetXmlValue("Home/Club_video")) && (_contentGroup.StartsWith("VIDEO") || _contentGroup == ""))
				//if (_contentGroup.StartsWith("VIDEO"))
				_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingClub(this.Request, cg, WapTools.GetDefaultContentType(cg), HttpUtility.UrlEncode(String.Format("CLUB_V_{0}|HOME_{1}", (is3g) ? "3g" : "xhtml", name)), "", _idContentSet.ToString());
			else
				_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(this.Request, cg, WapTools.GetDefaultContentType(cg), (is3g) ? HttpUtility.UrlEncode(String.Format("3g|HOME_{0}", name)) : HttpUtility.UrlEncode(String.Format("xhtml|HOME_{0}", name)), "", _idContentSet.ToString(), _mobile.MobileType, Convert.ToInt32(is2x1));
              */   
			if (club == 0 && Convert.ToBoolean(WapTools.GetXmlValue("Home/Club_video")) && Convert.ToBoolean(WapTools.GetXmlValue("Home/Show_viewV")) && (dwldsClubV < 10) && (_contentGroup.StartsWith("VIDEO") || _contentGroup == ""))
				_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, cg, WapTools.GetDefaultContentType(cg), HttpUtility.UrlEncode(String.Format("{0}|HOME_{1}", (is3g) ? "3g" : "xhtml", name)), "", _idContentSet.ToString());
			else if (club == 1 && Convert.ToBoolean(WapTools.GetXmlValue("Home/Club_video")) && (dwldsClubV < 10) && (_contentGroup.StartsWith("VIDEO") || _contentGroup == ""))
				_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingClub(this.Request, cg, WapTools.GetDefaultContentType(cg), HttpUtility.UrlEncode(String.Format("CLUB_V_{0}|HOME_{1}", (is3g) ? "3g" : "xhtml", name)), "", _idContentSet.ToString());
			else if (club == 1 && Convert.ToBoolean(WapTools.GetXmlValue("Home/Club_099")) && (dwldsClub099 < 15) && (_contentGroup.StartsWith("IMG")))
				_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingClub099(this.Request, cg, WapTools.GetDefaultContentType(cg), HttpUtility.UrlEncode(String.Format("CLUB099|HOME_{1}", (is3g) ? "3g" : "xhtml", name)), "", _idContentSet.ToString());
			else
				_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(this.Request, cg, WapTools.GetDefaultContentType(cg), (is3g) ? HttpUtility.UrlEncode(String.Format("3g|HOME_{0}", name)) : HttpUtility.UrlEncode(String.Format("xhtml|HOME_{0}", name)), "", _idContentSet.ToString(), _mobile.MobileType, Convert.ToInt32(is2x1));
				
							
     
			TableItemStyle tableStyle = new TableItemStyle();
			try
			{
				int start = DateTime.Now.Millisecond;
				tableStyle.HorizontalAlign = HorizontalAlign.Center; 
				int previews = (_mobile.ScreenPixelsWidth < 140 && newContentset.ContentGroup != "ANIM") ? 1 : 2;            
				for( int i = start; i < start + previews; i++ )
				{
					Content content = newContentset.ContentCollection[ i % newContentset.Count];
					if (content != null)
					{
						if (contentset.ContentGroup == "IMG")
						{
							//if (content.ContentRatings[0].Value > 0)
							//	_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(this.Request, cg, WapTools.GetDefaultContentType(cg), (is3g) ? HttpUtility.UrlEncode(String.Format("3g|HOME_{0}", name)) : HttpUtility.UrlEncode(String.Format("xhtml|HOME_{0}", name)), "", _idContentSet.ToString(), _mobile.MobileType);
							//else
							//	_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, cg, WapTools.GetDefaultContentType(cg), (is3g) ? HttpUtility.UrlEncode(String.Format("3g|HOME_{0}", name)) : HttpUtility.UrlEncode(String.Format("xhtml|HOME_{0}", name)), "", _idContentSet.ToString());
						
							if (Convert.ToBoolean(WapTools.GetXmlValue("Home/Show_view")))
								_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, cg, WapTools.GetDefaultContentType(cg), (is3g) ? HttpUtility.UrlEncode(String.Format("3g|HOME_{0}", name)) : HttpUtility.UrlEncode(String.Format("xhtml|HOME_{0}", name)), "", _idContentSet.ToString());
							else
								_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(this.Request, cg, WapTools.GetDefaultContentType(cg), (is3g) ? HttpUtility.UrlEncode(String.Format("3g|HOME_{0}", name)) : HttpUtility.UrlEncode(String.Format("xhtml|HOME_{0}", name)), "", _idContentSet.ToString(), _mobile.MobileType, Convert.ToInt32(is2x1));		
						}

						XhtmlTableCell tempCell = new XhtmlTableCell();
						//XhtmlTableCell textCell = new XhtmlTableCell();
						ImgDisplay imgDisplay = new ImgDisplay(_imgDisplayInst);
						//imgDisplay.Display(tempCell, textCell, content);
						imgDisplay.Display(tempCell, content);
						imgDisplay = null;
						tempCell.ApplyStyle(tableStyle);
						//textCell.ApplyStyle(tableStyle);
						row.Cells.Add(tempCell); 
						//rowTexts.Cells.Add(textCell);		
						tempCell = null;
					} 
					content = null;
				}
				t.Rows.Add(row);
				row = null;
			}
			catch{}
		}
		private void DisplayContentSets(XhtmlTable table, ContentSet contentSet, int nbItems)
		{
			XhtmlTableRow row = null;
			if( page == -1 ) page = 1;
			if (WapTools.noPreview(contentSet.IDContentSet, _mobile.MobileType)) nbItems = -1;
			if( nbItems == -1 ) nbItems = contentSet.ContentCollection.Count;
			int index = (page - 1) * nbItems;

			if (contentSet.ContentGroup == "COMPOSITE") // COMPOSITES
			{
				if( contentSet.ContentCollection.Count > index )
					for(int i = index; i < index + nbItems; i++)
					{
						row = new XhtmlTableRow();
						if(contentSet.ContentCollection.Count <= i) break;
						
						if(contentSet.ContentCollection[i].Name.ToLower().IndexOf("sex") > -1 || contentSet.ContentCollection[i].Name.ToLower().IndexOf("hentai") > -1) continue;
							
						XhtmlTableCell cell = new XhtmlTableCell();
						//cell.BackColor = (i%2 == 0) ? ColorTranslator.FromHtml("#FFFFBE") : ColorTranslator.FromHtml("#FFFF00");
						//cell.CssClass = (i%2 == 0) ? "yellow3" : "yellow2";
						cell.HorizontalAlign = HorizontalAlign.Left;
						DisplayContent(contentSet.ContentCollection[i], cell);
						row.Controls.Add(cell);
						table.Controls.Add(row);
					}
				_hasPreviousPage = (page > 1);
				_hasNextPage = (contentSet.ContentCollection.Count > index + nbItems);
			}
		}
		#endregion
	}
}
