<?xml version="1.0" encoding="UTF-8" ?>
<%@ Register tagprefix="xhtml" Namespace="xhtml_v7.Tools" Assembly="xhtml_v7" %>
<%@ Page language="c#" Codebehind="download.aspx.cs" AutoEventWireup="false" Inherits="xhtml_v7.download" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>
  <head>
                    <title>Im&aacute;genes y Fondos</title>
                    <link rel="stylesheet" href="xhtml.css" type="text/css" />
                    <meta forua="true" http-equiv="Cache-Control" content="no-cache, max-age=0, must-revalidate, proxy-revalidate, s-maxage=0" />
  </head>
          <body>
                    <a id="start" name="start" />
                    <Xhtml:XhtmlTable id="tbHeader" Runat="server" CssClass="normal" />
                    <Xhtml:XhtmlTable id="tbContent" Runat="server" CssClass="normal" />
                    <hr />
                    <table width="100%">
						<tr>
							<td align="center"><a href="./catalog.aspx" style="color: #696969">Avatares</a></td>
						</tr>
					</table>
				<% if (is3g) { %>
					<table width="100%" bgcolor="#cde4fd">
						<tr>
							<td width="50%" align="left">
								<img src="<%=volver%>" alt=""/><b><a href="javascript:history.back()">Volver</a></b>
							</td>
							<td width="50%" align="right">
								<b><a class="right" href="#start">Subir</a></b><img src="<%=subir%>" alt=""/>
							</td>
						</tr>
					</table>
				<% } %>   
		</body>
</html>
