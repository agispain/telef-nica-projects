<?xml version="1.0" encoding="UTF-8" ?>
<%@ Register tagprefix="xhtml" Namespace="xhtml_v7.Tools" Assembly="xhtml_v7" %>
<%@ Page language="c#" Codebehind="step3.aspx.cs" AutoEventWireup="false" Inherits="xhtml_v7.muggins.step3" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>
  <head>
         <title>Im&aacute;genes y Fondos</title>
         <link rel="stylesheet" href="../xhtml.css" type="text/css" />
         <meta forua="true" http-equiv="Cache-Control" content="no-cache, max-age=0, must-revalidate, proxy-revalidate, s-maxage=0" />
  </head>
         <body>
                    <a id="start" name="start" />
					 <Xhtml:XhtmlTable id="tbHeader" Runat="server" CssClass="normal" />
					 <form action="download.aspx" method="post" accept-charset="utf-8">
					 
						<Xhtml:XhtmlTable id="tbAttitude" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowTitle" Runat="server" />
                    		<Xhtml:XhtmlTableRow id="rowAttitude" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>			
							<% if (sex == "BOY") {%>
								<td><select name="mood">
										<option value="santa" selected="selected">pap&aacute; noel</option>
										<option value="cool">guay</option>
										<option value="flirt">lig&oacute;n</option>
										<option value="furious">enfadado</option>
										<option value="happy">contento</option>
										<option value="skeleton">esqueleto</option>
										<option value="vampire">vampiro</option>
										<option value="devil">diablo</option>
										<option value="werewolf">hombre lobo</option>
										<option value="frankenstein">frankenstein</option>
									</select></td>										
							<% } else { %>
								<td><select name="mood">
										<option value="xmas" selected="selected">Mam&aacute; noel</option>
										<option value="elf">elfa</option>
										<option value="happy">contenta</option>
										<option value="fun">divertida</option>
										<option value="angry">enfadada</option>
										<option value="standard">normal</option>
										<option value="sexy">sexy</option>
										<option value="witch">bruja</option>
										<option value="zombie">zombi</option>
										<option value="devil">diablesa</option>
										<option value="skeleton">esqueleto</option>
										<option value="frankenstein">frankenstein</option>
										<option value="vampire">vampiresa</option>										
									</select></td>																	
							<% } %>
							</tr>
						</table>
						<Xhtml:XhtmlTable id="tbHairType" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowHairType" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
							<% if (sex == "BOY") {%>	
								<td><select name="hairStyle">
										<option value="Soldier" selected="selected">rapado</option>
										<option value="santa">pap&aacute; noel</option>
										<option value="Dread">rasta</option>
										<option value="Short">corto</option>
										<option value="Posh">con flequillo</option>
										<option value="Long">largo</option>
										<option value="Spiky">de punta</option>
										<option value="vampire">dr&aacute;cula</option>
										<option value="werewolf">hombre lobo</option>
										<option value="frankenstein">frankenstein</option>
										<option value="">sin pelo</option>
									</select></td>						
							<% } else { %>
								<td><select name="hairStyle">
										<option value="xmas" selected="selected">mam&aacute; noel</option>
										<option value="short">corto</option>
										<option value="fringe">largo con flequillo</option>
										<option value="curly">largo ondulado</option>
										<option value="flat">largo liso</option>
										<option value="medium">media melena</option>
										<option value="witch">bruja</option>
										<option value="devil">diablesa</option>
										<option value="frankenstein">frankenstein</option>
										<option value="skeleton">esqueleto</option>
										<option value="zombie">zombie</option>
										<option value="">sin pelo</option>
									</select></td>										
							<% } %>								
							</tr>
						</table>
						<Xhtml:XhtmlTable id="tbHair" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowHair" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
							<% if (sex == "BOY") {%>	
								<td><select name="hairColor">
										<option value="brown" selected="selected">marr&oacute;n oscuro</option>
										<option value="lBrown">marr&oacute;n claro</option>
										<option value="black">negro</option>
										<option value="blond">rubio</option>
										<option value="red">pelirrojo</option>										
									</select></td>				
							<% } else { %>
								<td><select name="hairColor">
										<option value="brown" selected="selected">marr&oacute;n oscuro</option>
										<option value="black">negro</option>
										<option value="red">pelirrojo</option>
										<option value="blond">rubio</option>
										<option value="violet">violeta</option>										
									</select></td>		
							<% } %>
							</tr>
						</table>						
						<Xhtml:XhtmlTable id="tbMuggin" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowJacket" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
							<% if (sex == "BOY") {%>	
								<td><select name="clothes">
										<option value="classic_jacket" selected="selected">chaqueta cl&aacute;sica</option>
										<option value="furry_hoodie">borreguillo</option>
										<option value="hoodie">sudadera</option>
										<option value="motor_jacket">chupa de motero</option>
										<option value="pullover">jersey</option>
										<option value="trench_coat">gabardina</option>
										<option value="turtleneck_jersey">jersey cisne</option>
										<option value="zipped_hoodie">chupa</option>
										<option value="tshirt">camiseta</option>
										<option value="vampire_blouse">chaquet&oacute;n</option>
										<option value="frankenstein_blouse">camiseta rota</option>
										<option value="shirt">camisa</option>
										<option value="sweatshirt">sudadera capucha</option>
										<option value="coat">abrigo</option>
										<option value="nadal">camiseta de Nadal</option>										
										<option value="">sin ropa</option>										
									</select></td>
							<% } else { %>
								<td><select name="clothes">
										<option value="xmas_costume" selected="selected">mam&aacute; noel</option>										
										<option value="elf_costume">vestido &eacute;lfico</option>										
										<option value="short_dress_stars">vestido corto</option>										
										<option value="evening_gown">vestido de noche</option>										
										<option value="waistcoat">chaleco</option>										
										<option value="top">top</option>										
										<option value="tanktop">top tirantes</option>										
										<option value="witch_dress">vestido bruja</option>
										<option value="dress_devil">vestido diablesa</option>
										<option value="frankenstein_dress">vestido frankenstein</option>
										<option value="vampire_gown">vestido vampiresa</option>
										<option value="zombie_dress">vestido zombie</option>										
										<option value="">sin ropa</option>										
									</select></td>								
							<% } %>
							</tr>
						</table>
						
						<Xhtml:XhtmlTable id="colorClothes" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowColorClothes" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
								<td><select name="colorClothes">
										<option value="black" selected="selected">negro</option>																		
										<option value="blue">azul</option>
										<option value="green">verde</option>		
										<option value="red">rojo</option>											
										<option value="yellow">amarillo</option>																				
								<% if (sex == "GIRL") {%>	
										<option value="white">blanco</option>
										<option value="purple">p&uacute;rpura</option>										
										<option value="brown">marr&oacute;n</option>								
								<% } %>		
								</select></td>																							
							</tr>
						</table>
						
						<Xhtml:XhtmlTable id="tbTrouser" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowTrouser" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
							<% if (sex == "BOY") {%>	
								<td><select name="trousers">
										<option value="skater_low" selected="selected">skater</option>
										<option value="scottish">escoceses</option>
										<option value="underwear">gallumbos</option>
										<option value="baggies">con bolsos</option>
										<option value="classics">cl&aacute;sicos</option>
										<option value="elegant">elegantes</option>
										<option value="velvet_striped">terciopelo</option>
										<option value="jeans">vaqueros</option>
										<option value="pleated">pinzas</option>
										<option value="sport">chandal</option>
										<option value="casual">casual</option>
										<option value="army">camuflaje</option>										
										<option value="nadal_shorts">pantal&oacute;n corto</option>										
										<option value="frankenstein_pants">frankenstein</option>										
										<option value="">sin pantal&oacute;n</option>										
									</select></td>								
							<% } else { %>
								<td><select name="trousers">
										<option value="skinny_jeans" selected="selected">pitillo</option>
										<option value="mini_leggings">minifalda</option>
										<option value="bell_jeans">vaqueros campana</option>
										<option value="">sin pantal&oacute;n</option>										
									</select></td>	
							<% } %>
								
							</tr>
						</table>
						<Xhtml:XhtmlTable id="tbColorTrousers" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowColorTrousers" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
									<td><select name="colorTrousers">
										<option value="black" selected="selected">negro</option>
										<option value="blue">azul</option>
										<option value="brown">marr&oacute;n</option>
										<option value="green">verde</option>
										<option value="white">blanco</option>																					
										<option value="red">rojo</option>										
							<% if (sex == "GIRL") {%>	
										<option value="yellow">amarillo</option>
										<option value="khaki">kaki</option>
										<option value="purple">p&uacute;rpura</option>
							<% } %>
									</select></td>	
							</tr>
						</table>
						<% if (sex == "GIRL") {%>	
						<Xhtml:XhtmlTable id="tbUnderwear" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowUnderwear" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
								<td><select name="underwear">
										<option value="leggins" selected="selected">medias</option>
										<option value="stripedleggins">medias rayadas</option>
										<option value="">sin nada</option>										
									</select></td>	
							</tr>
						</table>
						<Xhtml:XhtmlTable id="tbColorUnderwear" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowColorUnderwear" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
								<td><select name="colorUnderwear">
										<option value="yellow" selected="selected">amarillo</option>
										<option value="blue">azul</option>
										<option value="white">blanco</option>										
										<option value="khaki">kaki</option>										
										<option value="brown">marr&oacute;n</option>										
										<option value="black">negro</option>										
										<option value="purple">p&uacute;rpura</option>										
										<option value="red">rojo</option>										
										<option value="green">verde</option>										
									</select></td>	
							</tr>
						</table>
						<Xhtml:XhtmlTable id="tbEarrings" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowEarrings" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
								<td><select name="earrings">
										<option value="" selected="selected">sin pendientes</option>
										<option value="earrings_black">negros</option>
										<option value="earrings_purple">violetas</option>
										<option value="earrings_yellow">amarillos</option>
										<option value="earrings_xmas">navide&ntilde;os</option>
										<option value="earrings_elf">&eacute;lficos</option>
									</select></td>	
							</tr>
						</table>
						<Xhtml:XhtmlTable id="tbHat" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowHat" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
								<td><select name="hat">
										<option value="" selected="selected">sin sombrero</option>
										<option value="elf_hat">gorro &eacute;lfico</option>
										<option value="xmas_hat">navide&ntilde;o</option>
										<option value="hat">de bruja</option>
										<option value="pink_hat">gorra</option>
										<option value="woolcap">de lana</option>
									</select></td>	
							</tr>
						</table>
						<Xhtml:XhtmlTable id="tbNecklace" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowNecklace" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
								<td><select name="necklace">
										<option value="" selected="selected">sin colgante</option>
										<option value="neck_cross">en cruz</option>
										<option value="neck_heart">coraz&oacute;</option>
										<option value="elf_necklace">&eacute;lfico</option>
										<option value="snow_necklace">nieve</option>
									</select></td>	
							</tr>
						</table>
						<% } %>
						<Xhtml:XhtmlTable id="tbShoes" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowShoes" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
							<% if (sex == "BOY") {%>	
								<td><select name="shoes">
										<option value="boots" selected="selected">botas</option>
										<option value="classie">antiguos</option>
										<option value="sneakers2">deportivos</option>
										<option value="snowboard_boots">botas de nieve</option>
										<option value="sporty_boots">botas de f&uacute;tbol</option>
										<option value="casual">cl&aacute;sicos</option>
										<option value="all_stars">all stars</option>
										<option value="sneakers">modernos</option>										
										<option value="sport_shoes">deportivos</option>										
										<option value="">descalzo</option>																		
									</select></td>													
							<% } else { %>								
								<td><select name="shoes">
										<option value="elf_boots" selected="selected">botas &eacute;lficas</option>																		
										<option value="xmas_boots">botas navide&ntilde;as</option>																		
										<option value="all_stars">all stars</option>																		
										<option value="ballerina">bailarinas</option>																		
										<option value="booties">botines</option>																		
										<option value="beach_slippers">chanclas</option>																		
										<option value="high_heels">zapatos tac&oacute;n</option>																		
										<option value="witch_boots">botas de bruja</option>
										<option value="devil_boots">botas de diablesa</option>
										<option value="vampire_shoes">zapatos de vampiresa</option>										
										<option value="zombie_shoes">zapatos de zombi</option>										
										<option value="">descalza</option>																		
									</select></td>		
							<% } %>	
							</tr>
						</table>
						<Xhtml:XhtmlTable id="tbColorShoes" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowColorShoes" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>								
								<td><select name="colorShoes">
										<option value="yellow" selected="selected">amarillo</option>
										<option value="blue">azul</option>
										<option value="white">blanco</option>										
										<option value="brown">marr&oacute;n</option>										
										<option value="black">negro</option>																		
							<% if (sex == "GIRL") {%>	
										<option value="gold">dorado</option>
										<option value="silver">plateado</option>
										<option value="red">rojo</option>										
										<option value="green">verde</option>										
							<% } %>		
								</select></td>										
							</tr>
						</table>
						<Xhtml:XhtmlTable id="tbGlasses" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowGlasses" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
							<% if (sex == "BOY") {%>	
								<td><select name="glasses">
										<option value="noglasses" selected="selected">sin gafas</option>
										<option value="glasses">cl&aacute;sicas</option>
										<option value="glassessport">modernas</option>										
										<option value="sunglasses">de aviador</option>																																			
									</select></td>		
							<% } else { %>
								<td><select name="glasses">
										<option value="noglasses" selected="selected">sin gafas</option>
										<option value="gl_sun_glasses_blue">azul</option>
										<option value="gl_sun_glasses_orange">naranja</option>										
										<option value="gl_sun_glasses_dark">oscura</option>																																			
									</select></td>								
							<% } %>
							</tr>
						</table>
						<Xhtml:XhtmlTable id="tbOthers" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowOthers" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							
							<% if (sex == "BOY") {%>	
								<tr><td><input type="checkbox" name="others" value="santa" />barba</td></tr>
								<tr><td><input type="checkbox" name="others" value="gl_xmas_tree" />&aacute;rbol de navidad</td></tr>
								<tr><td><input type="checkbox" name="others" value="gl_penguin" />pinguino</td></tr>
								<tr><td><input type="checkbox" name="others" value="gl_polar_bear" />osito polar</td></tr>
								<tr><td><input type="checkbox" name="others" value="gl_snow_man" />mu&ntilde;eco de nieve</td></tr>
								<tr><td><input type="checkbox" name="others" value="keys" />llavero</td></tr>
								<tr><td><input type="checkbox" name="others" value="watch"/>reloj</td></tr>
								<tr><td><input type="checkbox" name="others" value="laptopbag"/>port&aacute;til</td></tr>
								<tr><td><input type="checkbox" name="others" value="shoulderbag"/>bolso</td></tr>
								<tr><td><input type="checkbox" name="others" value="earpierce"/>micr&oacute;fono</td></tr>
								<tr><td><input type="checkbox" name="others" value="hand_band"/>mu&ntilde;equera</td></tr>
							<% } else { %>
								<tr><td><input type="checkbox" name="others" value="gl_angry_black_cat"/>gato terror&iacute;fico</td></tr>
								<tr><td><input type="checkbox" name="others" value="gl_broom"/>escoba de bruja</td></tr>
								<tr><td><input type="checkbox" name="others" value="bangle"/>brazalete</td></tr>
								<tr><td><input type="checkbox" name="others" value="gl_hairpin"/>prendedor</td></tr>
								<tr><td><input type="checkbox" name="others" value="gl_pink_scarf"/>bufanda</td></tr>
								<tr><td><input type="checkbox" name="others" value="gl_palestina_scarf"/>palestino</td></tr>
								<tr><td><input type="checkbox" name="others" value="shoulder_bag_blue" />bolso azul</td></tr>
								<tr><td><input type="checkbox" name="others" value="gl_trident" />tridente</td></tr>
								<tr><td><input type="checkbox" name="others" value="gl_xmas_tree" />&aacute;rbol de navidad</td></tr>
								<tr><td><input type="checkbox" name="others" value="gl_penguin" />pinguino</td></tr>
								<tr><td><input type="checkbox" name="others" value="gl_polar_bear" />osito polar</td></tr>
								<tr><td><input type="checkbox" name="others" value="gl_reindeer_horns" />cuernos de reno</td></tr>
								<tr><td><input type="checkbox" name="others" value="gl_snow_man" />mu&ntilde;eco de nieve</td></tr>
								<tr><td><input type="checkbox" name="others" value="gl_xmas_hairpin" />prendedor navide&ntilde;o</td></tr>
								<tr><td><input type="checkbox" name="others" value="gl_xmas_gift" />regalo navide&ntilde;o</td></tr>
								<tr><td><input type="checkbox" name="others" value="bufanda" />bufanda de lana</td></tr>
								<tr><td><input type="checkbox" name="others" value="maxi_bag" />bolso</td></tr>
								<tr><td><input type="checkbox" name="others" value="shopping_bags" />bolsas de compras</td></tr>
								<tr><td><input type="checkbox" name="others" value="gloves" />guantes</td></tr>
							<% } %>
						</table>

						<input type="hidden" name="sex" value="<%=sex%>" />
						<input type="hidden" name="skin" value="<%=skin%>" />
						<input type="hidden" name="eyes" value="<%=eyes%>" />
						<input type="hidden" name="c" value="<%=c%>" />
						<br/><input type="submit" value="DALE VIDA" class="caja" />
					</form>
					<hr />
                   <table width="100%">
						<tr>
							<td align="center"><a href="./catalog.aspx" style="color: #696969">Volver</a></td>
						</tr>
					</table>
          </body>
</html>
