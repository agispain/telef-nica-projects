using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;

namespace xhtml_v7.muggins
{
	public class instruct : XCatalogBrowsing
	{
		protected XhtmlTable tbHeader, tbTitle, tbPreviews;
		protected XhtmlTableRow rowPreviews, rowPreviews1, rowPreviews2;
		public string volver, subir;
		protected int ms;
		protected string text = "", title, page = "";
		public bool is3g = false;

		private void Page_Load(object sender, System.EventArgs e)
		{
			_mobile = (MobileCaps)Request.Browser;
			try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType));}
			catch{is3g = false;}

			try
			{
				WapTools.SetHeader(this.Context);
				WapTools.AddUIDatLog(Request, Response, this.Trace);				
			} 
			catch{}

			try 
			{
				_contentType = (Request.QueryString["ct"] != null) ? Request.QueryString["ct"] : "";
				_contentGroup = WapTools.GetDefaultContentGroup(_contentType);
				try{_idContentSet = (Request.QueryString["cs"] != null) ? Convert.ToInt32(Request.QueryString["cs"]) : 0;}
				catch{_idContentSet = 0;}
				_displayKey = WapTools.GetXmlValue("DisplayKey");
	
				title = "Crea tu YO VIRTUAL del Mundial 2010";
				
				XhtmlLink link1 = new XhtmlLink();
				//if(_mobile.ScreenPixelsWidth > 202) 
					link1.ImageUrl = WapTools.GetImage(this.Request, "mundial");
				//else
				//	link1.ImageUrl = WapTools.GetImage(this.Request, "boy");
				
				link1.NavigateUrl = String.Format("catalog.aspx");
				XhtmlTableCell cell = new XhtmlTableCell();
				cell.HorizontalAlign = HorizontalAlign.Center;
				cell.Controls.Add(link1);
				rowPreviews1.Controls.Add(cell);
			//	XhtmlTools.AddImgTableRow(rowPreviews, bmimg1);
				/*XhtmlLink link2 = new XhtmlLink();
				if(_mobile.ScreenPixelsWidth > 202) 
					link2.ImageUrl = WapTools.GetImage(this.Request, "girl_big");
				else 
					link2.ImageUrl = WapTools.GetImage(this.Request, "girl");
				
				link2.NavigateUrl = String.Format("./catalog.aspx");
				cell = new XhtmlTableCell();
				cell.HorizontalAlign = HorizontalAlign.Center;
				cell.Controls.Add(link2);
				rowPreviews1.Controls.Add(cell);*/
				
				//XhtmlTools.AddLinkTableRow("", rowPreviews2, WapTools.GetText("Muggin"), String.Format("./catalog.aspx"), Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
				XhtmlTools.AddLinkTableRow("", rowPreviews2, WapTools.GetText("Muggin"), String.Format("./catalog.aspx"), Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
				text ="<p>&#161;Bienvenido al Incre&#237;ble Mundo de los Avatares del Mundial de F&uacute;tbol 2010! donde podr&#225;s crear tu YO VIRTUAL!</p>"+
						"<p>Anima a la roja. V&iacute;stete con los colores de tu selecci&oacute;n favorita: Espa&ntilde;a, Brasil, Italia, Argentina...</p>";
						
				XhtmlTools.AddTextTableRow("", rowPreviews, "", text, Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall);
				
				link1 = null;
				//link2 = null;
				
			}
			catch(Exception caught) 
			{
					WapTools.SendMail(HttpContext.Current, caught);
					Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\xhtml\\muggins\\instruct.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
					Response.Redirect("../error.aspx");	
			}
			
			#region HEADER
			XhtmlImage img = new XhtmlImage();
			img.ImageUrl = WapTools.GetImage(this.Request, "muggin",  _mobile.ScreenPixelsWidth, is3g);
			XhtmlTools.AddImgTable(tbHeader, img); 
			XhtmlTools.AddTextTable(tbHeader, title, Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, true, FontUnit.XXSmall);
		
			#endregion

			#region PICTOS
			if(is3g)
			{
				volver = WapTools.getPicto3g(this.Request.ApplicationPath, "volver", _mobile.ScreenPixelsWidth);
				subir = WapTools.getPicto3g(this.Request.ApplicationPath, "subir", _mobile.ScreenPixelsWidth);
			}
			_mobile = null;

			#endregion
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
