<?xml version="1.0" encoding="UTF-8" ?>
<%@ Register tagprefix="xhtml" Namespace="xhtml_v7.Tools" Assembly="xhtml_v7" %>
<%@ Page language="c#" Codebehind="catalog.aspx.cs" AutoEventWireup="false" Inherits="xhtml_v7.club.catalog" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>
	<head>
		<title>
			<%=title%>
		</title>
		<link rel="stylesheet" href="<%=css%>" type="text/css" />
		<meta forua="true" http-equiv="Cache-Control" content="no-cache, max-age=0, must-revalidate, proxy-revalidate, s-maxage=0" />
	</head>
	<body>
		<a id="start" name="start" />
		<Xhtml:XhtmlTable id="tbHeader" Runat="server" CssClass="normal" />
			<Xhtml:XhtmlTable id="tbTitle" Runat="server" CssClass="normal" />
			<Xhtml:XhtmlTable id="tbPreviews" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2">
				<Xhtml:XhtmlTableRow id="rowPreviews" Runat="server" />
				<Xhtml:XhtmlTableRow id="rowPreviews2" Runat="server" />
			</Xhtml:XhtmlTable>
			<Xhtml:XhtmlTable id="tbCatalog" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2" />
			<Xhtml:XhtmlTable id="tbPages" Runat="server" CssClass="normal" />
			<Xhtml:XhtmlTable id="tbLinks" Runat="server" CssClass="normal" />
			<Xhtml:XhtmlTable id="tbSearch" Runat="server" CssClass="normal" />
		
			<hr />
			<table width="100%">
				<tr>
					<td align="center"><a href="./default.aspx">Zona VIP de Fondos</a></td>
				</tr>
			</table>		
		
		
		<% if (is3g) { %>
		<table width="100%" class="up_back" bgcolor="#cccccc">
			<tr>
				<td width="50%" align="left">
					<img src="<%=volver%>" alt=""/><b><a href="./catalog.aspx?cs=6664&cg=COMPOSITE">Categor&iacute;as</a></b>
				</td>
				<td width="50%" align="right">
					<b><a class="right" href="#start">Subir</a></b><img src="<%=subir%>" alt=""/>
				</td>
			</tr>
		</table>			
		<% } %>		
	</body>
</html>
