using System;
using System.Collections;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Presentation;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;

namespace xhtml_v7
{	
	public class default_t : XCatalogBrowsing  
	{
		private ArrayList _contentCollImg = new ArrayList(); 
		private ArrayList _contentCollContentSet = new ArrayList();  
		protected XhtmlTable tbEspecial, tbPostales, /*tbAhora2, */tbEnd3, tbPub, tbPub2, tbCanales, tbTopNews, tbImages, tbShops, tbHeader, tbCategorias, tbLinkImage, tbLinkImage2, tbEnd, tbTop, tbNew, tbTitleShop, tbAhora;  
		protected XhtmlTableRow row1, row2, row4, especiales1, especiales2, especiales3,rowPostales, rowEspecial, rowImg, rowTitlesImg, rowPub, rowTop, rowNew, rowmoreshops, rowTitleShops, rowEspecial2, rowEspecial3, rowEspecial4;
		protected XhtmlTableCell /*cellLink, cellImg, cellLink2, cellImg2, cellLink3, cellImg3, cellLink4, cellImg4,*/ cellShop1, cellShop2, cellShop3, cellShop4, cellShop5, cellShop6;
		protected XhtmlTable tbTitleCanales, tbHeaderAhora, /*tbHeaderDestacados, */tbHeaderEnd, tbHeaderEnd2, tbEnd2, tbHeaderApuntante;
		
		protected Panel especial1, especial2, especial3, especial4, categorias, felicitaciones, descargate, portales, alertas;
		protected Panel moreportales, morealertas, moredescargate;
		//protected System.Web.UI.WebControls.Image headerAhora, headerDestacados, headerFelicitaciones, headerDescargate, headerPortales, headerAlertas;

		int init = 0, day; //, promo = -1; 
		public string valentine, christmas, yahoo, buscar, emocion, search, back, up, fondo, marquee, musica, p1, p2, p6, p4, todas, vip, zvip, club, flash, avatar, amor, fondos, sexy, imagenexpaise, morecategorias, zonavip, fondodedi, postanim, videofelicit, anim, videos, viptemas, temas, disney, disenalogos, alertasamor, alertasnew, header, f1, f2, f3, f4, f5, banner_footer, volver, subir, css = "touch.css";
		public bool is3g = false, isIE, isMobile240 = false, isBig = false, isYahoo = false;
		//private bool isPurchaser = false; 
		//private bool isOldUser = false;
		private bool is2x1 = false;
		private bool is099Purchaser = false;
		//CommandCollection cc = new CommandCollection();

	 
		private void Page_Load(object sender, System.EventArgs e)
		{ 
			try
			{
				search = ConfigurationSettings.AppSettings["UrlSearch"];
				_mobile = (MobileCaps)Request.Browser;
				if (WapTools.isSomething(_mobile.MobileType, "Android") || WapTools.isSomething(_mobile.MobileType, "Android16B"))
				{
					try{Response.Redirect("./default_a.aspx", false);}
					catch{Response.Redirect("./default_a.aspx", true);}
				} 
				isMobile240 = _mobile.ScreenPixelsWidth > 240;
				try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType));}
				catch{is3g = false;}

				Trace.Warn("is3G: " + is3g.ToString());
				
				try{isBig = Convert.ToBoolean(WapTools.isBig(_mobile.MobileType));}
				catch{isBig = false;}
				
				try
				{
					WapTools.SetHeader(this.Context);
					WapTools.LogUser(this.Request, 199, _mobile.MobileType);
					WapTools.AddUIDatLog(Request, Response, this.Trace);				
				} 
				catch{} 

				//if (WapTools.isTestSite(this.Request))
				//{
					isYahoo = true;		
					//if (isBig)
					//	yahoo = WapTools.GetImage(this.Request, "yahoo2");
					//else
						yahoo = WapTools.GetImage(this.Request, "yahoo");
				//}

				if (_mobile.MobileType != null) 
				{	 
					_displayKey = WapTools.GetXmlValue("DisplayKey");
					_idContentSet = Convert.ToInt32(WapTools.GetXmlValue("Home/Composite"));					
					day = (Request.QueryString["day"] != null) ? Convert.ToInt32(Request.QueryString["day"]) : 0;
					_contentGroup = "IMG";
					BrowseContentSetExtended(null, -1, -1); 

					try
					{
						Customer customer = new Customer(this.Request);
						is099Purchaser = (WapTools.isPurchaserClub099(customer) > 0);
					}
					catch{}

					if( _mobile.IsCompatible("IMG_COLOR") )
					{
						#region HEADER
						XhtmlImage img = new XhtmlImage();
						img.ImageUrl = WapTools.GetImage(this.Request, "imagenes",  _mobile.ScreenPixelsWidth, is3g);
						XhtmlTools.AddImgTable(tbHeader, img);
						if (is3g) tbHeader.Visible = false;
						#endregion
  
						#region PUB
						try
						{
							//	WapTools.CallNewPubTEF2(this.Trace, this.Request, this.Request.Headers["TM_user-id"], tbPub2, (is3g) ? "es.emocion_20.imagenesyfondos.home.bottom" : "es.emocion_10.imagenesyfondos.home.bottom");
							WapTools.CallNewPubTEF2(this.Trace, this.Request, this.Request.Headers["TM_user-id"], tbPub, (is3g) ? "es.emocion_20.imagenesyfondos.home.top" : "es.emocion_10.imagenesyfondos.home.top");
							WapTools.CallNewPubTEF2(this.Trace, this.Request, this.Request.Headers["TM_user-id"], tbPub2, (is3g) ? "es.emocion_20.imagenesyfondos.home.bottom" : "es.emocion_10.imagenesyfondos.home.bottom");
						}
						catch{}
						#endregion   
    
						#region PROMO
						/*if (isOldUser && !isPurchaser)
						{
							WapTools.LogUser(this.Request, 206, _mobile.MobileType);
					
							XhtmlTableRow rowPromo = new XhtmlTableRow();
							XhtmlTools.AddTextTableRow(rowPromo, WapTools.GetText("2x1"), Color.Empty, Color.Black, 1, HorizontalAlign.Center,  VerticalAlign.Middle, true, FontUnit.XSmall);
							tbHeaderAhora.Rows.Add(rowPromo);
						}*/
						try
						{
							Customer customer = new Customer(this.Request);
							int dwldsClubV = WapTools.isPurchaserClubV(customer);					
							Trace.Warn(dwldsClubV.ToString());
						}
						catch{}
						#endregion

						#region AHORA EN IMAGENES - ESPECIAL 

						//headerAhora.ImageUrl = WapTools.GetImage(this.Request, is3g ? "ahora_home" : "ahora",  _mobile.ScreenPixelsWidth, is3g);
						
						int dayCounter = Convert.ToInt32(WapTools.GetText("DayCounter"));
						int dia = WapTools.isTestSite(this.Request) ? (day != 0 && day <= 31 && day >= 1) ? day : DateTime.Now.AddDays(dayCounter).Day : DateTime.Now.Day;
						
						Especial esp1 = new Especial();
						Especial esp2 = new Especial();
						Especial esp3 = new Especial();
						Especial esp4 = new Especial();
						string picto1 = "", picto2 = "", picto3 = "", picto4 = "";
						try  
						{	
							esp1 = WapTools.getEspecial(1, dia.ToString(), _mobile);
							if(esp1.name == "" || !_mobile.IsCompatible(esp1.filter))
								esp1 = WapTools.GetCompatibleEspecial(esp1);

							//if (_mobile.ScreenPixelsWidth > 300)
								picto1 = String.Format("{0}/Images/specials/{1}", this.Request.ApplicationPath, esp1.name.ToLower() + "_110.gif");									
							//else
							//	picto1 = String.Format("{0}/Images/specials/{1}", this.Request.ApplicationPath, esp1.name.ToLower() + "_110.gif");									
							//picto = String.Format("{0}/Images/hellokitty.png", this.Request.ApplicationPath);									

							//XhtmlTools.addImgLinkTable(especiales1, picto, "#", picto, "#");
							//XhtmlTools.addLinkPanel(especial1, picto, WapTools.GetText(esp.name), WapTools.GetText("Link" + esp.name), true);							
						}
						catch{especial1.Visible = false;} 
						try 
						{
							dia = WapTools.isTestSite(this.Request) ? (day != 0 && day <= 31 && day >= 1) ? day : DateTime.Now.AddDays(dayCounter).Day : DateTime.Now.Day;
							esp2 = WapTools.getEspecial(2, dia.ToString(), _mobile);
							if (esp2.name == "" || !_mobile.IsCompatible(esp2.filter))
								esp2 = WapTools.GetCompatibleEspecial(esp2);	
							
							//if (_mobile.ScreenPixelsWidth > 300)
							//	picto2 = String.Format("{0}/Images/specials/{1}", this.Request.ApplicationPath, esp2.name.ToLower() + "_110.gif");									
							//else
								picto2 = String.Format("{0}/Images/specials/{1}", this.Request.ApplicationPath, esp2.name.ToLower() + "_110.gif");									
							
							//XhtmlTools.addImgLinkTable(especiales2, picto1, WapTools.GetText("Link" + esp1.name), picto2, WapTools.GetText("Link" + esp2.name));
							
							//XhtmlTools.addLinkPanel(especial2, picto, WapTools.GetText(esp.name), WapTools.GetText("Link" + esp.name), true);							
						}
						catch{especial2.Visible = false;}
						try
						{ 
							dia = WapTools.isTestSite(this.Request) ? (day != 0 && day <= 31 && day >= 1) ? day : DateTime.Now.AddDays(dayCounter).Day : DateTime.Now.Day;
							esp3 = WapTools.getEspecial(3, dia.ToString(), _mobile);
							if(esp3.name == "" || !_mobile.IsCompatible(esp3.filter))
								esp3 = WapTools.GetCompatibleEspecial(esp3);
							
							//if (_mobile.ScreenPixelsWidth > 300)
							//	picto1 = String.Format("{0}/Images/specials/{1}", this.Request.ApplicationPath, esp1.name.ToLower() + "_150.gif");									
							//else
								picto3 = String.Format("{0}/Images/specials/{1}", this.Request.ApplicationPath, esp3.name.ToLower() + "_110.gif");									
							//XhtmlTools.addImgLinkTable(especiales2, picto1, "#", picto1, "#");
							//XhtmlTools.addImgLinkTable(especiales3, picto1, WapTools.GetText("Link" + esp1.name), picto2, WapTools.GetText("Link" + esp1.name));
							//	XhtmlTools.addLinkTable(especial3, picto1, WapTools.GetText(esp1.name), WapTools.GetText("Link" + esp1.name), true);							
							//XhtmlTools.addImgLinkTable(especiales1, picto1, WapTools.GetText("Link" + esp1.name), picto2, WapTools.GetText("Link" + esp2.name), picto3, WapTools.GetText("Link" + esp3.name));
							
						}
						catch{especial3.Visible = false;}

						if ((_mobile.ScreenPixelsWidth > 320)) 
						{
							XhtmlTools.addImgLinkTable(especiales1, picto1, WapTools.GetText("Link" + esp1.name), picto2, WapTools.GetText("Link" + esp2.name), picto3, WapTools.GetText("Link" + esp3.name));
							especiales2.Visible = false;
						}
						else
						{
							
							try
							{ 
								dia = WapTools.isTestSite(this.Request) ? (day != 0 && day <= 31 && day >= 1) ? day : DateTime.Now.AddDays(dayCounter).Day : DateTime.Now.Day;
								esp4 = WapTools.getEspecial(4, dia.ToString(), _mobile);
								if(esp4.name == "" || !_mobile.IsCompatible(esp4.filter))
									esp4 = WapTools.GetCompatibleEspecial(esp4);
								Trace.Warn("esp4: " + esp4.name);
								picto4 = String.Format("{0}/Images/specials/{1}", this.Request.ApplicationPath, esp4.name.ToLower() + "_110.gif");									
							
							}
							catch{especial4.Visible = false;}
							XhtmlTools.addImgLinkTable(especiales1, picto1, WapTools.GetText("Link" + esp1.name), picto2, WapTools.GetText("Link" + esp2.name));
							XhtmlTools.addImgLinkTable(especiales2, picto3, WapTools.GetText("Link" + esp3.name), picto4, WapTools.GetText("Link" + esp4.name));
						
						}
						
						#endregion 
						
						#region IMAGENES DESTACADAS

						//headerDestacados.ImageUrl = WapTools.GetImage(this.Request, is3g ? "destacados_home" : "destacados",  _mobile.ScreenPixelsWidth, is3g);
						christmas = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "christmas.gif");
						valentine = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "valentine.gif");
						todas = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "todo-0.99.gif");
						vip = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "zonaviptemas.gif");
						zvip = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "12-Zona-Vip.gif");
						club = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "club-videos.gif");
						flash = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "fondos-flash.gif");
						avatar = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "avatares.gif");
						fondos = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "topfondos.gif");
						amor = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "amor.gif");
						sexy = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "sexy.gif");
						imagenexpaise = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "imgs-por-paises.gif");
						morecategorias = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "categorias.gif");
						zonavip = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "zonavipfelicitaciones.gif");
						fondodedi = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "fondodedicatoria.gif");
						postanim = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "postalesanimadas.gif");
						videofelicit = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "videofelicitaciones.gif");
						anim = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "animaciones.gif");
						videos = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "videos.gif");
						viptemas = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "zonaviptemas.gif");
						temas = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "temas.gif");
						disney = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "disney.gif");
						disenalogos = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "disenatuslogosdeluxe.gif");
						//fun4phone = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "fun4phone.gif");
						//barriosemao = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "barriosesamo.gif");
						alertasamor = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "alertasamor.gif");
						alertasnew = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "alertasnovedades.gif");
						try      
						{ 
							init = DateTime.Now.Millisecond;			
							int value = Convert.ToInt32(WapTools.GetXmlValue("Home/Subcomposite"));
							if (_contentCollImg.Count > 0)
							{
								DisplayImages(row1, "IMG", "IMG_COLOR", (init % value), 3);
								//if (_mobile.IsXHTML)
								//{
								int c = value + (init % (_contentCollImg.Count - 3 - value));
								DisplayImages(row2, "IMG", "IMG_COLOR", c, 3);
								/*try
									{
										DisplayImages(row3, "IMG", "IMG_COLOR", c+3, 4);
									}
									catch{}*/
								//}
							}  
						}
						catch{}
 
						//	XhtmlTools.AddLinkTable("IMG", tbTop, "Galer�a", "http://emocion.kiwee.com/xhtml/galeria/catalog.aspx?cg=IMG&cs=2692", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						//	else 
						
						//XhtmlTools.AddLinkTableRow("IMG", rowNew,WapTools.GetText("NewImg"), String.Format("./linkto.aspx?cg=IMG&id={0}", WapTools.GetXmlValue("Home/New")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						 
						//XhtmlTools.AddLinkTableRow("IMG", rowRec, WapTools.GetText("Recomendados"), "#", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "movblue"));
						#endregion  

						#region POR CATEGORIAS 
				
						try
						{	
							/*XhtmlTools.addLinkPanel(categorias, WapTools.GetImage(this.Request, "club"), WapTools.GetText("club"), "./linkto.aspx?id=11036", true);
							// FLASH
							if (_mobile.IsCompatible("FLASH_SCREENSAVER")) {
								XhtmlTools.addLinkPanel(categorias, WapTools.GetImage(this.Request, "bullet"), WapTools.GetText("FondosFlash"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FLASH")));
							}
							XhtmlTools.addLinkPanel(categorias, WapTools.GetImage(this.Request, "muggins"), WapTools.GetText("avatar"), "./linkto.aspx?id=11040");
							XhtmlTools.addLinkPanel(categorias, WapTools.GetImage(this.Request, "bullet"), WapTools.GetText("TopImg"), String.Format("./linkto.aspx?cg=IMG&id={0}", WapTools.GetXmlValue("Home/Top")));
							XhtmlTools.addLinkPanel(categorias, WapTools.GetImage(this.Request, "bullet"), WapTools.GetText("AmorImg"), String.Format("./linkto.aspx?cg=IMG&id={0}", WapTools.GetXmlValue("Home/Amor")));
							XhtmlTools.addLinkPanel(categorias, WapTools.GetImage(this.Request, "bullet"), WapTools.GetText("ModelsImg"), String.Format("./linkto.aspx?cg=IMG&id={0}", WapTools.GetXmlValue("Home/Models")));
							XhtmlTools.addLinkPanel(categorias, WapTools.GetImage(this.Request, "bullet"), WapTools.GetText("Gigigo"), String.Format("./linkto.aspx?cg=IMG&id={0}", WapTools.GetXmlValue("Home/GIGIGO")));
							*/
							//	XhtmlTools.addLinkPanel(morecategorias, "", WapTools.GetText("Categorias"), "./linkto.aspx?cg=COMPOSITE&id=3619", true);
						}
						catch{}

						
						#endregion
				  
						#region FELICITACIONES
					
						//headerFelicitaciones.ImageUrl = WapTools.GetImage(this.Request,  "felicitaciones",  _mobile.ScreenPixelsWidth, is3g);
							
						//						XhtmlTools.addLinkPanel(felicitaciones, "", WapTools.GetText("ZonaVipFelicitaciones"), "./linkto.aspx?id=10093");
						//						XhtmlTools.addLinkPanel(felicitaciones, "", WapTools.GetText("FondoDedicatorias"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FONDODEDICATORIAS")));
						//						XhtmlTools.addLinkPanel(felicitaciones, "", WapTools.GetText("Animadas"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/ANIMADAS")));
						//						XhtmlTools.addLinkPanel(felicitaciones, "", WapTools.GetText("Videofelicitaciones"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEOFELICITACIONES")));
						
						#endregion	

						#region DESCARGATE
						//headerDescargate.ImageUrl = WapTools.GetImage(this.Request, is3g ? "descargate_home" : "descargate",  _mobile.ScreenPixelsWidth, is3g);
						
						/*
						if (WapTools.isCompatibleThemes(_mobile)) 
						{
							XhtmlTools.addLinkPanel(descargate, "", WapTools.GetText("VipTemas"), "./linkto.aspx?id=10096");
							XhtmlTools.addLinkPanel(descargate, "", WapTools.GetText("Temas"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/TEMAS")));
						}
						XhtmlTools.addLinkPanel(descargate, "", WapTools.GetText("ANIM"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/ANIM")));
						XhtmlTools.addLinkPanel(descargate, "", WapTools.GetText("VIDEO"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEO")));
						XhtmlTools.addLinkPanel(descargate, "", WapTools.GetText("FondoNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FONDONOMBRES")));

						*/
						string urlPicto = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "10.+imagenes.gif");
						XhtmlTools.addLinkPanel(moredescargate, urlPicto, "", "./linkto.aspx?id=27", false);
						//	XhtmlTools.addLinkPanel(moredescargate, urlPicto, WapTools.GetText("MasDESCARGATE"), "./linkto.aspx?id=27", true);
						#endregion						

						#region SHOPS y ALERTAS
						//headerPortales.ImageUrl = WapTools.GetImage(this.Request, is3g ? "portales_home" : "portales",  _mobile.ScreenPixelsWidth, is3g);
								
						/*
						XhtmlTools.addLinkPanel(portales, "", WapTools.GetText("Shop18"), "./linkto.aspx?id=18");
						XhtmlTools.addLinkPanel(portales, "", WapTools.GetText("Shop33"), "./linkto.aspx?id=33");
						XhtmlTools.addLinkPanel(portales, "", WapTools.GetText("Shop19"), "./linkto.aspx?id=19");
						XhtmlTools.addLinkPanel(portales, "", WapTools.GetText("Shop30"), "./linkto.aspx?id=30");
						*/
						urlPicto = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "11.+portales.gif");
						XhtmlTools.addLinkPanel(moreportales, urlPicto, "", "./linkto.aspx?id=20", false);
						

						//headerAlertas.ImageUrl = WapTools.GetImage(this.Request, is3g ? "apuntate_home" : "apuntate",  _mobile.ScreenPixelsWidth, is3g);
								
						//XhtmlTools.addLinkPanel(alertas, "", WapTools.GetText("Alerta6"), "./linkto.aspx?id=28");
						//XhtmlTools.addLinkPanel(alertas, "", WapTools.GetText("Alerta7"), "./linkto.aspx?id=29");
						urlPicto = String.Format("{0}/Images/touch/{1}", this.Request.ApplicationPath, "11.+alertas.gif");
						XhtmlTools.addLinkPanel(morealertas, urlPicto, "", "./linkto.aspx?id=26", false);
						//	XhtmlTools.addLinkPanel(morealertas, urlPicto, "M�s alertas de Im�genes", "./linkto.aspx?id=26", true);
						
						
						//XhtmlTools.AddTextTable(tbEnd3, WapTools.GetText("NewAlertas"), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall);
						
						#endregion

						#region PICTOS
						fondo = WapTools.GetImage(this.Request, "fondo", _mobile.ScreenPixelsWidth, false);
						buscar = WapTools.getPicto(this.Request, "buscar", _mobile);
						emocion = WapTools.getPicto(this.Request, "emocion", _mobile);
						back = WapTools.getPicto(this.Request, "back", _mobile);
						up = WapTools.getPicto(this.Request, "up", _mobile);
						musica = (_mobile.ScreenPixelsWidth > 128) ? "M&uacute;sica y Tonos" : "M&uacute;sica";
						#endregion 

						#region 3G
						//if (is3g)
						//{
							css = "touch.css";
							p1 = WapTools.getPicto3g(this.Request.ApplicationPath, "p1", _mobile.ScreenPixelsWidth);
							p2 = WapTools.getPicto3g(this.Request.ApplicationPath, "p2", _mobile.ScreenPixelsWidth);
							p6 = WapTools.getPicto3g(this.Request.ApplicationPath, "p6", _mobile.ScreenPixelsWidth);
							p4 = WapTools.getPicto3g(this.Request.ApplicationPath, "p4", _mobile.ScreenPixelsWidth);
							//							f1 = WapTools.getPicto3g(this.Request.ApplicationPath, "f1", _mobile.ScreenPixelsWidth);
							//							f2 = WapTools.getPicto3g(this.Request.ApplicationPath, "f2", _mobile.ScreenPixelsWidth);
							//							f3 = WapTools.getPicto3g(this.Request.ApplicationPath, "f3", _mobile.ScreenPixelsWidth);
							//							f4 = WapTools.getPicto3g(this.Request.ApplicationPath, "f4", _mobile.ScreenPixelsWidth);
							//							f5 = WapTools.getPicto3g(this.Request.ApplicationPath, "f5", _mobile.ScreenPixelsWidth);
							header = WapTools.GetImage3g(this.Request, "imagenes",  _mobile.ScreenPixelsWidth);	
							Trace.Warn(_mobile.MobileType);
							/*if (_mobile.MobileType == "HUAWEIIVY")
							{
								header = "http://emocion.dev.kiwee.com/xhtml/Images/450/imagenes.gif";
							}*/
							//banner_footer = WapTools.getPicto3g(this.Request.ApplicationPath, "footer",  _mobile.ScreenPixelsWidth);		
							volver = WapTools.getPicto3g(this.Request.ApplicationPath, "volver", _mobile.ScreenPixelsWidth);
							subir = WapTools.getPicto3g(this.Request.ApplicationPath, "subir", _mobile.ScreenPixelsWidth);			
						//}
						#endregion
					}
				}
				else
				{
					XhtmlTableRow row = new XhtmlTableRow();
					XhtmlTools.AddTextTableRow(row, WapTools.GetText("Compatibility2"), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XSmall);
					tbEnd.Rows.Add(row);
					row = null;
				}
			}
			catch(Exception caught)
			{  
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\xhtml\\default.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
				Response.Redirect("./error.aspx");				
			}
			finally
			{
				_contentCollImg = null;
				_contentCollContentSet = null;
			}
		}


		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
		
		#region Override
		protected override void DisplayContentSet(Content content, System.Web.UI.MobileControls.Panel pnl)
		{
			_contentCollContentSet.Add(content);
		}

		protected override void DisplayImg(Content content, System.Web.UI.MobileControls.Panel pnl)
		{
			//if (WapTools.isJunior(_mobile) && content.ContentRatings[0].Value > 0) return;
			/*if (cc != null && cc.Count > 0)
			{
				foreach (Command c in cc)
					if (c.Item.ContentId == content.IDContent)
						return;
			}*/
			if (content.ContentGroup.Name == "IMG") _contentCollImg.Add(content);
		}
		#endregion

		#region Display
		public void DisplayImages(TableRow row, string contentGroup, string contentType, int start, int nb)
		{			
			Content content = null;
			_imgDisplayInst = new ImgDisplayInstructions(_mobile);

			_imgDisplayInst.PreviewMaskUrl = ( nb < 3 || _mobile.IsXHTML) ?
				WapTools.GetXmlValue(WapTools.PreviewUrl(contentGroup, _mobile)) : 
				WapTools.GetXmlValue(WapTools.PreviewUrlSmall(contentGroup, _mobile));

			_imgDisplayInst.TextDwld = WapTools.GetText("Download");
			_imgDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "Img");

			if (/*WapTools.isTestSite(this.Request) ||*/ is099Purchaser)
				_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingClub099(this.Request, contentGroup, contentType, HttpUtility.UrlEncode("CLUB099|HOME"), "", "0");
			else if (Convert.ToBoolean(WapTools.GetXmlValue("Home/Show_view")))
				_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, contentGroup, contentType, (is3g) ? HttpUtility.UrlEncode("3g|HOME") : HttpUtility.UrlEncode("xhtml|HOME"), "", "0");
			else
				_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(this.Request, contentGroup, contentType, (is3g) ? HttpUtility.UrlEncode("3g|HOME") : HttpUtility.UrlEncode("xhtml|HOME"), "", "0", _mobile.MobileType, Convert.ToInt32(is2x1)); 
			Trace.Warn(_imgDisplayInst.UrlDwld + "-" + Convert.ToInt32(is2x1).ToString());				
               
			TableItemStyle tableStyle = new TableItemStyle();
			tableStyle.CssClass="cell";
			//tableStyle.HorizontalAlign = HorizontalAlign.Center;
			         
			for( int i = start; i < start + nb; i++ )  
			{
				if (contentGroup == "IMG")
					content = (Content)_contentCollImg[ (i) % _contentCollImg.Count];

				if (content != null)  
				{
					//					if (content.ContentRatings[0].Value > 0)
					//						_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(this.Request, contentGroup, contentType, (is3g) ? HttpUtility.UrlEncode("3g|HOME") : HttpUtility.UrlEncode("xhtml|HOME"), "", "0", _mobile.MobileType);
					//					else
					//						_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, contentGroup, contentType, (is3g) ? HttpUtility.UrlEncode("3g|HOME") : HttpUtility.UrlEncode("xhtml|HOME"), "", "0");
			
					XhtmlTableCell tempCell = new XhtmlTableCell();
					ImgDisplay imgDisplay = new ImgDisplay(_imgDisplayInst);
					imgDisplay.Display(tempCell, content);
					imgDisplay = null;
					tempCell.ApplyStyle(tableStyle);
					row.Cells.Add(tempCell);
					tempCell = null;
				}
			}  
			content = null;
			_imgDisplayInst = null;
			tableStyle = null;
		}          
        
		public void DisplayTitles(Table tb, int start)
		{			
			Content content = null;
			_imgDisplayInst = new ImgDisplayInstructions(_mobile);
			_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(WapTools.PreviewUrl("IMG", _mobile));
			_imgDisplayInst.TextDwld = WapTools.GetText("Download");
			_imgDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "down8");

			if (/*WapTools.isTestSite(this.Request) ||*/ is099Purchaser)
				_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingClub099(this.Request, "IMG", "IMG_COLOR", HttpUtility.UrlEncode("CLUB099|LINK"), "", "0");
			else if (Convert.ToBoolean(WapTools.GetXmlValue("Home/Show_view")))
				_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, "IMG", "IMG_COLOR", (is3g) ? HttpUtility.UrlEncode("3g|LINK") : HttpUtility.UrlEncode("xhtml|LINK"), "", "0");
			else
				_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(this.Request, "IMG", "IMG_COLOR", (is3g) ? HttpUtility.UrlEncode("3g|LINK") : HttpUtility.UrlEncode("xhtml|LINK"), "", "0", _mobile.MobileType, Convert.ToInt32(is2x1));
							               
			TableItemStyle tableStyle = new TableItemStyle();
			tableStyle.CssClass = "cell";
			//tableStyle.HorizontalAlign = HorizontalAlign.Center;
			XhtmlTableRow tempRow = new XhtmlTableRow();

			for( int i = start; i < start + 3; i++ )  
			{
				content = (Content)_contentCollImg[ (i) % _contentCollImg.Count];

				if (content != null)  
				{
					if (((i-start)%2) == 0 || (_mobile.ScreenPixelsWidth < 140))
						tempRow = new XhtmlTableRow();
					XhtmlTableCell tempCell = new XhtmlTableCell();
					ImgDisplay imgDisplay = new ImgDisplay(_imgDisplayInst);
					imgDisplay.Display(tempCell, content, false);
					imgDisplay = null;
					tempCell.ApplyStyle(tableStyle);
					tempRow.Cells.Add(tempCell);
					if (((i-start)%2) != 0 || (_mobile.ScreenPixelsWidth < 140))
						tb.Rows.Add(tempRow);
				}
			}  
			content = null;
			_imgDisplayInst = null;
			tableStyle = null;
		}                     


		public void DisplayContentSets(XhtmlTable t, string cg, int rangeInf, int rangeSup)
		{
			_contentSetDisplayInst = new ContentSetDisplayInstructions(_mobile);
			_contentSetDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "bullet");
			_contentSetDisplayInst.UrlDwld = "./linkto.aspx?id={0}&cg={1}";
			XhtmlTableCell cell = new XhtmlTableCell();
			XhtmlTableRow row = new XhtmlTableRow();
			if (rangeSup == -1) rangeSup = _contentCollContentSet.Count;
			if (rangeSup > _contentCollContentSet.Count) rangeSup = _contentCollContentSet.Count;
			for( int i = rangeInf; i < rangeSup; i++ )
			{
				Content content = (Content)_contentCollContentSet[i];
				//if (WapTools.FindProperty(content.PropertyCollection, "CompositeContentGroup") != cg) continue;
				ContentSetDisplay contentSetDisplay = new ContentSetDisplay(_contentSetDisplayInst);
				contentSetDisplay.Display(cell, content, true);			
				contentSetDisplay = null;
				row.Controls.Add(cell);
				cell = new XhtmlTableCell();
				t.Controls.Add(row);
				row = new XhtmlTableRow();
				content = null;
			}
			//t.Controls.Add(row);
			_contentSetDisplayInst = null;
			cell = null; row = null;
		}
		#endregion
	}
}
