<?xml version="1.0" encoding="UTF-8" ?>
<%@ Register tagprefix="xhtml" Namespace="xhtml_v7.Tools" Assembly="xhtml_v7" %>
<%@ Page language="c#" Codebehind="animation.aspx.cs" AutoEventWireup="false" Inherits="xhtml_v7.animation" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>
  <head>
                    <title>Im&aacute;genes y Fondos</title>
                    <link rel="stylesheet" href="xhtml.css" type="text/css" />
                     <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
                    <meta forua="true" http-equiv="Cache-Control" content="no-cache, max-age=0, must-revalidate, proxy-revalidate, s-maxage=0" />
  </head>
          <body>
                    <a id="start" name="start" />
                    <Xhtml:XhtmlTable id="tbHeader" Runat="server" CssClass="normal" />
                    <div class="download_this">
                     <td width="50%" align="center">
					 <a href="#" onclick="goToDownloadPage();">
					 <span><img  align="middle" src="<%=downloadButtonUrl%>"  alt="" /></span>
					 </a>
					 </td>
					 </tr>
					 </table>
					 <Xhtml:XhtmlTable id="tbContent" Runat="server" CssClass="normal" />
					 </div>
                   
                    <hr />
                    <table width="100%">
						<tr>
							<td align="center"><a href="./default.aspx" style="color: #696969">Im&aacute;genes y Fondos</a></td>
						</tr>
					</table>
				<% if (is3g) { %>
					<table width="100%" bgcolor="#cde4fd">
						<tr>
							<td width="50%" align="left">
								<img src="<%=volver%>" alt=""/><b><a href="javascript:history.back()">Volver</a></b>
							</td>
							<td width="50%" align="right">
								<b><a class="right" href="#start">Subir</a></b><img src="<%=subir%>" alt=""/>
							</td>
						</tr>
					</table>	
				<% } %>   
				<script type="text/javascript">
				
				function goToDownloadPage()
{
       window.setTimeout(function () {$('.download_this').html('<h2 style="font-weight:normal;text-align:center;padding:20">La descarga del contenido ha comenzado de forma autom&aacute;tica.De todas forma, recuedra que si tienes alg &uacute;n problema puedes a llamar gratis al 1004 para resolverlo!</h2>');}, 1000);
       window.parent.location.href ="download.aspx";
       
}
</script>			
		</body>
</html>
