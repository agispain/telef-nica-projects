using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;
using System.Reflection;
using System.Net;
using System.IO;

namespace xhtml_v7
{
	/// <summary>
	/// Summary description for download1.
	/// </summary>
	public class download1 : XCatalogBrowsing
	{
		public string fileName;
		public string name;
		private void Page_Load(object sender, System.EventArgs e)
		{
			fileName = "attachment; filename=";
            name=(string)(Session["fileName"]);
			byte[] imageBytes = (byte[])(Session["contentByte"]);           
			string   extension = (string)(Session["extension"]);
			fileName = fileName+name;
			Trace.Warn(fileName);
			Response.BufferOutput = true;
			Response.Buffer = false;
			Response.ContentType = "*/*";
			Response.AppendHeader("Content-Disposition",fileName);
			Response.OutputStream.Write(imageBytes, 0, imageBytes.Length);
			Response.End();

			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
