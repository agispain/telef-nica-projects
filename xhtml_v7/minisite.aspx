<?xml version="1.0" encoding="UTF-8" ?>
<%@ Register tagprefix="xhtml" Namespace="xhtml_v7.Tools" Assembly="xhtml_v7" %>
<%@ Page language="c#" Codebehind="minisite.aspx.cs" AutoEventWireup="false" Inherits="xhtml_v7.minisite" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>
	<head>
		<title>
			<%= title %>
		</title>
		<link rel="stylesheet" href="<%=css%>" type="text/css" />
			<meta forua="true" http-equiv="Cache-Control" content="no-cache, max-age=0, must-revalidate, proxy-revalidate, s-maxage=0" />
	</head>
	<body>
			<xhtml:XhtmlTable id="tbHeader" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />
			<xhtml:XhtmlTable id="tbHeaderDestacados" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />
			<marquee><p><b><font color="#FF0000"><%=textMarquee%></font></b></p></marquee>
			<xhtml:XhtmlTable id="tbImages" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2">
				<xhtml:XhtmlTableRow Runat="server" ID="rowImages" />
				<xhtml:XhtmlTableRow Runat="server" ID="rowImg" />
				<xhtml:XhtmlTableRow Runat="server" ID="rowTitlesImg" />
			</xhtml:XhtmlTable>
			<xhtml:XhtmlTable id="tbTop" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2"></xhtml:XhtmlTable>
			<xhtml:XhtmlTable id="tbHeader4" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />
			<xhtml:XhtmlTable id="tbThemes" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2">
				<xhtml:XhtmlTableRow Runat="server" ID="rowTemas" />
			</xhtml:XhtmlTable>
			<xhtml:XhtmlTable id="tbHeader2" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />
			<xhtml:XhtmlTable id="tbAnims" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2">
				<xhtml:XhtmlTableRow Runat="server" ID="rowAnims" />
			</xhtml:XhtmlTable>
			<xhtml:XhtmlTable id="tbHeader3" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />
			<xhtml:XhtmlTable id="tbVideos" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2">
				<xhtml:XhtmlTableRow Runat="server" ID="rowVideos" />
			</xhtml:XhtmlTable>
			<xhtml:XhtmlTable id="tbTemas" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2" />
			<xhtml:XhtmlTable id="tbHeader5" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />
			<xhtml:XhtmlTable id="tbFlash" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2">
				<xhtml:XhtmlTableRow Runat="server" ID="rowFlash" />
			</xhtml:XhtmlTable>
			<xhtml:XhtmlTable id="tbLinks" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2" />
			
					<hr />
			<table width="100%">
				<tr>
					<td align="center"><a href="./default.aspx">Im&aacute;genes y Fondos</a></td>
				</tr>
			</table>			
	</body>
</html>
