<?xml version="1.0" encoding="UTF-8" ?>
<%@ Register tagprefix="xhtml" Namespace="xhtml_v7.Tools" Assembly="xhtml_v7" %>
<%@ Page language="c#" Codebehind="muggins2.aspx.cs" AutoEventWireup="false" Inherits="xhtml_v7.muggins2" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>
  <head>
                    <title>Im&aacute;genes y Fondos</title>
                    <link rel="stylesheet" href="xhtml.css" type="text/css" />
                    <meta forua="true" http-equiv="Cache-Control" content="no-cache, max-age=0, must-revalidate, proxy-revalidate, s-maxage=0" />
  </head>
          <body>
                    <a id="start" name="start" />
                    <Xhtml:XhtmlTable id="tbHeader" Runat="server" CssClass="normal" />
					<Xhtml:XhtmlTable id="tbTitle" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2">
						<Xhtml:XhtmlTableRow id="rowPreviews1" Runat="server" />
						<Xhtml:XhtmlTableRow id="rowPreviews2" Runat="server" />
						<Xhtml:XhtmlTableRow id="rowPreviews3" Runat="server" />
						<Xhtml:XhtmlTableRow id="rowPreviews4" Runat="server" />
						<Xhtml:XhtmlTableRow id="rowPreviews5" Runat="server" />
						<Xhtml:XhtmlTableRow id="rowPreviews6" Runat="server" />
						<Xhtml:XhtmlTableRow id="rowPreviews7" Runat="server" />
						<Xhtml:XhtmlTableRow id="rowPreviews8" Runat="server" />
						<Xhtml:XhtmlTableRow id="rowPreviews9" Runat="server" />
						<Xhtml:XhtmlTableRow id="rowPreviews10" Runat="server" />
						<Xhtml:XhtmlTableRow id="rowPreviews11" Runat="server" />
						<Xhtml:XhtmlTableRow id="rowPreviews12" Runat="server" />
					</Xhtml:XhtmlTable>
					<Xhtml:XhtmlTable id="tbPreviews" Runat="server" CssClass="normal" >
						<Xhtml:XhtmlTableRow id="rowPreviews" Runat="server" />		
					</Xhtml:XhtmlTable>
                    <hr />
                    <table width="100%">
						<tr>
							<td align="center"><a href="./default.aspx" style="color: #696969">Im&aacute;genes y Fondos</a></td>
						</tr>
					</table>
					<% if (is3g) { %>
					<table width="100%" bgcolor="#cde4fd">
						<tr>
							<td width="50%" align="left">
								<img src="<%=volver%>" alt=""/><b><a href="javascript:history.back()">Volver</a></b>
							</td>
							<td width="50%" align="right">
								<b><a class="right" href="#start">Subir</a></b><img src="<%=subir%>" alt=""/>
							</td>
						</tr>
					</table>
				<% } %>	     
		</body>
</html>

