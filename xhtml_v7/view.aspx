<?xml version="1.0" encoding="UTF-8" ?>
<%@ Register tagprefix="xhtml" Namespace="xhtml_v7.Tools" Assembly="xhtml_v7" %>
<%@ Page language="c#" Codebehind="view.aspx.cs" AutoEventWireup="false" Inherits="xhtml_v7.view" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>
          <head>
                    <title>Im&aacute;genes</title>
                    <link rel="stylesheet" href="./xhtml.css" type="text/css" />
                    <meta forua="true" http-equiv="Cache-Control" content="no-cache, max-age=0, must-revalidate, proxy-revalidate, s-maxage=0" />
          </head>
          <body>
                     <a id="start" name="start" />
                    <Xhtml:XhtmlTable id="tbHeader" Runat="server" CssClass="normal" />
                    <Xhtml:XhtmlTable id="tbTitle" Runat="server" CssClass="normal" />
                    <Xhtml:XhtmlTable id="tbPreviews" Runat="server" CssClass="normal">
						<Xhtml:XhtmlTableRow id="rowPreview" Runat="server" />
						<Xhtml:XhtmlTableRow id="rowClub1" Runat="server" />
						<Xhtml:XhtmlTableRow id="rowClub2" Runat="server" />
						<Xhtml:XhtmlTableRow id="rowLinkPPD" Runat="server" />
                    </Xhtml:XhtmlTable>

                    <Xhtml:XhtmlTable id="tbContentset" Runat="server" CssClass="normal"/>
                    <Xhtml:XhtmlTable id="tbLinks" Runat="server" CssClass="normal"/>

                    <hr />
                    <table width="100%">
						<tr>
							<td align="center"><a href="./default.aspx" style="color: #696969">Im&aacute;genes y Fondos</a></td>
						</tr>
					</table>                                     
          </body>
</html>
