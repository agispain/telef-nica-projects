﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Drawing;
using AGInteractive.Business;
using KMobile.Catalog.Presentation;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;
using System.Xml;

namespace xhtml_v7 
{
    public partial class test_default : System.Web.UI.Page
    {
        private string res = "";
        private string res2 = "";
        private string res3 = "";
        private string res4 = "";
        BillingTools st = new BillingTools();
        XmlDocument xmldoc = new XmlDocument();
       

        protected void Page_Load(object sender, EventArgs e)
        {

            res = getChargingPro();
            this.Trace.Warn(res);
           if ((res != "200") && (res!="#")) Response.Redirect("./clubtest.aspx");
         
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {



            if (suscribeEvnt() == "300") Response.Redirect("./clubSubscrib.aspx");
            else if (res == "310") Response.Redirect("clubtest.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            
        }

        protected void Button6_Click(object sender, EventArgs e)
        { 
           
            if (checkSubscrip() == "420") Response.Redirect("./download1.aspx");
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            if (suscribeEvnt() == "300") Response.Redirect("./Subscripcion.aspx");
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            if (suscribeEvnt() == "300") Response.Redirect("./Subscripcion.aspx");
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            if (checkSubscrip() == "420") Response.Redirect("./download1.aspx");
        }

        protected void Button9_Click(object sender, EventArgs e)
        {
            if (suscribeEvnt() == "300") Response.Redirect("./Subscripcion.aspx");
        }

        protected void Button10_Click(object sender, EventArgs e)
        {
            if (checkSubscrip() == "420") Response.Redirect("./download1.aspx");
        }

        protected void Button11_Click(object sender, EventArgs e)
        {
            if (suscribeEvnt() == "300") Response.Redirect("./Subscripcion.aspx");
        }

        protected void Button12_Click(object sender, EventArgs e)
        {
            if (checkSubscrip() == "420") Response.Redirect("./download1.aspx");
        }

        protected void Button13_Click(object sender, EventArgs e)
        {
            if (suscribeEvnt() == "300") Response.Redirect("./Subscripcion.aspx");
        }

        protected void Button14_Click(object sender, EventArgs e)
        {
            if (checkSubscrip() == "420") Response.Redirect("./download1.aspx");
        }

        protected void Button15_Click(object sender, EventArgs e)
        {
            if (suscribeEvnt() == "300") Response.Redirect("./Subscripcion.aspx");
        }

        protected void Button16_Click(object sender, EventArgs e)
        {
            if (checkSubscrip() == "420") Response.Redirect("./download1.aspx");
        }

        public string checkSubscrip() 
        {

            XmlNamespaceManager mn = new XmlNamespaceManager(xmldoc.NameTable);

            xmldoc = st.checkContentSuscValidate(this.Trace, this.Request.Headers["TM_user-id"]);

            mn.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
            mn.AddNamespace("ns", "http://mesv5.tme.com/spg180");

            XmlNode nodelist = xmldoc.SelectSingleNode("//SOAP-ENV:Body//ns:checkContentSuscValidateResponse//ns:checkContentSuscValidateResult", mn);
            this.Trace.Warn(nodelist.ChildNodes.Count.ToString());
            res4 = nodelist.ChildNodes.Item(25).InnerXml;
            
            return res4;
        }

        public string suscribeEvnt()
        {

            XmlNamespaceManager mn = new XmlNamespaceManager(xmldoc.NameTable);
            mn.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
            mn.AddNamespace("ns", "http://mesv5.tme.com/spg180");
            xmldoc = st.suscribeEvent(this.Trace, this.Request.Headers["TM_user-id"]);

            XmlNode nodelist = xmldoc.SelectSingleNode("//SOAP-ENV:Body//ns:suscribeEventResponse//ns:suscribeEventResult", mn);


            res2 = nodelist.ChildNodes.Item(25).InnerXml;
            this.Trace.Warn(res2);
           
            return res2;
        }


        public string chargeEvnt()
        {

            XmlNamespaceManager mn = new XmlNamespaceManager(xmldoc.NameTable);

            xmldoc = st.chargeEvent(this.Trace, this.Request.Headers["TM_user-id"]);

            mn.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
            mn.AddNamespace("ns", "http://mesv5.tme.com/spg180");

            XmlNode nodelist = xmldoc.SelectSingleNode("//SOAP-ENV:Body//ns:chargeEventResponse//ns:chargeEventResult", mn);

            res3 = nodelist.ChildNodes.Item(25).InnerXml;
            this.Trace.Warn(res3);
            return res3;
        }
        public string getChargingPro()
        {
            XmlNamespaceManager mn = new XmlNamespaceManager(xmldoc.NameTable);

            xmldoc = st.getChargingProfiles(this.Trace, this.Request.Headers["TM_user-id"]);

            mn.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
            mn.AddNamespace("ns", "http://mesv5.tme.com/spg180");

            XmlNode nodelist = xmldoc.SelectSingleNode("//SOAP-ENV:Body//ns:getChargingProfilesResponse//ns:getChargingProfilesResult", mn);

            res = nodelist.ChildNodes.Item(25).InnerXml;
           
            return res;
        }

        protected void Button3_Click(object sender, EventArgs e)
        {

        }
    }
}
