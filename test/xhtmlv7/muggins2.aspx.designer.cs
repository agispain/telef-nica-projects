﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3643
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace xhtml_v7 {
    
    
    public partial class muggins2 {
        
        /// <summary>
        /// tbHeader control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTable tbHeader;
        
        /// <summary>
        /// tbTitle control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTable tbTitle;
        
        /// <summary>
        /// rowPreviews1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTableRow rowPreviews1;
        
        /// <summary>
        /// rowPreviews2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTableRow rowPreviews2;
        
        /// <summary>
        /// rowPreviews3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTableRow rowPreviews3;
        
        /// <summary>
        /// rowPreviews4 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTableRow rowPreviews4;
        
        /// <summary>
        /// rowPreviews5 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTableRow rowPreviews5;
        
        /// <summary>
        /// rowPreviews6 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTableRow rowPreviews6;
        
        /// <summary>
        /// rowPreviews7 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTableRow rowPreviews7;
        
        /// <summary>
        /// rowPreviews8 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTableRow rowPreviews8;
        
        /// <summary>
        /// rowPreviews9 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTableRow rowPreviews9;
        
        /// <summary>
        /// rowPreviews10 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTableRow rowPreviews10;
        
        /// <summary>
        /// rowPreviews11 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTableRow rowPreviews11;
        
        /// <summary>
        /// rowPreviews12 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTableRow rowPreviews12;
        
        /// <summary>
        /// tbPreviews control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTable tbPreviews;
        
        /// <summary>
        /// rowPreviews control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTableRow rowPreviews;
    }
}
