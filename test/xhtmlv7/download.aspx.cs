﻿using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;
using System.Net;
using System.IO;

namespace xhtml_v7
{
    public partial class download1 : XCatalogBrowsing
    {
        public bool is3g = false;
        public string group = "web";
        public string dwldUrl;
        public string name;
        public string fileName;
        public string extension;

        protected void Page_Load(object sender, EventArgs e)
        {
           
            
                Response.Cache.SetCacheability(HttpCacheability.Private, "Community=DEV");
                byte[] imageBytes = (byte[])(Session["contentByte"]);
                name = (string)(Session["contentName"]);
                extension = (string)(Session["extension"]);
                Response.Clear();

            switch(extension)
            {
                case ".jpg":
                Response.BufferOutput = true;
                Response.Buffer = false;
                Response.ContentType = "image/jpeg";
                Response.AppendHeader("Content-Disposition", "attachment; filename="+name+".jpg");
                Response.OutputStream.Write(imageBytes, 0, imageBytes.Length);
                Response.End();
                break;

                case ".gif":
                Response.BufferOutput = true;
                Response.Buffer = false;
                Response.ContentType = "image/gif";
                Response.AppendHeader("Content-Disposition", "attachment; filename="+name+".gif");
                Response.OutputStream.Write(imageBytes, 0, imageBytes.Length);
                Response.End();
                break;
                    
                case ".mp4":
                Response.BufferOutput = true;
                Response.Buffer = false;
                Response.AddHeader("Content-Disposition", "attachment; filename="+name+".mp4");
                Response.ContentType = "application/octet-stream";
                Response.OutputStream.Write(imageBytes, 0, imageBytes.Length);
                Response.End();
                break;

                case ".3gp":
                Response.BufferOutput = true;
                Response.Buffer = false;
                Response.AddHeader("Content-Disposition", "attachment; filename="+name +".3gp");
                Response.ContentType = "application/octet-stream";
                Response.OutputStream.Write(imageBytes, 0, imageBytes.Length);
                Response.End();
                break;

                case ".swf":
                Response.BufferOutput = true;
                Response.Buffer = false;
                Response.AddHeader("Content-Disposition", "attachment; filename="+name+ ".swf");
                Response.ContentType = "application/x-shockwave-flash";
                Response.OutputStream.Write(imageBytes, 0, imageBytes.Length);
                Response.End();
                break;

                case ".sis":
                Response.BufferOutput = true;
                Response.Buffer = false;
                Response.AddHeader("Content-Disposition", "attachment; filename="+name+".sis");
                Response.ContentType = "application/octet-stream";
                Response.OutputStream.Write(imageBytes, 0, imageBytes.Length);
                break;


            }
           
                

            
        }

        
    }
}
