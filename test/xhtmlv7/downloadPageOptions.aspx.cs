﻿using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;

namespace xhtml_v7
{
    public partial class downloadPageOptions : XCatalogBrowsing
    {
        public bool is3g = false;
        public string subscriptionButton;
        public string group = "web";
        string name = "";
        string contentName = "";
        bool isImageBranded;
        bool isVideoBranded;
        bool isAnimationBranded;
        int id;
        string cg;
        public bool isMobile240;

    
        protected void Page_Load(object sender, EventArgs e)
        {

            
            _mobile = (MobileCaps)Request.Browser;
            try { is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType)); }
            catch { is3g = false; }
            
             cg = (Request.QueryString["cg"] != null) ? Request.QueryString["cg"] : "";
             id = (Request.QueryString["id"] != null && Request.QueryString["id"] != "") ? Convert.ToInt32(Request.QueryString["id"]) : 0;
            
            _imgDisplayInst = new ImgDisplayInstructions(_mobile);
            _imgDisplayInst.TextDwld = WapTools.GetText("Download");
            _imgDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "Img");
            _contentType = WapTools.GetDefaultContentType(cg);
            _contentGroup = WapTools.GetDefaultContentGroup(cg);

            isMobile240 = _mobile.ScreenPixelsWidth > 240; // Group8
            group = (isMobile240) ? "web" : "web16";
           
            try
            {
                WapTools.SetHeader(this.Context);
                WapTools.LogUser(this.Request, 198, _mobile.MobileType);
                WapTools.AddUIDatLog(Request, Response, this.Trace);

            }
            catch { }

           

            var content = (KMobile.Catalog.Presentation.ContentSet)(Session["homePageCatalog"]);
            var contentCatalog = (KMobile.Catalog.Presentation.ContentSet)(Session["catalog"]);           
            _imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(WapTools.PreviewUrl(_contentGroup, _mobile));

         
           switch(_contentGroup)
            {
                case "IMG":

             if (content != null)
                    {

                        for (int i = 0; i < content.ContentCollection.Count; i++)
                        {
                            if (content.ContentCollection[i].IDContent == id)
                            {
                                name = content.ContentCollection[i].Name;
                                contentName = content.ContentCollection[i].ContentName;
                                isImageBranded = WapTools.isBranded(content.ContentCollection[i]);
                                Session["contentName"] = name;
                                break;
                            }
                        }
                    }

             if (contentCatalog != null)
             {
                 for (int i = 0; i < contentCatalog.ContentCollection.Count; i++)
                 {
                     if (contentCatalog.ContentCollection[i].IDContent == id)
                     {
                         name = contentCatalog.ContentCollection[i].Name;
                         contentName = contentCatalog.ContentCollection[i].ContentName;
                         isImageBranded = WapTools.isBranded(contentCatalog.ContentCollection[i]);
                         Session["contentName"] = name;
                         break;
                     }
                 }

             }
                    
                createImageContext(isImageBranded);
                break;

                case "VIDEO_RGT":

                if (contentCatalog != null)
                {
                    for (int i = 0; i < contentCatalog.ContentCollection.Count; i++)
                    {
                        if (contentCatalog.ContentCollection[i].IDContent == id)
                        {
                            name = contentCatalog.ContentCollection[i].Name;
                            contentName = contentCatalog.ContentCollection[i].ContentName;
                            isVideoBranded = WapTools.isBranded(contentCatalog.ContentCollection[i]);
                            Session["contentName"] = name;
                            break;
                        }

                    }
                }

                createVideoContext(isVideoBranded);

                break;

                case "SCHEME":

                if (contentCatalog != null)
                {
                    for (int i = 0; i < contentCatalog.ContentCollection.Count; i++)
                    {
                        if (contentCatalog.ContentCollection[i].IDContent == id)
                        {
                            name = contentCatalog.ContentCollection[i].Name;
                            contentName = contentCatalog.ContentCollection[i].ContentName;
                            Session["contentName"] = name;
                            break;
                        }
                    }
                }
                createTemasContext();
                break;

                case "ANIM":
                if (contentCatalog != null)
                {
                    Trace.Warn("catalog not null");
                    Trace.Warn(contentCatalog.Count.ToString());
                    for (int i = 0; i < contentCatalog.ContentCollection.Count; i++)
                    {
                        if (contentCatalog.ContentCollection[i].IDContent == id)
                        {
                            name = contentCatalog.ContentCollection[i].Name;
                            contentName = contentCatalog.ContentCollection[i].ContentName;
                            isAnimationBranded = WapTools.isBranded(contentCatalog.ContentCollection[i]);
                            Session["contentName"] = name;
                            Trace.Warn(name);
                            break;
                            
                        }
                       
                    }
                }
                createAnimationContext(isAnimationBranded);
                break;

                case "FLASH":
                if (contentCatalog != null)
                {
                    for (int i = 0; i < contentCatalog.ContentCollection.Count; i++)
                    {
                        if (contentCatalog.ContentCollection[i].IDContent == id)
                        {
                            name = contentCatalog.ContentCollection[i].Name;
                            contentName = contentCatalog.ContentCollection[i].ContentName;
                            Session["contentName"] = name;
                            break;
                        }
                    }
                }
                createFlashContext();
                break;

                   
            }
       
            
        
            try
            {
                WapTools.SetHeader(this.Context);
                WapTools.AddUIDatLog(Request, Response, this.Trace);
            }
            catch { }

          
            #region HEADER
            XhtmlTableRow rowTitle = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowTitle, "", name, Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XLarge);
            tbContentTitle.Rows.Add(rowTitle);

            XhtmlImage img = new XhtmlImage();
            img.ImageUrl = WapTools.GetImage(this.Request, "imagenes", _mobile.ScreenPixelsWidth, is3g);
            XhtmlTools.AddImgTable(tbHeaderImg, img);

            XhtmlImage thumb = new XhtmlImage();
            thumb.ImageUrl = String.Format(_imgDisplayInst.PreviewMaskUrl, contentName.Substring(0, 1), contentName);
            thumb.ImageAlign = ImageAlign.Left;
            XhtmlTools.AddImgTable(tbContentThumb, thumb);
            #endregion
           
        }

        private void createVideoContext(bool branded)
        {
            string subscribeProfileIDvideo = "31192";
            string contentProfileID = "22110";

            if (branded)
                contentProfileID = "22120";

            #region IMAGEDESCRIPTION

            XhtmlTableRow rowPriceSub = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowPriceSub, "", "Precio con IVA(2,41 Euros)", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
            tbContentPriceSub.Rows.Add(rowPriceSub);

            XhtmlTableRow rowPricePPD = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowPricePPD, "", "Precio con IVA(2.42)", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
            tbContentPricePPD.Rows.Add(rowPricePPD);


            #endregion

            #region BUTTONS

            XhtmlImage buttonPPD = new XhtmlImage();
            buttonPPD.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "ppdBranded.png");
            buttonPPD.ImageAlign = ImageAlign.Left;

            XhtmlTableRow row1 = new XhtmlTableRow();
            XhtmlTools.addImgLinkTable(row1, buttonPPD.ImageUrl, String.Format("./chargeEvent.aspx?cg={0}&id={1}&cpi={2}", cg, id, contentProfileID));
            tbContentPPDButton.Controls.Add(row1);



            XhtmlImage buttonSubscription = new XhtmlImage();
            buttonSubscription.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "subscribe.png");
            buttonSubscription.ImageAlign = ImageAlign.Left;

            XhtmlTableRow row2 = new XhtmlTableRow();
            XhtmlTools.addImgLinkTable(row2, buttonSubscription.ImageUrl, String.Format("./subscribeEvent.aspx?cg={0}&id={1}&cpi={2}", cg, id, subscribeProfileIDvideo));
            tbContentButtonSub.Controls.Add(row2);
            #endregion
        }

        private void createImageContext(bool branded)
        {
            string imageSubscriptionID = "31690";
            string contentProfileID = "22108";

            if (branded)
                contentProfileID = "22118";
            

            #region IMAGEDESCRIPTION    

            XhtmlTableRow rowPriceSub = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowPriceSub, "", "Precio con IVA(1,20 Euros)", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
            tbContentPriceSub.Rows.Add(rowPriceSub);

            XhtmlTableRow rowPricePPD = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowPricePPD, "", "Precio con IVA(1.82)", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
            tbContentPricePPD.Rows.Add(rowPricePPD);

            

            #endregion

            #region BUTTONS

            XhtmlImage buttonPPD = new XhtmlImage();
            buttonPPD.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "ppd.png");
            buttonPPD.ImageAlign = ImageAlign.Left;

            XhtmlTableRow row1 = new XhtmlTableRow();
            XhtmlTools.addImgLinkTable(row1, buttonPPD.ImageUrl,String.Format("./chargeEvent.aspx?cg={0}&id={1}&cpi={2}", cg, id, contentProfileID));
            tbContentPPDButton.Controls.Add(row1);



            XhtmlImage buttonSubscription = new XhtmlImage();
            buttonSubscription.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "Subscribete099.png");
            buttonSubscription.ImageAlign = ImageAlign.Left;

            XhtmlTableRow row2 = new XhtmlTableRow();
            XhtmlTools.addImgLinkTable(row2, buttonSubscription.ImageUrl, String.Format("./subscribeEvent.aspx?cg={0}&id={1}&cpi={2}", cg, id, imageSubscriptionID));
            tbContentButtonSub.Controls.Add(row2);

            #endregion
        }

        private void createAnimationContext(bool branded)
        {
            string contentProfileID = "22112";
            if (branded)         
                contentProfileID = "22122";
            
            
            #region PPD

            XhtmlTableRow rowPricePPD = new XhtmlTableRow();
            XhtmlImage buttonPPD = new XhtmlImage();

          
            XhtmlTools.AddTextTableRow("", rowPricePPD, "", "Precio con IVA(1.82 e)", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
            tbContentPriceSub.Rows.Add(rowPricePPD);
           
            buttonPPD.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "ppd.png");
            buttonPPD.ImageAlign = ImageAlign.Left;
            XhtmlTableRow row1 = new XhtmlTableRow();
            XhtmlTools.addImgLinkTable(row1, buttonPPD.ImageUrl, String.Format("./chargeEvent.aspx?cg={0}&id={1}&cpi={2}", cg, id, contentProfileID));
            tbContentButtonSub.Controls.Add(row1);
         
            #endregion


        }

        private void createFlashContext()
        {

       

            #region FLASHDESCRIPTION

            XhtmlTableRow rowPricePPD = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowPricePPD, "", "Precio con IVA(2,42 Euros)", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
            tbContentPriceSub.Rows.Add(rowPricePPD);

           
            #endregion

            #region BUTTONS

            string contentProfileId = "28347";

            XhtmlImage buttonPPD = new XhtmlImage();
            buttonPPD.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "ppdBranded.png");
            buttonPPD.ImageAlign = ImageAlign.Left;

            XhtmlTableRow row2 = new XhtmlTableRow();
            XhtmlTools.addImgLinkTable(row2, buttonPPD.ImageUrl, String.Format("./chargeEvent.aspx?cg={0}&id={1}&cpi={2}", cg, id, contentProfileId));
            tbContentButtonSub.Controls.Add(row2);

            #endregion


        }

        private void createTemasContext()
        {
            string contentProfileId = "31688";

            #region TEMASDESCRIPTION

            XhtmlTableRow rowPricePPD = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowPricePPD, "", "Precio con IVA(2,41 Euros)", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
            tbContentPriceSub.Rows.Add(rowPricePPD);



            #endregion

            #region BUTTONS

            XhtmlImage buttonPPD = new XhtmlImage();
            buttonPPD.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "ppdBranded.png");
            buttonPPD.ImageAlign = ImageAlign.Left;

            XhtmlTableRow row2 = new XhtmlTableRow();
            XhtmlTools.addImgLinkTable(row2, buttonPPD.ImageUrl, String.Format("./chargeEvent.aspx?cg={0}&id={1}&cpi={2}", cg, id, contentProfileId));
            tbContentButtonSub.Controls.Add(row2);

            #endregion
        }
    }
}
