﻿using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Net;
using System.IO;

namespace xhtml_v7
{
    /// <summary>
    /// Summary description for $codebehindclassname$
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class Handler1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string filePath = "E://test.jpg"; ; // get from somewhere
  string contentType="image.jpeg"; // get from somewhere

  FileInfo fileInfo = new FileInfo(filePath);

  context.Response.Clear();
  context.Response.ContentType = contentType;
  context.Response.AddHeader("content-disposition", "attachment; filename=" + Path.GetFileName(filePath));
  context.Response.AddHeader("Content-Length", fileInfo.Length.ToString());
  context.Response.TransmitFile(filePath);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
