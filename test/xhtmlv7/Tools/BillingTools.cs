using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Mail;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Xml;
using AGInteractive.Business;
using KMobile.BasicTools;
using KMobile.Catalog.Presentation;
using KMobile.Catalog.Services;
using FontSize = System.Web.UI.MobileControls.FontSize;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using xhtml_v7.Tools;




namespace xhtml_v7.Tools
{

    public class checksubscriptions
    {
        private string _price;
        private string _returnCode;
        private string _contentProfileID;
        private string _chargingProfileType;

        public string price
        {
            get { return _price; }
            set { _price = value; }
        }
        public string returnCode
        {
            get { return _returnCode; }
            set { _returnCode = value; }
        }
        public string contentProfileType
        {
            get { return _contentProfileID; }
            set { _contentProfileID = value; }
        }

        public string chargingProfileType
        {
            get { return _chargingProfileType; }
            set { _chargingProfileType = value; }
        }
    }


    public class suscribeEvents
    {
        private string _price;
        private string _returnCode;
        private string _contentProfileID;
        private string _chargingProfileType;

        public string price
        {
            get { return _price; }
            set { _price = value; }
        }
        public string returnCode
        {
            get { return _returnCode; }
            set { _returnCode = value; }
        }
        public string contentProfileType
        {
            get { return _contentProfileID; }
            set { _contentProfileID = value; }
        }

        public string chargingProfileType
        {
            get { return _chargingProfileType; }
            set { _chargingProfileType = value; }
        }
    }
    
    public class Subscriptions
    {
        private string _price;
        private string _returnCode;
        private string _contentProfileID;
        private string _chargingProfileType;

        public string price
        {
            get { return _price; }
            set { _price = value; }
        }
        public string returnCode
        {
            get { return _returnCode; }
            set { _returnCode = value; }
        }
        public string contentProfileType
        {
            get { return _contentProfileID; }
            set { _contentProfileID = value; }
        }

        public string chargingProfileType
        {
            get { return _chargingProfileType; }
            set { _chargingProfileType = value; }
        }
    }

    public class chargingevents
    {
        private string _price;
        private string _returnCode;
        private string _contentProfileID;
        private string _chargingProfileType;

        public string price
        {
            get { return _price; }
            set { _price = value; }
        }
        public string returnCode
        {
            get { return _returnCode; }
            set { _returnCode = value; }
        }
        public string contentProfileType
        {
            get { return _contentProfileID; }
            set { _contentProfileID = value; }
        }

        public string chargingProfileType
        {
            get { return _chargingProfileType; }
            set { _chargingProfileType = value; }
        }
    }

   


    public class PrefixedSignedXML : SignedXml
    {
       

        public PrefixedSignedXML(XmlDocument document)
            : base(document)
        { }

        public PrefixedSignedXML(XmlElement element)
            : base(element)
        { }

        public PrefixedSignedXML()
            : base()
        { }

        public override XmlElement GetIdElement(XmlDocument doc, string id)
        {
            // check to see if it's a standard ID reference
            XmlElement idElem = base.GetIdElement(doc, id);

            if (idElem == null)
            {
                XmlNamespaceManager nsManager = new XmlNamespaceManager(doc.NameTable);
                nsManager.AddNamespace("wsu", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");

                idElem = doc.SelectSingleNode("//*[@wsu:Id=\"" + id + "\"]", nsManager) as XmlElement;
            }

            return idElem;
        }

        public void ComputeSignature(string prefix)
        {
            this.BuildDigestedReferences();
            AsymmetricAlgorithm signingKey = this.SigningKey;
            if (signingKey == null)
            {
                throw new CryptographicException("Cryptography_Xml_LoadKeyFailed");
            }
            if (this.SignedInfo.SignatureMethod == null)
            {
                if (!(signingKey is DSA))
                {
                    if (!(signingKey is RSA))
                    {
                        throw new CryptographicException("Cryptography_Xml_CreatedKeyFailed");
                    }
                    if (this.SignedInfo.SignatureMethod == null)
                    {
                        this.SignedInfo.SignatureMethod = "http://www.w3.org/2000/09/xmldsig#rsa-sha1";
                    }
                }
                else
                {
                    this.SignedInfo.SignatureMethod = "http://www.w3.org/2000/09/xmldsig#rsa-sha1";
                }
            }
            SignatureDescription description = CryptoConfig.CreateFromName(this.SignedInfo.SignatureMethod) as SignatureDescription;
            if (description == null)
            {
                throw new CryptographicException("Cryptography_Xml_SignatureDescriptionNotCreated");
            }
            HashAlgorithm hash = description.CreateDigest();
            if (hash == null)
            {
                throw new CryptographicException("Cryptography_Xml_CreateHashAlgorithmFailed");
            }
            this.GetC14NDigest(hash, prefix);
            this.m_signature.SignatureValue = description.CreateFormatter(signingKey).CreateSignature(hash);
        }

        public XmlElement GetXml(string prefix)
        {
            XmlElement e = this.GetXml();
            SetPrefix(prefix, e);
            return e;
        }

        //Invocar por reflexi�n al m�todo privado SignedXml.BuildDigestedReferences
        private void BuildDigestedReferences()
        {
            Type t = typeof(SignedXml);
            MethodInfo m = t.GetMethod("BuildDigestedReferences", BindingFlags.NonPublic | BindingFlags.Instance);
            m.Invoke(this, new object[] { });
        }

        private byte[] GetC14NDigest(HashAlgorithm hash, string prefix)
        {
            //string securityUrl = (this.m_containingDocument == null) ? null : this.m_containingDocument.BaseURI;
            //XmlResolver xmlResolver = new XmlSecureResolver(new XmlUrlResolver(), securityUrl);
            XmlDocument document = new XmlDocument();
            document.PreserveWhitespace = true;
            XmlElement e = this.SignedInfo.GetXml();
            document.AppendChild(document.ImportNode(e, true));
            //CanonicalXmlNodeList namespaces = (this.m_context == null) ? null : Utils.GetPropagatedAttributes(this.m_context);
            //Utils.AddNamespaces(document.DocumentElement, namespaces);

            Transform canonicalizationMethodObject = this.SignedInfo.CanonicalizationMethodObject;
            //canonicalizationMethodObject.Resolver = xmlResolver;
            //canonicalizationMethodObject.BaseURI = securityUrl;
            SetPrefix(prefix, document.DocumentElement); //establecemos el prefijo antes de se que calcule el hash (o de lo contrario la firma no ser� v�lida)
            canonicalizationMethodObject.LoadInput(document);
            return canonicalizationMethodObject.GetDigestedOutput(hash);
        }

        private void SetPrefix(string prefix, XmlNode node)
        {
            foreach (XmlNode n in node.ChildNodes)
                SetPrefix(prefix, n);
            node.Prefix = prefix;
        }
    }

    public class SignedXmlWithId : SignedXml
    {
        public SignedXmlWithId(XmlDocument xml)
            : base(xml)
        {
        }

        public SignedXmlWithId(XmlElement xmlElement)
            : base(xmlElement)
        {
        }

        public override XmlElement GetIdElement(XmlDocument doc, string id)
        {
            // check to see if it's a standard ID reference
            XmlElement idElem = base.GetIdElement(doc, id);

            if (idElem == null)
            {
                XmlNamespaceManager nsManager = new XmlNamespaceManager(doc.NameTable);
                nsManager.AddNamespace("wsu", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");

                idElem = doc.SelectSingleNode("//*[@wsu:Id=\"" + id + "\"]", nsManager) as XmlElement;
            }

            return idElem;
        }
    }


	public class BillingTools:XCatalogBrowsing
	{
		
		private static string on_subscribe;
		private static string subject_type;
		private static string content_selector;
		private static string content_value;
		private static string profile_type=null;
		private  static string default_profile=null;
		private static string suscribe_single_event=null;
		private  static string charging_info=null;
		private static string platform_id;
	    private static string	doValidation;
		private static string correlationId;
		private static string serviceId;
		private static string platformId="";
        private static string service_id = "";
        private static string notification_type;
        private static string notification_data;
        private static string end_point;
        private static string service_logic;
        


		private static string SoapUri = "https://pre-3rd.services.telefonica.es:8444/services/2SSL/SOAP/Charging/SPG180";
    //soap message for chargeEvent
		private static string SoapEnvelope4 =
          "<soapenv:Envelope  xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:spg=\"http://mesv5.tme.com/spg180\">" +
            "<soapenv:Header>" +
            "<wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsse:BinarySecurityToken EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\" ValueType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3\" wsu:Id=\"1\">MIIGOjCCBSKgAwIBAgICAdkwDQYJKoZIhvcNAQEFBQAwgYYxCzAJBgNVBAYTAkVTMQwwCgYDVQQKEwNUTUUxJzAlBgNVBAsTHlNlZ3VyaWRhZCBkZSBSZWRlcyB5IFNlcnZpY2lvczEeMBwGA1UEAxMVRW50aWRhZCBDZXJ0aWZpY2Fkb3JhMSAwHgYJKoZIhvcNAQkBFhFTRUdSRURfUkVEQHRzbS5lczAeFw0xMjA5MTAwOTA5NTVaFw0xNzA4MTUwOTA5NTVaMIGmMQswCQYDVQQGEwJFUzEPMA0GA1UECBMGTUFEUklEMQ8wDQYDVQQHEwZNQURSSUQxIjAgBgNVBAoTGVRFTEVGT05JQ0EgTU9WSUxFUyBFU1BBTkExJzAlBgNVBAsTHlNFR1VSSURBRCBERSBSRURFUyBZIFNFUlZJQ0lPUzEaMBgGA1UEAxMRZW1vY2lvbi5raXdlZS5jb20xDDAKBgNVBAUTAzQ3MzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMZUlXM1ixak3OHu0cq2vVT0LGWJQupsgWzO9lhLFqrN0mf1Dm5yhISbnbIlsfZYvywbrBO8HCuG0Y0jq2l672SOd35yKbyBsa71Mjai1m0kDkfyIMzk3/mHanmuD0SYUtN9kpxXSqR+H+RuAdt71dBVYpCZie787lNTAgUI2JrHlq9tr4f0d0xMk1yTnmAN7z6lyEXsUcoizDIsc4skGA8YuFDe+9rY1xHb4JEJiDyc+kHQ9cYiRzhtYBDclKZUw8VSePl6Et6e3WKGVisf6jJ8wisnASlL9g+pdZ0FlRnCGgAndHKcR5qvfLd9ypGz7QR6vbh/mP4kZYXJIx8pZqMCAwEAAaOCAo4wggKKMAkGA1UdEwQCMAAwWQYDVR0gBFIwUDAHBgUqAwPOCjAGBgQqAwMFMAYGBCoDAwYwBgYEKgMDBzAtBgQqAwMIMCUwIwYIKwYBBQUHAgEWF2h0dHA6Ly9zb21lLnVybC5vcmcvY3BzMBEGCWCGSAGG+EIBAQQEAwIGQDALBgNVHQ8EBAMCBPAwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMCMDYGCWCGSAGG+EIBDQQpFidXV1ctU2VydmVyIG9mIFRFTEVGT05JQ0EgTU9WSUxFUyBFU1BBTkEwHQYDVR0OBBYEFAvWRyryb5AUq/YwP7lO4GhF16/hMIGzBgNVHSMEgaswgaiAFFVHHVzlTaw0hlgBSqYrIJxQJAm8oYGMpIGJMIGGMQswCQYDVQQGEwJFUzEMMAoGA1UEChMDVE1FMScwJQYDVQQLEx5TZWd1cmlkYWQgZGUgUmVkZXMgeSBTZXJ2aWNpb3MxHjAcBgNVBAMTFUVudGlkYWQgQ2VydGlmaWNhZG9yYTEgMB4GCSqGSIb3DQEJARYRU0VHUkVEX1JFREB0c20uZXOCAQAwHQYDVR0RBBYwFIESYWNjZXNvc19zb3JAdHNtLmVzMBwGA1UdEgQVMBOBEVNFR1JFRF9SRURAdHNtLmVzMDEGCWCGSAGG+EIBBAQkFiJodHRwOi8vbG9jYWxob3N0L3B1Yi9jcmwvY2FjcmwuY3JsMDEGCWCGSAGG+EIBAwQkFiJodHRwOi8vbG9jYWxob3N0L3B1Yi9jcmwvY2FjcmwuY3JsMDMGA1UdHwQsMCowKKAmoCSGImh0dHA6Ly9sb2NhbGhvc3QvcHViL2NybC9jYWNybC5jcmwwDQYJKoZIhvcNAQEFBQADggEBAHh1//CbC7itcDCwN81H78TNvl2e3Bw4L5E091po1Zr7r6SK8c34e8w2sDlBUq8Avn1QQIzyNhFl03cPBRwxQ0c4ytnpodBkBTthDQ51IMAc4xUypNqtOMk46EUAnOsmag8HOCjF1EytIfjizeRdYpHvlC0ydjOSCxAm9scPmZ7w5gwna9ErohM2bT1yOu+0vT0g9z3tV/UIIf6qoncLeH06CPM/4J5DZEKI7oy+sh1SzlBEhsXEONdCN3MK2RC2EZj12l9yd1O9iZNovY2gr7z0rbnM+judcQDhyTLI0zizgI8E/fnzSV2CGxyIB54H/Jy2y+cmrkLpugImvq+HPYQ=</wsse:BinarySecurityToken>" +
            "</wsse:Security>" +
            "</soapenv:Header>" +
            " <soapenv:Body wsu:Id=\"Body\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">" +
            "<spg:chargeEvent>" +
            "<spg:subject_type>{0}</spg:subject_type>" +
            "<spg:subject>{1}</spg:subject>" +
            "<spg:correlationId>{2}</spg:correlationId>" +
            "<spg:serviceId>{3}</spg:serviceId>" +
            "<spg:content_selector>{4}</spg:content_selector>" +
            "<spg:content_value>{5}</spg:content_value>" +
            "<spg:platformId>{6}</spg:platformId>" +
            "<spg:default_profile>{7}</spg:default_profile>" +
            "<spg:charging_info>{8}</spg:charging_info>" +
            "</spg:chargeEvent>" +
            "</soapenv:Body>" +
            "</soapenv:Envelope>";
	//soap message fro getChargingProfiles		
		private static string SoapEnvelope = 
			"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:spg=\"http://mesv5.tme.com/spg180\">" +
            "<soapenv:Body>" +
            "<spg:getChargingProfiles>" +
            "<spg:subject_type>{0}</spg:subject_type>" +
            "<spg:subject>{1}</spg:subject>" +
            "<spg:content_selector>{2}</spg:content_selector>" +
            "<spg:content_value>{3}</spg:content_value>" +
            " <spg:on_suscribe>{4}</spg:on_suscribe>" +
            " </spg:getChargingProfiles>" +
			"</soapenv:Body>" +
			"</soapenv:Envelope>";
        //soap message for suscribeEvent

		private static string SoapEnvelope1 = 
			"<soapenv:Envelope  xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:spg=\"http://mesv5.tme.com/spg180\">"+
            "<soapenv:Header>"+
            "<wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsse:BinarySecurityToken EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\" ValueType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3\" wsu:Id=\"1\">MIIGOjCCBSKgAwIBAgICAdkwDQYJKoZIhvcNAQEFBQAwgYYxCzAJBgNVBAYTAkVTMQwwCgYDVQQKEwNUTUUxJzAlBgNVBAsTHlNlZ3VyaWRhZCBkZSBSZWRlcyB5IFNlcnZpY2lvczEeMBwGA1UEAxMVRW50aWRhZCBDZXJ0aWZpY2Fkb3JhMSAwHgYJKoZIhvcNAQkBFhFTRUdSRURfUkVEQHRzbS5lczAeFw0xMjA5MTAwOTA5NTVaFw0xNzA4MTUwOTA5NTVaMIGmMQswCQYDVQQGEwJFUzEPMA0GA1UECBMGTUFEUklEMQ8wDQYDVQQHEwZNQURSSUQxIjAgBgNVBAoTGVRFTEVGT05JQ0EgTU9WSUxFUyBFU1BBTkExJzAlBgNVBAsTHlNFR1VSSURBRCBERSBSRURFUyBZIFNFUlZJQ0lPUzEaMBgGA1UEAxMRZW1vY2lvbi5raXdlZS5jb20xDDAKBgNVBAUTAzQ3MzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMZUlXM1ixak3OHu0cq2vVT0LGWJQupsgWzO9lhLFqrN0mf1Dm5yhISbnbIlsfZYvywbrBO8HCuG0Y0jq2l672SOd35yKbyBsa71Mjai1m0kDkfyIMzk3/mHanmuD0SYUtN9kpxXSqR+H+RuAdt71dBVYpCZie787lNTAgUI2JrHlq9tr4f0d0xMk1yTnmAN7z6lyEXsUcoizDIsc4skGA8YuFDe+9rY1xHb4JEJiDyc+kHQ9cYiRzhtYBDclKZUw8VSePl6Et6e3WKGVisf6jJ8wisnASlL9g+pdZ0FlRnCGgAndHKcR5qvfLd9ypGz7QR6vbh/mP4kZYXJIx8pZqMCAwEAAaOCAo4wggKKMAkGA1UdEwQCMAAwWQYDVR0gBFIwUDAHBgUqAwPOCjAGBgQqAwMFMAYGBCoDAwYwBgYEKgMDBzAtBgQqAwMIMCUwIwYIKwYBBQUHAgEWF2h0dHA6Ly9zb21lLnVybC5vcmcvY3BzMBEGCWCGSAGG+EIBAQQEAwIGQDALBgNVHQ8EBAMCBPAwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMCMDYGCWCGSAGG+EIBDQQpFidXV1ctU2VydmVyIG9mIFRFTEVGT05JQ0EgTU9WSUxFUyBFU1BBTkEwHQYDVR0OBBYEFAvWRyryb5AUq/YwP7lO4GhF16/hMIGzBgNVHSMEgaswgaiAFFVHHVzlTaw0hlgBSqYrIJxQJAm8oYGMpIGJMIGGMQswCQYDVQQGEwJFUzEMMAoGA1UEChMDVE1FMScwJQYDVQQLEx5TZWd1cmlkYWQgZGUgUmVkZXMgeSBTZXJ2aWNpb3MxHjAcBgNVBAMTFUVudGlkYWQgQ2VydGlmaWNhZG9yYTEgMB4GCSqGSIb3DQEJARYRU0VHUkVEX1JFREB0c20uZXOCAQAwHQYDVR0RBBYwFIESYWNjZXNvc19zb3JAdHNtLmVzMBwGA1UdEgQVMBOBEVNFR1JFRF9SRURAdHNtLmVzMDEGCWCGSAGG+EIBBAQkFiJodHRwOi8vbG9jYWxob3N0L3B1Yi9jcmwvY2FjcmwuY3JsMDEGCWCGSAGG+EIBAwQkFiJodHRwOi8vbG9jYWxob3N0L3B1Yi9jcmwvY2FjcmwuY3JsMDMGA1UdHwQsMCowKKAmoCSGImh0dHA6Ly9sb2NhbGhvc3QvcHViL2NybC9jYWNybC5jcmwwDQYJKoZIhvcNAQEFBQADggEBAHh1//CbC7itcDCwN81H78TNvl2e3Bw4L5E091po1Zr7r6SK8c34e8w2sDlBUq8Avn1QQIzyNhFl03cPBRwxQ0c4ytnpodBkBTthDQ51IMAc4xUypNqtOMk46EUAnOsmag8HOCjF1EytIfjizeRdYpHvlC0ydjOSCxAm9scPmZ7w5gwna9ErohM2bT1yOu+0vT0g9z3tV/UIIf6qoncLeH06CPM/4J5DZEKI7oy+sh1SzlBEhsXEONdCN3MK2RC2EZj12l9yd1O9iZNovY2gr7z0rbnM+judcQDhyTLI0zizgI8E/fnzSV2CGxyIB54H/Jy2y+cmrkLpugImvq+HPYQ=</wsse:BinarySecurityToken>" +
            "</wsse:Security>"+ 
            "</soapenv:Header>" +
            "<soapenv:Body wsu:Id=\"id-17\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">" +
            "<spg:suscribeEvent>" +                      
			"<spg:subject_type>{0}</spg:subject_type>" +
            "<spg:subject>{1}</spg:subject>" +
            "<spg:content_selector>{2}</spg:content_selector>" +                       
			"<spg:content_value>{3}</spg:content_value>" +
            "<spg:platform_id>{4}</spg:platform_id>" +
			"<spg:profile_type>{5}</spg:profile_type>"+
			"<spg:default_profile>{6}</spg:default_profile>"+
			"<spg:suscribe_single_event>{7}</spg:suscribe_single_event>"+
			"<spg:charging_info>{8}</spg:charging_info>"+               
			"</spg:suscribeEvent>" +
			"</soapenv:Body>" +
			"</soapenv:Envelope>";

        //soap message for checkContentSuscValidate
		private static string SoapEnvelope2 =
            "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:spg=\"http://mesv5.tme.com/spg180\">" +
            "<soapenv:Header/>" +
			"<soapenv:Body>" +
            " <spg:checkContentSuscValidate>" +                      
			"<spg:subject_type>{0}</spg:subject_type>" +                                 
			"<spg:subject>{1}</spg:subject>" +                                                                                                  
			"<spg:content_selector>{2}</spg:content_selector>" +                       
			"<spg:content_value>{3}</spg:content_value>" + 
			"<spg:platform_id>{4}</spg:platform_id>"+
			"<spg:profile_type>{5}</spg:profile_type>"+
			"<spg:doValidation>{6}</spg:doValidation>"+
			"</spg:checkContentSuscValidate>" +
			"</soapenv:Body>" +
			"</soapenv:Envelope>";
        //soap message for cancelaAutoRenovacion
		private static string SoapEnvelope3 =
			"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:idp=\"http://fw3rd.services.telefonica.es/services/IDP\" xmlns:spg=\"http://mesv5.tme.com/spg180\">"+
			"<soapenv:Header>" +
            "<idp:IDPHeader>"+
            "<serviceProviderId></serviceProviderId>"+            "<autoProvisioning></autoProvisioning>"+            "</idp:IDPHeader>"+            "</soapenv:Header>"+
			"<soapenv:Body>" + 
			"<spg:cancelaAutoRenovacion>" +                      
			"<spg:subject_type>{0}</spg:subject_type>" +                                 
			"<spg:subject>{1}</spg:subject>" +                                                                                                  
			"<spg:content_selector>{2}</spg:content_selector>" +                       
			"<spg:content_value>{3}</spg:content_value>" + 
			"</spg:cancelaAutoRenovacion>" +
			"</soapenv:Body>" +
			"</soapenv:Envelope>";

        //soap message for notify event
        private static string SoapEnvelope5 =
            "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:spg=\"http://mesv5.tme.com/spg180\">" +
            "<soapenv:Body>" +
            "<spg:notifyEvent>" +
            "<spg:service_id>{0}</spg:service_id>" +
            "<spg:notification_type>{1}</spg:notification_type>" +
            " <spg:notification_data>{2}</spg:notification_data>" +
            "<spg:end_point>{3}</spg:end_point>" +
            "  <spg:service_logic>{4}</spg:service_logic>" +
            " </spg:notifyEvent>" +
            "</soapenv:Body>" +
            "</soapenv:Envelope>";

      

        public  XmlDocument getChargingProfiles(System.Web.TraceContext t, string subject , string contentID)
        {
            
            //reading from windows certificate store
            System.Net.ServicePointManager.CertificatePolicy = new MyPolicy();
            string result = "";
            X509Certificate2 cer = new X509Certificate2();
            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly);
            X509Certificate2Collection cert2 = store.Certificates;
            t.Warn(store.Certificates.Count.ToString());
            cer = cert2[0];
       
            //checking if private key is loaded
            int p = 0;
            if (cer.HasPrivateKey) p = 1;
           
            HttpWebRequest req;
            if (subject == "" || subject == null) Response.Redirect("./error.aspx");
            if (subject_type == "" || subject_type == null) subject_type = "USER_ID";
             content_selector = "CONTENT_PROFILE_ID";
             content_value = contentID;
            if (on_subscribe == "" || on_subscribe == null) on_subscribe = "YES";

            Object[] obj_params = { subject_type, subject, content_selector, content_value, on_subscribe };
            string message = String.Format(SoapEnvelope, obj_params);
            byte[] message_bytes = System.Text.Encoding.ASCII.GetBytes(message);
            Uri soapUrl = new Uri(SoapUri);
            t.Write(message);
            t.Warn(soapUrl.ToString());
            req = (HttpWebRequest)WebRequest.Create(soapUrl);
            req.ContentType = "text/xml;charset=utf-8";
            req.Method = "POST";
            req.KeepAlive = false;
            req.Accept = "application/soap+xml,soap+xml,text/xml;charset=utf-8, application/dime, multipart/related, text/*";
            req.Headers.Add("SOAPAction", "http://mesv5.tme.com/spg180/getChargingProfiles");
            t.Write(req.Headers.ToString());
            req.ContentLength = message_bytes.Length;
            req.Timeout = 15000;
            req.ClientCertificates.Add(cer);

            t.Warn(message_bytes.ToString());
            Stream req_str = req.GetRequestStream();
            t.Warn(message_bytes.Length.ToString());
            req_str.Write(message_bytes, 0, message_bytes.Length);
            req_str.Flush();
            message_bytes = null;
            req_str.Close();
            req_str = null;
            t.Warn("Sending request");

            try
            {

                HttpWebResponse myHttpWebResponse = (HttpWebResponse)req.GetResponse();
                t.Write("Getting response");
                Stream streamResponse = myHttpWebResponse.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse);
                result = streamRead.ReadToEnd();
                t.Warn(result);
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(result);
              
                streamRead.Close();
                streamResponse.Close();
                myHttpWebResponse.Close();
                return xml ;
       

            }
            catch (WebException ex)
            {
                t.Warn(ex.ToString());
                WebResponse errRsp = ex.Response;
                using (StreamReader rdr = new StreamReader(errRsp.GetResponseStream()))
                {
                    t.Warn(rdr.ReadToEnd().ToString());
                }
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(result);
                return xml;
            }
        }



        public static bool ValidateSoapBodySignature(XmlDocument doc, X509Certificate2 cert)
        {

            // *** Load the doc this time
            
           SignedXmlWithId sdoc = new SignedXmlWithId(doc);



            // *** Find the signature and load it into SignedXml

            XmlNodeList nodeList = doc.GetElementsByTagName("Signature");

            sdoc.LoadXml((XmlElement)nodeList[0]);



            // *** Now read the actual signature and validate

            bool result = sdoc.CheckSignature(cert, true);



            return result;

        }

        public XmlDocument suscribeEvent(System.Web.TraceContext t, string subject, string contentid)
		{
            
            System.Net.ServicePointManager.CertificatePolicy = new MyPolicy();
            string result = "";
            X509Certificate2 cer = new X509Certificate2();
            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly);
            X509Certificate2Collection cert2 = store.Certificates;
            t.Warn(store.Certificates.Count.ToString());
            cer = cert2[0];
            t.Warn(cert2.Count.ToString());
            t.Warn(cer.FriendlyName);
            //checking if private key is loaded
            int p = 0;
            if (cer.HasPrivateKey) p = 1;
            t.Warn(p.ToString());
            HttpWebRequest req;

            if (subject == "" || subject == null) Response.Redirect("./error.aspx");
            if (subject_type == "" || subject_type == null) subject_type = "USER_ID";
            content_selector = "CONTENT_PROFILE_ID";
            content_value = contentid;
            if (platform_id == "" || platform_id == null) platform_id = "BROKER";


            Object[] obj_params = { subject_type, subject, content_selector, content_value, platform_id, profile_type, default_profile, suscribe_single_event, charging_info };
            string message = String.Format(SoapEnvelope1, obj_params);

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.PreserveWhitespace = false;
            xmldoc.LoadXml(message);
            XmlNamespaceManager ns=new XmlNamespaceManager(xmldoc.NameTable);
            ns.AddNamespace("soapenv","http://schemas.xmlsoap.org/soap/envelope/"); 
            ns.AddNamespace("spg","http://mesv5.tme.com/spg180");
            ns.AddNamespace("wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            ns.AddNamespace("wsu", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
            SignedXmlWithId signedXml = new SignedXmlWithId(xmldoc);
            signedXml.SignedInfo.SignatureMethod = "http://www.w3.org/2000/09/xmldsig#rsa-sha1";
            signedXml.SignedInfo.CanonicalizationMethod = "http://www.w3.org/2001/10/xml-exc-c14n#";
            XmlDsigExcC14NTransform canMethod = (XmlDsigExcC14NTransform)signedXml.SignedInfo.CanonicalizationMethodObject;
            canMethod.Algorithm = "http://www.w3.org/2001/10/xml-exc-c14n#";
            canMethod.InclusiveNamespacesPrefixList = "idp soapenv spg";
            signedXml.SigningKey = cer.PrivateKey;
            Reference tRef = new Reference("#id-17");
            XmlDsigExcC14NTransform env = new XmlDsigExcC14NTransform();
            env.InclusiveNamespacesPrefixList = "idp spg";
            tRef.AddTransform(env);
            signedXml.AddReference(tRef);
            signedXml.ComputeSignature();

          
            XmlElement xmlDsig = signedXml.GetXml();
            xmlDsig.SetAttribute("Id", "Signature-5");


            XmlElement soapheader = xmldoc.DocumentElement.SelectSingleNode("//soapenv:Header//wsse:Security", ns) as XmlElement;
            soapheader.AppendChild(xmlDsig);
            XmlElement docInputs = xmldoc.CreateElement("wsse", "Reference","http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            docInputs.SetAttribute("URI","#1");
            docInputs.SetAttribute("ValueType", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3");
            XmlElement soapheader2 = xmldoc.DocumentElement.SelectSingleNode("//soapenv:Header//wsse:Security", ns) as XmlElement;
          
            bool validation= BillingTools.ValidateSoapBodySignature(xmldoc, cer);
            t.Warn(validation.ToString());
            XmlElement docinputs2 = xmldoc.CreateElement("wsse", "SecurityTokenReference", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            docinputs2.SetAttribute("id", "s");
            docinputs2.AppendChild(docInputs);
            XmlElement docinputs3 = xmldoc.CreateElement(null,"KeyInfo","aaa");
            docinputs3.SetAttribute("id","KI-CD2772B43698B1361876839780155");
            docinputs3.AppendChild(docinputs2);
            soapheader2.LastChild.AppendChild(docinputs3);
     

           
            StringWriter sw = new StringWriter();
            XmlTextWriter xml = new XmlTextWriter(sw);
            xmldoc.WriteTo(xml);
            string mess = sw.ToString();
            t.Warn(mess);
            byte[] message_bytes = System.Text.Encoding.ASCII.GetBytes(mess);
            Uri soapUrl = new Uri(SoapUri);
            t.Write(message);
            t.Warn(soapUrl.ToString());
            req = (HttpWebRequest)WebRequest.Create(soapUrl);
            req.ContentType = "text/xml;charset=utf-8";
            req.Method = "POST";
            req.KeepAlive = false;
            req.Accept = "application/soap+xml,soap+xml,text/xml;charset=utf-8, application/dime, multipart/related, text/*";
            req.Headers.Add("SOAPAction", "http://mesv5.tme.com/spg180/getChargingProfiles");
            t.Write(req.Headers.ToString());
            req.ContentLength = message_bytes.Length;
            req.Timeout = 15000;
            req.ClientCertificates.Add(cer);

            t.Warn(message_bytes.ToString());
            Stream req_str = req.GetRequestStream();
            t.Warn(message_bytes.Length.ToString());
            req_str.Write(message_bytes, 0, message_bytes.Length);
            req_str.Flush();
            message_bytes = null;
            req_str.Close();
            req_str = null;
            t.Warn("Sending request");

            try
            {

                HttpWebResponse myHttpWebResponse = (HttpWebResponse)req.GetResponse();
                t.Write("Getting response");
                Stream streamResponse = myHttpWebResponse.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse);
                result = streamRead.ReadToEnd();
                XmlDocument xmls = new XmlDocument();
                xmls.LoadXml(result);
                t.Warn(result);
                streamRead.Close();
                streamResponse.Close();
                myHttpWebResponse.Close();
                return xmls;
               
                
                
       

            }
            catch (WebException ex)
            {
                t.Warn(ex.ToString());
                WebResponse errRsp = ex.Response;
                using (StreamReader rdr = new StreamReader(errRsp.GetResponseStream()))
                {
                    t.Warn(rdr.ReadToEnd().ToString());
                }

                XmlDocument xmls = new XmlDocument();
                xmls.LoadXml(result);
                return xmls;
            }


        }

		
		public XmlDocument checkContentSuscValidate(System.Web.TraceContext t, string subject, string contentid)
		{
            HttpWebRequest req;
            System.Net.ServicePointManager.CertificatePolicy = new MyPolicy();
            string result = "";
            X509Certificate2 cer = new X509Certificate2();
            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly);
            X509Certificate2Collection cert2 = store.Certificates;
            t.Warn(store.Certificates.Count.ToString());
            cer = cert2[0];
            t.Warn(cert2.Count.ToString());
            t.Warn(cer.FriendlyName);
            int p = 0;
            if (cer.HasPrivateKey) p = 1;
            t.Warn(p.ToString());

            if (subject == "" || subject == null) Response.Redirect("./error.aspx");
			if (subject_type == "" || subject_type == null) subject_type = "USER_ID";
			  content_selector = "CONTENT_PROFILE_ID";
              content_value = contentid;
			if (platform_id == "" || platform_id == null) platform_id = "BROKER";
			if (doValidation == "" || doValidation == null) doValidation = "YES";

			Object[] obj_params = {subject_type,subject,content_selector,content_value,platform_id,profile_type,doValidation};
            string message = String.Format(SoapEnvelope2, obj_params);
            byte[] message_bytes = System.Text.Encoding.ASCII.GetBytes(message);
            Uri soapUrl = new Uri(SoapUri);
            t.Write(message);
            t.Warn(soapUrl.ToString());
            req = (HttpWebRequest)WebRequest.Create(soapUrl);

            req.ContentType = "text/xml;charset=utf-8";
            req.Method = "POST";
            req.KeepAlive = false;
            req.Accept = "application/soap+xml,soap+xml,text/xml;charset=utf-8, application/dime, multipart/related, text/*";
            req.Headers.Add("SOAPAction", "http://mesv5.tme.com/spg180/checkContentSuscValidate");
            t.Write(req.Headers.ToString());
            req.ContentLength = message_bytes.Length;
            req.Timeout = 15000;
            req.ClientCertificates.Add(cer);

            t.Warn(message_bytes.ToString());
            Stream req_str = req.GetRequestStream();
            t.Warn(message_bytes.Length.ToString());
            req_str.Write(message_bytes, 0, message_bytes.Length);
            req_str.Flush();
            message_bytes = null;
            req_str.Close();
            req_str = null;
            t.Warn("Sending request");

            try
            {

                HttpWebResponse myHttpWebResponse = (HttpWebResponse)req.GetResponse();
                t.Write("Getting response");
                Stream streamResponse = myHttpWebResponse.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse);
                result = streamRead.ReadToEnd();
                XmlDocument xmlc = new XmlDocument();
                xmlc.LoadXml(result);
                t.Warn(result);
                streamRead.Close();
                streamResponse.Close();
                myHttpWebResponse.Close();
                return xmlc;
               
            }
            catch (WebException ex)
            {
                t.Warn(ex.ToString());
                WebResponse errRsp = ex.Response;
                using (StreamReader rdr = new StreamReader(errRsp.GetResponseStream()))
                {
                    t.Warn(rdr.ReadToEnd().ToString());
                }
                XmlDocument xmlc = new XmlDocument();
                xmlc.LoadXml(result);
                return xmlc;
            }
        }
		
		public XmlDocument chargeEvent(System.Web.TraceContext t, string subject, string contentID)
		{


            HttpWebRequest req;
            System.Net.ServicePointManager.CertificatePolicy = new MyPolicy();
            string result = "";
            X509Certificate2 cer = new X509Certificate2();
            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly);
            X509Certificate2Collection cert2 = store.Certificates;
            t.Warn(store.Certificates.Count.ToString());
            cer = cert2[0];
            t.Warn(cert2.Count.ToString());
            t.Warn(cer.FriendlyName);
            int p = 0;
            if (cer.HasPrivateKey) p = 1;
            t.Warn(p.ToString());


            if (subject == "" || subject == null) Response.Redirect("./error.aspx");
            if (subject_type == "" || subject_type == null) subject_type = "USER_ID";
            if (correlationId == "" || correlationId == null) correlationId = "IP_Nodo-735659086-1233245823281";
            if (serviceId == "" || serviceId == null) serviceId = "1";
            content_selector = "CONTENT_PROFILE_ID";
            content_value = contentID;
            if (platformId == "" || platformId == null) platformId = "BROKER";

            Object[] obj_params = { subject_type, subject, correlationId, serviceId, content_selector, content_value, platformId, default_profile, charging_info };


            string message = String.Format(SoapEnvelope4, obj_params);
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.PreserveWhitespace = false;
            xmldoc.LoadXml(message);
            XmlNamespaceManager ns = new XmlNamespaceManager(xmldoc.NameTable);
            ns.AddNamespace("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
            ns.AddNamespace("spg", "http://mesv5.tme.com/spg180");
            ns.AddNamespace("wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            ns.AddNamespace("wsu", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
            SignedXmlWithId signedXml = new SignedXmlWithId(xmldoc);
            signedXml.SignedInfo.SignatureMethod = "http://www.w3.org/2000/09/xmldsig#rsa-sha1";
            signedXml.SignedInfo.CanonicalizationMethod = "http://www.w3.org/2001/10/xml-exc-c14n#";
            XmlDsigExcC14NTransform canMethod = (XmlDsigExcC14NTransform)signedXml.SignedInfo.CanonicalizationMethodObject;
            canMethod.Algorithm = "http://www.w3.org/2001/10/xml-exc-c14n#";
            canMethod.InclusiveNamespacesPrefixList = "idp soapenv spg";
            signedXml.SigningKey = cer.PrivateKey;
            Reference tRef = new Reference("#Body");
            XmlDsigExcC14NTransform env = new XmlDsigExcC14NTransform();
            env.InclusiveNamespacesPrefixList = "idp spg";
            tRef.AddTransform(env);
            signedXml.AddReference(tRef);
            signedXml.ComputeSignature();


            XmlElement xmlDsig = signedXml.GetXml();
            xmlDsig.SetAttribute("Id", "Signature-5");


            XmlElement soapheader = xmldoc.DocumentElement.SelectSingleNode("//soapenv:Header//wsse:Security", ns) as XmlElement;
            soapheader.AppendChild(xmlDsig);
            XmlElement docInputs = xmldoc.CreateElement("wsse", "Reference", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            docInputs.SetAttribute("URI", "#1");
            docInputs.SetAttribute("ValueType", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3");
            XmlElement soapheader2 = xmldoc.DocumentElement.SelectSingleNode("//soapenv:Header//wsse:Security", ns) as XmlElement;

            bool validation = BillingTools.ValidateSoapBodySignature(xmldoc, cer);
            t.Warn(validation.ToString());
            XmlElement docinputs2 = xmldoc.CreateElement("wsse", "SecurityTokenReference", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            docinputs2.SetAttribute("id", "s");
            docinputs2.AppendChild(docInputs);
            XmlElement docinputs3 = xmldoc.CreateElement(null, "KeyInfo", "aaa");
            docinputs3.SetAttribute("id", "KI-CD2772B43698B1361876839780155");
            docinputs3.AppendChild(docinputs2);
            soapheader2.LastChild.AppendChild(docinputs3);



            StringWriter sw = new StringWriter();
            XmlTextWriter xml = new XmlTextWriter(sw);
            xmldoc.WriteTo(xml);
            string mess = sw.ToString();
           // t.Warn(mess);

            byte[] message_bytes = System.Text.Encoding.ASCII.GetBytes(mess);
            Uri soapUrl = new Uri(SoapUri);
            t.Warn(mess);
            t.Warn(soapUrl.ToString());
            req = (HttpWebRequest)WebRequest.Create(soapUrl);
            req.ContentType = "text/xml;charset=utf-8";
            req.Method = "POST";
            req.KeepAlive = false;
            req.Accept = "application/soap+xml,soap+xml,text/xml;charset=utf-8, application/dime, multipart/related, text/*";
            req.Headers.Add("SOAPAction", "http://mesv5.tme.com/spg180/chargeEvent");
         
            t.Write(req.Headers.ToString());
            req.ContentLength = message_bytes.Length;
            req.Timeout = 15000;
            req.ClientCertificates.Add(cer);

            t.Warn(message_bytes.ToString());
            Stream req_str = req.GetRequestStream();
            t.Warn(message_bytes.Length.ToString());
            req_str.Write(message_bytes, 0, message_bytes.Length);
            req_str.Flush();
            message_bytes = null;
            req_str.Close();
            req_str = null;
            t.Warn("Sending request");

            try
            {

                HttpWebResponse myHttpWebResponse = (HttpWebResponse)req.GetResponse();
                t.Write("Getting response");
                Stream streamResponse = myHttpWebResponse.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse);
                result = streamRead.ReadToEnd();
                t.Warn(result);
                XmlDocument xmlc = new XmlDocument();
                xmlc.LoadXml(result);

                streamRead.Close();
                streamResponse.Close();
                myHttpWebResponse.Close();
                return xmlc;


            }
            catch (WebException ex)
            {
                t.Warn(ex.ToString());
                WebResponse errRsp = ex.Response;
                using (StreamReader rdr = new StreamReader(errRsp.GetResponseStream()))
                {
                    t.Warn(rdr.ReadToEnd().ToString());
                }

                XmlDocument xmlc = new XmlDocument();
                xmlc.LoadXml(result);
                return xmlc;
            }
        }
        public static void notifyEvent(System.Web.TraceContext t)
        {
            HttpWebRequest req;
            System.Net.ServicePointManager.CertificatePolicy = new MyPolicy();
            string result = "";
            X509Certificate2 cer = new X509Certificate2();
            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly);
            X509Certificate2Collection cert2 = store.Certificates;
            t.Warn(store.Certificates.Count.ToString());
            cer = cert2[0];
            t.Warn(cert2.Count.ToString());
            t.Warn(cer.FriendlyName);
            int p = 0;
            if (cer.HasPrivateKey) p = 1;
            t.Warn(p.ToString());

            if (service_id == "" || service_id == null) service_id = "0";
            if (notification_data == "" || notification_data == null) notification_data = "";
            if (notification_type == "" || notification_type == null) notification_type = "";
            if (end_point == "" || end_point == null) end_point = "";
            if (service_logic == "" || service_logic == null) service_logic = "";

            Object[] obj_params = { service_id, notification_data, notification_type, end_point, service_logic };
            string message = String.Format(SoapEnvelope5, obj_params);
            byte[] message_bytes = System.Text.Encoding.ASCII.GetBytes(message);
            Uri soapUrl = new Uri(SoapUri);
            t.Write(message);
            t.Warn(soapUrl.ToString());


            req = (HttpWebRequest)WebRequest.Create(soapUrl);

            req.ContentType = "text/xml;charset=utf-8";
            req.Method = "POST";
            req.KeepAlive = false;
            req.Accept = "application/soap+xml,soap+xml,text/xml;charset=utf-8, application/dime, multipart/related, text/*";
            req.Headers.Add("SOAPAction", "http://mesv5.tme.com/spg180/noitfyEvent");
            t.Write(req.Headers.ToString());
            req.ContentLength = message_bytes.Length;
            req.Timeout = 15000;
            req.ClientCertificates.Add(cer);

            t.Warn(message_bytes.ToString());
            Stream req_str = req.GetRequestStream();
            t.Warn(message_bytes.Length.ToString());
            req_str.Write(message_bytes, 0, message_bytes.Length);
            req_str.Flush();
            message_bytes = null;
            req_str.Close();
            req_str = null;
            t.Warn("Sending request");

            try
            {

                HttpWebResponse myHttpWebResponse = (HttpWebResponse)req.GetResponse();
                t.Write("Getting response");
                Stream streamResponse = myHttpWebResponse.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse);
                result = streamRead.ReadToEnd();
                t.Warn(result);
                streamRead.Close();
                streamResponse.Close();
                myHttpWebResponse.Close();
                //if (Convert.ToBoolean(WapTools.GetXmlValue("JumpTap/SendEmail")))
                //WapTools.SendMail(HttpContext.Current, SoapUri + message, result);	

            }
            catch (WebException ex)
            {
                t.Warn(ex.ToString());
                WebResponse errRsp = ex.Response;
                using (StreamReader rdr = new StreamReader(errRsp.GetResponseStream()))
                {
                    t.Warn(rdr.ReadToEnd().ToString());
                }
            }
        }
		public static void cancelaAutoRenovacion (System.Web.TraceContext t, string subject)
		{
            System.Net.ServicePointManager.CertificatePolicy = new MyPolicy();
            string result = "";
            X509Certificate2 cer = new X509Certificate2();
            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly);
            X509Certificate2Collection cert2 = store.Certificates;
            t.Warn(store.Certificates.Count.ToString());
            cer = cert2[0];
            t.Warn(cert2.Count.ToString());
            t.Warn(cer.FriendlyName);
            int p = 0;
            if (cer.HasPrivateKey) p = 1;
            t.Warn(p.ToString());

            HttpWebRequest req;

            if (subject == "" || subject == null) subject = "155649465";
			if (subject_type == "" || subject_type == null) subject_type = "MSISDN";
            if (content_selector == "" || content_selector == null) content_selector = "CONTENT_ID";
            if (content_value == "" || content_value == null) content_value = "32568";
			
			Object[] obj_params = {subject_type,subject,content_selector,content_value};
            string message = String.Format(SoapEnvelope3, obj_params);
            byte[] message_bytes = System.Text.Encoding.ASCII.GetBytes(message);
            Uri soapUrl = new Uri(SoapUri);
            t.Write(message);
            t.Warn(soapUrl.ToString());


            req = (HttpWebRequest)WebRequest.Create(soapUrl);

            req.ContentType = "text/xml;charset=utf-8";
            req.Method = "POST";
            req.KeepAlive = false;
            req.Accept = "application/soap+xml,soap+xml,text/xml;charset=utf-8, application/dime, multipart/related, text/*";
            req.Headers.Add("SOAPAction", "http://mesv5.tme.com/spg180/cancelaAutoRenovacion");
            t.Write(req.Headers.ToString());
            req.ContentLength = message_bytes.Length;
            req.Timeout = 15000;
            req.ClientCertificates.Add(cer);

            t.Warn(message_bytes.ToString());
            Stream req_str = req.GetRequestStream();
            t.Warn(message_bytes.Length.ToString());
            req_str.Write(message_bytes, 0, message_bytes.Length);
            req_str.Flush();
            message_bytes = null;
            req_str.Close();
            req_str = null;
            t.Warn("Sending request");

            try
            {

                HttpWebResponse myHttpWebResponse = (HttpWebResponse)req.GetResponse();
                t.Write("Getting response");
                Stream streamResponse = myHttpWebResponse.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse);
                result = streamRead.ReadToEnd();
                t.Warn(result);
                streamRead.Close();
                streamResponse.Close();
                myHttpWebResponse.Close();
              
            }
            catch (WebException ex)
            {
                t.Warn(ex.ToString());
                WebResponse errRsp = ex.Response;
                using (StreamReader rdr = new StreamReader(errRsp.GetResponseStream()))
                {
                    t.Warn(rdr.ReadToEnd().ToString());
                }
            }
        }


        public checksubscriptions checkSubscrip(System.Web.TraceContext t, string subject, string contentID)
        {
            checksubscriptions subscrip = new checksubscriptions();
            XmlDocument xmldoc = new XmlDocument();

            XmlNamespaceManager mn = new XmlNamespaceManager(xmldoc.NameTable);

            xmldoc = checkContentSuscValidate(this.Trace, subject, contentID);
            

            mn.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
            mn.AddNamespace("ns", "http://mesv5.tme.com/spg180");

            XmlNode nodelist = xmldoc.SelectSingleNode("//SOAP-ENV:Body//ns:checkContentSuscValidateResponse//ns:checkContentSuscValidateResult", mn);
            
            subscrip.returnCode = nodelist.ChildNodes.Item(25).InnerXml;
            subscrip.price = nodelist.ChildNodes.Item(31).InnerXml;
            Trace.Warn(subscrip.returnCode);
            Trace.Warn(subscrip.price);
            //Trace.Warn(codeCheckSubsc);

            return subscrip;
        }

        public suscribeEvents suscribeEvnt(System.Web.TraceContext t, string subject, string contentI)
        {

            suscribeEvents scb = new suscribeEvents();
            XmlDocument xmldoc = new XmlDocument();

            XmlNamespaceManager mn = new XmlNamespaceManager(xmldoc.NameTable);
            mn.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
            mn.AddNamespace("ns", "http://mesv5.tme.com/spg180");
            xmldoc = suscribeEvent(this.Trace, subject,contentI);

            XmlNode nodelist = xmldoc.SelectSingleNode("//SOAP-ENV:Body//ns:suscribeEventResponse//ns:suscribeEventResult", mn);


            scb.returnCode = nodelist.ChildNodes.Item(25).InnerXml;
            scb.price = nodelist.ChildNodes.Item(31).InnerXml;

            Trace.Warn(scb.returnCode);
            Trace.Warn(scb.price);
            return scb;
        }


        public chargingevents chargeEvnt(System.Web.TraceContext t, string subject, string contentid)
        {
            chargingevents charge = new chargingevents();

            XmlDocument xmldoc = new XmlDocument();

            XmlNamespaceManager mn = new XmlNamespaceManager(xmldoc.NameTable);

            xmldoc = chargeEvent(this.Trace, subject,contentid);

            mn.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
            mn.AddNamespace("ns", "http://mesv5.tme.com/spg180");

            XmlNode nodelist = xmldoc.SelectSingleNode("//SOAP-ENV:Body//ns:chargeEventResponse//ns:chargeEventResult", mn);

            charge.returnCode = nodelist.ChildNodes.Item(25).InnerXml;
            charge.price = nodelist.ChildNodes.Item(31).InnerXml;
            
            return charge;
        }
        public Subscriptions getChargingPro(string subject, string contentProfileid)
        {
            Subscriptions sub = new Subscriptions();
            Trace.Warn("here");
            XmlDocument xmldoc = new XmlDocument();

            XmlNamespaceManager mn = new XmlNamespaceManager(xmldoc.NameTable);
            Trace.Warn("here");

            xmldoc = getChargingProfiles(this.Trace, subject,contentProfileid);
            Trace.Warn("here");

            mn.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
            mn.AddNamespace("ns", "http://mesv5.tme.com/spg180");

            XmlNode nodelist = xmldoc.SelectSingleNode("//SOAP-ENV:Body//ns:getChargingProfilesResponse//ns:getChargingProfilesResult", mn);
            //XmlNode nodelist2 = xmldoc.SelectSingleNode("//SOAP-ENV:Body//ns:getChargingProfilesResponse//ns:getChargingProfilesResult//content", mn);
            //string nr = nodelist2.ChildNodes.Count.ToString();
           // Trace.Warn(nr);
           // XmlNode nodeprice = xmldoc.SelectSingleNode("//SOAP-ENV:Body//ns:getChargingProfilesResponse//ns:getChargingProfilesResult//charging-profile-price", mn);
            sub.returnCode = nodelist.ChildNodes.Item(25).InnerXml;
            sub.contentProfileType = nodelist.ChildNodes.Item(7).InnerXml;
            Trace.Warn(sub.contentProfileType);
           // codeGetChargingPro = nodelist.ChildNodes.Item(25).InnerXml;
           // Trace.Warn(nodelist2.ChildNodes.Item(2).InnerXml);
            return sub;
            
        }
               


         }

    }

