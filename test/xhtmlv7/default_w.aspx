<?xml version="1.0" encoding="UTF-8" ?>
<%@ Register tagprefix="xhtml" Namespace="xhtml_v7.Tools" Assembly="xhtml_v7" %>
<%@ Page language="c#" Codebehind="default_w.aspx.cs" AutoEventWireup="True" Inherits="xhtml_v7.default_w" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<% if (isIE) { %>
		<link rel="shortcut icon" href="kiwee.ico" /><% } %><title>Im&aacute;genes y Fondos</title>
		<link rel="stylesheet" href="<%=css%>" type="text/css" />
		<meta forua="true" http-equiv="Cache-Control" content="no-cache, max-age=0, must-revalidate, proxy-revalidate, s-maxage=0" />
		<style type="text/css">
			.gamelist .minimal    {text-align:left}
			.gamelist .minimal a    {background-color:#d3e1e4;color:#005c84;display:block; min-height:20px;padding:6px 6px 6px 6px; border:0px; text-decoration:none;}
			.gamelist .minimal .row   {text-align:left}
			.gamelist .minimal .row a  {}
			.gamelist .minimal .rowalt   {text-align:left}
			.gamelist .minimal .rowalt a  {}
			.gamelist .minimal .buy a   {}
			.titleon {margin-top: -1.1em;padding: 0px 0; margin-left:4.1em;}
		</style>
	</head>
	<body>
		
		<div> 
			<img src="./Images/<%=group%>/Publicidad_EmoWeb.gif" alt="publicidad"/> 
		</div>
		<xhtml:XhtmlTable id="tbPub" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />
		<div> 
			<img src="./Images/<%=group%>/ahoraenimagenes.gif" alt="ahoraenimagenes"/> 
		</div>
		
		<div class="titleon">
			<div class="gamelist">
				<div class="minimal">
					<div class="row">
						<hr/>
						<a href="<%=link1%>">
						<span><img class="icon" align="absmiddle" src="<%=picto1%>" width="60" alt="" /><%=espname1%></span>
						</a>
					</div>
					<div class="row">
						<hr/>
						<a href="<%=link2%>">
						<span><img class="icon" align="absmiddle" src="<%=picto2%>" width="60" alt="" /><%=espname2%></span>
						</a>
					</div>
					<div class="row">
						<hr/>
						<a href="<%=link3%>">
						<span><img class="icon" align="absmiddle" src="<%=picto3%>" width="60" alt="" /><%=espname3%></span>
						</a>
					</div>
					<div class="row">
						<hr/>
						<a href="<%=link4%>">
						<span><img class="icon" align="absmiddle" src="<%=picto4%>" width="60" alt="" /><%=espname4%></span>
						</a>
					</div>
				</div>
			</div>
		</div>
		<!--<table width="100%">
			<xhtml:XhtmlTableRow id="especiales1" runat="server" />
			<xhtml:XhtmlTableRow id="especiales2" runat="server" />
		</table>-->
		<div> 
			<img src="./Images/<%=group%>/destacados.gif" alt="destacados"/> 
		</div>
		<table width="100%">
			<xhtml:XhtmlTableRow Runat="server" ID="row1" />
			<xhtml:XhtmlTableRow Runat="server" ID="row4" />
			<xhtml:XhtmlTableRow Runat="server" ID="row2" />
			<xhtml:XhtmlTableRow Runat="server" ID="row3" />
		</table>
		
		<table width="100%" class="todas">
			<% if (is_live_wp_compatible) { %>
			<tr>
				<td height="50px" width="100%" colspan="2"><a href="./linkto.aspx?cg=LIVE_WP&amp;id=7409"><img src="<%=live_wp%>" alt="Live Wallpapers"/></a></td>				
			</tr>
			<% } %>
			<tr>
				<td height="50px" width="100%" colspan="2"><a href="./linkto.aspx?cg=COMPOSITE&amp;id=3619&amp;cl=1"><img src="<%=todas%>" alt="todo a 0.99"/></a></td>				
			</tr>
			<tr>
				<td height="50px" width="50%"><a href="./linkto.aspx?id=11036"><img src="<%=zvip%>" alt="zona vip"/></a></td>
				<td><a href="./linkto.aspx?id=5862&amp;cg=COMPOSITE&amp;cl=1"><img src="<%=club%>" alt="club videos todo a 1.99"/></a></td>
			</tr>
			<% if (is_flash_compatible) { %>
			<tr>
				<td height="50px" width="50%"><a href="./linkto.aspx?id=6579&amp;cg=COMPOSITE"><img src="<%=flash%>" alt="fondos flash"/></a></td>
				<td><a href="./linkto.aspx?id=11039"><img src="<%=avatar%>" alt="fondo avatares"/></a></td>
			</tr>
			<% } %>
			<tr>
				<td height="50px" width="50%"><a href="./linkto.aspx?id=5745&amp;cg=IMG&amp;cl=1"><img src="<%=fondos%>" alt="top fondos"/></a></td>
				<td><a href="./linkto.aspx?id=7176&amp;cg=IMG"><img src="<%=amor%>" alt="amor"/></a></td>
			</tr>
			<tr>
				<td height="50px" width="50%"><a href="./linkto.aspx?id=5748&amp;cg=IMG"><img src="<%=sexy%>" alt="modelos y sexy"/></a></td>
				<td><a href="./linkto.aspx?id=11016"><img src="<%=imagenexpaise%>" alt="imagenes x paises"/></a></td>
			</tr>
			<tr>
				<td height="50px" width="100%" colspan="2"><a href="./linkto.aspx?cg=COMPOSITE&amp;id=3619&amp;cl=1"><img src="<%=morecategorias%>" alt="mas categorias"/></a></td>
			</tr>
			
		</table>
		
		<div> 
			<img src="./Images/<%=group%>/descargatetus.gif" alt="descargatetus"/> 
		</div>
		<table width="100%" class="todas">
			<tr>
				<td height="50px" width="50%"><a href="./linkto.aspx?id=5863&amp;cg=COMPOSITE"><img src="<%=anim%>" alt="animaciones"/></a></td>
				<td ><a href="./linkto.aspx?id=5862&amp;cg=COMPOSITE"><img src="<%=videos%>" alt="videos"/></a></td>
			</tr>
			<% if (showTemas) { %>
			<tr>
				<td height="50px" width="50%"><a href="./linkto.aspx?id=10096"><img src="<%=viptemas%>" alt="zona vip temas"/></a></td>
				<td ><a href="./linkto.aspx?id=7311&amp;cg=COMPOSITE"><img src="<%=temas%>" alt="temas"/></a></td>
				<!--<td class="links"><a href="./linkto.aspx?id=10001">Temas</a></td>-->
			</tr>
			<% } %>
		</table>
		<div> 
			<img src="./Images/<%=group%>/felicitaciones.gif" alt="felicitaciones"/> 
		</div>
		<table width="100%" class="todas">
			<tr>
				<td height="50px" width="50%"><a href="./linkto.aspx?id=10093"><img src="<%=zonavip%>" alt="zona vip felicitaciones"/></a></td>
				<td ><a href="./linkto.aspx?id=10004"><img src="<%=fondodedi%>" alt="fondo dedicatorias"/></a></td>
			</tr>
			<tr>
				<td height="50px" width="50%"><a href="./linkto.aspx?id=10059"><img src="<%=postanim%>" alt="postales animadas"/></a></td>
				<td><a href="./linkto.aspx?id=10060"><img src="<%=videofelicit%>" alt="videofelicitaciones"/></a></td>
			</tr>
		</table>
		<asp:Panel class="todas" id="moredescargate" runat="server" height="50px" />
		<div> 
			<img src="./Images/<%=group%>/portalesdeimagenes.gif" alt="portalesdeimagenes"/> 
		</div>
		<table width="100%" class="todas">
			<tr>
				<td height="50px" width="50%"><a href="./linkto.aspx?id=37"><img src="<%=veo%>" alt="Veo Veo"/></a></td>
				<td><a href="./linkto.aspx?id=33"><img src="<%=disena%>" alt="disenatuslogosdeluxe"/></a></td>
			</tr>
			
		</table>
		<asp:Panel class="todas" id="moreportales" runat="server" height="50px" />
		<div> 
			<img src="./Images/<%=group%>/apuntategratis.gif" alt="apuntategratis"/> 
		</div>
		<div class="texthome">Prueba tus alertas de Im&aacute;genes GRATIS el primer mes.</div>
		<table width="100%" class="todas">
			<tr>
				<td height="50px" width="50%"><a href="./linkto.aspx?id=36"><img src="<%=alertasamor%>" alt="no_save"/></a></td>
				<td><a href="./linkto.aspx?id=34"><img src="<%=alertasnew%>" alt="no_save"/></a></td>
			</tr>
		</table>
		<asp:Panel class="todas" id="morealertas" runat="server" height="50px" />
		<div> 
			<img src="./Images/<%=group%>/Publicidad_EmoWeb.gif" alt="publicidad"/> 
		</div>
		<xhtml:XhtmlTable id="tbPub2" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />
		
	</body>
</html>
