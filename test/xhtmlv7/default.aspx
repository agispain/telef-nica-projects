<?xml version="1.0" encoding="UTF-8" ?>
<%@ Page language="c#" Codebehind="default.aspx.cs" AutoEventWireup="True" Inherits="xhtml_v7._default" %>
<%@ Register tagprefix="xhtml" Namespace="xhtml_v7.Tools" Assembly="xhtml_v7" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Im&aacute;genes y Fondos</title>
		<% if (isIE) { %><link rel="shortcut icon" href="kiwee.ico"><% } %>
		<link rel="stylesheet" href="<%=css%>" type="text/css" />
		<meta forua="true" http-equiv="Cache-Control" content="no-cache, max-age=0, must-revalidate, proxy-revalidate, s-maxage=0" />
	</head>
	<body>
			<xhtml:XhtmlTable id="tbHeader" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />
			<div> 
				<img src="./Images/web/Publicidad_EmoWeb.gif" alt="publicidad"/> 
			</div>	
			<xhtml:XhtmlTable id="tbPub" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />
			<xhtml:XhtmlTable id="tbHeaderAhora" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />
			<xhtml:XhtmlTable id="tbAhora" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2">
				<xhtml:XhtmlTableRow id="rowEspecial" Runat="server">
					<xhtml:XhtmlTableCell Runat="server" ID="cellImg" />
					<xhtml:XhtmlTableCell Runat="server" ID="cellLink" />
				</xhtml:XhtmlTableRow>
				<xhtml:XhtmlTableRow id="rowEspecial2" Runat="server">
					<xhtml:XhtmlTableCell Runat="server" ID="cellImg2" />
					<xhtml:XhtmlTableCell Runat="server" ID="cellLink2" />
				</xhtml:XhtmlTableRow>
				<xhtml:XhtmlTableRow id="rowEspecial3" Runat="server">
					<xhtml:XhtmlTableCell Runat="server" ID="cellImg3" />
					<xhtml:XhtmlTableCell Runat="server" ID="cellLink3" />
				</xhtml:XhtmlTableRow>
				<xhtml:XhtmlTableRow id="rowEspecial4" Runat="server">
					<xhtml:XhtmlTableCell Runat="server" ID="cellImg4" />
					<xhtml:XhtmlTableCell Runat="server" ID="cellLink4" />
				</xhtml:XhtmlTableRow>
			</xhtml:XhtmlTable><xhtml:XhtmlTable id="tbAhora2" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2" />
			<xhtml:XhtmlTable id="tbHeaderDestacados" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />
			<xhtml:XhtmlTable id="tbImages" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2">
				<xhtml:XhtmlTableRow id="rowImages" Runat="server"></xhtml:XhtmlTableRow>
				<xhtml:XhtmlTableRow id="rowImg" Runat="server"></xhtml:XhtmlTableRow>
				<xhtml:XhtmlTableRow id="rowButtons1" Runat="server"></xhtml:XhtmlTableRow>
				<xhtml:XhtmlTableRow id="rowTitlesImg" Runat="server"></xhtml:XhtmlTableRow>
				<xhtml:XhtmlTableRow id="rowButtons2" Runat="server"></xhtml:XhtmlTableRow>
				
			</xhtml:XhtmlTable>
			<xhtml:XhtmlTable id="tbTop" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2">
				<xhtml:XhtmlTableRow id="rowTop" Runat="server"></xhtml:XhtmlTableRow>
			</xhtml:XhtmlTable>
			<xhtml:XhtmlTable id="tbHeaderEnd" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />
			<xhtml:XhtmlTable id="tbEnd" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2" />
			<xhtml:XhtmlTable id="tbEnd2" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2" />
			<xhtml:XhtmlTable id="tbTitleShop" Runat="server" CssClass="normal" cellspacing="0" cellpadding="0" />
			<xhtml:XhtmlTable id="tbShops" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2">
				<xhtml:XhtmlTableRow id="rowTitleShops" Runat="server"></xhtml:XhtmlTableRow>
				<xhtml:XhtmlTableRow id="rowShop1" Runat="server">
					<xhtml:XhtmlTableCell Runat="server" id="cellShop1" />
				</xhtml:XhtmlTableRow>
				<xhtml:XhtmlTableRow id="Xhtmltablerow1" Runat="server">
					<xhtml:XhtmlTableCell Runat="server" id="cellShop2" />
				</xhtml:XhtmlTableRow>
				<xhtml:XhtmlTableRow id="rowmoreshops" Runat="server"></xhtml:XhtmlTableRow>
			</xhtml:XhtmlTable>
			<xhtml:XhtmlTable id="tbHeaderApuntante" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />
			<xhtml:XhtmlTable id="tbEnd3" CssClass="normal" Runat="server" cellspacing="2" cellpadding="2" />
			
			<div> 
				<img src="./Images/web/Publicidad_EmoWeb.gif" alt="publicidad"/> 
			</div>	
			<xhtml:XhtmlTable id="tbPub2" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />

	</body>
</html>
