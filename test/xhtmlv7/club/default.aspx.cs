using System;
using System.Collections;
using System.Configuration;
using System.Web;
using System.Drawing;
using System.Web.UI.WebControls;
using KMobile.Catalog.Services;
using KMobile.Catalog.Presentation;
using xhtml_v7.Tools;

namespace xhtml_v7.club
{
	public partial class _default : XCatalogBrowsing
	{
		private ArrayList _contentCollImg = new ArrayList(); 
		private ArrayList _contentCollContentSet = new ArrayList();  
		private ArrayList _contentCollTexts = new ArrayList();  
		public string buscar, emocion, atras, up, fondo, back, title =  "Im&aacute;genes y Fondos", css = "../club.css";
		public string marquee, header, p1, p2, p3, p4, f1, f2, f3, f4, f5, banner_footer, volver, subir;
		public bool isHome = false, is3g = false, showHeader = false, showFooter = false;
		protected int isSexy = 0;
        public Subscriptions sub;
        public string is4purchaser;
        public string subscribe, comprar, download;
        public string group = "web";
        public bool isMobile240;
        public string contentProfileID4;


		protected void Page_Load(object sender, System.EventArgs e)
		{  
			try 
			{
				_mobile = (MobileCaps)Request.Browser;
				try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType));}
				catch{is3g = false;}


				try
				{
					WapTools.SetHeader(this.Context);
					WapTools.AddUIDatLog(Request, Response, this.Trace);																	
				} 
				catch{}


                
                isMobile240 = _mobile.ScreenPixelsWidth > 240; // Group8
                group = (isMobile240) ? "web" : "web16";


                subscribe = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "subscribe.gif");
                comprar = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "comprar.gif");
                download = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "download.gif");

				if (_mobile.MobileType != null &&  _mobile.IsCompatible("IMG_COLOR"))
				{
					_idContentSet = 7016;
					_displayKey = WapTools.GetXmlValue("DisplayKeyClub"); 

					var content=BrowseContentSetExtended(null, -1, -1, false);
                    Session["homePageCatalog"] = content;

					marquee = WapTools.GetText("Marquee");
			
					WapTools.LogUser(this.Request, 109, _mobile.MobileType);		

					#region HEADER
					XhtmlImage img = new XhtmlImage();
					img.ImageUrl = WapTools.GetImage(this.Request, "bannervip",  _mobile.ScreenPixelsWidth, is3g);
					XhtmlTools.AddImgTable(tbHeader, img);
					#endregion
				
					XhtmlImage imgDia = new XhtmlImage();
					imgDia.ImageUrl = WapTools.GetImage(this.Request, "imagendia",  _mobile.ScreenPixelsWidth, is3g);					
					imgDia.ImageAlign = ImageAlign.Middle;
					XhtmlTools.AddImgTableRow(rowPreviews, imgDia);
					//XhtmlTools.AddTextTableRow(rowPreviews, WapTools.GetText("DailyImage"), Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, true, FontUnit.XSmall);
					DisplayImage(rowPreviews2);
					//DisplayContentSets(tbLinks);
					//XhtmlTools.AddLinkTable("IMG", tbLinks, WapTools.GetText("ZonaPromo"), "promo.aspx", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XSmall, String.Format(WapTools.GetXmlValue("Url_PictoClub"), 4));
					
					#region BANNERS
					img = new XhtmlImage();
					img.ImageUrl = WapTools.GetImage(this.Request, "especiales",  _mobile.ScreenPixelsWidth, is3g);
					XhtmlTools.AddImgTable(tbLinks, img);
					img = null;
					
					XhtmlLink lnk = new XhtmlLink();
					lnk.ImageUrl = WapTools.GetImage(this.Request, "sexy",  _mobile.ScreenPixelsWidth, is3g); //sexy.gif
					lnk.NavigateUrl = "../linkto.aspx?id=11050";
					XhtmlTools.AddLinkTable(tbLinks, lnk, 1);
					
					lnk = new XhtmlLink();
					lnk.ImageUrl = WapTools.GetImage(this.Request, "banneramor",  _mobile.ScreenPixelsWidth, is3g); //banneramor.gif
					lnk.NavigateUrl = "../linkto.aspx?id=11051";
					XhtmlTools.AddLinkTable(tbLinks, lnk, 1);
					
					lnk = new XhtmlLink();
					lnk.ImageUrl = WapTools.GetImage(this.Request, "zonaespecial",  _mobile.ScreenPixelsWidth, is3g);
					lnk.NavigateUrl = "../linkto.aspx?id=11052";
					XhtmlTools.AddLinkTable(tbLinks, lnk, 1);
					
					lnk = new XhtmlLink();
					lnk.ImageUrl = WapTools.GetImage(this.Request, "zonapromo",  _mobile.ScreenPixelsWidth, is3g);
					lnk.NavigateUrl = "../linkto.aspx?id=11053";
					XhtmlTools.AddLinkTable(tbLinks, lnk, 1);
					#endregion
					
					
					XhtmlTools.AddLinkTable("IMG", tbLinks, WapTools.GetText("MoreCategories"), "catalog.aspx?cs=6664&cg=COMPOSITE", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XSmall, WapTools.GetImage(this.Request, "bullet"));
					
					
                                    
					#region PICTOS
					fondo = WapTools.GetImage(this.Request, "fondo", _mobile.ScreenPixelsWidth, false);
					buscar = WapTools.getPicto(this.Request, "buscar", _mobile);
					emocion = WapTools.getPicto(this.Request, "emocion", _mobile);
					back = WapTools.getPicto(this.Request, "back", _mobile);
					up = WapTools.getPicto(this.Request, "up", _mobile);
					#endregion

					#region 3G
					if (is3g)
					{
						//css = "../3g.css";
						p1 = WapTools.getPicto3g(this.Request.ApplicationPath, "p1", _mobile.ScreenPixelsWidth);
						p2 = WapTools.getPicto3g(this.Request.ApplicationPath, "p2", _mobile.ScreenPixelsWidth);
						p3 = WapTools.getPicto3g(this.Request.ApplicationPath, "p3", _mobile.ScreenPixelsWidth);
						p4 = WapTools.getPicto3g(this.Request.ApplicationPath, "p4", _mobile.ScreenPixelsWidth);
						f1 = WapTools.getPicto3g(this.Request.ApplicationPath, "f1", _mobile.ScreenPixelsWidth);
						f2 = WapTools.getPicto3g(this.Request.ApplicationPath, "f2", _mobile.ScreenPixelsWidth);
						f3 = WapTools.getPicto3g(this.Request.ApplicationPath, "f3", _mobile.ScreenPixelsWidth);
						f4 = WapTools.getPicto3g(this.Request.ApplicationPath, "f4", _mobile.ScreenPixelsWidth);
						f5 = WapTools.getPicto3g(this.Request.ApplicationPath, "f5", _mobile.ScreenPixelsWidth);
						banner_footer = WapTools.getPicto3g(this.Request.ApplicationPath, "footer",  _mobile.ScreenPixelsWidth);		
						volver = WapTools.getPicto3g(this.Request.ApplicationPath, "volver_club", _mobile.ScreenPixelsWidth);
						subir = WapTools.getPicto3g(this.Request.ApplicationPath, "subir_club", _mobile.ScreenPixelsWidth);								
						//if (_idContentSet == 5862 || _idContentSet == 5863 ||  _idContentSet == 3580) showFooter = true;
					}
					#endregion

					_mobile = null;
					
					atras = WapTools.UpdateFooter(_mobile, this.Context, null); 				
				}
				else
				{
					tbPreviews.Visible = false;
					tbTitle.Visible = false;
					XhtmlTableRow row = new XhtmlTableRow();
					XhtmlTools.AddTextTableRow(row, WapTools.GetText("Compatibility2"), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XSmall);
					tbLinks.Rows.Add(row);
					row = null;
				}
			}
			catch(Exception caught) 
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\xhtml\\club\\catalog.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
				Response.Redirect("../error.aspx");				
			}
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Override
        protected override void DisplayContentSet(KMobile.Catalog.Presentation.Content content, System.Web.UI.MobileControls.Panel pnl)
		{
			_contentCollContentSet.Add(content);
		}

        protected override void DisplayImg(KMobile.Catalog.Presentation.Content content, System.Web.UI.MobileControls.Panel pnl)
		{
			if (content.ContentGroup.Name == "IMG") _contentCollImg.Add(content);
		}
		#endregion

		#region Display
		public void DisplayImage(TableRow row)
		{
            KMobile.Catalog.Presentation.Content content = null;
			_imgDisplayInst = new ImgDisplayInstructions(_mobile);
			_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(WapTools.PreviewUrl("IMG", _mobile));
			_imgDisplayInst.TextDwld = WapTools.GetText("Download");
			_imgDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "Img");
			//_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, "IMG", "IMG_COLOR", (is3g) ? HttpUtility.UrlEncode("CLUB_xHTML|HOME") : HttpUtility.UrlEncode("xhtml|HOME"), "", "0");
			//_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingClub(this.Request, "IMG", "IMG_COLOR", HttpUtility.UrlEncode("CLUB_xHTML|HOME"), "", "0");
			_imgDisplayInst.DisplayDescription = false;
            _imgDisplayInst.UrlDwld = WapTools.GetChargingClubProfilesUrl();
		
							               
			TableItemStyle tableStyle = new TableItemStyle();
			tableStyle.HorizontalAlign = HorizontalAlign.Center;

            content = (KMobile.Catalog.Presentation.Content)_contentCollImg[DateTime.Now.Day % _contentCollImg.Count];

			if (content != null)  
			{
				XhtmlTableCell tempCell = new XhtmlTableCell();
				XhtmlTableCell tempCellTitle = new XhtmlTableCell();
				ImgDisplay imgDisplay = new ImgDisplay(_imgDisplayInst);
				imgDisplay.Display(tempCell, tempCellTitle, content);
				imgDisplay = null;
				tempCell.ApplyStyle(tableStyle);
				row.Cells.Add(tempCell);
				tempCellTitle.ApplyStyle(tableStyle);
			//	rowTitle.Cells.Add(tempCellTitle);
				tempCell = null;
			}
			 
			content = null;
			_imgDisplayInst = null;
			tableStyle = null;
		}

 
		public void DisplayContentSets(XhtmlTable t)
		{
			_contentSetDisplayInst = new ContentSetDisplayInstructions(_mobile);
			_contentSetDisplayInst.UrlDwld = "./catalog.aspx?cs={0}&cg={1}";
			XhtmlTableCell cell = new XhtmlTableCell();
			XhtmlTableRow row = new XhtmlTableRow();
			for( int i = 0; i < _contentCollContentSet.Count; i++ )
			{
				_contentSetDisplayInst.UrlPicto = String.Format(WapTools.GetXmlValue("Url_PictoClub"), i+1);

                KMobile.Catalog.Presentation.Content content = (KMobile.Catalog.Presentation.Content)_contentCollContentSet[i];
				ContentSetDisplay contentSetDisplay = new ContentSetDisplay(_contentSetDisplayInst);
				contentSetDisplay.Display(cell, content, true);			
				contentSetDisplay = null;
				row.Controls.Add(cell);
				cell = new XhtmlTableCell();
				t.Controls.Add(row);
				row = new XhtmlTableRow();
				content = null;
			}
			//t.Controls.Add(row);
			_contentSetDisplayInst = null;
			cell = null; row = null;
		}
		#endregion       
	}
}
