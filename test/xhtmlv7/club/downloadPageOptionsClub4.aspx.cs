﻿using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using xhtml_v7.club;
using xhtml_v7.Tools;

namespace xhtml_v7.club
{
    public partial class downloadPageOptionsClub4 :XCatalogBrowsing
    {
        public bool is3g = false;
        public string subscriptionButton;
        public string group = "web";
        string name = "";
        string contentName = "";
        bool isImageBranded;
        bool isVideoBranded;
        bool isAnimationBranded;
        int id;
        string cg;
        public bool isMobile240;


        protected void Page_Load(object sender, EventArgs e)
        {
            _mobile = (MobileCaps)Request.Browser;
            try { is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType)); }
            catch { is3g = false; }

            cg = (Request.QueryString["cg"] != null) ? Request.QueryString["cg"] : "";
            id = (Request.QueryString["id"] != null && Request.QueryString["id"] != "") ? Convert.ToInt32(Request.QueryString["id"]) : 0;

            try
            {
                WapTools.SetHeader(this.Context);
                WapTools.LogUser(this.Request, 198, _mobile.MobileType);
                WapTools.AddUIDatLog(Request, Response, this.Trace);

            }
            catch { }


            _imgDisplayInst = new ImgDisplayInstructions(_mobile);
            _imgDisplayInst.TextDwld = WapTools.GetText("Download");
            _imgDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "Img");
            _contentType = WapTools.GetDefaultContentType(cg);
            _contentGroup = WapTools.GetDefaultContentGroup(cg);

            isMobile240 = _mobile.ScreenPixelsWidth > 240; // Group8
            group = (isMobile240) ? "web" : "web16";

         
            var contentCatalog = (KMobile.Catalog.Presentation.ContentSet)(Session["catalogClub"]);
            var contentHomePage = (KMobile.Catalog.Presentation.ContentSet)(Session["homePageCatalog"]);

            _imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(WapTools.PreviewUrl(_contentGroup, _mobile));


           if (contentHomePage != null)
            {
                for (int i = 0; i < contentHomePage.ContentCollection.Count; i++)
                {
                    if (contentHomePage.ContentCollection[i].IDContent == id)
                    {
                        name = contentHomePage.ContentCollection[i].Name;
                        contentName = contentHomePage.ContentCollection[i].ContentName;
                        isImageBranded = WapTools.isBranded(contentHomePage.ContentCollection[i]);
                        Session["contentName"] = name;
                        break;
                    }
                }
            }

           if (contentCatalog != null)
           {

               for (int i = 0; i < contentCatalog.ContentCollection.Count; i++)
               {
                   if (contentCatalog.ContentCollection[i].IDContent == id)
                   {
                       name = contentCatalog.ContentCollection[i].Name;
                       contentName = contentCatalog.ContentCollection[i].ContentName;
                       isImageBranded = WapTools.isBranded(contentCatalog.ContentCollection[i]);
                       Session["contentName"] = name;
                       break;
                   }
               }
           }
            
                    createImageContext(isImageBranded);
                  



            try
            {
                WapTools.SetHeader(this.Context);
                WapTools.AddUIDatLog(Request, Response, this.Trace);
            }
            catch { }


            #region HEADER
            XhtmlTableRow rowTitle = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowTitle, "", name, Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XLarge);
            tbContentTitle.Rows.Add(rowTitle);

            XhtmlImage img = new XhtmlImage();
            img.ImageUrl = WapTools.GetImage(this.Request, "imagenes", _mobile.ScreenPixelsWidth, is3g);
            XhtmlTools.AddImgTable(tbHeaderImg, img);

            XhtmlImage thumb = new XhtmlImage();
            thumb.ImageUrl = String.Format(_imgDisplayInst.PreviewMaskUrl, contentName.Substring(0, 1), contentName);
            thumb.ImageAlign = ImageAlign.Left;
            XhtmlTools.AddImgTable(tbContentThumb, thumb);
            #endregion

        }

        private void createVideoContext(bool branded)
        {
            string subscribeProfileIDvideo = "31192";
            string contentProfileID = "22110";

            if (branded)
                contentProfileID = "22120";

            #region IMAGEDESCRIPTION

            XhtmlTableRow rowPriceSub = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowPriceSub, "", "Precio con IVA(2,41 Euros)", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
            tbContentPriceSub.Rows.Add(rowPriceSub);

            XhtmlTableRow rowPricePPD = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowPricePPD, "", "Precio con IVA(2.42)", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
            tbContentPricePPD.Rows.Add(rowPricePPD);

         

            #endregion

            #region BUTTONS

            XhtmlImage buttonPPD = new XhtmlImage();
            buttonPPD.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "ppdBranded.png");
            buttonPPD.ImageAlign = ImageAlign.Left;

            XhtmlTableRow row1 = new XhtmlTableRow();
            XhtmlTools.addImgLinkTable(row1, buttonPPD.ImageUrl, String.Format("./chargeEvent.aspx?cg={0}&id={1}&cpi={2}", cg, id, contentProfileID));
            tbContentPPDButton.Controls.Add(row1);



            XhtmlImage buttonSubscription = new XhtmlImage();
            buttonSubscription.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "subscribe.png");
            buttonSubscription.ImageAlign = ImageAlign.Left;

            XhtmlTableRow row2 = new XhtmlTableRow();
            XhtmlTools.addImgLinkTable(row2, buttonSubscription.ImageUrl, String.Format("./subscribeEvent.aspx?cg={0}&id={1}&cpi={2}", cg, id, subscribeProfileIDvideo));
            tbContentButtonSub.Controls.Add(row2);
            #endregion
        }

        private void createImageContext(bool branded)
        {
            string imageSubscriptionID = "25609";
            string contentProfileID = "22108";

            if (branded)
                contentProfileID = "22118";


            #region IMAGEDESCRIPTION

            XhtmlTableRow rowPriceSub = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowPriceSub, "", "Precio con IVA(4,84 Euros)", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
            tbContentPriceSub.Rows.Add(rowPriceSub);

            XhtmlTableRow rowPricePPD = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowPricePPD, "", "Precio con IVA(1.82)", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
            tbContentPricePPD.Rows.Add(rowPricePPD);

            #endregion

            #region BUTTONS

            XhtmlImage buttonPPD = new XhtmlImage();
            buttonPPD.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "ppd.png");
            buttonPPD.ImageAlign = ImageAlign.Left;

            XhtmlTableRow row1 = new XhtmlTableRow();
            XhtmlTools.addImgLinkTable(row1, buttonPPD.ImageUrl, String.Format("./chargeEvent.aspx?cg={0}&id={1}&cpi={2}", cg, id, contentProfileID));
            tbContentPPDButton.Controls.Add(row1);



            XhtmlImage buttonSubscription = new XhtmlImage();
            buttonSubscription.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "subscribete4.png");
            buttonSubscription.ImageAlign = ImageAlign.Left;

            XhtmlTableRow row2 = new XhtmlTableRow();
            XhtmlTools.addImgLinkTable(row2, buttonSubscription.ImageUrl, String.Format("./subscribeEvent.aspx?cg={0}&id={1}&cpi={2}", cg, id, imageSubscriptionID));
            tbContentButtonSub.Controls.Add(row2);

            #endregion
        }

        }
    }
