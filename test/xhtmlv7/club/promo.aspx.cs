using System;
using System.Configuration;
using System.Web;
using System.Drawing;
using System.Web.UI.WebControls;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;

namespace xhtml_v7.club
{
	public partial class promo : XCatalogBrowsing
	{
		public string buscar, emocion, atras, up, fondo, back, title =  "Im&aacute;genes y Fondos", css = "../club.css";
		public string header, p1, p2, p3, p4, f1, f2, f3, f4, f5, banner_footer, volver, subir;
		public bool isHome = false, is3g = false, showHeader = false, showFooter = false;
		protected int isSexy = 0;

		protected void Page_Load(object sender, System.EventArgs e)
		{  
			try 
			{
				_mobile = (MobileCaps)Request.Browser;
				try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType));}
				catch{is3g = false;}

				try
				{
					WapTools.SetHeader(this.Context);
					WapTools.AddUIDatLog(Request, Response, this.Trace);				
				} 
				catch{}

				if (_mobile.MobileType != null &&  _mobile.IsCompatible("IMG_COLOR"))
				{
					XhtmlTools.AddTextTable(tbLinks, "Promoci�n Zona VIP", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, true, FontUnit.XSmall);
					XhtmlTools.AddTextTable(tbLinks, "Bienvenido a la Zona VIP de emoci�n Im�genes. S�lo por ser parte de este exclusivo Club tendr�s ventajas, premios y descuentos en miles de productos. Estate atento a esta secci�n y te iremos informando peri�dicamente de las novedades:", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XSmall);
					XhtmlTools.AddTextTable(tbLinks, " ", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XSmall);
					XhtmlTools.AddTextTable(tbLinks, "1. Abierta - Desc�rgate un contenido durante los meses de Mayo, Junio y Julio y entra en el sorteo de dos magn�ficos Nokia 5800XpressMusic!!!", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XSmall);
					XhtmlTools.AddTextTable(tbLinks, "2. Abierta - S�lo para ti, y en exclusiva, contenidos de Mr.Bean: Wallpapers y Videos. GRATIS!!!!!", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XSmall);
					XhtmlTools.AddTextTable(tbLinks, "3. Pr�ximamente - Sorteo de material y merchandising de El jueves : libros, relojes, frisbees...", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XSmall);
					XhtmlTools.AddTextTable(tbLinks, "4. Pr�ximamente - Un mes GRATIS de las mejores im�genes de Bugs Bunny, Piol�n y sus amigos", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XSmall);
					XhtmlTools.AddTextTable(tbLinks, "5. Pr�ximamente - V�deos GRATIS para todos aquellos que formen parte del CLUB de la ZONA VIP de Im�genes de emoci�n", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XSmall);
					XhtmlTools.AddTextTable(tbLinks, " ", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XSmall);
					XhtmlTools.AddTextTable(tbLinks, "Estate pendiente de Zona VIP PROMOCIONAL y apr�vechate de las ventajas de pertenecer a tan exclusivo CLUB!!", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XSmall);
					
					#region HEADER
					XhtmlImage img = new XhtmlImage();
					img.ImageUrl = WapTools.GetImage(this.Request, "bannervip",  _mobile.ScreenPixelsWidth, is3g);
					XhtmlTools.AddImgTable(tbHeader, img);
					#endregion
                                    
					#region PICTOS
					fondo = WapTools.GetImage(this.Request, "fondo", _mobile.ScreenPixelsWidth, false);
					buscar = WapTools.getPicto(this.Request, "buscar", _mobile);
					emocion = WapTools.getPicto(this.Request, "emocion", _mobile);
					back = WapTools.getPicto(this.Request, "back", _mobile);
					up = WapTools.getPicto(this.Request, "up", _mobile);
					#endregion

					#region 3G
					if (is3g)
					{
						//css = "../3g.css";
						p1 = WapTools.getPicto3g(this.Request.ApplicationPath, "p1", _mobile.ScreenPixelsWidth);
						p2 = WapTools.getPicto3g(this.Request.ApplicationPath, "p2", _mobile.ScreenPixelsWidth);
						p3 = WapTools.getPicto3g(this.Request.ApplicationPath, "p3", _mobile.ScreenPixelsWidth);
						p4 = WapTools.getPicto3g(this.Request.ApplicationPath, "p4", _mobile.ScreenPixelsWidth);
						f1 = WapTools.getPicto3g(this.Request.ApplicationPath, "f1", _mobile.ScreenPixelsWidth);
						f2 = WapTools.getPicto3g(this.Request.ApplicationPath, "f2", _mobile.ScreenPixelsWidth);
						f3 = WapTools.getPicto3g(this.Request.ApplicationPath, "f3", _mobile.ScreenPixelsWidth);
						f4 = WapTools.getPicto3g(this.Request.ApplicationPath, "f4", _mobile.ScreenPixelsWidth);
						f5 = WapTools.getPicto3g(this.Request.ApplicationPath, "f5", _mobile.ScreenPixelsWidth);
						banner_footer = WapTools.getPicto3g(this.Request.ApplicationPath, "footer",  _mobile.ScreenPixelsWidth);		
						volver = WapTools.getPicto3g(this.Request.ApplicationPath, "volver_club", _mobile.ScreenPixelsWidth);
						subir = WapTools.getPicto3g(this.Request.ApplicationPath, "subir_club", _mobile.ScreenPixelsWidth);								
						//if (_idContentSet == 5862 || _idContentSet == 5863 ||  _idContentSet == 3580) showFooter = true;
					}
					#endregion

					_mobile = null;
					
					atras = WapTools.UpdateFooter(_mobile, this.Context, null); 				
				}
				else
				{
					tbTitle.Visible = false;
					XhtmlTableRow row = new XhtmlTableRow();
					XhtmlTools.AddTextTableRow(row, WapTools.GetText("Compatibility2"), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XSmall);
					tbLinks.Rows.Add(row);
					row = null;
				}
			}
			catch(Exception caught) 
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\xhtml\\club\\catalog.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
				Response.Redirect("../error.aspx");				
			}
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
