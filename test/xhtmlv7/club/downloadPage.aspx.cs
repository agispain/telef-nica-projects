﻿using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using xhtml_v7.club;
using xhtml_v7.Tools;
using System.Net;
using System.IO;


namespace xhtml_v7.club
{
    public partial class downloadPage :XCatalogBrowsing
    {
        public string group = "web";
        public bool isMobile240;
        double finalprice;
        public string downloadButtonUrl;
        public string contentProfileID;
        public bool is3g = false;
        string contentType;
        int id;
        string name;
        string dwldUrl;
        public string downloadButtonSrc;
       

        protected void Page_Load(object sender, EventArgs e)
        {

            _mobile = (MobileCaps)Request.Browser;
            try { is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType)); }
            catch { is3g = false; }

            contentType = (Request.QueryString["cg"] != null) ? Request.QueryString["cg"] : "";
            _displayKey = WapTools.GetXmlValue("DisplayKey");
            _contentGroup = WapTools.GetDefaultContentGroup(contentType);
          
            id = (Request.QueryString["id"] != null) ? Convert.ToInt32(Request.QueryString["id"]) : 0;

            try
            {
                WapTools.SetHeader(this.Context);
                WapTools.LogUser(this.Request, 198, _mobile.MobileType);
                WapTools.AddUIDatLog(Request, Response, this.Trace);

            }
            catch { }


            //calculate price

            var price = (double)(Session["contentInfo"]);
            string contentName = (string)(Session["contentName"]);    
            price = price + 0.21 * price;
            finalprice = Math.Round((double)price, 2);

            isMobile240 = _mobile.ScreenPixelsWidth > 240; // Group8
            group = (isMobile240) ? "web" : "web16";

            #region getUrl

           Operator op = new Operator(Request.UserHostAddress);

           if (op.OperatorName != null && op.OperatorName == "MOVISTAR")
             {

                 DownloadInfo downloadInfo = null;
                 BillingRequest billingRequest = null;
                 CommandItem commandItem = new CommandItem(new Guid(_displayKey), id, contentType, null, "", _mobile.MobileType, _contentGroup);
                 BillingManager billingManager = new BillingManager();
                 billingRequest = billingManager.CreateCommand(Request, WapTools.GetXmlValue("Billing/Provider"), commandItem);
                 downloadInfo = billingManager.DeliverCommand(Request, billingRequest.GUIDCommand, null, null, WrapperType.DescriptorWrapper | WrapperType.ForwardLockWrapper);
                 dwldUrl = downloadInfo.Uri;

             }

            #endregion


            #region contentInfo

            var contentHomePage = (KMobile.Catalog.Presentation.ContentSet)(Session["homePageCatalog"]);

            if (contentHomePage != null)
            {
                for (int i = 0; i < contentHomePage.ContentCollection.Count; i++)
                {
                    if (contentHomePage.ContentCollection[i].IDContent == id)
                    {
                        name = contentHomePage.ContentCollection[i].Name;
                        break;
                    }
                }
            }

            var contentCatalog = (KMobile.Catalog.Presentation.ContentSet)(Session["catalogClub"]);

            if (contentCatalog != null)
            {

                for (int i = 0; i < contentCatalog.ContentCollection.Count; i++)
                {
                    if (contentCatalog.ContentCollection[i].IDContent == id)
                    {
                        name = contentCatalog.ContentCollection[i].Name;
                        break;
                    }
                }
            }

            #endregion

            downloadButtonSrc = "descargaImagen.png";
            downloadButtonUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, downloadButtonSrc);
                   


            XhtmlTableRow rowTitle1 = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowTitle1, "", "Te acabas de susbcribir con éxito al Club Todo Incluido al precio de " + finalprice + " ( IVA INCLUIDO) al mes", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
            tbHeaderRow1.Rows.Add(rowTitle1);

            XhtmlTableRow rowTitle2 = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowTitle2, "", "Podrás descargarte por este precio todos los contenidos que quieras de nuestro portal", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
            tbHeaderRow2.Rows.Add(rowTitle2);

            XhtmlTableRow rowTitle3 = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowTitle3, "", "Recuedra que se tienes algun problema puedes a llamar  gratis al  1004 par resolvero", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
            tbHeaderRow3.Rows.Add(rowTitle3);

            #region buttons
           
            XhtmlImage ayuda = new XhtmlImage();
            ayuda.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "ayuda.png");
            ayuda.ImageAlign = ImageAlign.Middle;
            XhtmlTableRow row = new XhtmlTableRow();
            XhtmlTools.addImgLinkTable(row, ayuda.ImageUrl, "./helpPage.aspx");
            tbAyudaButton.Controls.Add(row);
          
            XhtmlImage terms = new XhtmlImage();
            terms.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "terms.png");
            terms.ImageAlign = ImageAlign.Middle;
            XhtmlTableRow row1 = new XhtmlTableRow();
            XhtmlTools.addImgLinkTable(row1, terms.ImageUrl, "./Terms.aspx");
            tbTermsButtons.Controls.Add(row1);
        
            XhtmlImage inicio = new XhtmlImage();
            inicio.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "inicio.png");
            inicio.ImageAlign = ImageAlign.Middle;
            XhtmlTableRow row2 = new XhtmlTableRow();
            if (_mobile.Family == "Android")
                XhtmlTools.addImgLinkTable(row2, inicio.ImageUrl, "http://emocion.dev.kiwee.com/testWeb/");
            else
                XhtmlTools.addImgLinkTable(row2, inicio.ImageUrl, "./default_w.aspx");
            tbGoToHomePage.Controls.Add(row2);

            var webClient = new WebClient();
            Trace.Warn(dwldUrl);
            byte[] imageBytes = webClient.DownloadData(dwldUrl);
            string extension = Path.GetExtension(dwldUrl);
            Session["contentByte"] = imageBytes;
            Session["contentName"] = contentName;
            Session["extension"] = extension;
       

            #endregion

        }

    }
}


