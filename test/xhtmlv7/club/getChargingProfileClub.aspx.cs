﻿using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;
using System.Net;
using System.IO;

namespace xhtml_v7.club
{
    public partial class getCharginProfileClub : XCatalogBrowsing
    {
        public int is4purchaser;
        public string contentProfileID4;

        protected void Page_Load(object sender, EventArgs e)
        {


            BillingTools webService = new BillingTools();

            string cg = (Request.QueryString["cg"] != null) ? Request.QueryString["cg"] : "";
            int id = (Request.QueryString["id"] != null && Request.QueryString["id"] != "") ? Convert.ToInt32(Request.QueryString["id"]) : 0;

            #region Subscription4
            try
            {
                contentProfileID4 = "25609";
                Subscriptions subscription4 = new Subscriptions();
                subscription4 = webService.getChargingPro(this.Request.Headers["TM_user-id"], contentProfileID4);
                is4purchaser = Int32.Parse(subscription4.returnCode);
            }
            catch
            {
                Response.Redirect("../error.aspx");
            }
            #endregion

            if (is4purchaser == 220)
                Response.Redirect(String.Format("./downloadPageClub4.aspx?cg={0}&id={1}", cg, id));
            else
                Response.Redirect(String.Format("./downloadPageOptionsClub4.aspx?cg={0}&id={1}", cg, id));

        }
    }
}
