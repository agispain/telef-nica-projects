<?xml version="1.0" encoding="UTF-8" ?>
<%@ Page language="c#" Codebehind="default.aspx.cs" AutoEventWireup="True" Inherits="xhtml_v7.club._default" %>
<%@ Register tagprefix="xhtml" Namespace="xhtml_v7.Tools" Assembly="xhtml_v7" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
		<title> 
			<%=title%>
		</title>
		<link rel="stylesheet" href="<%=css%>" type="text/css" />
		<meta forua="true" http-equiv="Cache-Control" content="no-cache, max-age=0, must-revalidate, proxy-revalidate, s-maxage=0" />
  </head>
	<body>
		<a id="start" name="start" />
		<Xhtml:XhtmlTable id="tbHeader" Runat="server" CssClass="normal" />
			<Xhtml:XhtmlTable id="tbTitle" Runat="server" CssClass="normal" />
			<marquee><%=marquee%></marquee>
			<Xhtml:XhtmlTable id="tbPreviews" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2">
				<Xhtml:XhtmlTableRow id="rowPreviews" Runat="server" />
				<Xhtml:XhtmlTableRow id="rowPreviews2" Runat="server" />
				<Xhtml:XhtmlTableRow id="rowPreviews3" Runat="server" />
			</Xhtml:XhtmlTable>
			<Xhtml:XhtmlTable id="tbLinks" Runat="server" CssClass="normal" /><br>
			
		<% if (is3g) { %>
		<table width="100%" class="up_back" bgcolor="#cccccc">
			<tr>
				<td width="50%" align="left">
					<img src="<%=volver%>" alt=""><b><a href="../default.aspx">Salir de la Zona VIP de Fondos</a></b>
				</td>
				<td width="50%" align="right">
					<b><a class="right" href="#start">Subir</a></b><img src="<%=subir%>" alt="">
				</td>
			</tr>
		</table>
		
			<% } else { %>		
			<hr />
			<table width="100%">
				<tr>
					<td align="center"><b><a href="../default.aspx">Salir de la Zona VIP de Fondos</a></b></td>
				</tr>
			</table>				
			
			<% } %></a>
	</body>
</html>
