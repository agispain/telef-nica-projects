﻿using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using xhtml_v7.club;
using xhtml_v7.Tools;

namespace xhtml_v7.club
{
    public partial class helpPage : XCatalogBrowsing
    {
        public bool is3g = false;
        public string group = "web";
        protected void Page_Load(object sender, EventArgs e)
        {

            _mobile = (MobileCaps)Request.Browser;
            try { is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType)); }
            catch { is3g = false; }

            try
            {
                WapTools.SetHeader(this.Context);
                WapTools.LogUser(this.Request, 198, _mobile.MobileType);
                WapTools.AddUIDatLog(Request, Response, this.Trace);	
            }
            catch { }


            XhtmlTableRow rowTitle1 = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowTitle1, "", "Ayuda", Color.AliceBlue, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXLarge);
            tbHeaderTitle.Rows.Add(rowTitle1);

            XhtmlTableRow rowTitle2 = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowTitle2, "", "Como puedo darme de baja de mi suscripcion?", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XLarge);
            tbHeaderTitle.Rows.Add(rowTitle2);

            XhtmlTableRow rowTitle3 = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowTitle3, "", "Puedes solicitar la cancelacion de tu suscripcion siempre que quieras llamando gratis al 1004. ", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.Large);
            tbHeaderTitle.Rows.Add(rowTitle3);

            XhtmlTableRow rowTitle4 = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowTitle4, "", "Que hacer si un juego no funciona o tengo problemas para descargar mis juegos?", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XLarge);
            tbMiddleText.Rows.Add(rowTitle4);

            XhtmlTableRow rowTitle5 = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowTitle5, "", "Si has tenido algun problema al acceder a tu juego , por favor informanos de ello llamando al 1004 y lo solucionaremos lo antes possible. ", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.Large);
            tbMiddleText.Rows.Add(rowTitle5);

            XhtmlTableRow rowTitle6 = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowTitle6, "", "Como puedo  descargar juegos en Juegos 365?", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XLarge);
            tbMiddleText.Rows.Add(rowTitle6);

            XhtmlTableRow rowTitle7 = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowTitle7, "", "Si tienes tarjeta propago deberas tener saldo suficiente , ya que se requiere una conexion da datos y tiene un coste. ", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.Large);
            tbMiddleText.Rows.Add(rowTitle7);

            XhtmlTableRow rowTitle8 = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowTitle8, "", "Como puedo  volver a Juegos 365?", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XLarge);
            tbMiddleText.Rows.Add(rowTitle8);

            XhtmlTableRow rowTitle9 = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowTitle9, "", "Podras acceder simepre que quieras a traves del menu Emocion > Juegos > Juegos 365 y si nos guardas en favoritos , sera aun mas facil!", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.Large);
            tbMiddleText.Rows.Add(rowTitle9);

            XhtmlImage terms = new XhtmlImage();
            terms.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "terms.png");
            terms.ImageAlign = ImageAlign.Middle;
            XhtmlTableRow row = new XhtmlTableRow();
            XhtmlTools.addImgLinkTable(row, terms.ImageUrl, "./Terms.aspx");
            tbTerms.Controls.Add(row);


            XhtmlImage inicio = new XhtmlImage();
            inicio.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "inicio.png");
            inicio.ImageAlign = ImageAlign.Middle;
            XhtmlTableRow row1 = new XhtmlTableRow();
            XhtmlTools.addImgLinkTable(row1, inicio.ImageUrl, "./default.aspx");
            tbInicio.Controls.Add(row1);


        }
    }
}

