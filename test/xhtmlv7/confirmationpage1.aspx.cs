﻿using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;

namespace xhtml_v7
{
    public partial class confirmationpage1 : XCatalogBrowsing
    {

        public bool is3g = false;
        public string volver, subir;
        public string ok;
        public string group = "web";
        public int var;
        public int _contentset;
        public chargingevents charge;
        protected void Page_Load(object sender, EventArgs e)
        {
            _mobile = (MobileCaps)Request.Browser;
            try { is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType)); }
            catch { is3g = false; }
            BillingTools web = new BillingTools();

            try
            {
                WapTools.SetHeader(this.Context);
                WapTools.AddUIDatLog(Request, Response, this.Trace);
            }
            catch { }

            _contentset = (Request.QueryString["cs"] != null) ? Convert.ToInt32(Request.QueryString["cs"]) : 0;
            _contentType = (Request.QueryString["ct"] != null) ? Request.QueryString["ct"] : "";
            _contentGroup = WapTools.GetDefaultContentGroup(_contentType);
            _contentProfileID = (Request.QueryString["ci"] != null) ? Request.QueryString["ci"] : "";
            var = (Request.QueryString["c"] != null) ? Convert.ToInt32(Request.QueryString["c"]) : 0;
            ok = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "OK.gif");

           

          
            XhtmlTableRow row = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", row, "", "Gracias por Descargarte el Contenido "+var+".Ahora solo tienes que pinchar en el contenido  y dar a Salvar", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XSmall);
            tbContentContent.Rows.Add(row);

            #region HEADER
            XhtmlImage img = new XhtmlImage();
            img.ImageUrl = WapTools.GetImage(this.Request, "imagenes", _mobile.ScreenPixelsWidth, is3g);
            XhtmlTools.AddImgTable(tbHeaderContent, img);
            #endregion

        }

        public string goTosave()
        {
            string url="";

            string branded=null;
            
               
           // url = WapTools.GetUrlBillingPPD(this.Request, _contentGroup, _contentType, (is3g) ? HttpUtility.UrlEncode("3g|HOME") : HttpUtility.UrlEncode("xhtml|HOME"),_contentset,var);
            if (_contentProfileID == "22120" || _contentProfileID == "22118" || _contentProfileID == "22122")
                url = String.Format(url,"branded");
            else url = String.Format(url, "");
            return url;
        }

    }

}


