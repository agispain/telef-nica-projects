﻿using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;
using System.Net;
using System.IO;


namespace xhtml_v7
{
    public partial class downloadPageClub : XCatalogBrowsing
    {
        public string group = "web";
        public bool isMobile240;
        public string downloadButtonUrl;
        public string downloadButtonSrc;
        public string contentProfileID;
        string displayClubNrText;
        public bool is3g = false;
        public int id;
        public string name;
        string contentType;
        public   string dwldUrl;
        public string contentUrl;
        public string goToHmPageUrl;
        public string goToHmPageSrc;
        
       

        protected void Page_Load(object sender, EventArgs e)
        {
            
             contentType = (Request.QueryString["cg"] != null) ? Request.QueryString["cg"] : "";
             contentProfileID = (Request.QueryString["cpi"] != null) ? Request.QueryString["cpi"] : "";
             id = (Request.QueryString["id"] != null && Request.QueryString["id"] != "") ? Convert.ToInt32(Request.QueryString["id"]) : 0;
             _contentGroup = WapTools.GetDefaultContentGroup(contentType);
             _displayKey = WapTools.GetXmlValue("DisplayKey");
            _mobile = (MobileCaps)Request.Browser;
            


            try { is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType)); }
            catch { is3g = false; }


            try
            {
                WapTools.SetHeader(this.Context);
                WapTools.AddUIDatLog(Request, Response, this.Trace);
            }
            catch { }


            isMobile240 = _mobile.ScreenPixelsWidth > 240; // Group8
            group = (isMobile240) ? "web" : "web16";

            try
            {
                WapTools.SetHeader(this.Context);
                WapTools.LogUser(this.Request, 198, _mobile.MobileType);
                WapTools.AddUIDatLog(Request, Response, this.Trace);


            }
            catch { }

           
              var content = (KMobile.Catalog.Presentation.ContentSet)(Session["homePageCatalog"]);
              if (content != null) Trace.Warn(content.Count.ToString());
              var contentCatalog = (KMobile.Catalog.Presentation.ContentSet)(Session["catalog"]);
              if (contentCatalog != null) Trace.Warn(contentCatalog.Count.ToString());

              Operator op = new Operator(Request.UserHostAddress);
              Trace.Warn(op.OperatorName);

             if (op.OperatorName != null && op.OperatorName == "MOVISTAR")
              {

                  #region getUrl
                  DownloadInfo downloadInfo = null;
                  BillingRequest billingRequest = null;
                  CommandItem commandItem = new CommandItem(new Guid(_displayKey), id, contentType, null, "", _mobile.MobileType, _contentGroup);
                  BillingManager billingManager = new BillingManager();

                  billingRequest = billingManager.CreateCommand(Request, WapTools.GetXmlValue("Billing/Provider"), commandItem);
                  downloadInfo = billingManager.DeliverCommand(Request, billingRequest.GUIDCommand, null, null, WrapperType.DescriptorWrapper | WrapperType.ForwardLockWrapper);
                  dwldUrl = downloadInfo.Uri;
                  #endregion
              }

              #region contentInfo

              if (content != null)
              {
                  for (int i = 0; i < content.ContentCollection.Count; i++)
                  {
                      if (content.ContentCollection[i].IDContent == id)
                      {
                          name = content.ContentCollection[i].Name;
                          break; 
                      }                
                  }
              }

            

              if (contentCatalog != null)
              {

                  for (int i = 0; i < contentCatalog.ContentCollection.Count; i++)
                  {
                      if (contentCatalog.ContentCollection[i].IDContent == id)
                      {
                          name = contentCatalog.ContentCollection[i].Name;
                          break;
                      }

                  
                  }
              }
 
              #endregion

              switch (_contentGroup)
              {
                  case "IMG":
                      downloadButtonSrc = "descargaImagen.png";
                      downloadButtonUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, downloadButtonSrc);
                      displayClubNrText = "0.99";
                      break;

                  case "VIDEO_RGT":
                      downloadButtonSrc = "descargaVideo.png";
                      downloadButtonUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, downloadButtonSrc);
                      displayClubNrText = "Video";
                      break;

                  case "VIDEO_CLIP":
                      downloadButtonSrc = "descargaVideo.png";
                      downloadButtonUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, downloadButtonSrc);
                      displayClubNrText = "Video";
                      break;
              }

              XhtmlTableRow rowTitle1 = new XhtmlTableRow();
              XhtmlTools.AddTextTableRow("", rowTitle1, "", "Compra realizada con Exito.", Color.Empty, Color.Black, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
              tbHeaderRow1.Rows.Add(rowTitle1);

              XhtmlTableRow rowTitle2 = new XhtmlTableRow();
              XhtmlTools.AddTextTableRow("", rowTitle2, "", "Acabas de descargarte el contenido ' "+name+" ' Gratis porque perteneces al Club "+displayClubNrText+"", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
              tbHeaderRow2.Rows.Add(rowTitle2);

              XhtmlTableRow rowTitle3 = new XhtmlTableRow();
              XhtmlTools.AddTextTableRow("", rowTitle3, "", "Recuedra que se tienes algun problema puedes a llamar  gratis al  1004 par resolvero", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
              tbHeaderRow3.Rows.Add(rowTitle3);

              #region buttons
        
             
              XhtmlImage ayuda = new XhtmlImage();
              ayuda.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "ayuda.png");
              ayuda.ImageAlign = ImageAlign.Middle;
              XhtmlTableRow row = new XhtmlTableRow();
              XhtmlTools.addImgLinkTable(row, ayuda.ImageUrl, "./helpPage.aspx");
              tbAyudaButton.Controls.Add(row);


              XhtmlImage terms = new XhtmlImage();
              terms.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "terms.png");
              terms.ImageAlign = ImageAlign.Middle;
              XhtmlTableRow row1 = new XhtmlTableRow();
              XhtmlTools.addImgLinkTable(row1, terms.ImageUrl, "./Terms.aspx");
              tbTermsButtons.Controls.Add(row1);



              goToHmPageSrc = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "inicio.png");

              if (_mobile.Family == "Android")
                  goToHmPageUrl = "http://emocion.dev.kiwee.com/testWeb/";
              else
                 goToHmPageUrl = "./default_w.aspx";
              

              #endregion

              //pass content bytes and name  to download page
            
              var webClient = new WebClient();
              Trace.Warn(dwldUrl);
              byte[] imageBytes = webClient.DownloadData(dwldUrl);
              string extension = Path.GetExtension(dwldUrl);
              Session["contentByte"] = imageBytes;
              Session["contentName"] = name;
              Session["extension"] = extension;
              
          }
              
        }
    }



