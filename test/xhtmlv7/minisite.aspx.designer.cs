﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3643
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace xhtml_v7 {
    
    
    public partial class minisite {
        
        /// <summary>
        /// tbHeader control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTable tbHeader;
        
        /// <summary>
        /// tbHeaderDestacados control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTable tbHeaderDestacados;
        
        /// <summary>
        /// tbImages control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTable tbImages;
        
        /// <summary>
        /// rowImages control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTableRow rowImages;
        
        /// <summary>
        /// rowImg control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTableRow rowImg;
        
        /// <summary>
        /// rowTitlesImg control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTableRow rowTitlesImg;
        
        /// <summary>
        /// tbTop control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTable tbTop;
        
        /// <summary>
        /// tbHeader4 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTable tbHeader4;
        
        /// <summary>
        /// tbThemes control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTable tbThemes;
        
        /// <summary>
        /// rowTemas control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTableRow rowTemas;
        
        /// <summary>
        /// tbHeader2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTable tbHeader2;
        
        /// <summary>
        /// tbAnims control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTable tbAnims;
        
        /// <summary>
        /// rowAnims control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTableRow rowAnims;
        
        /// <summary>
        /// tbHeader3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTable tbHeader3;
        
        /// <summary>
        /// tbVideos control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTable tbVideos;
        
        /// <summary>
        /// rowVideos control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTableRow rowVideos;
        
        /// <summary>
        /// tbTemas control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTable tbTemas;
        
        /// <summary>
        /// tbLinks control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::xhtml_v7.Tools.XhtmlTable tbLinks;
    }
}
