﻿using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;

namespace xhtml_v7
{
    public partial class savecontent : XCatalogBrowsing
    {
        public bool is3g = false;
        public string volver, subir;
        public string salvar;
        public string compartir;
        public string enviar;
        public string group = "web";
        public int var;
       


        protected void Page_Load(object sender, EventArgs e)
        {

          
            _mobile = (MobileCaps)Request.Browser;
            try { is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType)); }
            catch { is3g = false; }

            try
            {
                WapTools.SetHeader(this.Context);
                WapTools.AddUIDatLog(Request, Response, this.Trace);
            }
            catch { }

            _contentType = (Request.QueryString["ct"] != null) ? Request.QueryString["ct"] : "";
            _contentGroup = WapTools.GetDefaultContentGroup(_contentType);

            var = (Request.QueryString["c"] != null) ? Convert.ToInt32(Request.QueryString["c"]) : 0;
            salvar = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "salvar.gif");
            enviar = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "enviar.gif");
            compartir = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "compartir.gif");


            XhtmlTableRow row = new XhtmlTableRow();
            
            #region HEADER
            XhtmlImage img = new XhtmlImage();
            img.ImageUrl = WapTools.GetImage(this.Request, "imagenes", _mobile.ScreenPixelsWidth, is3g);
            XhtmlTools.AddImgTable(tbHeaderSave, img);
            #endregion

        }
    }
}

