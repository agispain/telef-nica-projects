﻿using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;


namespace xhtml_v7
{
    public partial class chargeEvent : XCatalogBrowsing
    {

        string cg;
        int contentID;
        double price;
        public chargingevents chargeList;
        public string contentProfileID;

        protected void Page_Load(object sender, EventArgs e)
        {
            BillingTools ws = new BillingTools();

            contentProfileID = (Request.QueryString["cpi"] != null) ? Request.QueryString["cpi"] : "";
            cg = (Request.QueryString["cg"] != null) ? Request.QueryString["cg"] : "";
            contentID = (Request.QueryString["id"] != null && Request.QueryString["id"] != "") ? Convert.ToInt32(Request.QueryString["id"]) : 0;

            try
            {
               chargeList = ws.chargeEvnt(this.Trace, this.Request.Headers["TM_user-id"], contentProfileID);
               price = double.Parse(chargeList.price);
               Session["contentInfo"] = price;
            }
            catch
            { 
                Response.Redirect("./error.aspx");
            }

            if (Int32.Parse(chargeList.returnCode) ==110)
                Response.Redirect(String.Format("./downloadPage.aspx?cg={0}&id={1}&cpi={2}", cg, contentID, contentProfileID));
            else
                Response.Redirect("./error.aspx");


        }


       




    }
}
