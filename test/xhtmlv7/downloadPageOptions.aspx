﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="downloadPageOptions.aspx.cs" Inherits="xhtml_v7.downloadPageOptions" %>
<%@ Register tagprefix="xhtml" Namespace="xhtml_v7.Tools" Assembly="xhtml_v7" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
                    <title>Im&aacute;genes y Fondos</title>
                    <link rel="stylesheet" href="xhtml.css" type="text/css" />
                    <meta forua="true" http-equiv="Cache-Control" content="no-cache, max-age=0, must-revalidate, proxy-revalidate, s-maxage=0" />
  </head>
<body>
                    <a id="contentPage" name="contentPage" />
                    <Xhtml:XhtmlTable id="tbHeaderImg" Runat="server" CssClass="normal" />
                    <Xhtml:XhtmlTable id="tbContentTitle" Runat="server" CssClass="normal" />
                    <Xhtml:XhtmlTable id="tbContentThumb" Runat="server"/>
                    <Xhtml:XhtmlTable id="tbContentButtonSub" Runat="server"/>
                    <Xhtml:XhtmlTable id="tbContentPriceSub" Runat="server" CssClass="normal" />
                    <Xhtml:XhtmlTable id="tbContentImgDescription" Runat="server" CssClass="normal" CellSpacing="50" />
                    <Xhtml:XhtmlTable id="tbContentPPDButton" Runat="server" ></Xhtml:XhtmlTable>
                    <Xhtml:XhtmlTable id="tbContentPricePPD" Runat="server" CssClass="normal" CellSpacing="50" />
                    	
</body>
</html>
