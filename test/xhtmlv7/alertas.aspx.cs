using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;

namespace xhtml_v7
{
	public partial class alertas : System.Web.UI.Page
	{
		public string buscar, emocion, back, up, fondo, css = "xhtml.css", header,  volver, subir; 
		public bool is3g = false, isWeb = false;
		
		protected void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				MobileCaps _mobile = (MobileCaps)Request.Browser;
				
				try{isWeb = WapTools.isSomething(_mobile.MobileType, "TouchWeb");}
				catch{isWeb = false;}
				
				if (!isWeb) 
				{
					try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType));}
					catch{is3g = false;}
				} else
				{
					css = "web.css";
				}

				try
				{
					WapTools.SetHeader(this.Context);
					WapTools.AddUIDatLog(Request, Response, this.Trace);				
				}
				catch{}

				#region HEADER
				/*XhtmlImage img = new XhtmlImage();
				img.ImageUrl = WapTools.GetImage(this.Request, "apuntate",  _mobile.ScreenPixelsWidth, is3g, WapTools.isSomething(_mobile.MobileType, "TouchWeb"));
				XhtmlTools.AddImgTable(tbHeader, img);*/
				#endregion

				
				XhtmlTools.AddLinkTable("", tbAlertas, WapTools.GetText("Alerta1"), "./linkto.aspx?id=21", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
				XhtmlTools.AddLinkTable("", tbAlertas, WapTools.GetText("Alerta2"), "./linkto.aspx?id=22", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
				
				XhtmlTools.AddLinkTable("", tbAlertas, WapTools.GetText("Alerta3"), "./linkto.aspx?id=23", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
				XhtmlTools.AddLinkTable("", tbAlertas, WapTools.GetText("Alerta4"), "./linkto.aspx?id=24", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
				
				try
				{
					if (WapTools.isTestSite(this.Request) || Request.Headers["TM_user-id"].ToString() != "0349636776174915535")
					{
						XhtmlTools.AddLinkTable("", tbAlertas, WapTools.GetText("Alerta8"), "./linkto.aspx?id=31", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));						
						XhtmlTools.AddLinkTable("", tbAlertas, WapTools.GetText("Alerta9"), "./linkto.aspx?id=32", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));						
					}
				}
				catch{}
				
				XhtmlTools.AddLinkTable("", tbAlertas, WapTools.GetText("Alerta5"), "./linkto.aspx?id=25", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
				
				#region 3G
				if (is3g)
				{
					css = "3g.css";
					//tbHeader.Visible = false;
					header = WapTools.GetImage3g(this.Request, "apuntate",  _mobile.ScreenPixelsWidth);		
					volver = WapTools.getPicto3g(this.Request.ApplicationPath, "volver", _mobile.ScreenPixelsWidth);
					subir = WapTools.getPicto3g(this.Request.ApplicationPath, "subir", _mobile.ScreenPixelsWidth);		
				}
				#endregion

				
				_mobile = null;

			}
			catch(Exception caught)
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\xhtml\\alertas.aspx - UA : {0}", Request.UserAgent), caught);
				Response.Redirect("./error.aspx");			
			}
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}