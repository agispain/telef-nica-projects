﻿using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;

namespace xhtml_v7
{
    public partial class checkContentValidatePPD : XCatalogBrowsing
    {
        string cg;
       int contentID;
        public checksubscriptions checkSub;
        double price, finalprice;
        protected void Page_Load(object sender, EventArgs e)
        {
            BillingTools web = new BillingTools();

            _contentProfileID = (Request.QueryString["cpi"] != null) ? Request.QueryString["cpi"] : "";
            cg = (Request.QueryString["cg"] != null) ? Request.QueryString["cg"] : "";
            contentID=(Request.QueryString["id"] != null && Request.QueryString["id"] != "") ? Convert.ToInt32(Request.QueryString["id"]) : 0;


            #region checksubscription
            try
            {
                Trace.Warn("blocer");
                checkSub = web.checkSubscrip(this.Trace, this.Request.Headers["TM_user-id"], _contentProfileID);


                price = Double.Parse(checkSub.price);
                price = price + 0.21 * price;


                finalprice = Math.Round((double)price, 2);
            }
            catch
            {
                Response.Redirect("./error.aspx");
            }
            if (Int32.Parse(checkSub.returnCode) == 420)
            {
                Response.Redirect(String.Format("./downloadPage.aspx?cg={0}&id={1}&cpi={2}",cg,contentID,_contentProfileID));
            }

            else Response.Redirect("./error");

            #endregion

            
        }
    }
}
