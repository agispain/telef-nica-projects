using System;
using System.Collections;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Presentation;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;
using System.Xml;

namespace xhtml_v7
{	
	public partial class _default : XCatalogBrowsing  
	{
		private ArrayList _contentCollImg = new ArrayList(); 
		private ArrayList _contentCollContentSet = new ArrayList();  
		protected XhtmlTable tbEspecial, tbPostales,     tbCanales, tbTopNews,    tbCategorias, tbLinkImage, tbLinkImage2,   tbNew  ;  
		protected XhtmlTableRow rowPostales,    rowPub,  rowNew     ;
		protected XhtmlTableCell           cellShop3, cellShop4, cellShop5, cellShop6;
		protected XhtmlTable tbTitleCanales,    tbHeaderEnd2  ;
		int init = 0, day; //, promo = -1; 
		public string yahoo, buscar, emocion, search, back, up, fondo, marquee, musica, p1, p2, p3, p4, p6, header, f1, f2, f3, f4, f5, banner_footer, volver, subir, css = "xhtml.css";
		public bool is3g = false, isIE, isBig = false;
		private bool isPurchaser = false; 
		private bool is099Purchaser = false; 
		private bool isOldUser = false;
		private bool is2x1 = false;
		protected xhtml_v7.Tools.XhtmlTableRow Xhtmltablerow2;
		protected xhtml_v7.Tools.XhtmlTableRow Xhtmltablerow3;
		protected xhtml_v7.Tools.XhtmlTableRow Xhtmltablerow4;
		public bool isMstore = true;
		//CommandCollection cc = new CommandCollection();
        public string subscribe, comprar, download;
        public Subscriptions sb;
        public Subscriptions sub;
        public string Is099purchaser;
        public string is4purchaser;
        public string contentProfileID;
        public string contentProfileID4;
        public string group = "web";
        public bool isMobile240 = false;

	 
		protected void Page_Load(object sender, System.EventArgs e)
		{ 
			try
			{

               #region 09

                contentProfileID = "31690";
                Subscriptions sb = new Subscriptions();
                BillingTools ws = new BillingTools();

                sb = ws.getChargingPro(this.Request.Headers["TM_user-id"], contentProfileID);
                Is099purchaser = sb.returnCode;
                #endregion

                #region 4
               
                contentProfileID4 = "25609";
                Subscriptions sub = new Subscriptions();
                sub = ws.getChargingPro(this.Request.Headers["TM_user-id"], contentProfileID4);
                is4purchaser = sub.returnCode;
                #endregion

                

                _mobile = (MobileCaps)Request.Browser;
				Trace.Write("Family: " + _mobile.MobileType);
                isMobile240 = _mobile.ScreenPixelsWidth > 240; // Group8
                group = (isMobile240) ? "web" : "web16";
				
			/*	if (_mobile.Family == "Android") // || WapTools.isSomething(_mobile.MobileType, "Android") || WapTools.isSomething(_mobile.MobileType, "Android16B"))
				{
					try{Response.Redirect("http://emocion.kiwee.com/web", false);}
					catch{Response.Redirect("http://emocion.kiwee.com/web", true);}
				} 
				else if (_mobile.Family == "Web")// (WapTools.isSomething(_mobile.MobileType, "TouchWeb"))
				{
					try{Response.Redirect("./default_w.aspx", false);}
					catch{Response.Redirect("./default_w.aspx", true);}
				} */
				/*else if (WapTools.isSomething(_mobile.MobileType, "Touch"))
				{
					 try{Response.Redirect("./default_t.aspx", false);}
					 catch{Response.Redirect("./default_t.aspx", true);}
				}*/
				//else
				{
					Trace.Warn("_mobile: " + _mobile.MobileType);
					
					search = ConfigurationSettings.AppSettings["UrlSearch"];
				
					try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType));}
					catch{is3g = false;}

					Trace.Warn("is3G: " + is3g.ToString());
					if (!WapTools.isMStore(_mobile.MobileType)) 
						isMstore = false;
					Trace.Warn("isMstore: " + isMstore.ToString());
					
					try{isBig = Convert.ToBoolean(WapTools.isBig(_mobile.MobileType));}
					catch{isBig = false;}
					Trace.Warn("isBig: " + isBig.ToString());
										
					try
					{
						WapTools.SetHeader(this.Context);
						WapTools.LogUser(this.Request, is3g ? 107 : 100, _mobile.MobileType);
						WapTools.AddUIDatLog(Request, Response, this.Trace);				
					} 
					catch{} 

					if (isBig)
						yahoo = WapTools.GetImage(this.Request, "yahoo2");
					else
						yahoo = WapTools.GetImage(this.Request, "yahoo");
					Trace.Warn("yahoo: " + yahoo);
					
					if (_mobile.MobileType != null) 
					{	
						Trace.Warn("MobileType: " + _mobile.MobileType);
					
						_displayKey = WapTools.GetXmlValue("DisplayKey");
					
						try
						{
							Customer customer = new Customer(this.Request);
							int p099 = WapTools.isPurchaserClub099(customer);
							is099Purchaser = (p099 > 0 && p099 < 15);
							//						int numVisits = -1;
							//						
							//						Customer customer = new Customer(this.Request);
							//						User u = new User();
							//						numVisits = u.countVisits(customer.Alias, DateTime.Now.AddMonths(-6), DateTime.Today.AddDays(-1).AddHours(18), 100, 100000);									
							//						isOldUser = (numVisits > 0);
							//
							//						if (isOldUser)
							//						{
							//							isPurchaser = WapTools.isPurchaser(customer);
							//
							//							if (!isPurchaser)
							//								is2x1 = true;
							//
							//							//if (!isPurchaser)
							//							//	is2x1 = WapTools.is2x1(customer);
							//
							//							//Trace.Warn("isOldUser: " + isOldUser.ToString() + " - is2x1: " + is2x1.ToString() + Convert.ToInt32(is2x1).ToString());
							//						}
							//
							//						 //cc = customer.LoadCommands(CommandStatus.Delivered, DateTime.Now.AddMonths(-3), DateTime.Now, new Guid(_displayKey), _mobile.MobileType, "IMG_COLOR");
							//						//cc = customer.LoadCommands(CommandStatus.Delivered, new Guid(_displayKey), "IMG_COLOR");
							//						//Trace.Write("Commands: " + cc.Count.ToString());
							//						customer = null;
						}
						catch(Exception ex)
						{
							Trace.Warn(ex.Message);
							//cc = null;
						}
						Trace.Warn("is099Purchaser: " + is099Purchaser.ToString());
					

						//					int rating = 0;
						//					if (cc != null && cc.Count > 0)
						//						foreach (Command c in cc)
						//							if (c.Item.Referer.ToLower().IndexOf("sex")>0) rating++;
						//
						//					Trace.Write("Rating: " + rating.ToString());
						//					if ((rating > 0 && (Convert.ToDouble(rating)/Convert.ToDouble(cc.Count)) > 0.10))
						//						_idContentSet = Convert.ToInt32(WapTools.GetXmlValue("Home/CompositeSexy"));
						//					else
					
						/*if ((DateTime.Now.Hour >= 17 && DateTime.Now.Hour <= 24) || (DateTime.Now.Hour >= 0 && DateTime.Now.Hour <= 2))
							_idContentSet = Convert.ToInt32(WapTools.GetXmlValue("Home/Models"));					
						else*/
						_idContentSet = Convert.ToInt32(WapTools.GetXmlValue("Home/Composite"));					

						day = (Request.QueryString["day"] != null) ? Convert.ToInt32(Request.QueryString["day"]) : 0;
						/*	if(WapTools.isTestSite(this.Request)&& _mobile.IsCompatible("FLASH_SCREENSAVER"))
							{
								try { _idContentSet = Convert.ToInt32(WapTools.GetXmlNode("Home/Composite").Attributes["test"].Value); }
								catch { _idContentSet = 0; }
							}
							else 
						*/		
						_contentGroup = "IMG";
						Trace.Warn("_idContentSet: " + _idContentSet.ToString());
					
						var content=BrowseContentSetExtended(null, -1, -1);
                        Session["homePageCatalog"] = content;
						//try{isIE = (_mobile.MobileType == "_IE");}
						//catch{isIE = false;}

						if( _mobile.IsCompatible("IMG_COLOR") )
						{
							#region HEADER
							Trace.Warn("Header");
					
							XhtmlImage img = new XhtmlImage();
							img.ImageUrl = WapTools.GetImage(this.Request, "imagenes",  _mobile.ScreenPixelsWidth, is3g);
							XhtmlTools.AddImgTable(tbHeader, img);
							//if (is3g) 
								tbHeader.Visible = false;
							#endregion
  
							#region PUB
							try
							{
								Trace.Warn("Showing Pub");
					
								//							if (WapTools.isTestSite(this.Request))
								//							{
								//							if (is2x1)
								//								WapTools.CallNewPubTEF(this.Trace, this.Request, this.Request.Headers["TM_user-id"], tbEnd2, (is3g) ? 1095 : 1093);
								//							else

								//if (WapTools.isTestSite(this.Request))
								//{
							   	//WapTools.soaprequest(this.Trace);
                                WapTools.CallNewPubTEF2(this.Trace, this.Request, this.Request.Headers["TM_user-id"], tbPub, "183983");
                                WapTools.CallNewPubTEF2(this.Trace, this.Request, this.Request.Headers["TM_user-id"], tbPub2, "183981");
								//}
								//else
								//{
								//if (DateTime.Now.Hour >= 15 && DateTime.Now.Hour <= 16)
								//	WapTools.CallNewPubTEF(this.Trace, this.Request, this.Request.Headers["TM_user-id"], tbEnd2, (is3g) ? 1095 : 1093);
								//else
								//		WapTools.CallNewPubTEF(this.Trace, this.Request, this.Request.Headers["TM_user-id"], tbPub, (is3g) ? 1095 : 1093);
								
								//	WapTools.CallNewPubTEF(this.Trace, this.Request, this.Request.Headers["TM_user-id"], tbPub2, (is3g) ? 1543 : 1589);
								//}
								//							}
								//							else
								//							{
								//								WapTools.CallPubTEF(this.Trace, this.Request, this.Request.Headers["TM_user-id"], WapTools.SearchType.ts_e2_0_9_p_banner_top.ToString(), tbPub, (is3g) ? 1095 : 1093, _mobile.MobileType);
								//								WapTools.CallPubTEF(this.Trace, this.Request, this.Request.Headers["TM_user-id"], WapTools.SearchType.ts_e2_0_9_p_banner_top.ToString(), tbPub2, (is3g) ?  1543 : 1589, _mobile.MobileType);
								//							}
							}
							catch{}
							#endregion  
    
							#region PROMO
							if (isOldUser && !isPurchaser)
							{
								WapTools.LogUser(this.Request, 206, _mobile.MobileType);
					
								XhtmlTableRow rowPromo = new XhtmlTableRow();
								XhtmlTools.AddTextTableRow(rowPromo, WapTools.GetText("2x1"), Color.Empty, Color.Black, 1, HorizontalAlign.Center,  VerticalAlign.Middle, true, FontUnit.XSmall);
								tbHeaderAhora.Rows.Add(rowPromo);
							}
							//						promo = WapTools.isPromo(this.Request);
							//						Trace.Warn(promo.ToString());
							//						if (promo >= 0)
							//						{
							//							XhtmlTableRow rowPromo = new XhtmlTableRow();
							//							XhtmlTools.AddTextTableRow(rowPromo, WapTools.GetXmlValue("Promo/Message" + promo.ToString()), Color.Empty, Color.Green, 1, HorizontalAlign.Center,  VerticalAlign.Middle, true, FontUnit.XSmall);
							//							tbHeaderAhora.Rows.Add(rowPromo);
							//						}
							#endregion

							#region AHORA EN IMAGENES - ESPECIAL 
							img = new XhtmlImage();
							img.ImageUrl = WapTools.GetImage(this.Request, "ahora",  _mobile.ScreenPixelsWidth, is3g);
							XhtmlTools.AddImgTable(tbHeaderAhora, img);
							//marquee="<marquee><a href='./linkto.aspx?cg=IMG&id=11032'><b>"+Server.HtmlEncode("Entra en la Zona VIP y participa en el sorteo 2 Nokia 6110")+"</b></a></marquee>";
							//if (WapTools.isTestSite(this.Request))
							//	XhtmlTools.AddTextTable(tbHeaderAhora, marquee, Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XSmall);
							int dayCounter = Convert.ToInt32(WapTools.GetText("DayCounter"));
							int dia = WapTools.isTestSite(this.Request) ? (day != 0 && day <= 31 && day >= 1) ? day : DateTime.Now.AddDays(dayCounter).Day : DateTime.Now.Day;
						
							Especial esp = new Especial();
						
							try  
							{						
								esp = WapTools.getEspecial(1, dia.ToString(), _mobile);
								if(esp.name == "" || !_mobile.IsCompatible(esp.filter))
									esp = WapTools.GetCompatibleEspecial(esp);
								//							try{if (Request.Headers["TM_user-id"].ToString() == "0349636776174915535" && esp.name == "Especial190") esp.name = "Especial5";}
								//							catch{}
								//							try{if (Request.Headers["TM_user-id"].ToString() == "0349636776174915535" && esp.name == "Especial194") esp.name = "Especial20";}
								//							catch{}
								XhtmlTools.AddLinkTableCell("img", cellLink, WapTools.GetText(esp.name), WapTools.GetText("Link" + esp.name), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, "");
								XhtmlImage picto = new XhtmlImage();
								if (isBig)
									picto.ImageUrl = String.Format("{0}/Images/specials/{1}", this.Request.ApplicationPath, esp.name.ToLower() + "_8.gif");									
								else if (is3g && _mobile.ScreenPixelsWidth>=229)
									picto.ImageUrl = String.Format("{0}/Images/specials/{1}", this.Request.ApplicationPath, esp.name.ToLower() + "_3G.gif");		
								else
									picto.ImageUrl = String.Format("{0}/Images/specials/{1}", this.Request.ApplicationPath, esp.name.ToLower() + ".gif");		
							
								cellImg.Controls.Add(picto); 
								cellImg.HorizontalAlign = HorizontalAlign.Center; 
								picto = null;   
							}
							catch{rowEspecial.Visible = false;} 
							try 
							{
								dia = WapTools.isTestSite(this.Request) ? (day != 0 && day <= 31 && day >= 1) ? day : DateTime.Now.AddDays(dayCounter).Day : DateTime.Now.Day;
								//dia = 26;
								esp = WapTools.getEspecial(2, dia.ToString(), _mobile);
								if (esp.name == "" || !_mobile.IsCompatible(esp.filter))
									esp = WapTools.GetCompatibleEspecial(esp);	
								//							try{if (Request.Headers["TM_user-id"].ToString() == "0349636776174915535" && esp.name == "Especial190") esp.name = "Especial5";}
								//							catch{}
								//							try{if (Request.Headers["TM_user-id"].ToString() == "0349636776174915535" && esp.name == "Especial194") esp.name = "Especial20";}
								//							catch{}

								XhtmlTools.AddLinkTableCell("img", cellLink2, WapTools.GetText(esp.name), WapTools.GetText("Link" + esp.name), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, "");

								XhtmlImage picto2 = new XhtmlImage();
								//picto2.ImageUrl = WapTools.GetImage(this.Request, especial);
								if (isBig)
									picto2.ImageUrl = String.Format("{0}/Images/specials/{1}", this.Request.ApplicationPath, esp.name.ToLower() + "_8.gif");									
								else if (is3g && _mobile.ScreenPixelsWidth>=229)
									picto2.ImageUrl = String.Format("{0}/Images/specials/{1}", this.Request.ApplicationPath, esp.name.ToLower() + "_3G.gif");		
								else
									picto2.ImageUrl = String.Format("{0}/Images/specials/{1}", this.Request.ApplicationPath, esp.name.ToLower() + ".gif");		
								cellImg2.Controls.Add(picto2); 
								cellImg2.HorizontalAlign = HorizontalAlign.Center;
								picto2 = null;  
							}
							catch{rowEspecial2.Visible = false;}
							try
							{ 
								dia = WapTools.isTestSite(this.Request) ? (day != 0 && day <= 31 && day >= 1) ? day : DateTime.Now.AddDays(dayCounter).Day : DateTime.Now.Day;
								//dia = 26; 
								esp = WapTools.getEspecial(3, dia.ToString(), _mobile);
								if(esp.name == "" || !_mobile.IsCompatible(esp.filter))
									esp = WapTools.GetCompatibleEspecial(esp);
								XhtmlTools.AddLinkTableCell("img", cellLink3, WapTools.GetText(esp.name), WapTools.GetText("Link" + esp.name), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, "");
								XhtmlImage picto2 = new XhtmlImage();
								//picto2.ImageUrl = WapTools.GetImage(this.Request, especial);
								if (isBig)
									picto2.ImageUrl = String.Format("{0}/Images/specials/{1}", this.Request.ApplicationPath, esp.name.ToLower() + "_8.gif");									
								else if (is3g && _mobile.ScreenPixelsWidth>=229)
									picto2.ImageUrl = String.Format("{0}/Images/specials/{1}", this.Request.ApplicationPath, esp.name.ToLower() + "_3G.gif");		
								else
									picto2.ImageUrl = String.Format("{0}/Images/specials/{1}", this.Request.ApplicationPath, esp.name.ToLower() + ".gif");		
								cellImg3.Controls.Add(picto2);
								cellImg3.HorizontalAlign = HorizontalAlign.Center;
								picto2 = null;
							}
							catch{rowEspecial3.Visible = false;}
							if (is3g)
							{
								try
								{ 
									dia = WapTools.isTestSite(this.Request) ? (day != 0 && day <= 31 && day >= 1) ? day : DateTime.Now.AddDays(dayCounter).Day : DateTime.Now.Day;
									//dia = 26; 
									esp = WapTools.getEspecial(4, dia.ToString(), _mobile);
									if(esp.name == "" || !_mobile.IsCompatible(esp.filter))
										esp = WapTools.GetCompatibleEspecial(esp);
									XhtmlTools.AddLinkTableCell("img", cellLink4, WapTools.GetText(esp.name), WapTools.GetText("Link" + esp.name), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, "");
									XhtmlImage picto2 = new XhtmlImage();
									if (isBig)
										picto2.ImageUrl = String.Format("{0}/Images/specials/{1}", this.Request.ApplicationPath, esp.name.ToLower() + "_8.gif");									
									else if (is3g && _mobile.ScreenPixelsWidth>=229)
										picto2.ImageUrl = String.Format("{0}/Images/specials/{1}", this.Request.ApplicationPath, esp.name.ToLower() + "_3G.gif");		
									else
										picto2.ImageUrl = String.Format("{0}/Images/specials/{1}", this.Request.ApplicationPath, esp.name.ToLower() + ".gif");		
									cellImg4.Controls.Add(picto2);
									cellImg4.HorizontalAlign = HorizontalAlign.Center;
									picto2 = null;
								}
								catch{rowEspecial4.Visible = false;}
							}
							#endregion 
						
							#region IMAGENES DESTACADAS

                            subscribe = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "subscribe.gif");
                            comprar = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "comprar.gif");
                            download = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "download.gif");

							img = new XhtmlImage(); 
							img.ImageUrl = WapTools.GetImage(this.Request, is3g ? "destacados_home" : "destacados",  _mobile.ScreenPixelsWidth, is3g);
							XhtmlTools.AddImgTable(tbHeaderDestacados, img);
							try      
							{ 
								init = DateTime.Now.Millisecond;			
								int value = Convert.ToInt32(WapTools.GetXmlValue("Home/Subcomposite"));
								if (_contentCollImg.Count > 0)
								{
									// Display Images from 0 to value (5)
									DisplayImages(rowImg, "IMG", "IMG_COLOR", (init % value));
                                    int c = value + (init % (_contentCollImg.Count - 2 - value));

									//if (_mobile.MobileType != "SONYERICSSONZ610I")
									// int c = value + (init % (_contentCollImg.Count - 2 - value));
                                   
									   DisplayImages(rowTitlesImg, "IMG", "IMG_COLOR", c);

                                      
								
									//								if (_mobile.ScreenPixelsWidth < 140)
									//									DisplayImages(rowTitlesImg, "IMG", "IMG_COLOR", init+1, 0);
									//								else
									//									DisplayImages(rowTitlesImg, "IMG", "IMG_COLOR", init+2, 0);
									try
									{
										//DisplayTitles(tbImages, c+2);
										//if (DateTime.Now.Hour >= 15 && DateTime.Now.Hour <= 16)
										//if ((DateTime.Now.Hour >= 17 && DateTime.Now.Hour <= 24) || (DateTime.Now.Hour >= 0 && DateTime.Now.Hour <= 2))					
										//	DisplayTitles(tbImages, c+6);
									}
									catch{}
								}  
							}
							catch{}
 
							//	XhtmlTools.AddLinkTable("IMG", tbTop, "Galer�a", "http://emocion.kiwee.com/xhtml/galeria/catalog.aspx?cg=IMG&cs=2692", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//	else 
						
							//XhtmlTools.AddLinkTableRow("IMG", rowNew,WapTools.GetText("NewImg"), String.Format("./linkto.aspx?cg=IMG&id={0}", WapTools.GetXmlValue("Home/New")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						 
							//XhtmlTools.AddLinkTableRow("IMG", rowRec, WapTools.GetText("Recomendados"), "#", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "movblue"));
							#endregion  

							#region POR CATEGORIAS 
				
							try
							{							
								//							if (_mobile.MobileType == "NOKIA2630" || _mobile.MobileType == "NOKIA2760" || _mobile.MobileType == "NOKIA6085" || _mobile.MobileType == "BLACKBERRY8320")
								//								XhtmlTools.AddLinkTableRow("", rowTop, WapTools.GetText("club"), "./linkto.aspx?id=11036", Color.Empty, Color.White, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "club"));
								//							else 
								//XhtmlTools.AddLinkTable("IMG", tbTop, "Espa�a Campeona del Mundo de Futbol 2010", "./linkto.aspx?id=6591&cg=IMG", Color.Empty, Color.White, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								//XhtmlTools.AddLinkTable("IMG", tbTop, "Especial NAVIDAD", "./linkto.aspx?id=11062", Color.Empty, Color.White, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								XhtmlTools.AddLinkTable("IMG", tbTop, "NEW!!! Todas los fondos a 0.99� por semana", "./linkto.aspx?id=3619&cg=COMPOSITE&cl=1", Color.Empty, Color.White, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								//XhtmlTools.AddLinkTable("IMG", tbTop, "Especial San Ferm�n", "./linkto.aspx?id=11031", Color.Empty, Color.White, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								XhtmlTools.AddLinkTable("IMG", tbTop, WapTools.GetText("avatar"), "./linkto.aspx?id=11039", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "muggins"));
								
								//XhtmlTools.AddLinkTable("IMG", tbTop, "Especial Navidad!!!", "./linkto.aspx?cg=IMG&id=6339", Color.Empty, Color.White, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								//XhtmlTools.AddLinkTable("IMG", tbTop, "Viva San Ferm�n!!!", "./linkto.aspx?cg=COMPOSITE&id=11031", Color.Empty, Color.White, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "love"));
								//XhtmlTools.AddLinkTable("IMG", tbTop, "Halloween", "./linkto.aspx?id=11015", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "halloween"));
								//XhtmlTools.AddLinkTable("IMG", tbTop, "Especial San Valent�n", "./linkto.aspx?id=11017", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "love"));
							
								if (_mobile.IsCompatible("FLASH_SCREENSAVER"))
									XhtmlTools.AddLinkTable("IMG", tbTop, WapTools.GetText("FondosFlash"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FLASH")), Color.Empty, Color.White, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "club"));
								//XhtmlTools.AddLinkTableRow("", rowTop, WapTools.GetText("club"), "./linkto.aspx?id=11032", Color.Empty, Color.White, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "club"));
								XhtmlTools.AddLinkTableRow("", rowTop, WapTools.GetText("club"), "./linkto.aspx?id=11036", Color.Empty, Color.White, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "club"));

								//if (WapTools.isTestSite(this.Request))
								
							 
								//							if (WapTools.isTestSite(this.Request) || (DateTime.Now.Day == 30 || DateTime.Now.Day == 1))
								//								XhtmlTools.AddLinkTable("IMG", tbTop, "Especial D�a del Trabajo", "./linkto.aspx?id=6568&cg=IMG", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "love"));
								//							if (WapTools.isTestSite(this.Request) || (DateTime.Now.Day == 2 || DateTime.Now.Day == 3))
								//								XhtmlTools.AddLinkTable("IMG", tbTop, "Especial D�a de la Madre", "./linkto.aspx?id=6567&cg=IMG", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "love"));
							
								//	XhtmlTools.AddLinkTable("IMG", tbTop, WapTools.GetText("6050"), "./linkto.aspx?id=11011", Color.Red, Color.White, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								//	if(WapTools.isTestSite(this.Request))
								//		XhtmlTools.AddLinkTable("IMG", tbTop, WapTools.GetText("BatmanImg"), String.Format("./linkto.aspx?cg=IMG&id={0}", WapTools.GetXmlValue("Home/Batman")), Color.Red, Color.White, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								//								if(WapTools.isTestSite(this.Request))
								//									XhtmlTools.AddLinkTable("IMG", tbTop, WapTools.GetText("TopImg"), String.Format("./linkto.aspx?cg=IMG&id={0}&Destacado=Destacado_emocion", WapTools.GetXmlValue("Home/Top")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								//								else
								XhtmlTools.AddLinkTable("IMG", tbTop, WapTools.GetText("TopImg"), String.Format("./linkto.aspx?cg=IMG&id={0}&cl=1", WapTools.GetXmlValue("Home/Top")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
									
								XhtmlTools.AddLinkTable("IMG", tbTop, WapTools.GetText("AmorImg"), String.Format("./linkto.aspx?cg=IMG&id={0}", WapTools.GetXmlValue("Home/Amor")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								//if(! WapTools.isJunior(_mobile))
								XhtmlTools.AddLinkTable("IMG", tbTop, WapTools.GetText("ModelsImg"), String.Format("./linkto.aspx?cg=IMG&id={0}", WapTools.GetXmlValue("Home/Models")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								XhtmlTools.AddLinkTable("IMG", tbTop, WapTools.GetText("Gigigo"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/GIGIGO")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.NotSet, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								//XhtmlTools.AddLinkTable("IMG", tbTop, WapTools.GetText("Alerta3"), "./linkto.aspx?id=23", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								//XhtmlTools.AddLinkTable("IMG", tbTop, WapTools.GetText("Alerta4"), "./linkto.aspx?id=24", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));											
							}
							catch{}
							if (is3g)
								XhtmlTools.AddLinkTable("IMG", tbTop, WapTools.GetText("Categorias"), "./linkto.aspx?cg=COMPOSITE&id=3619", Color.FromName("#cde4fd"), Color.Empty, 1, HorizontalAlign.Right, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							else
								XhtmlTools.AddLinkTable("IMG", tbTop, WapTools.GetText("Categorias"), "./linkto.aspx?cg=COMPOSITE&id=3619", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							#endregion
				  
							#region FELICITACIONES
					
							img = new XhtmlImage();
							img.ImageUrl = WapTools.GetImage(this.Request,  "felicitaciones",  _mobile.ScreenPixelsWidth, is3g);
							XhtmlTools.AddImgTable(tbEnd2, img);
							
							//if (Request.Headers["TM_user-id"] == WapTools.GetXmlValue("Home/JSV_id") || (Request.Headers["TM_user-id"] == WapTools.GetXmlValue("Home/Fer_id")))
							if (is3g)
								XhtmlTools.AddLinkTable("IMG", tbEnd2, WapTools.GetText("ZonaVipFelicitaciones"), "./linkto.aspx?cg=COMPOSITE&id=10093", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "club"));
						
							XhtmlTools.AddLinkTable("IMG", tbEnd2, WapTools.GetText("FondoDedicatorias"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FONDODEDICATORIAS")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							XhtmlTools.AddLinkTable("IMG", tbEnd2, WapTools.GetText("Animadas"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/ANIMADAS")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							if (_mobile.IsCompatible("VIDEO_DWL"))
								XhtmlTools.AddLinkTable("IMG", tbEnd2, WapTools.GetText("Videofelicitaciones"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEOFELICITACIONES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//if (!(Request.Headers["TM_user-id"] == WapTools.GetXmlValue("Home/JSV_id") || (Request.Headers["TM_user-id"] == WapTools.GetXmlValue("Home/Fer_id"))))
							//	XhtmlTools.AddLinkTable("IMG", tbEnd2, WapTools.GetText("ZonaVipFelicitaciones"), "./linkto.aspx?cg=COMPOSITE&id=10093", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "club"));
							
							#endregion	

							#region DESCARGATE
							img = new XhtmlImage();
							img.ImageUrl = WapTools.GetImage(this.Request, is3g ? "descargate_home" : "descargate",  _mobile.ScreenPixelsWidth, is3g);
							XhtmlTools.AddImgTable(tbHeaderEnd, img);
						
							//if (_mobile.IsCompatible("FLASH_SCREENSAVER"))
							//	XhtmlTools.AddLinkTable("IMG", tbEnd, WapTools.GetText("FLASH"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FLASH")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							if (_mobile.IsCompatible("BG_SCHEME")) 
							{
								XhtmlTools.AddLinkTable("IMG", tbEnd, WapTools.GetText("Temas"), "./linkto.aspx?id=7311&cg=COMPOSITE", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							}
							else if (WapTools.isCompatibleThemes(_mobile)) 
							{
								if (is3g)
									XhtmlTools.AddLinkTable("IMG", tbEnd, WapTools.GetText("VipTemas"), "./linkto.aspx?cg=COMPOSITE&id=10096", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "club"));

								//							if (_mobile.MobileType == "NOKIA2630" || _mobile.MobileType == "NOKIA2760")
								//								XhtmlTools.AddLinkTable("IMG", tbEnd, WapTools.GetText("Temas"), "./linkto.aspx?cg=SCHEME&id=5846", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								//XhtmlTools.AddLinkTable("IMG", tbEnd, WapTools.GetText("Temas"), "./linkto.aspx?id=10001", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							}
							
							if (_mobile.IsCompatible("ANIM_COLOR"))
								XhtmlTools.AddLinkTable("IMG", tbEnd, WapTools.GetText("ANIM"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/ANIM")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							if (_mobile.IsCompatible("VIDEO_DWL")) 
							{
								XhtmlTools.AddLinkTable("IMG", tbEnd, WapTools.GetText("VIDEO"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEO")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								XhtmlTools.AddLinkTable("IMG", tbEnd, "NEW!!! Todas los videos a 1.99� por semana", String.Format("./linkto.aspx?cg=COMPOSITE&id={0}&cl=1", WapTools.GetXmlValue("Home/VIDEO")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							}
							//	if (WapTools.isTestSite(this.Request))
							//		XhtmlTools.AddLinkTable("IMG", tbEnd, WapTools.GetText("6222"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", "11020"), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//	if (WapTools.isTestSite(this.Request))
							//		XhtmlTools.AddLinkTable("IMG", tbEnd, WapTools.GetText("FondoNombresC"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", "10052"), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						
							XhtmlTools.AddLinkTable("IMG", tbEnd, WapTools.GetText("FondoNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FONDONOMBRES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//	XhtmlTools.AddLinkTable("IMG", tbEnd, WapTools.GetText("AnimaNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/ANIMANOMBRES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//	XhtmlTools.AddLinkTable("IMG", tbEnd, WapTools.GetText("FondoDedicatorias"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FONDODEDICATORIAS")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//	if (_mobile.IsCompatible("VIDEO_DWL"))
							//		XhtmlTools.AddLinkTable("IMG", tbEnd, WapTools.GetText("VideoNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEONOMBRES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//	if (_mobile.IsCompatible("VIDEO_DWL"))
							//		XhtmlTools.AddLinkTable("IMG", tbEnd, WapTools.GetText("VideoAnimaciones"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEOANIMACIONES")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//	XhtmlTools.AddLinkTable("IMG", tbEnd, WapTools.GetText("Gigigo"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/GIGIGO")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							if (is3g)
								XhtmlTools.AddLinkTable("IMG", tbEnd, WapTools.GetText("MasDESCARGATE"), "./linkto.aspx?id=27", Color.FromName("#cde4fd"), Color.Empty, 1, HorizontalAlign.Right, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							else
								XhtmlTools.AddLinkTable("IMG", tbEnd, WapTools.GetText("MasDESCARGATE"), "./linkto.aspx?id=27", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							#endregion						

							#region SHOPS y ALERTAS
							//XhtmlTools.AddLinkTable("IMG", tbTitleShop, WapTools.GetText("TitleGallery"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/Gallery")), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.NotSet, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								
							img = new XhtmlImage();
							img.ImageUrl = WapTools.GetImage(this.Request, "portales_home",  _mobile.ScreenPixelsWidth, is3g);
							XhtmlTools.AddImgTable(tbTitleShop, img);

							XhtmlTools.AddLinkTableCell("IMG", cellShop1, WapTools.GetText("Shop37"), "./linkto.aspx?id=37", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));	
							XhtmlTools.AddLinkTableCell("IMG", cellShop2, WapTools.GetText("Shop33"), "./linkto.aspx?id=33", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//XhtmlTools.AddLinkTableCell("IMG", cellShop3, WapTools.GetText("Shop6"), "./linkto.aspx?id=6", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//XhtmlTools.AddLinkTableCell("IMG", cellShop4, WapTools.GetText("Shop19"), "./linkto.aspx?id=19", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//XhtmlTools.AddLinkTableCell("IMG", cellShop5, WapTools.GetText("Shop30"), "./linkto.aspx?id=30", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//						if (is3g)
							//							XhtmlTools.AddLinkTableCell("IMG", cellShop6, WapTools.GetText("Shop4"), "./linkto.aspx?id=4", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//	XhtmlTools.AddLinkTableCell("IMG", cellShop4, WapTools.GetText("Shop11"), "./linkto.aspx?id=11", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//XhtmlTools.AddLinkTableCell("IMG", cellShop5, "Promo Quad", "./linkto.aspx?id=10020", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//XhtmlTools.AddLinkTableCell("IMG", cellShop6, "Promo LFP", "./linkto.aspx?id=10022", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//						XhtmlTools.AddLinkTableRow("IMG", rowShop1,WapTools.GetText("Shop1"), "./linkto.aspx?id=1", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//						XhtmlTools.AddLinkTableRow("IMG", rowShop2,WapTools.GetText("Shop6"), "./linkto.aspx?id=6", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//						XhtmlTools.AddLinkTableRow("IMG", rowShop3,WapTools.GetText("Shop4"), "./linkto.aspx?id=4", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//						XhtmlTools.AddLinkTableRow("IMG", rowShop4,WapTools.GetText("Shop2"), "./linkto.aspx?id=2", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//XhtmlTools.AddLinkTableRow("IMG", rowShop4,WapTools.GetText("Cateto"), "./linkto.aspx?id=10002&cg=COMPOSITE", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							if (is3g)
								XhtmlTools.AddLinkTableRow("IMG", rowmoreshops, WapTools.GetText("moreshops"), "./linkto.aspx?id=20", Color.FromName("#cde4fd"), Color.Empty, 1, HorizontalAlign.Right, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							else
								XhtmlTools.AddLinkTableRow("IMG", rowmoreshops, WapTools.GetText("moreshops"), "./linkto.aspx?id=20", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						
							img = new XhtmlImage();
							img.ImageUrl = WapTools.GetImage(this.Request, "apuntate_home",  _mobile.ScreenPixelsWidth, is3g);
							XhtmlTools.AddImgTable(tbHeaderApuntante, img);
						
							XhtmlTools.AddTextTable(tbEnd3, WapTools.GetText("NewAlertas"), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall);
							//XhtmlTools.AddLinkTable("IMG", tbEnd3, WapTools.GetText("Alerta3"), "./linkto.aspx?id=23", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//XhtmlTools.AddLinkTable("IMG", tbEnd3, WapTools.GetText("Alerta4"), "./linkto.aspx?id=24", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							XhtmlTools.AddLinkTable("IMG", tbEnd3, WapTools.GetText("Alerta6"), "./linkto.aspx?id=28", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							XhtmlTools.AddLinkTable("IMG", tbEnd3, WapTools.GetText("Alerta7"), "./linkto.aspx?id=29", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));

						
							//XhtmlTools.AddLinkTable("IMG", tbEnd3, WapTools.GetText("Alerta1"), "./linkto.aspx?id=21", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//XhtmlTools.AddLinkTable("IMG", tbEnd3, WapTools.GetText("Alerta2"), "./linkto.aspx?id=22", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							//XhtmlTools.AddLinkTable("IMG", tbEnd3, WapTools.GetText("Alerta5"), "./linkto.aspx?id=25", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
						
						
							if (is3g)
								XhtmlTools.AddLinkTable("IMG", tbEnd3, "M�s alertas de Im�genes", "./linkto.aspx?id=26", Color.FromName("#cde4fd"), Color.Empty, 1, HorizontalAlign.Right, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));						
							else
								XhtmlTools.AddLinkTable("IMG", tbEnd3, "M�s alertas de Im�genes", "./linkto.aspx?id=26", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));						
							#endregion

							#region PICTOS
							fondo = WapTools.GetImage(this.Request, "fondo", _mobile.ScreenPixelsWidth, false);
							buscar = WapTools.getPicto(this.Request, "buscar", _mobile);
							emocion = WapTools.getPicto(this.Request, "emocion", _mobile);
							back = WapTools.getPicto(this.Request, "back", _mobile);
							up = WapTools.getPicto(this.Request, "up", _mobile);
							musica = (_mobile.ScreenPixelsWidth > 128) ? "M&uacute;sica y Tonos" : "M&uacute;sica";
							#endregion 

							#region 3G
							if (is3g)
							{
								css = "3g.css";
								p1 = WapTools.getPicto3g(this.Request.ApplicationPath, "p1", _mobile.ScreenPixelsWidth);
								p2 = WapTools.getPicto3g(this.Request.ApplicationPath, "p2", _mobile.ScreenPixelsWidth);
								p3 = WapTools.getPicto3g(this.Request.ApplicationPath, "p3", _mobile.ScreenPixelsWidth);
								p4 = WapTools.getPicto3g(this.Request.ApplicationPath, "p4", _mobile.ScreenPixelsWidth);
								p6 = WapTools.getPicto3g(this.Request.ApplicationPath, "p6", _mobile.ScreenPixelsWidth);
								//							f1 = WapTools.getPicto3g(this.Request.ApplicationPath, "f1", _mobile.ScreenPixelsWidth);
								//							f2 = WapTools.getPicto3g(this.Request.ApplicationPath, "f2", _mobile.ScreenPixelsWidth);
								//							f3 = WapTools.getPicto3g(this.Request.ApplicationPath, "f3", _mobile.ScreenPixelsWidth);
								//							f4 = WapTools.getPicto3g(this.Request.ApplicationPath, "f4", _mobile.ScreenPixelsWidth);
								//							f5 = WapTools.getPicto3g(this.Request.ApplicationPath, "f5", _mobile.ScreenPixelsWidth);
								header = WapTools.GetImage3g(this.Request, "imagenes",  _mobile.ScreenPixelsWidth);		
								//banner_footer = WapTools.getPicto3g(this.Request.ApplicationPath, "footer",  _mobile.ScreenPixelsWidth);		
								volver = WapTools.getPicto3g(this.Request.ApplicationPath, "volver", _mobile.ScreenPixelsWidth);
								subir = WapTools.getPicto3g(this.Request.ApplicationPath, "subir", _mobile.ScreenPixelsWidth);			
							}
							#endregion
						}
					}
					else
					{
						XhtmlTableRow row = new XhtmlTableRow();
						XhtmlTools.AddTextTableRow(row, WapTools.GetText("Compatibility2"), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XSmall);
						tbEnd.Rows.Add(row);
						row = null;
					}
				}
			}
			catch(Exception caught)
			{  
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\xhtml\\default.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
				Response.Redirect("./error.aspx");				
			}
			finally
			{
				_contentCollImg = null;
				_contentCollContentSet = null;
			}
		}


		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
		
		#region Override
        protected override void DisplayContentSet(KMobile.Catalog.Presentation.Content content, System.Web.UI.MobileControls.Panel pnl)
		{
			_contentCollContentSet.Add(content);
		}

        protected override void DisplayImg(KMobile.Catalog.Presentation.Content content, System.Web.UI.MobileControls.Panel pnl)
		{
			//if (WapTools.isJunior(_mobile) && content.ContentRatings[0].Value > 0) return;
			/*if (cc != null && cc.Count > 0)
			{
				foreach (Command c in cc)
					if (c.Item.ContentId == content.IDContent)
						return;
			}*/
			if (content.ContentGroup.Name == "IMG") _contentCollImg.Add(content);
		}
		#endregion

		#region Display
		public void DisplayImages(TableRow row, string contentGroup, string contentType, int start)
		{
            KMobile.Catalog.Presentation.Content content = null;
			_imgDisplayInst = new ImgDisplayInstructions(_mobile);
			_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(WapTools.PreviewUrl(contentGroup, _mobile));
			_imgDisplayInst.TextDwld = WapTools.GetText("Download");
			_imgDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "Img");

			//if (/*WapTools.isTestSite(this.Request) ||*/ is099Purchaser)
			//	_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingClub099(this.Request, contentGroup, contentType, HttpUtility.UrlEncode("CLUB099|HOME"), "", "0");
			//else if (Convert.ToBoolean(WapTools.GetXmlValue("Home/Show_view")))
			//	_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, contentGroup, contentType, (is3g) ? HttpUtility.UrlEncode("3g|HOME") : HttpUtility.UrlEncode("xhtml|HOME"), "", "0");
			//else
			//	_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(this.Request, contentGroup, contentType, (is3g) ? HttpUtility.UrlEncode("3g|HOME") : HttpUtility.UrlEncode("xhtml|HOME"), "", "0", _mobile.MobileType, Convert.ToInt32(is2x1)); 
			//Trace.Warn(_imgDisplayInst.UrlDwld + "-" + Convert.ToInt32(is2x1).ToString());	

            _imgDisplayInst.UrlDwld = WapTools.GetChargingProfilesUrl();
               
			TableItemStyle tableStyle = new TableItemStyle();
			tableStyle.HorizontalAlign = HorizontalAlign.Center;
			int previews = (_mobile.ScreenPixelsWidth < 140 || _mobile.MobileType == "SONYERICSSONZ610I") ? 1 : 2;
                     
			for( int i = start; i < start + previews; i++ )  
			{
				if (contentGroup == "IMG")
                    content = (KMobile.Catalog.Presentation.Content)_contentCollImg[(i) % _contentCollImg.Count];

				if (content != null)  
				{
//					if (content.ContentRatings[0].Value > 0)
//						_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(this.Request, contentGroup, contentType, (is3g) ? HttpUtility.UrlEncode("3g|HOME") : HttpUtility.UrlEncode("xhtml|HOME"), "", "0", _mobile.MobileType);
//					else
//						_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, contentGroup, contentType, (is3g) ? HttpUtility.UrlEncode("3g|HOME") : HttpUtility.UrlEncode("xhtml|HOME"), "", "0");
			
					XhtmlTableCell tempCell = new XhtmlTableCell();
					ImgDisplay imgDisplay = new ImgDisplay(_imgDisplayInst);
					imgDisplay.Display(tempCell, content);
					imgDisplay = null;
					tempCell.ApplyStyle(tableStyle);
					row.Cells.Add(tempCell);
					tempCell = null;
				}
			}  
			content = null;
			_imgDisplayInst = null;
			tableStyle = null;
		}
      
		public void DisplayTitles(Table tb, int start)
		{
            KMobile.Catalog.Presentation.Content content = null;
			_imgDisplayInst = new ImgDisplayInstructions(_mobile);
			_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(WapTools.PreviewUrl("IMG", _mobile));
			_imgDisplayInst.TextDwld = WapTools.GetText("Download");
			_imgDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "down8");

			//if (/*WapTools.isTestSite(this.Request) ||*/ is099Purchaser)
			//	_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingClub099(this.Request, "IMG", "IMG_COLOR", HttpUtility.UrlEncode("CLUB099|LINK"), "", "0");
			//else if (Convert.ToBoolean(WapTools.GetXmlValue("Home/Show_view")))
			//	_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, "IMG", "IMG_COLOR", (is3g) ? HttpUtility.UrlEncode("3g|LINK") : HttpUtility.UrlEncode("xhtml|LINK"), "", "0");
			//else
			//	_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(this.Request, "IMG", "IMG_COLOR", (is3g) ? HttpUtility.UrlEncode("3g|LINK") : HttpUtility.UrlEncode("xhtml|LINK"), "", "0", _mobile.MobileType, Convert.ToInt32(is2x1));

            _imgDisplayInst.UrlDwld = WapTools.GetChargingProfilesUrl();
				               
			TableItemStyle tableStyle = new TableItemStyle();
			tableStyle.HorizontalAlign = HorizontalAlign.Center;
			XhtmlTableRow tempRow = new XhtmlTableRow();

			for( int i = start; i < start + 4; i++ )  
			{
                content = (KMobile.Catalog.Presentation.Content)_contentCollImg[(i) % _contentCollImg.Count];

				if (content != null)  
				{
					if (((i-start)%2) == 0 || (_mobile.ScreenPixelsWidth < 140))
						tempRow = new XhtmlTableRow();
					XhtmlTableCell tempCell = new XhtmlTableCell();
					ImgDisplay imgDisplay = new ImgDisplay(_imgDisplayInst);
					imgDisplay.Display(tempCell, content, false);
					imgDisplay = null;
					tempCell.ApplyStyle(tableStyle);
					tempRow.Cells.Add(tempCell);
					if (((i-start)%2) != 0 || (_mobile.ScreenPixelsWidth < 140))
						tb.Rows.Add(tempRow);
				}
			}  
			content = null;
			_imgDisplayInst = null;
			tableStyle = null;
		}                     


		public void DisplayContentSets(XhtmlTable t, string cg, int rangeInf, int rangeSup)
		{
			_contentSetDisplayInst = new ContentSetDisplayInstructions(_mobile);
			_contentSetDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "bullet");
			_contentSetDisplayInst.UrlDwld = "./linkto.aspx?id={0}&cg={1}";
			XhtmlTableCell cell = new XhtmlTableCell();
			XhtmlTableRow row = new XhtmlTableRow();
			if (rangeSup == -1) rangeSup = _contentCollContentSet.Count;
			if (rangeSup > _contentCollContentSet.Count) rangeSup = _contentCollContentSet.Count;
			for( int i = rangeInf; i < rangeSup; i++ )
			{
                KMobile.Catalog.Presentation.Content content = (KMobile.Catalog.Presentation.Content)_contentCollContentSet[i];
				//if (WapTools.FindProperty(content.PropertyCollection, "CompositeContentGroup") != cg) continue;
				ContentSetDisplay contentSetDisplay = new ContentSetDisplay(_contentSetDisplayInst);
				contentSetDisplay.Display(cell, content, true);			
				contentSetDisplay = null;
				row.Controls.Add(cell);
				cell = new XhtmlTableCell();
				t.Controls.Add(row);
				row = new XhtmlTableRow();
				content = null;
			}
			//t.Controls.Add(row);
			_contentSetDisplayInst = null;
			cell = null; row = null;
		}
		#endregion
	}
}
