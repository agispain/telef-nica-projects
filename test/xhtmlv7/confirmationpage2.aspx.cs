﻿using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;

namespace xhtml_v7
{
    public partial class confirmationpage2 : XCatalogBrowsing
    {

        public bool is3g = false;
        public string volver, subir;
        public string ok;
        public string group = "web";
        public int var;
        public suscribeEvents subscribes;
        public chargingevents charges;
        public Subscriptions sub;
        public string cpi;
        public string refer;
        int p = 0;
        public int contentset;
        public string t;
        public chargingevents charge;


        public string checkCall2()
        {
            string url;
            string pageurl;
            pageurl = Request.Url.ToString();
            BillingTools web = new BillingTools();
           // charges = web.chargeEvnt(this.Trace, this.Request.Headers["TM_user-id"], _contentProfileID);
            if (refer.StartsWith("CLUB099") && (_contentProfileID == "31690") && (p != 0))
            {

                url = WapTools.GetUrlBillingClub099Test(_contentType, refer, contentset, var, p, t);
            }
            else if (refer.StartsWith("CLUB099"))
                url = WapTools.GetUrlBillingClub099Test2(_contentType, refer, contentset, var);
            else if ((refer.StartsWith("CLUB_V") || refer.StartsWith("CLUBv_")) && (_contentProfileID == "31192"))
                url = WapTools.GetUrlBillingClubTest(_contentType, refer, contentset, var);
            else if (_contentProfileID=="25609")
                url = WapTools.GetUrlBillingClub099TestVip(_contentType, refer, contentset, var, p, t);
            else url = "./error.aspx";

            return url;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            _mobile = (MobileCaps)Request.Browser;
            try { is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType)); }
            catch { is3g = false; }

            BillingTools web = new BillingTools();
            
            try
            {
                WapTools.SetHeader(this.Context);
                WapTools.AddUIDatLog(Request, Response, this.Trace);
            }
            catch { }
            p = (Request.QueryString["p"] != null) ? Convert.ToInt32(Request.QueryString["p"]) : 0;
            contentset = (Request.QueryString["cs"] != null) ? Convert.ToInt32(Request.QueryString["cs"]) : 0;
            refer = (Request.QueryString["ref"] != null) ? Request.QueryString["ref"] : "";
            _contentType = (Request.QueryString["ct"] != null) ? Request.QueryString["ct"] : "";
            _contentGroup = WapTools.GetDefaultContentGroup(_contentType);
            _contentProfileID = (Request.QueryString["ci"] != null) ? Request.QueryString["ci"] : "";
            var = (Request.QueryString["c"] != null) ? Convert.ToInt32(Request.QueryString["c"]) : 0;
            ok = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "OK.gif");
            t = (Request.QueryString["t"] != null) ? Request.QueryString["t"] : "";

            #region subscribeEvent

            try
            {

                subscribes = web.suscribeEvnt(this.Trace, this.Request.Headers["TM_user-id"], _contentProfileID);


            #endregion
                if (subscribes.returnCode == "300")
                {

                    #region chargeEvent
                    try
                    {

                        charge = web.chargeEvnt(this.Trace, this.Request.Headers["TM_user-id"], _contentProfileID);
                        if (charge.returnCode != "100") Response.Redirect("./error.aspx");
                       
                    }
                    catch { }

                    #endregion
                }
            }
            catch { }
            XhtmlTableRow row = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", row, "", "Gracias por Subscribirte al Canal " + _contentProfileID + " .A partir de ahora disfutra de todos tus fondos por 0/Mes.Ahora solo tienes que pinchar  en el contenido y dar a Salvar", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XSmall);
            tbContentContent1.Rows.Add(row);
            #region HEADER
            XhtmlImage img = new XhtmlImage();
            img.ImageUrl = WapTools.GetImage(this.Request, "imagenes", _mobile.ScreenPixelsWidth, is3g);
            XhtmlTools.AddImgTable(tbHeaderContent1, img);
            #endregion

        }

       
       
    }
}

