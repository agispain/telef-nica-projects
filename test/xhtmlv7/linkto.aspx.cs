using System;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;

namespace xhtml_v7
{
	public partial class linkto : System.Web.UI.Page
	{
		protected void Page_Load(object sender, System.EventArgs e)
		{
			MobileCaps _mobile = (MobileCaps)Request.Browser;
			Trace.Write("Family: " + _mobile.MobileType);

			string idSite = Request.QueryString["id"];
			string url = Request.QueryString["url"];
			string cg = Request.QueryString["cg"];

			

				try
				{ 
					WapTools.SetHeader(this.Context);
					WapTools.AddUIDatLog(Request, Response, this.Trace);
				}
				catch{}
				if (idSite == "5747") idSite = "7176";
				if (idSite == "7347") idSite = "3059";
				if (idSite == "5862") cg = "COMPOSITE";
				if (url != null && url != "")
				{
					WapTools.LogUser(this.Request, 106, _mobile.MobileType);
					try{Response.Redirect(WapTools.GetText("urlLinkto") + url, false);}
					catch{Response.Redirect(WapTools.GetText("urlLinkto") + url, true);}				
				}
				else if (Convert.ToInt32(idSite) <= 50)
				{
					try
					{
						int log = 110;
						log += Convert.ToInt32(idSite);
						WapTools.LogUser(this.Request, log, _mobile.MobileType);
						Response.Redirect(WapTools.GetText("LnkShop" + idSite), false);
					}
					catch{Response.Redirect(WapTools.GetText("LnkShop" + idSite), true);}
				}
				else if (Convert.ToInt32(idSite) >= 10000)
				{
					WapTools.LogUser(this.Request, Convert.ToInt32(idSite), _mobile.MobileType);
					Response.Redirect(WapTools.GetText("LnkGenera" + idSite), false);
				}
				else
				{
					try
					{
						WapTools.LogUser(this.Request, Convert.ToInt32(idSite), _mobile.MobileType);
						Response.Redirect(String.Format("./catalog.aspx?cg={0}&cs={1}&p={2}&t={3}&cl={4}", cg, idSite, Request.QueryString["p"], Request.QueryString["t"], Request.QueryString["cl"]), false);
					}
					catch{Response.Redirect(String.Format("./catalog.aspx?cg={0}&cs={1}&p={2}&t={3}&cl={4}", cg, idSite, Request.QueryString["p"], Request.QueryString["t"], Request.QueryString["cl"]), true);}
				}
			}
		
		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
