﻿using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;

namespace xhtml_v7
{
    public partial class confirmation2 :XCatalogBrowsing
    {
        public bool is3g = false;
        public string volver, subir;
        public string accept;
        public string cancel;
        public string group = "web";
        public int var;

        public Subscriptions sub;
        public string code;
        public chargingevents charge;
        public checksubscriptions checkSub;
        double price=0;
        double finalprice;
       

        protected void Page_Load(object sender, EventArgs e)
        {


            BillingTools web = new BillingTools();
           
            


            _mobile = (MobileCaps)Request.Browser;
            try { is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType)); }
            catch { is3g = false; }

            try
            {
                WapTools.SetHeader(this.Context);
                WapTools.AddUIDatLog(Request, Response, this.Trace);
            }
            catch { }

            _contentType = (Request.QueryString["ct"] != null) ? Request.QueryString["ct"] : "";
            _contentGroup = WapTools.GetDefaultContentGroup(_contentType);
            _contentProfileID = (Request.QueryString["ci"] != null) ? Request.QueryString["ci"] : "";
            _imgName = (Request.QueryString["cn"] != null) ? Request.QueryString["cn"] : "";
            var = (Request.QueryString["c"] != null) ? Convert.ToInt32(Request.QueryString["c"]) : 0;
            accept = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "accept.gif");
            cancel = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "cancel.gif");
            finalprice = (Request.QueryString["pr"] != null) ? Convert.ToDouble(Request.QueryString["pr"]) : 0;

        
            

           XhtmlTableRow row = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", row, "", "El fondo de Pentalla imagen"+" " +"'"+ _imgName +"'"+ " "+ ", que vas a comprar cuesta " + finalprice + " euro/semana IVA incl.", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XSmall);
            tbContentImg.Rows.Add(row);
            #region HEADER
            XhtmlImage img = new XhtmlImage();
            img.ImageUrl = WapTools.GetImage(this.Request, "imagenes", _mobile.ScreenPixelsWidth, is3g);
            XhtmlTools.AddImgTable(tbHeaderImg, img);
            #endregion

            


        }

        public  string getUrlCall()
        {
            string pageurl;
            string url;
           
            pageurl = Request.Url.ToString();

          
           url = pageurl.Replace("confirmation2.aspx","chargeEvent.aspx");
          
           return url;
        }
    }
}

