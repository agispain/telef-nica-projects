﻿using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;
using System.Reflection;
using System.Net;
using System.IO;

namespace xhtml_v7
{
    public partial class downloadPage : XCatalogBrowsing
    {
        public string group = "web";
        public bool isMobile240;
        double  finalprice;
        public string downloadButtonSrc;
        public   string downloadButtonUrl;
        public string contentProfileID;
        string displayText;
        public bool is3g = false;
        string contentType;
        string dwldUrl;
        string contentName;
        public string goToHmPageUrl;
        public string goToHmPageSrc;

        protected void Page_Load(object sender, EventArgs e)
        {
            XhtmlTableRow rowTitle1 = new XhtmlTableRow();
            XhtmlTableRow rowTitle2 = new XhtmlTableRow();
            XhtmlTableRow rowTitle3 = new XhtmlTableRow();

            _mobile = (MobileCaps)Request.Browser;
            
            contentType = (Request.QueryString["cg"] != null) ? Request.QueryString["cg"] : "";
            contentProfileID = (Request.QueryString["cpi"] != null) ? Request.QueryString["cpi"] : "";
            _displayKey = WapTools.GetXmlValue("DisplayKey");         
            _contentGroup = WapTools.GetDefaultContentGroup(contentType);
            _idContent=(Request.QueryString["id"] != null) ? Convert.ToInt32(Request.QueryString["id"]) : 0;


            try
            {
                WapTools.SetHeader(this.Context);
                WapTools.LogUser(this.Request, 198, _mobile.MobileType);
                WapTools.AddUIDatLog(Request, Response, this.Trace);

            }
            catch { }

            //calculate price
            var price = (double)(Session["contentInfo"]);
            contentName = (string)(Session["contentName"]);   
            price = price + 0.21 * price;
            finalprice = Math.Round((double)price, 2);

            isMobile240 = _mobile.ScreenPixelsWidth > 240; // Group8
            group = (isMobile240) ? "web" : "web16";

            Operator op = new Operator(Request.UserHostAddress);
            Trace.Warn(op.OperatorName);
           
            if (op.OperatorName != null && op.OperatorName == "MOVISTAR")
            {
                #region getUrl
                DownloadInfo downloadInfo = null;
                BillingRequest billingRequest = null;
                CommandItem commandItem = new CommandItem(new Guid(_displayKey), _idContent, contentType, null, "", _mobile.MobileType, _contentGroup);
                BillingManager billingManager = new BillingManager();

                billingRequest = billingManager.CreateCommand(Request, WapTools.GetXmlValue("Billing/Provider"), commandItem);
                downloadInfo = billingManager.DeliverCommand(Request, billingRequest.GUIDCommand, null, null, WrapperType.DescriptorWrapper | WrapperType.ForwardLockWrapper); 
                dwldUrl = downloadInfo.Uri;              
               
                #endregion


            }

            switch (_contentGroup)
            {
                case "IMG":

                    downloadButtonSrc = "descargaImagen.png";
                    downloadButtonUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, downloadButtonSrc);
                   
                    if (Int32.Parse(contentProfileID) == 31690)
                    {
                        rowTitle1 = new XhtmlTableRow();
                        XhtmlTools.AddTextTableRow("", rowTitle1, "", "Te acabas de subscribir con exito al Club 0,99 al precio de " + finalprice + " ( IVA INCLUIDO) por semana", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
                        tbHeaderRow1.Rows.Add(rowTitle1);


                        rowTitle2 = new XhtmlTableRow();
                        XhtmlTools.AddTextTableRow("", rowTitle2, "", "Podrás descargar todos los fondos que quieras en nuestro portal", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
                        tbHeaderRow2.Rows.Add(rowTitle2);
                    }
                    else 
                    {
                        rowTitle1 = new XhtmlTableRow();
                        XhtmlTools.AddTextTableRow("", rowTitle1, "", "Acabas de comprar el fondo '" + contentName + "' al precio de " + finalprice + "(IVA incluido).", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
                        tbHeaderRow1.Rows.Add(rowTitle1);


                        rowTitle2 = new XhtmlTableRow();
                        XhtmlTools.AddTextTableRow("", rowTitle2, "", "La descarga del contenido ha comenzado de forma automática.", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
                        tbHeaderRow2.Rows.Add(rowTitle2);

                         rowTitle3 = new XhtmlTableRow();
                        XhtmlTools.AddTextTableRow("", rowTitle3, "", "De todas forma, recuedra que si tienes algún problema puedes a llamar gratis al 1004 para resolverlo", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
                        tbHeaderRow2.Rows.Add(rowTitle3);
 
                    
                    }

                    break;

                case "VIDEO_RGT":

                    downloadButtonSrc = "descargaVideo.png";
                    downloadButtonUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, downloadButtonSrc);

                        if (Int32.Parse(contentProfileID) == 31192)
                        {
                            rowTitle1 = new XhtmlTableRow();
                            XhtmlTools.AddTextTableRow("", rowTitle1, "", "Te acabas de subscribir con exito al Club Video al precio de " + finalprice + " ( IVA INCLUIDO) al mes", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
                            tbHeaderRow1.Rows.Add(rowTitle1);


                            rowTitle2 = new XhtmlTableRow();
                            XhtmlTools.AddTextTableRow("", rowTitle2, "", "Podrás descargar todos los fondos que quieras en nuestro portal", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
                            tbHeaderRow2.Rows.Add(rowTitle2);
                        }
                        else
                        {
                            rowTitle1 = new XhtmlTableRow();
                            XhtmlTools.AddTextTableRow("", rowTitle1, "", "Acabas de comprar el video '" + contentName + "' al precio de " + finalprice + "(IVA incluido).", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
                            tbHeaderRow1.Rows.Add(rowTitle1);


                            rowTitle2 = new XhtmlTableRow();
                            XhtmlTools.AddTextTableRow("", rowTitle2, "", "La descarga del contenido ha comenzado de forma automática.", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
                            tbHeaderRow2.Rows.Add(rowTitle2);

                            rowTitle3 = new XhtmlTableRow();
                            XhtmlTools.AddTextTableRow("", rowTitle3, "", "De todas forma, recuedra que si tienes algún problema puedes a llamar gratis al 1004 para resolverlo", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
                            tbHeaderRow2.Rows.Add(rowTitle3);


                        }
                    break;

                case "ANIM":

                    downloadButtonSrc = "descargaAnimacion.png";
                    downloadButtonUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, downloadButtonSrc);

                    rowTitle1 = new XhtmlTableRow();
                    XhtmlTools.AddTextTableRow("", rowTitle1, "", "Acabas de comprar la animación  '" + contentName + "' al precio de " + finalprice + "(IVA incluido).", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
                    tbHeaderRow1.Rows.Add(rowTitle1);


                    rowTitle2 = new XhtmlTableRow();
                    XhtmlTools.AddTextTableRow("", rowTitle2, "", "La descarga del contenido ha comenzado de forma automática.", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
                    tbHeaderRow2.Rows.Add(rowTitle2);

                     rowTitle3 = new XhtmlTableRow();
                    XhtmlTools.AddTextTableRow("", rowTitle3, "", "De todas forma, recuedra que si tienes algún problema puedes a llamar gratis al 1004 para resolverlo", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
                    tbHeaderRow2.Rows.Add(rowTitle3);

                    break;

                case "SCHEME":

                    downloadButtonSrc = "descargaImagen.png";
                    downloadButtonUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, downloadButtonSrc);

                    rowTitle1 = new XhtmlTableRow();
                    XhtmlTools.AddTextTableRow("", rowTitle1, "", "Acabas de comprar el tema  '" + contentName + "' al precio de " + finalprice + "(IVA incluido).", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
                    tbHeaderRow1.Rows.Add(rowTitle1);


                    rowTitle2 = new XhtmlTableRow();
                    XhtmlTools.AddTextTableRow("", rowTitle2, "", "La descarga del contenido ha comenzado de forma automática.", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
                    tbHeaderRow2.Rows.Add(rowTitle2);

                    rowTitle3 = new XhtmlTableRow();
                    XhtmlTools.AddTextTableRow("", rowTitle3, "", "De todas forma, recuedra que si tienes algún problema puedes a llamar gratis al 1004 para resolverlo", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
                    tbHeaderRow2.Rows.Add(rowTitle3);

                    break;

                 case "FLASH":

                    downloadButtonSrc = "descargaImagen.png";
                    downloadButtonUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, downloadButtonSrc);

                    rowTitle1 = new XhtmlTableRow();
                    XhtmlTools.AddTextTableRow("", rowTitle1, "", "Acabas de comprar la animación flash '" + contentName + "' al precio de " + finalprice + "(IVA incluido).", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
                    tbHeaderRow1.Rows.Add(rowTitle1);


                    rowTitle2 = new XhtmlTableRow();
                    XhtmlTools.AddTextTableRow("", rowTitle2, "", "La descarga del contenido ha comenzado de forma automática.", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
                    tbHeaderRow2.Rows.Add(rowTitle2);

                    rowTitle3 = new XhtmlTableRow();
                    XhtmlTools.AddTextTableRow("", rowTitle3, "", "De todas forma, recuedra que si tienes algún problema puedes a llamar gratis al 1004 para resolverlo", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
                    tbHeaderRow2.Rows.Add(rowTitle3);
                   
                    break;

                 default:

                    downloadButtonUrl = "descargaImagen.png";
                    displayText = "Acabas de comprar el fondo '" + contentName + "' al precio de " + finalprice + "(IVA incluido).";
                    break;

            }


            

            #region buttons
            
            XhtmlImage ayuda = new XhtmlImage();
            ayuda.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "ayuda.png");
            ayuda.ImageAlign = ImageAlign.Middle;
            XhtmlTableRow row = new XhtmlTableRow();
            XhtmlTools.addImgLinkTable(row, ayuda.ImageUrl, "./helpPage.aspx");
            tbAyudaButton.Controls.Add(row);
            

            XhtmlImage terms = new XhtmlImage();
            terms.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "terms.png");
            terms.ImageAlign = ImageAlign.Middle;
            XhtmlTableRow row1 = new XhtmlTableRow();
            XhtmlTools.addImgLinkTable(row1, terms.ImageUrl, "./Terms.aspx");
            tbTermsButtons.Controls.Add(row1);



            goToHmPageSrc = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "inicio.png");

            if (_mobile.Family == "Android")
                goToHmPageUrl = "http://emocion.dev.kiwee.com/testWeb/";
            else
                goToHmPageUrl = "./default_w.aspx";
          

            var webClient = new WebClient();
            Trace.Warn(dwldUrl);
            byte[] imageBytes = webClient.DownloadData(dwldUrl);
            string extension = Path.GetExtension(dwldUrl);
            Session["contentByte"] = imageBytes;
            Session["contentName"] = contentName;
            Session["extension"] = extension;


            #endregion

        }

       
    }
}
