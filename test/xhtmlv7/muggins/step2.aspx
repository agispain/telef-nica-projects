<?xml version="1.0" encoding="UTF-8" ?>
<%@ Register tagprefix="xhtml" Namespace="xhtml_v7.Tools" Assembly="xhtml_v7" %>
<%@ Page language="c#" Codebehind="step2.aspx.cs" AutoEventWireup="True" Inherits="xhtml_v7.muggins.step2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
         <title>Im&aacute;genes y Fondos</title>
         <link rel="stylesheet" href="../xhtml.css" type="text/css" />
         <meta forua="true" http-equiv="Cache-Control" content="no-cache, max-age=0, must-revalidate, proxy-revalidate, s-maxage=0" />
  </head>
         <body>
                    <a id="start" name="start" />
					 <Xhtml:XhtmlTable id="tbHeader" Runat="server" CssClass="normal" />
					 <form action="getChargingProfileAvatar.aspx?c="+"<%=c%>"+"" method="post" accept-charset="utf-8">
					 
						<Xhtml:XhtmlTable id="tbAttitude" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowTitle" Runat="server" />
                    		<Xhtml:XhtmlTableRow id="rowAttitude" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>			
							<% if (sex == "BOY") {%>
								<td><select name="mood">
										<option value="happy" selected="selected">contento</option>
										<option value="cool">guay</option>
										<option value="flirt" >lig&#243;n</option>
										<option value="furious">enfadado</option>
										<option value="in_love">enamorado</option>
										<option value="heartbroken">coraz&oacute;n roto</option>	
										<option value="">p&aacute;pa noel</option>
														
									</select></td>										
							<% } else { %>
								<td><select name="mood">
										<option value="standard" selected="selected">normal</option>
										<option value="happy">contenta</option>
										<option value="fun">divertida</option>
										<option value="sexy">sexy</option>
										<option value="heartbroken">triste</option>
										<option value="in_love">enamorada</option>
										<option value="angry">enfadada</option>	
										<option value="Christmas">Navidad</option>	
										<option value="elf">elfo</option>				
									</select></td>																	
							<% } %>
							</tr>
						</table>
						<Xhtml:XhtmlTable id="tbHairType" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowHairType" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
							<% if (sex == "BOY") { %>	
								<td><select name="hairStyle">
										<option value="short" selected="selected">corto</option>
										<option value="posh">corto con flequillo</option>
										<option value="spiky">de punta</option>
										<option value="long">largo</option>
										<option value="dread">rasta</option>
										<option value="soldier">rapado</option>
										<option value="no hair">sin pelo</option>
										<option value="santa">p&aacute;pa noel</option>
									</select></td>						
							<% } else { %>
								<td><select name="hairStyle">										
										<option value="short" selected="selected">corto</option>
										<option value="fringe">largo con flequillo</option>
										<option value="curly">largo ondulado</option>
										<option value="flat">largo liso</option>
										<option value="medium">media melena</option>
										<option value="Christmas">Navidad</option>								
									</select></td>										
							<% } %>								
							</tr>
						</table>
						<Xhtml:XhtmlTable id="tbHair" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowHair" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
							<% if (sex == "BOY") {%>	
								<td><select name="hairColor">
										<option value="brown" selected="selected">marr&oacute;n oscuro</option>
										<option value="lbrown">marr&oacute;n claro</option>
										<option value="black">negro</option>
										<option value="blond">rubio</option>
										<option value="red">rojo</option>										
									</select></td>				
							<% } else { %>
								<td><select name="hairColor">
										<option value="brown" selected="selected">marr&oacute;n oscuro</option>
										<option value="black">negro</option>
										<option value="red">pelirrojo</option>
										<option value="blond">rubio</option>
										<option value="violet">violeta</option>										
									</select></td>		
							<% } %>
							</tr>
						</table>						
						<Xhtml:XhtmlTable id="tbMuggin" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowJacket" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
							<% if (sex == "BOY") {%>	
								<td><select name="clothes">
										<option value="striped_blouse">camiseta rayas</option>										
										<option value="polo_tshirt">polo</option>									
										<option value="jacket_2rows">chaqueta kimono</option>										
										<option value="tshirt">camiseta</option>
										<option value="classic_jacket">chaqueta cl&aacute;sica</option>
										<option value="motor_jacket">chaqueta de moto</option>
										<option value="nadal">camiseta a la Nadal</option>										
										<option value="shirt">camisa</option>
										<option value="sweatshirt">sudadera con capucha</option>
										<option value="coat">abrigo</option>											
										<option value="furry_hoodie">furry hoodie</option>
										<option value="hoodie">hoodie</option>
										<option value="pull-over">jersey</option>
										<option value="turtleneck_jersey">jersey cuello cisne</option>
										<option value="nothing">nada</option>
																					
									</select></td>
							<% } else { %>
								<td><select name="clothes">										
										<option value="valentine_dress">vestido de s. valentin</option>
										<option value="skirt_overall">peto</option>
										<option value="short_dress_stars">vestido corto</option>										
										<option value="evening_gown">vestido de noche</option>										
										<option value="50s_dress">vestido de los 50</option>
										<option value="waistcoat">chaleco</option>										
										<option value="top">superior</option>										
										<option value="corset_tshirt">vestido corset</option>
										<option value="tanktop">top tirantes</option>								
										<option value="snowboard_jacket">chaqueta snowboard</option>
										<option value="x-mas_costume">vestido de navidad</option>	
										<option value="elf_costume">vestido de elfo</option>	
										<option value="">sin ropa</option>												
									</select></td>								
							<% } %>
							</tr>
						</table>
						
						<Xhtml:XhtmlTable id="colorClothes" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowColorClothes" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
								<td><select name="colorClothes">
										<option value="black" selected="selected">negro</option>																		
										<option value="blue">azul</option>
										<option value="green">verde</option>		
										<option value="red">rojo</option>											
										<option value="yellow">amarillo</option>																				
								<% if (sex == "GIRL") {%>	
										<option value="white">blanco</option>
										<option value="purple">p&uacute;rpura</option>										
										<option value="brown">marr&oacute;n</option>								
								<% } %>		
								</select></td>																							
							</tr>
						</table>
						
						<Xhtml:XhtmlTable id="tbTrouser" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowTrouser" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
							<% if (sex == "BOY") {%>	
								<td><select name="trousers">
										<option value="skater_low">skater</option>
										<option value="scottish">escoceses</option>
										<option value="underwear">gallumbos</option>
										<option value="baggies">baggies</option>
										<option value="classics">cl&aacute;sicos</option>
										<option value="elegant">elegantes</option>
										<option value="velvet_striped">terciopelo</option>
										<option value="jeans">vaqueros</option>
										<option value="casual">de trajes</option>
										<option value="pleated">de pinzas</option>
										<option value="sport">ch&aacute;ndal</option>
										<option value="army">camuflaje</option>										
										<option value="nadal_shorts">pantal&oacute;n tenis</option>										
										<option value="">sin pantal&oacute;n</option>									
									</select></td>								
							<% } else { %>
								<td><select name="trousers">
										
										<option value="pleated_skirt">falda corta</option>
										<option value="high_waisted_skirt">falda antiqua</option>
										<option value="tartan">falda de colegiala</option>
										<option value="mini_skirt_red">minifalda roja</option>
										<option value="bell_jeans">vaqueros campana</option>
										<option value="">sin pantal&oacute;n</option>	
										<option value="mini leggins">minifalda</option>
										<option value="skinny leggins">apretados</option>							
									</select></td>	
							<% } %>
								
							</tr>
						</table>
						<Xhtml:XhtmlTable id="tbColorTrousers" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowColorTrousers" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
									<td><select name="colorTrousers">
										<option value="black" selected="selected">negro</option>
										<option value="blue">azul</option>
										<option value="brown">marr&oacute;n</option>
										<option value="green">verde</option>
										<option value="white">blanco</option>																					
										<option value="red">rojo</option>										
							<% if (sex == "GIRL") {%>	
										<option value="yellow">amarillo</option>
										<option value="khaki">kaki</option>
										<option value="purple">p&uacute;rpura</option>
							<% } %>
									</select></td>	
							</tr>
						</table>
						<% if (sex == "GIRL") {%>	
						<Xhtml:XhtmlTable id="tbUnderwear" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowUnderwear" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
								<td><select name="underwear">
										<option value="leggins" selected="selected">leggins</option>
										<option value="stripedleggins">leggins a rayas</option>
										<option value=""> nada</option>										
									</select></td>	
							</tr>
						</table>
						<Xhtml:XhtmlTable id="tbColorUnderwear" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowColorUnderwear" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
								<td><select name="colorUnderwear">
										<option value="yellow" selected="selected">amarillo</option>
										<option value="blue">azul</option>
										<option value="white">blanco</option>										
										<option value="khaki">kaki</option>										
										<option value="brown">marr&oacute;n</option>										
										<option value="black">negro</option>										
										<option value="purple">p&uacute;rpura</option>										
										<option value="red">rojo</option>										
										<option value="green">verde</option>										
									</select></td>	
							</tr>
						</table>
						<% } %>
						<Xhtml:XhtmlTable id="tbShoes" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowShoes" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
							<% if (sex == "BOY") {%>	
								<td><select name="shoes">
										<option value="classie" selected="selected">classie</option>
										<option value="sneakers2">sneakers</option>
								        <option value="classical">zapatos</option>
										<option value="snowboard_boots">botas de nieve</option>
										<option value="sporty_boots">botas de deporte</option>
										<option value="casual">cl&aacute;sicos</option>
										<option value="all_stars">all stars</option>
										<option value="sport_shoes">zapatillas</option>	
										<option value="camper">botas</option>								
										<option value="">descalzo</option>																			
									</select></td>													
							<% } else { %>								
								<td><select name="shoes">
										<option value="high_heels" selected="selected">zapatos tac&oacute;n</option>																		
										<option value="ankle_belt">atados al tobillo</option>
										<option value="wedges">zapatos de plataforma</option>
										<option value="frayed_boots">frayed boots</option>
										<option value="all_stars">all stars</option>																		
										<option value="ballerina">bailarinas</option>																		
										<option value="snow_boots">botas de nieve</option>																		
										<option value="booties">botines</option>									
										<option value="x-mas_boots">botas navide&ntilde;as</option>	
										<option value="elf_boots">botas de elfo</option>
										<option value="">descalza</option>																
									</select></td>		
							<% } %>	
							</tr>
						</table>
						<Xhtml:XhtmlTable id="tbColorShoes" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowColorShoes" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>								
								<td><select name="colorShoes">
										<option value="yellow" selected="selected">amarillo</option>
										<option value="blue">azul</option>
										<option value="white">blanco</option>										
										<option value="brown">marr&oacute;n</option>										
										<option value="black">negro</option>																		
							<% if (sex == "GIRL") {%>	
										<option value="gold">dorado</option>
										<option value="silver">plateado</option>
										<option value="red">rojo</option>										
										<option value="green">verde</option>										
							<% } %>		
								</select></td>										
							</tr>
						</table>
						
						<input type="hidden" name="sex" value="<%=sex%>" />
						<input type="hidden" name="skin" value="<%=skin%>" />
						<input type="hidden" name="eyes" value="<%=eyes%>" />
						<input type="hidden" name="c" value="<%=c%>" />
						<br/><input type="submit" value="DALE VIDA" class="caja"  />
					</form>
					<hr />
                   <table width="100%">
						<tr>
							<td align="center"><a href="./catalog.aspx" style="color: #696969">Volver</a></td>
						</tr>
					</table>
				
          </body>
</html>
