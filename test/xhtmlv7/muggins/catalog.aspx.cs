using System;
using System.Configuration;
using System.Web;
using System.Drawing;
using System.Web.UI.WebControls;
using KMobile.Catalog.Services;
using KMobile.Catalog.Presentation;
using xhtml_v7.Tools;

namespace xhtml_v7.muggins
{
	public partial class catalog : XCatalogBrowsing
	{
		private int page;
		protected string cg_temp;
		//protected XhtmlTable tbHeader;
		public string buscar, emocion, atras, up, fondo, back, title =  "Im&aacute;genes y Fondos", css = "../xhtml.css";
		public string header, volver, subir;
		public bool is3g = false;

		protected void Page_Load(object sender, System.EventArgs e)
		{  
			try 
			{
				_mobile = (MobileCaps)Request.Browser;
				try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType));}
				catch{is3g = false;}

				try
				{
					WapTools.LogUser(this.Request, 201, _mobile.MobileType);
					WapTools.SetHeader(this.Context);
					WapTools.AddUIDatLog(Request, Response, this.Trace);				
				} 
				catch{}

				if (_mobile.MobileType != null &&  _mobile.IsCompatible("IMG_COLOR"))
				{
					//bool show = true;
					//try{if (Request.Headers["TM_user-id"].ToString() == "0343178316378333515") show = false;}
					//catch{show = true;}

					//if (show)
					//{
						_idContentSet = Convert.ToInt32(WapTools.GetXmlValue("Muggins/Backgrounds"));
						page = (Request.QueryString["n"] != null) ? Convert.ToInt32(Request.QueryString["n"]) : 1;
						_contentGroup = "IMG"; _contentType = "IMG_COLOR";
						_displayKey = WapTools.GetXmlValue("DisplayKey"); 

						ContentSet contentSet = BrowseContentSetExtended();
                         Session["catalogClub"] = contentSet;
			
						if (Request.QueryString["ms"]!=null && Request.QueryString["ms"]!="" && WapTools.GetText(Request.QueryString["ms"]) != "")
							title = WapTools.GetText(Request.QueryString["ms"]);                         

						int nbrows = Convert.ToInt32(WapTools.GetXmlValue("Home/Nb_Rows"));
						int nbcols = (_contentGroup == "ANIM" || _mobile.ScreenPixelsWidth>140) ? 2 : 1;
                      						
						#region CONTENTSET
						_imgDisplayInst = new ImgDisplayInstructions(_mobile);
						_imgDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "bullet");
						_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(WapTools.PreviewUrl(_contentGroup, _mobile));
						_imgDisplayInst.UrlDwld = WapTools.GetUrlMuggin(this.Request);
						if (_mobile.IsXHTML) nbrows += 1;

						ReadContentSet(contentSet, tbCatalog, page, nbrows*nbcols);
						page_max = contentSet.Count / (nbrows * nbcols);
						if (contentSet.Count % (nbrows * nbcols) > 0) page_max++;
						_imgDisplayInst = null;
						contentSet = null;
						#endregion
					//}

					#region HEADER
					/*XhtmlImage img = new XhtmlImage();
					img.ImageUrl = WapTools.GetImage(this.Request, "muggin",  _mobile.ScreenPixelsWidth, is3g);
					XhtmlTools.AddImgTable(tbHeader, img);*/
					XhtmlTools.AddTextTable(tbTitle, "1er Paso: Escoge tu FONDO", Color.Empty, Color.Empty, 2, HorizontalAlign.Center, VerticalAlign.Middle, true, FontUnit.XXSmall);
					#endregion
                                     
					#region PAGES

					TableCell cellTemp = new TableCell();
					cellTemp.HorizontalAlign = HorizontalAlign.Center;
					XhtmlTableRow rowTemp = new XhtmlTableRow();
					int premiere = 0, derniere = 0, cont = 0;
					int[] limits = new int[page_max/5];
					while (cont<(page_max/5))
						limits[cont]=(++cont)*5;
					for (cont=0;cont<page_max/5;cont++)
						if (page<=limits[cont])
						{
							premiere=limits[cont]-4;
							derniere=limits[cont];
							break;
						}
					if (premiere==0 && derniere==0 && page>0)
					{
						derniere = page_max;
						if (limits.Length==0)
							premiere = 1; 
						else
							premiere = limits[cont-1]+1;
					}				
					string URL_Suivant = String.Format("./catalog.aspx?cg={0}&cs={1}&n={2}&p={3}&t={4}", _contentGroup, _idContentSet, derniere + 1, Request.QueryString["p"], Server.UrlEncode(Request.QueryString["t"]));
					string URL_Precedent = String.Format("./catalog.aspx?cg={0}&cs={1}&n={2}&p={3}&t={4}", _contentGroup, _idContentSet, premiere - 1, Request.QueryString["p"], Server.UrlEncode(Request.QueryString["t"]));
					if (derniere>1)
					{
						XhtmlLink link = new XhtmlLink();				
						if (premiere > 1)
						{
							//link.ImageUrl = WapTools.GetImage(this.Request, "Previous");
							link.Text = "Atr&aacute;s";
							link.NavigateUrl = URL_Precedent;
							cellTemp.Controls.Add(link);
						}
						else
							cellTemp.Text = "&nbsp;";
						rowTemp.Cells.Add(cellTemp);
						link = null;
						cellTemp = new TableCell();
						cellTemp.HorizontalAlign = HorizontalAlign.Center;

						for (cont=premiere;cont<=derniere;cont++)
						{
							if (cont!=page)
							{
								link = new XhtmlLink();
								link.CssClass = _contentGroup;
								link.NavigateUrl = String.Format("./catalog.aspx?cg={0}&cs={1}&n={2}&p={3}&t={4}", _contentGroup, _idContentSet, cont, Request.QueryString["p"], Server.UrlEncode(Request.QueryString["t"]));
								link.Text = cont.ToString();
								cellTemp.Controls.Add(link);
							}
							else
							{
								//cellTemp.ForeColor = Color.FromName(WapTools.GetText("Color_" +  _contentGroup));
								cellTemp.Text = cont.ToString();
							}
							rowTemp.Cells.Add(cellTemp);
							link = null;
							cellTemp = new TableCell();
							cellTemp.HorizontalAlign = HorizontalAlign.Center;
						}
			
						if (derniere < page_max)
						{
							link = new XhtmlLink();
							//link.ImageUrl = WapTools.GetImage(this.Request, "Next");
							link.Text = "M�s";
							link.NavigateUrl = URL_Suivant;
							cellTemp.Controls.Add(link);
						}
						else
							cellTemp.Text = "&nbsp;";
						rowTemp.Cells.Add(cellTemp);
						link = null;
						tbPages.Rows.Add(rowTemp);
					}
					else
						tbPages.Visible = false;

					#endregion

					#region PICTOS
					fondo = WapTools.GetImage(this.Request, "fondo", _mobile.ScreenPixelsWidth, false);
					buscar = WapTools.getPicto(this.Request, "buscar", _mobile);
					emocion = WapTools.getPicto(this.Request, "emocion", _mobile);
					back = WapTools.getPicto(this.Request, "back", _mobile);
					up = WapTools.getPicto(this.Request, "up", _mobile);
					#endregion					
					
					atras = WapTools.UpdateFooter(_mobile, this.Context, null); 				
				}
				else
				{
					tbCatalog.Visible = false;
					tbPages.Visible = false;
					tbPreviews.Visible = false;
					tbSearch.Visible = false;
					tbTitle.Visible = false;
					XhtmlTableRow row = new XhtmlTableRow();
					XhtmlTools.AddTextTableRow(row, WapTools.GetText("Compatibility2"), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XSmall);
					tbLinks.Rows.Add(row);
					row = null;
				}

				#region 3G
				if (is3g)
				{
					css = "../3g.css";
					volver = WapTools.getPicto3g(this.Request.ApplicationPath, "volver", _mobile.ScreenPixelsWidth);
					subir = WapTools.getPicto3g(this.Request.ApplicationPath, "subir", _mobile.ScreenPixelsWidth);		
				}
				#endregion
				
				_mobile = null;					
			}
			catch(Exception caught) 
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\xhtml\\muggins\\catalog.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
				Response.Redirect("../error.aspx");				
			}
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Display
		public void DisplayContents(TableRow row, TableRow row2, ContentSet contentset, string name, string paramBack)
		{
			_imgDisplayInst = new ImgDisplayInstructions(_mobile);
			_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(WapTools.PreviewUrl(contentset.ContentGroup, _mobile));
		//	_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(String.Format("Url_{0}", contentset.ContentGroup));
			_imgDisplayInst.TextDwld = WapTools.GetText("Download");
			_imgDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "download");
			//_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup), HttpUtility.UrlEncode(String.Format("{0}|HOME_{1}",referer, name)), "", _idContentSet.ToString());
			_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup),  (is3g) ? HttpUtility.UrlEncode(String.Format("3g|HOME_{0}", name)) : HttpUtility.UrlEncode(String.Format("xhtml|HOME_{0}", name)), "", _idContentSet.ToString(), _mobile.MobileType) + "&p=" + _idContentSet + "&t=" + Server.UrlEncode(name);
                   
			TableItemStyle tableStyle = new TableItemStyle();
			try
			{
				tableStyle.HorizontalAlign = HorizontalAlign.Center; 
				int previews = (_mobile.ScreenPixelsWidth < 140 && contentset.ContentGroup != "ANIM") ? 1 : 2;            
				for( int i = DateTime.Now.Day; i < DateTime.Now.Day + previews; i++ )
				{
                    KMobile.Catalog.Presentation.Content content = contentset.ContentCollection[i % contentset.Count];
					if (content != null)
					{
						XhtmlTableCell tempCell = new XhtmlTableCell();
						//XhtmlTableCell textCell = new XhtmlTableCell();
						ImgDisplay imgDisplay = new ImgDisplay(_imgDisplayInst);
						//imgDisplay.Display(tempCell, textCell, content);
						imgDisplay.Display(tempCell, content);
						imgDisplay = null;
						tempCell.ApplyStyle(tableStyle);
						//textCell.ApplyStyle(tableStyle);
						row.Cells.Add(tempCell); 
						//rowTexts.Cells.Add(textCell);		
						tempCell = null;
					} 
					content = null;
				}
			}
			catch{}
			try
			{
				tableStyle.HorizontalAlign = HorizontalAlign.Center; 
				int previews = (_mobile.ScreenPixelsWidth < 140 && contentset.ContentGroup != "ANIM") ? 1 : 2;            
				for( int i = DateTime.Now.Day + 2; i < DateTime.Now.Day + 2 + previews; i++ )
				{
                    KMobile.Catalog.Presentation.Content content = contentset.ContentCollection[i % contentset.Count];
					if (content != null)
					{
						XhtmlTableCell tempCell = new XhtmlTableCell();
						//XhtmlTableCell textCell = new XhtmlTableCell();
						ImgDisplay imgDisplay = new ImgDisplay(_imgDisplayInst);
						//imgDisplay.Display(tempCell, textCell, content);
						imgDisplay.Display(tempCell, content);
						imgDisplay = null;
						tempCell.ApplyStyle(tableStyle);
						//textCell.ApplyStyle(tableStyle);
						row2.Cells.Add(tempCell); 
						//rowTexts.Cells.Add(textCell);		
						tempCell = null;
					} 
					content = null;
				}
			}
			catch{}
		}


		public void DisplayContents(string cg, Table t, ContentSet contentset, string name, string paramBack)
		{
			TableRow row = new TableRow();
			ContentSet newContentset = new ContentSet();
			newContentset.ContentCollection = new ContentCollection();
            foreach (KMobile.Catalog.Presentation.Content c in contentset.ContentCollection)
				if (c.ContentGroup.Name == cg)
					newContentset.ContentCollection.Add(c);
			_imgDisplayInst = new ImgDisplayInstructions(_mobile);
			_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(WapTools.PreviewUrl(cg, _mobile));
		//	_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(String.Format("Url_{0}", cg));
			_imgDisplayInst.TextDwld = WapTools.GetText("Download");
			_imgDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "download");
			//_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup), HttpUtility.UrlEncode(String.Format("{0}|HOME_{1}",referer, name)), "", _idContentSet.ToString());
			_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(this.Request, cg, WapTools.GetDefaultContentType(cg), (is3g) ? HttpUtility.UrlEncode(String.Format("3g|HOME_{0}", name)) : HttpUtility.UrlEncode(String.Format("xhtml|HOME_{0}", name)), "", _idContentSet.ToString(), _mobile.MobileType);
                           
			TableItemStyle tableStyle = new TableItemStyle();
			try
			{
				int start = DateTime.Now.Millisecond;
				tableStyle.HorizontalAlign = HorizontalAlign.Center; 
				int previews = (_mobile.ScreenPixelsWidth < 140 && newContentset.ContentGroup != "ANIM") ? 1 : 2;            
				for( int i = start; i < start + previews; i++ )
				{
                    KMobile.Catalog.Presentation.Content content = newContentset.ContentCollection[i % newContentset.Count];
					if (content != null)
					{
						XhtmlTableCell tempCell = new XhtmlTableCell();
						//XhtmlTableCell textCell = new XhtmlTableCell();
						ImgDisplay imgDisplay = new ImgDisplay(_imgDisplayInst);
						//imgDisplay.Display(tempCell, textCell, content);
						imgDisplay.Display(tempCell, content);
						imgDisplay = null;
						tempCell.ApplyStyle(tableStyle);
						//textCell.ApplyStyle(tableStyle);
						row.Cells.Add(tempCell); 
						//rowTexts.Cells.Add(textCell);		
						tempCell = null;
					} 
					content = null;
				}
				t.Rows.Add(row);
				row = null;
			}
			catch{}
		}

		#endregion
	}
}
