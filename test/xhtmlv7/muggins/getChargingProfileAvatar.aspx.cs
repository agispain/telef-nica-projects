﻿using System;
using System.Configuration;
using System.Web;
using System.Drawing;
using System.Web.UI.WebControls;
using KMobile.Catalog.Services;
using KMobile.Catalog.Presentation;
using xhtml_v7.Tools;

namespace xhtml_v7.muggins
{
    public partial class getChargingProfileAvatar : XCatalogBrowsing
    {
        public int avatarSubscription;
        public string contentProfileID;

        protected void Page_Load(object sender, EventArgs e)
        {

            string url = Request.Url.ToString();
           
            _mobile = (MobileCaps)Request.Browser;
            _idContentSet = Convert.ToInt32(WapTools.GetXmlValue("Muggins/Backgrounds"));
            _contentGroup = "IMG"; _contentType = "IMG_COLOR";
            _displayKey = WapTools.GetXmlValue("DisplayKey"); 

            ContentSet contentSet = BrowseContentSetExtended();
            Session["catalogClub"] = contentSet;

            BillingTools webService = new BillingTools();

            int id = (Request.QueryString["c"] != null && Request.QueryString["c"] != "") ? Convert.ToInt32(Request.QueryString["c"]) : 0;

            #region SubscriptionAvatar
            try
            {
                contentProfileID = "25828";
                Subscriptions subscription4 = new Subscriptions();
                subscription4 = webService.getChargingPro(this.Request.Headers["TM_user-id"], contentProfileID);
                avatarSubscription = Int32.Parse(subscription4.returnCode);
            }
            catch
            {
                Response.Redirect("../error.aspx");
            }
            #endregion

             if (avatarSubscription == 220)
                 Response.Redirect(url.Replace("getChargingProfileAvatar", "downloadPageSubscribed"));
             else
                 Response.Redirect(url.Replace("getChargingProfileAvatar", "downloadOptions"));

        }
    }
}
