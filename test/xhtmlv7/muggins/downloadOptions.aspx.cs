﻿using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using xhtml_v7.club;
using xhtml_v7.Tools;

namespace xhtml_v7.muggins
{
    public partial class downloadOptions : XCatalogBrowsing
    {
        public bool is3g = false;
        public string subscriptionButton;
        public string group = "web";
        string name = "";
        string contentName = "";
        int id;
        public bool isMobile240;
        string url;


        protected void Page_Load(object sender, EventArgs e)
        {
            _mobile = (MobileCaps)Request.Browser;
            try { is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType)); }
            catch { is3g = false; }

            try
            {
                WapTools.SetHeader(this.Context);
                WapTools.LogUser(this.Request, 198, _mobile.MobileType);
                WapTools.AddUIDatLog(Request, Response, this.Trace);	
            }
            catch { }


            url = Request.Url.ToString();

            string suf = (_mobile.ScreenPixelsWidth >= 200) ? "_100" : "_80";
            id = (Request.QueryString["id"] != null && Request.QueryString["id"] != "") ? Convert.ToInt32(Request.QueryString["id"]) : 0;
            _imgDisplayInst = new ImgDisplayInstructions(_mobile);
            _imgDisplayInst.TextDwld = WapTools.GetText("Download");
            _imgDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "Img");
            //_contentType = WapTools.GetDefaultContentType(cg);
            _contentGroup = WapTools.GetDefaultContentGroup(_contentType);

            isMobile240 = _mobile.ScreenPixelsWidth > 240; // Group8
            group = (isMobile240) ? "web" : "web16";
            
            var contentCatalog = (KMobile.Catalog.Presentation.ContentSet)(Session["catalogClub"]);
            if (contentCatalog != null)
            {
                for (int i = 0; i < contentCatalog.ContentCollection.Count; i++)
                {
                    if (contentCatalog.ContentCollection[i].IDContent == id)
                    {
                        name = contentCatalog.ContentCollection[i].Name;
                        contentName = contentCatalog.ContentCollection[i].ContentName;
                        Session["contentName"] = name;
                        break;
                    }
                }
            }

            createAvatarContext();
       

            try
            {
                WapTools.SetHeader(this.Context);
                WapTools.AddUIDatLog(Request, Response, this.Trace);
            }
            catch { }


            #region HEADER
            XhtmlTableRow rowTitle = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowTitle, "", name, Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XLarge);
            tbContentTitle.Rows.Add(rowTitle);

            XhtmlImage img = new XhtmlImage();
            img.ImageUrl = WapTools.GetImage(this.Request, "imagenes", _mobile.ScreenPixelsWidth, is3g);
            img.ImageAlign = ImageAlign.Middle;
            XhtmlTools.AddImgTable(tbHeaderImg, img);

            XhtmlImage thumb = new XhtmlImage();
            thumb.ImageUrl = String.Format("{0}/Images/muggins/{1}", this.Request.ApplicationPath, "g_alternativa" + suf + ".gif");
            thumb.ImageAlign = ImageAlign.Left;
            XhtmlTools.AddImgTable(tbContentThumb, thumb);
            #endregion

        }

        private void createAvatarContext()
        {
            string contentProfileID = "25828";
            url = url.Replace("downloadOptions", "subscribeEvent");
            url += "cpi=" + contentProfileID + "";
            
            #region IMAGEDESCRIPTION

            XhtmlTableRow rowPriceSub = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowPriceSub, "", "Precio con IVA(3,63 Euros)", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
            tbContentPriceSub.Rows.Add(rowPriceSub);

            #endregion

            #region BUTTONS
 

            XhtmlImage buttonSubscription = new XhtmlImage();
            buttonSubscription.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "Subscribete3.png");
            buttonSubscription.ImageAlign = ImageAlign.Left;

            XhtmlTableRow row2 = new XhtmlTableRow();
            XhtmlTools.addImgLinkTable(row2, buttonSubscription.ImageUrl, url);
            tbContentButtonSub.Controls.Add(row2);

            #endregion
        }

    }
}
