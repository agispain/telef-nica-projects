<?xml version="1.0" encoding="UTF-8" ?>
<%@ Register tagprefix="xhtml" Namespace="xhtml_v7.Tools" Assembly="xhtml_v7" %>
<%@ Page language="c#" Codebehind="step1.aspx.cs" AutoEventWireup="True" Inherits="xhtml_v7.muggins.step1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
         <title>Im&aacute;genes y Fondos</title>
         <link rel="stylesheet" href="../xhtml.css" type="text/css" />
         <meta forua="true" http-equiv="Cache-Control" content="no-cache, max-age=0, must-revalidate, proxy-revalidate, s-maxage=0" />
  </head>
         <body>
                     <form action="step2.aspx" method="post" accept-charset="utf-8">
						<Xhtml:XhtmlTable id="tbMuggin" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowTitle" Runat="server" />
                    		<Xhtml:XhtmlTableRow id="rowSex" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
								<td><select name="sex">
										<option value="girl" selected="selected">mujer</option>
										<option value="boy">hombre</option>
									</select></td>
							</tr>
						</table>						
						<Xhtml:XhtmlTable id="tbSkin" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowSkin" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
								<td><select name="skin">
										<option value="white" selected="selected">blanco</option>
										<option value="mulato">moreno</option>
										<option value="black">negro</option>
									</select></td>
								</tr>
						</table>
						<Xhtml:XhtmlTable id="tbEyes" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowEyes" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
								<td><select name="eyes">
										<option value="Blue" selected="selected">azul</option>
										<option value="LBrown">marr&oacute;n claro</option>
										<option value="Brown">marr&oacute;n oscuro</option>
										<option value="Green">verde</option>
									</select></td>
							</tr>
						</table>
						
						<input type="hidden" name="c" value="<%=c%>" />
						<input type="submit" value="Siguiente paso" class="caja" />
					</form>
					<hr />
                   <table width="100%">
						<tr>
							<td align="center"><a href="./catalog.aspx" style="color: #696969">Volver</a></td>
						</tr>
					</table>                 
          </body>
</html>
