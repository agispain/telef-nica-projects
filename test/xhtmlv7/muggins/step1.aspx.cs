using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;

namespace xhtml_v7.muggins
{
	public partial class step1 : System.Web.UI.Page
	{
		//protected XhtmlTable tbHeader;
		protected XhtmlTableRow     rowHair, rowAttitude;
		protected XhtmlTable  tbAttitude,   tbHair;
		public bool is3g = false;
		public int c;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			MobileCaps _mobile = (MobileCaps)Request.Browser;
			try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType));}
			catch{is3g = false;}
			c = (Request.QueryString["c"] != null) ? Convert.ToInt32(Request.QueryString["c"]) : 47712;

			try
			{
				WapTools.LogUser(this.Request, 204, _mobile.MobileType);						
				WapTools.SetHeader(this.Context);
				WapTools.AddUIDatLog(Request, Response, this.Trace);				
			}
			catch{}
			
			try
			{
				#region HEADER
				/*XhtmlImage img = new XhtmlImage();
				img.ImageUrl = WapTools.GetImage(this.Request, "muggin",  _mobile.ScreenPixelsWidth, is3g);
				XhtmlTools.AddImgTable(tbHeader, img);*/
				#endregion

				#region MUGGINS
				XhtmlTools.AddTextTableRow(rowTitle, "Crea tu avatar", Color.Black, Color.White, 2, HorizontalAlign.Center, VerticalAlign.Middle, true, FontUnit.Small);
				XhtmlTools.AddTextTableRow(rowSex, "1.Escoge tu sexo", Color.Blue, Color.White, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XSmall);
				XhtmlTools.AddTextTableRow(rowSkin, "2.Escoge color de piel", Color.Blue, Color.White, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XSmall);
				XhtmlTools.AddTextTableRow(rowEyes, "3.Escoge color de ojos", Color.Blue, Color.White, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XSmall);
				#endregion
			}
			catch(Exception caught)
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\xhtml\\muggins\\home.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
				Response.Redirect("../error.aspx");	
			}
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
