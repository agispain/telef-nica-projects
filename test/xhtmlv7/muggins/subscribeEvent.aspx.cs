﻿using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using xhtml_v7.Tools;

namespace xhtml_v7.muggins
{
    public partial class subscribeEvent : XCatalogBrowsing
    {
       string cg;
        int contentID;
        double price;
        public string contentProfileID;

        public suscribeEvents subscribeEventList;

        protected void Page_Load(object sender, EventArgs e)
        {
            BillingTools web = new BillingTools();

            string url = Request.Url.ToString();


            contentProfileID = "25828";
            cg = (Request.QueryString["cg"] != null) ? Request.QueryString["cg"] : "";
            contentID = (Request.QueryString["c"] != null && Request.QueryString["c"] != "") ? Convert.ToInt32(Request.QueryString["c"]) : 0;

            try
            {
                subscribeEventList = web.suscribeEvnt(this.Trace, this.Request.Headers["TM_user-id"], contentProfileID);
                price = double.Parse(subscribeEventList.price);
                Session["contentInfo"] = price;

                Trace.Warn(subscribeEventList.returnCode);
            }
                 catch 
            {
                Response.Redirect("../error.aspx");
            }

                if (subscribeEventList.returnCode == "300")
                    Response.Redirect(url.Replace("subscribeEvent", "downloadPage"));
                else
                    Response.Redirect("../error.aspx");
            
           
        }
    }
}


