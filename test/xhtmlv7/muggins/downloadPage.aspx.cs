﻿using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using xhtml_v7.club;
using xhtml_v7.Tools;
using System.Net;
using System.IO;

namespace xhtml_v7.muggins
{
    public partial class downloadPage : XCatalogBrowsing
    {
        public string group = "web";
        public bool isMobile240;
        double finalprice;
        string contentGroup;
        public  string downloadButtonUrl;
        public string contentProfileID;
        string displayText;
        public bool is3g = false;
        string dwldUrl;
        public  string contentUrl;
        int d;
        public string name;
        int id;
        public string downloadButtonSrc;
        public string goToHmPageUrl;
        public string goToHmPageSrc;

        protected void Page_Load(object sender, EventArgs e)
        {

            id = (Request.QueryString["c"] != null && Request.QueryString["c"] != "") ? Convert.ToInt32(Request.QueryString["c"]) : 0;
            _mobile = (MobileCaps)Request.Browser;
            try { is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType)); }
            catch { is3g = false; }

            try
            {
                WapTools.SetHeader(this.Context);
                WapTools.LogUser(this.Request, 198, _mobile.MobileType);
                WapTools.AddUIDatLog(Request, Response, this.Trace);
            }
            catch { }

            _displayKey = WapTools.GetXmlValue("DisplayKeyAvatars");
            _idContent = (Request.QueryString["c"] != null) ? Convert.ToInt32(Request.QueryString["c"]) : 0;

            isMobile240 = _mobile.ScreenPixelsWidth > 240; // Group8
            group = (isMobile240) ? "web" : "web16";

            //calculate price
            var price = (double)(Session["contentInfo"]);
            price = price + 0.21 * price;
            finalprice = Math.Round((double)price, 2);

            var contentCatalog = (KMobile.Catalog.Presentation.ContentSet)(Session["catalogClub"]);

            if (contentCatalog != null)
            {

                for (int i = 0; i < contentCatalog.ContentCollection.Count; i++)
                {
                    if (contentCatalog.ContentCollection[i].IDContent == id)
                    {
                        name = contentCatalog.ContentCollection[i].Name;
                        break;
                    }
                }
            }

            #region getDownloadUrl

            _idContent = (Request["c"] != null && Request["c"] != "") ? Convert.ToInt32(Request["c"]) : 64697; //Convert.ToInt32(Request["c"]);
            string sex = (Request["sex"] != null && Request["sex"] != "") ? Request["sex"].ToUpper() : "GIRL";
            string skin = (Request["skin"] != null && Request["skin"] != "") ? Request["skin"].ToUpper() : "WHITE";
            string hairColor = (Request["hairColor"] != null) ? Request["hairColor"].ToUpper() : "";
            string hairStyle = (Request["hairStyle"] != null) ? Request["hairStyle"].ToUpper() : "";
            string mood = (Request["mood"] != null && Request["mood"] != "") ? Request["mood"].ToUpper() : "HAPPY";
            string eyes = (Request["eyes"] != null && Request["eyes"] != "") ? Request["eyes"].ToUpper() : "BLUE";
            string clothes = (Request["clothes"] != null) ? Request["clothes"].ToUpper() : "";
            string colorClothes = (Request["colorClothes"] != null) ? Request["colorClothes"].ToUpper() : "";
            string trousers = (Request["trousers"] != null) ? Request["trousers"].ToUpper() : "";
            string colorTrousers = (Request["colorTrousers"] != null) ? Request["colorTrousers"].ToUpper() : "";
            string shoes = (Request["shoes"] != null) ? Request["shoes"].ToUpper() : "";
            string colorShoes = (Request["shoes"] != null) ? Request["colorShoes"].ToUpper() : "";
            string underwear = (Request["underwear"] != null) ? Request["underwear"].ToUpper() : "";
            string colorUnderwear = (Request["colorUnderwear"] != null) ? Request["colorUnderwear"].ToUpper() : "";
            string glasses = (Request["glasses"] != null) ? Request["glasses"].ToUpper() : "";
            string others = (Request["others"] != null) ? Request["others"].ToUpper() : "";
            string necklace = (Request.Form["necklace"] != null) ? Request.Form["necklace"].ToUpper() : "";
            string hat = (Request.Form["hat"] != null) ? Request.Form["hat"].ToUpper() : "";
            string earrings = (Request.Form["earrings"] != null) ? Request.Form["earrings"].ToUpper() : "";

            _displayKey = WapTools.GetXmlValue("DisplayKeyAvatars");
            ParameterList pl = new ParameterList();
            pl.Add("MUGGIN", String.Format("{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}-{8}-{9}-{10}-{11}-{12}-{13}-{14}-{15}-{16}-{17}-{18}", sex, skin, mood, eyes, hairColor, hairStyle, clothes, colorClothes, trousers, colorTrousers, shoes, colorShoes, underwear, colorUnderwear, others, glasses, earrings, hat, necklace));


            Operator op = new Operator(Request.UserHostAddress);
            Trace.Warn(op.OperatorName);
            if (op.OperatorName != null && op.OperatorName == "MOVISTAR")
            {
                DownloadInfo downloadInfo = null;
                BillingRequest billingRequest = null;

                CommandItem commandItem = new CommandItem(new Guid(_displayKey), _idContent, "IMG_COLOR", null, "xHTML", _mobile.MobileType, "IMG");
                BillingManager billingManager = new BillingManager();

                billingRequest = billingManager.CreateCommand(Request, WapTools.GetXmlValue("Billing/Provider_MUGGIN"), commandItem);
                downloadInfo = billingManager.DeliverCommand(Request, billingRequest.GUIDCommand, null, pl, WrapperType.DescriptorWrapper | WrapperType.ForwardLockWrapper);

                dwldUrl = downloadInfo.Uri;
                Trace.Warn("Uri : " + dwldUrl);
               
            }

            #endregion

            downloadButtonSrc = "descargaImagen.png";
            downloadButtonUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, downloadButtonSrc);                                   
          
               
            XhtmlTableRow rowTitle1 = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowTitle1, "", "Te acabas de susbcribir con éxito al Servicio de Avatares al precio " + finalprice + " euros ( IVA INCLUIDO) al mes.", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
            tbHeaderRow1.Rows.Add(rowTitle1);

            XhtmlTableRow rowTitle2 = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowTitle2, "", "Podrás crear y descargarte  por este precio todos los avatares que quieras de nuestro portal", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
            tbHeaderRow2.Rows.Add(rowTitle2);

            XhtmlTableRow rowTitle3 = new XhtmlTableRow();
            XhtmlTools.AddTextTableRow("", rowTitle3, "", "Recuedra que se tienes algun problema puedes a llamar  gratis al  1004 par resolvero", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.Large);
            tbHeaderRow3.Rows.Add(rowTitle3);

            #region buttons
            
           
            XhtmlImage ayuda = new XhtmlImage();
            ayuda.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "ayuda.png");
            ayuda.ImageAlign = ImageAlign.Middle;
            XhtmlTableRow row = new XhtmlTableRow();
            XhtmlTools.addImgLinkTable(row, ayuda.ImageUrl, "./helpPage.aspx");
            tbAyudaButton.Controls.Add(row);


            XhtmlImage terms = new XhtmlImage();
            terms.ImageUrl = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "terms.png");
            terms.ImageAlign = ImageAlign.Middle;
            XhtmlTableRow row1 = new XhtmlTableRow();
            XhtmlTools.addImgLinkTable(row1, terms.ImageUrl, "./Terms.aspx");
            tbTermsButtons.Controls.Add(row1);



            goToHmPageSrc = String.Format("{0}/Images/{1}/{2}", this.Request.ApplicationPath, group, "inicio.png");

            if (_mobile.Family == "Android")
                goToHmPageUrl = "http://emocion.dev.kiwee.com/testWeb/";
            else
                goToHmPageUrl = "../muggins.aspx";
            

            #endregion

            var webClient = new WebClient();
            Trace.Warn(dwldUrl);
            byte[] imageBytes = webClient.DownloadData(dwldUrl);
            string extension = Path.GetExtension(dwldUrl);
            Session["contentByte"] = imageBytes;
            Session["contentName"] = name;
            Session["extension"] = extension;
        }

    }
}
