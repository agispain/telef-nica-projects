<?xml version="1.0" encoding="UTF-8" ?>
<%@ Register tagprefix="xhtml" Namespace="xhtml_v7.Tools" Assembly="xhtml_v7" %>
<%@ Page language="c#" Codebehind="catalog.aspx.cs" AutoEventWireup="True" Inherits="xhtml_v7.muggins.catalog" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>
			<%=title%>
		</title>
		<link rel="stylesheet" href="<%=css%>" type="text/css" />
		<meta forua="true" http-equiv="Cache-Control" content="no-cache, max-age=0, must-revalidate, proxy-revalidate, s-maxage=0" />
	</head>
	<body>
		
			<Xhtml:XhtmlTable id="tbTitle" Runat="server" CssClass="normal" />
			<Xhtml:XhtmlTable id="tbPreviews" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2">
				<Xhtml:XhtmlTableRow id="rowPreviews" Runat="server" />
				<Xhtml:XhtmlTableRow id="rowPreviews2" Runat="server" />
			</Xhtml:XhtmlTable>
			<Xhtml:XhtmlTable id="tbCatalog" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2" />
			<Xhtml:XhtmlTable id="tbPages" Runat="server" CssClass="normal" />
			<Xhtml:XhtmlTable id="tbLinks" Runat="server" CssClass="normal" />
			<Xhtml:XhtmlTable id="tbSearch" Runat="server" CssClass="normal" />
			<hr />
			<table width="100%">
				<tr>
					<td align="center"><a href="../default.aspx">Im&aacute;genes y Fondos</a></td>
				</tr>
			</table>			
	</body>
</html>
