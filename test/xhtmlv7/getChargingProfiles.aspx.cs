﻿using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using KMobile.Catalog.Presentation;
using xhtml_v7.Tools;
using System.Net;
using System.IO;

namespace xhtml_v7
{
    public partial class getChargingProfiles : XCatalogBrowsing
    {
        public int is099purchaser;
        public int is4purchaser;
        public string contentProfileID;
        public int isVideoSubscribed;
        public string contentProfileIDvideo;
        public string contentProfileID4;

        protected void Page_Load(object sender, EventArgs e)
        {

            _mobile = (MobileCaps)Request.Browser;

           int projectWeb=(Request.QueryString["p"] != null && Request.QueryString["p"] != "") ? Convert.ToInt32(Request.QueryString["p"]) : 0;
           if (projectWeb == 1)
           {
               _idContentSet = (Request.QueryString["cs"] != null && Request.QueryString["cs"] != "") ? Convert.ToInt32(Request.QueryString["cs"]) : 0;
               _contentGroup = "";
               _contentType = "";
               _displayKey = WapTools.GetXmlValue("DisplayKey");

               ContentSet contentSet = BrowseContentSetExtended();
               Session["catalog"] = contentSet;
           }
            BillingTools webService = new BillingTools();

            string cg = (Request.QueryString["cg"] != null) ? Request.QueryString["cg"] : "";
            int id = (Request.QueryString["id"] != null && Request.QueryString["id"] != "") ? Convert.ToInt32(Request.QueryString["id"]) : 0;
            _contentGroup = WapTools.GetDefaultContentGroup(cg);
            Trace.Warn(_contentGroup);
            switch (_contentGroup)
            {

          case "IMG":

              #region Subscription1.99
              try
              {
                  Subscriptions subscription099 = new Subscriptions();
                  contentProfileID = "31690";
                  subscription099 = webService.getChargingPro(this.Request.Headers["TM_user-id"], contentProfileID);
                  is099purchaser = Int32.Parse(subscription099.returnCode);
              }
              catch 
              {
                  Response.Redirect("./error.aspx");
              }
              #endregion

              #region Subscription4      
               try
               {
                   contentProfileID4 = "25609";
                   Subscriptions subscription4 = new Subscriptions();
                   subscription4 = webService.getChargingPro(this.Request.Headers["TM_user-id"], contentProfileID4);
                   is4purchaser = Int32.Parse(subscription4.returnCode);
               }
               catch 
               {
                   Response.Redirect("./error.aspx");
               }
               #endregion

            if ((is099purchaser == 220) || (is4purchaser == 220))
                Response.Redirect(String.Format("./downloadPageClub.aspx?cg={0}&id={1}",cg,id));
            else
                Response.Redirect(String.Format("./downloadPageOptions.aspx?cg={0}&id={1}", cg, id));

            break;


        case "VIDEO_RGT":

            #region videoSubscription
            try
            {
                Subscriptions subscribeVideo = new Subscriptions();
                contentProfileIDvideo = "31192";
                subscribeVideo = webService.getChargingPro(this.Request.Headers["TM_user-id"], contentProfileIDvideo);
                isVideoSubscribed = Int32.Parse(subscribeVideo.returnCode);
            }
            catch
            {
                Response.Redirect("./error.aspx");
            }
            #endregion

            if (isVideoSubscribed == 220)
                Response.Redirect(String.Format("./downloadPageClub.aspx?cg={0}&id={1}", cg, id));
            else
                Response.Redirect(String.Format("./downloadPageOptions.aspx?cg={0}&id={1}", cg, id));
            break;

            default:

            Response.Redirect(String.Format("./downloadPageOptions.aspx?cg={0}&id={1}", cg, id));
            break;

            }
 
        }
    }
}
