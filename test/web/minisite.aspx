<?xml version="1.0" encoding="UTF-8" ?>
<%@ Register tagprefix="xhtml" Namespace="web.Tools" Assembly="web" %>
<%@ Page language="c#" Codebehind="minisite.aspx.cs" AutoEventWireup="false" Inherits="web.minisite" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>
			<%= title %>
		</title>
		<link rel="stylesheet" href="3g.css" type="text/css" />
			<meta forua="true" http-equiv="Cache-Control" content="no-cache, max-age=0, must-revalidate, proxy-revalidate, s-maxage=0" />
	</head>
	<script type="text/javascript">
		function link(Url) {
			document.location.href = Url;
		} 
	</script>
	<body>
			<xhtml:XhtmlTable id="tbHeader" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />
			<xhtml:XhtmlTable id="tbHeaderDestacados" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />
			<% if (showMarquee) { %>
				<marquee><%=textMarquee%></marquee>
			<% } %>
			<xhtml:XhtmlTable id="tbImages" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2">
				<xhtml:XhtmlTableRow Runat="server" ID="rowImages" />
				<xhtml:XhtmlTableRow Runat="server" ID="rowImg" />
				<xhtml:XhtmlTableRow Runat="server" ID="rowTitlesImg" />
			</xhtml:XhtmlTable>
			<xhtml:XhtmlTable id="tbTop" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2"></xhtml:XhtmlTable>
			<xhtml:XhtmlTable id="tbHeader4" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />
			<xhtml:XhtmlTable id="tbThemes" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2">
				<xhtml:XhtmlTableRow Runat="server" ID="rowTemas" />
			</xhtml:XhtmlTable>
			<xhtml:XhtmlTable id="tbHeader2" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />
			<xhtml:XhtmlTable id="tbAnims" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2">
				<xhtml:XhtmlTableRow Runat="server" ID="rowAnims" />
			</xhtml:XhtmlTable>
			<xhtml:XhtmlTable id="tbHeader3" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />
			<xhtml:XhtmlTable id="tbVideos" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2">
				<xhtml:XhtmlTableRow Runat="server" ID="rowVideos" />
			</xhtml:XhtmlTable>
			<xhtml:XhtmlTable id="tbTemas" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2" />
			<xhtml:XhtmlTable id="tbLinks" Runat="server" CssClass="normal" cellspacing="2" cellpadding="2" />
		
	</body>
</html>
