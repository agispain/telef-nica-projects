using System;
using System.Configuration;
using System.Drawing;
using System.Web.UI.WebControls;
using KMobile.Catalog.Services;
using web.Tools;

namespace web
{
	public class error : System.Web.UI.Page
	{
		protected XhtmlTableRow rowError, rowError2;
		protected XhtmlTable tbHeader;
		protected web.Tools.XhtmlTable tbError;
		public bool is3g = false;

		private void Page_Load(object sender, System.EventArgs e)
		{
			MobileCaps _mobile = (MobileCaps)Request.Browser;
			try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType));}
			catch{is3g = false;}
			try
			{
				WapTools.SetHeader(this.Context);
				WapTools.AddUIDatLog(Request, Response, this.Trace);				
				WapTools.LogUser(this.Request, 103, _mobile.MobileType);					
			}
			catch{}
			
			#region HEADER
			XhtmlImage img = new XhtmlImage();
			img.ImageUrl = WapTools.GetImage(this.Request, "imagenes",  _mobile.ScreenPixelsWidth, is3g);
			XhtmlTools.AddImgTable(tbHeader, img);
			#endregion

			_mobile = null;
			
			XhtmlTools.AddTextTableRow("IMG", rowError, "", WapTools.GetText("Error"), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall);
			XhtmlTools.AddTextTableRow("", rowError2, "", WapTools.GetText("Error2"), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall);
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}