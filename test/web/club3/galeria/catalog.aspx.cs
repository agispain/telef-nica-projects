using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using KMobile.Catalog.Services;
using KMobile.Catalog.Presentation;
using xhtml.Tools;

namespace xhtml.galeria
{
	public class catalog : XCatalogBrowsing
	{
		protected XhtmlTable tbTitle, tbCatalog, tbLinks, tbSearch, tbLinkImages, tbPages;
		protected XhtmlTableRow rowPreviews, rowPreviews2;
		private int page;
		protected string cg_temp, contentGroupDisplay;
		protected XhtmlTable tbHeader, tbPreviews, tbDescription; 
		public string buscar, emocion, atras, up, fondo, back, css;
		public bool is3g = false;
		protected System.Web.UI.HtmlControls.HtmlImage imgGallery;
 
		private void Page_Load(object sender, System.EventArgs e)
		{  
			try 
			{
				_mobile = (MobileCaps)Request.Browser;
				try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType));}
				catch{is3g = false;}

				try
				{
					WapTools.SetHeader(this.Context);
					WapTools.AddUIDatLog(Request, Response);					
					WapTools.LogUser(this.Request, 108, _mobile.MobileType);
				} 
				catch{}

				_idContentSet = (Request.QueryString["cs"] != null) ? Convert.ToInt32(Request.QueryString["cs"]) : 0;
				int page_max = 0;
				page = (Request.QueryString["n"] != null) ? Convert.ToInt32(Request.QueryString["n"]) : 1;
				_contentGroup = (Request.QueryString["cg"] != null) ? Request.QueryString["cg"].ToString() : ""; 
				_contentType = WapTools.GetDefaultContentType(_contentGroup);
				contentGroupDisplay = (Request.QueryString["cgd"] != null) ? Request.QueryString["cgd"].ToString() : _contentGroup;
				_displayKey = WapTools.GetXmlValue("DisplayKey"); 

				string paramBack = String.Format("a1=n&a2={0}&a3=cg&a4={1}&a5=cs&a6={2}",
					page, _contentGroup, _idContentSet);
				
				if (_idContentSet > 0)
				{

					ContentSet contentSet = BrowseContentSetExtended();                                               

					if (contentSet.Count > 0)
					{
						#region CONTENTSET
						try{ReadContentSetGaleria(contentSet, tbCatalog, page);}
						catch{}
						page_max = contentSet.Count;
						if (contentSet.Count % 1 > 0) page_max++;			
						_imgDisplayInst = null;

						XhtmlTableRow row = new XhtmlTableRow();
						XhtmlTools.AddTextTableRow("IMG", row, "", contentSet.Name.ToUpper(), Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, true, FontUnit.XXSmall);
						tbTitle.Controls.Add(row); 
						#endregion
					}

					#region HEADER
					XhtmlImage img = new XhtmlImage();
					img.ImageUrl = WapTools.GetImage(this.Request, "imagenes",  _mobile.ScreenPixelsWidth, is3g);
					XhtmlTools.AddImgTable(tbHeader, img);
					#endregion
                                     
					#region PAGES

					if (page_max > 0)
					{
						TableCell cellTemp = new TableCell();
						cellTemp.HorizontalAlign = HorizontalAlign.Center;
						XhtmlTableRow rowTemp = new XhtmlTableRow();
						int premiere = 0, derniere = 0, cont = 0;
						int[] limits = new int[page_max/5];
						while (cont<(page_max/5))
							limits[cont]=(++cont)*5;
						for (cont=0;cont<page_max/5;cont++)
							if (page<=limits[cont])
							{
								premiere=limits[cont]-4;
								derniere=limits[cont];
								break;
							}
						if (premiere==0 && derniere==0 && page>0)
						{
							derniere = page_max;
							if (limits.Length==0)
								premiere = 1; 
							else
								premiere = limits[cont-1]+1;
						}				
						string URL_Suivant = String.Format("./catalog.aspx?cg={0}&cs={1}&n={2}&cgd={3}&{4}", _contentGroup, _idContentSet, derniere + 1, contentGroupDisplay, paramBack);
						string URL_Precedent = String.Format("./catalog.aspx?cg={0}&cs={1}&n={2}&cgd={3}&{4}", _contentGroup, _idContentSet, premiere - 1, contentGroupDisplay, paramBack);
						if (derniere>1)
						{
							XhtmlLink link = new XhtmlLink();				
							if (premiere > 1)
							{
								//link.ImageUrl = WapTools.GetImage(this.Request, "Previous");
								link.Text = "Atr&aacute;s";
								link.NavigateUrl = URL_Precedent;
								cellTemp.Controls.Add(link);
							}
							else
								cellTemp.Text = "&nbsp;";
							rowTemp.Cells.Add(cellTemp);
							link = null;
							cellTemp = new TableCell();
							cellTemp.HorizontalAlign = HorizontalAlign.Center;

							for (cont=premiere;cont<=derniere;cont++)
							{
								if (cont!=page)
								{
									link = new XhtmlLink();
									link.CssClass = _contentGroup;
									link.NavigateUrl = String.Format("./catalog.aspx?cg={0}&cs={1}&n={2}&cgd={3}&{4}", _contentGroup, _idContentSet, cont, contentGroupDisplay, paramBack);
									link.Text = cont.ToString();
									cellTemp.Controls.Add(link);
								}
								else
								{
									//cellTemp.ForeColor = Color.FromName(WapTools.GetText("Color_" +  _contentGroup));
									cellTemp.Text = cont.ToString();
								}
								rowTemp.Cells.Add(cellTemp);
								link = null;
								cellTemp = new TableCell();
								cellTemp.HorizontalAlign = HorizontalAlign.Center;
							}
			
							if (derniere < page_max)
							{
								link = new XhtmlLink();
								//link.ImageUrl = WapTools.GetImage(this.Request, "Next");
								link.Text = "M�s";
								link.NavigateUrl = URL_Suivant;
								cellTemp.Controls.Add(link);
							}
							else
								cellTemp.Text = "&nbsp;";
							rowTemp.Cells.Add(cellTemp);
							link = null;
							tbPages.Rows.Add(rowTemp);
						}
						else
							tbPages.Visible = false;
					}
					else
						tbPages.Visible = false;

					#endregion

					#region PICTOS
					fondo = WapTools.GetImage(this.Request, "fondo", _mobile.ScreenPixelsWidth, false);
					buscar = WapTools.getPicto(this.Request, "buscar", _mobile);
					emocion = WapTools.getPicto(this.Request, "emocion", _mobile);
					back = WapTools.getPicto(this.Request, "back", _mobile);
					up = WapTools.getPicto(this.Request, "up", _mobile);
					_mobile = null;
					#endregion

					atras = WapTools.UpdateFooter(_mobile, this.Context, "../default.aspx"); 
					contentSet = null;
				}
				else
					Response.Redirect("../gallery.aspx", false);
			}
			catch(Exception caught) 
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\xhtml\\galeria\\catalog.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
				Response.Redirect("../gallery.aspx", false);				
			}
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion


	}
}
