<?xml version="1.0" encoding="UTF-8" ?>
<%@ Register tagprefix="xhtml" Namespace="web.Tools" Assembly="web" %>
<%@ Page language="c#" Codebehind="step1.aspx.cs" AutoEventWireup="false" Inherits="web.muggins.step1" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>
  <head>
         <title>Im&aacute;genes y Fondos</title>
         <link rel="stylesheet" href="../3g.css" type="text/css" />
         <meta forua="true" http-equiv="Cache-Control" content="no-cache, max-age=0, must-revalidate, proxy-revalidate, s-maxage=0" />
  </head>
         <body>
                     <form action="step2.aspx" method="post" accept-charset="utf-8">
						<Xhtml:XhtmlTable id="tbMuggin" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowTitle" Runat="server" />
                    		<Xhtml:XhtmlTableRow id="rowSex" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
								<td><select name="sex">
										<option value="girl" selected="selected">mujer</option>
										<option value="boy">hombre</option>
									</select></td>
							</tr>
						</table>						
						<Xhtml:XhtmlTable id="tbSkin" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowSkin" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
								<td><select name="skin">
										<option value="white" selected="selected">blanco</option>
										<option value="mulato">moreno</option>
										<option value="black">negro</option>
									</select></td>
								</tr>
						</table>
						<Xhtml:XhtmlTable id="tbEyes" Runat="server" CssClass="normal">
							<Xhtml:XhtmlTableRow id="rowEyes" Runat="server" />
						</Xhtml:XhtmlTable>
	                   	<table>
							<tr>
								<td><select name="eyes">
										<option value="Blue" selected="selected">azul</option>
										<option value="LBrown">marr&oacute;n claro</option>
										<option value="Brown">marr&oacute;n oscuro</option>
										<option value="Green">verde</option>
									</select></td>
							</tr>
						</table>
						
						<input type="hidden" name="c" value="<%=c%>" />
						<input type="submit" value="Siguiente paso" class="caja" />
					</form>
					<hr />
                   <table width="100%">
						<tr>
							<td align="center" class="LongCell"><a href="./catalog.aspx" style="color: #696969">Volver</a></td>
						</tr>
					</table>                 
          </body>
</html>
