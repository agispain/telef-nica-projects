using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using web.Tools;

namespace web
{
	public class download : XCatalogBrowsing
	{
		protected XhtmlTable tbContent;
		public string volver, subir;
		protected string theme = "", page = "";
		public bool is3g = false;

		private void Page_Load(object sender, System.EventArgs e)
		{
			_mobile = (MobileCaps)Request.Browser;
			try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType));}
			catch{is3g = false;}

			try
			{
				WapTools.SetHeader(this.Context);
				WapTools.AddUIDatLog(Request, Response, this.Trace);				
			} 
			catch{}
		
			_idContent = (Request["c"] != null && Request["c"] != "") ? Convert.ToInt32(Request["c"]): 64697; //Convert.ToInt32(Request["c"]);
			string sex = (Request["sex"] != null && Request["sex"] != "") ? Request["sex"].ToUpper(): "GIRL";
			string skin = (Request["skin"] != null && Request["skin"] != "") ? Request["skin"].ToUpper(): "WHITE";
			string hairColor = (Request["hairColor"] != null) ? Request["hairColor"].ToUpper(): "";
			string hairStyle = (Request["hairStyle"] != null) ? Request["hairStyle"].ToUpper(): "";
			string mood = (Request["mood"] != null && Request["mood"] != "") ? Request["mood"].ToUpper(): "HAPPY";
			string eyes = (Request["eyes"] != null && Request["eyes"] != "") ? Request["eyes"].ToUpper(): "BLUE";
			string clothes = (Request["clothes"] != null) ? Request["clothes"].ToUpper(): "";
			string colorClothes = (Request["colorClothes"] != null) ? Request["colorClothes"].ToUpper(): "";
			string trousers = (Request["trousers"] != null) ? Request["trousers"].ToUpper() : "";
			string colorTrousers = (Request["colorTrousers"] != null) ? Request["colorTrousers"].ToUpper() : "";
			string shoes = (Request["shoes"] != null) ? Request["shoes"].ToUpper() : "";
			string colorShoes = (Request["shoes"] != null) ? Request["colorShoes"].ToUpper() : "";
			string underwear = (Request["underwear"] != null) ? Request["underwear"].ToUpper() : "";
			string colorUnderwear = (Request["colorUnderwear"] != null) ? Request["colorUnderwear"].ToUpper() : "";
			string glasses = (Request["glasses"] != null) ? Request["glasses"].ToUpper() : "";
			string others = (Request["others"] != null) ? Request["others"].ToUpper() : "";
			string necklace = (Request.Form["necklace"] != null) ? Request.Form["necklace"].ToUpper() : "";
			string hat = (Request.Form["hat"] != null) ? Request.Form["hat"].ToUpper() : "";
			string earrings = (Request.Form["earrings"] != null) ? Request.Form["earrings"].ToUpper() : "";

			//XhtmlTableRow row1 = new XhtmlTableRow();
			//XhtmlTools.AddTextTableRow("", row1, "", sex + "-" + skin + "-" + hairStyle  + "-" + hairColor + "-" + attitude + "-" + eyes + "-" + jacket + "-" + trousers + "-" + shoes + "-" + glasses + "-" + others, Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall);
			//tbContent.Rows.Add(row1);

			ParameterList pl = new ParameterList();
			pl.Add("MUGGIN", String.Format("{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}-{8}-{9}-{10}-{11}-{12}-{13}-{14}-{15}-{16}-{17}-{18}", sex, skin, mood, eyes, hairColor, hairStyle, clothes, colorClothes, trousers, colorTrousers, shoes, colorShoes, underwear, colorUnderwear, others, glasses, earrings, hat, necklace));

			try 
			{
				_displayKey = WapTools.GetXmlValue("DisplayKeyAvatars");										

				Operator op = new Operator(Request.UserHostAddress);
				Trace.Warn(op.OperatorName);
				if (op.OperatorName!= null && op.OperatorName == "MOVISTAR")
				{
					DownloadInfo downloadInfo = null;
					BillingRequest billingRequest = null;
	
					CommandItem commandItem = new CommandItem(new Guid(_displayKey), _idContent, "IMG_COLOR", null, "xHTML", _mobile.MobileType, "IMG");
					BillingManager billingManager = new BillingManager(); 
	
					billingRequest = billingManager.CreateCommand(Request, WapTools.GetXmlValue("Billing/Provider_MUGGIN"), commandItem);
					downloadInfo = billingManager.DeliverCommand(Request, billingRequest.GUIDCommand, null, pl, WrapperType.DescriptorWrapper | WrapperType.ForwardLockWrapper);
					
					string dwldUrl = downloadInfo.Uri;
					Trace.Warn( "Uri : " + dwldUrl );

					XhtmlTableRow row = new XhtmlTableRow();
					XhtmlTools.AddTextTableRow("", row, "", "Para finalizar tu descarga, pulsa en el siguiente enlace UNA SOLA VEZ:", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall);
					tbContent.Rows.Add(row);

					row = new XhtmlTableRow();
					XhtmlTools.AddLinkTableRow("LongCell", row, "Descargar aqu�", dwldUrl, Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, true, FontUnit.XXSmall, "" , "_top"); // "_parent");
					tbContent.Rows.Add(row);
		
					row = new XhtmlTableRow();
					XhtmlTools.AddTextTableRow("", row, "", "Puede durar varios segundos", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall);
					tbContent.Rows.Add(row);	
		
					pl = null;
				}
				else
				{
					XhtmlTableRow row = new XhtmlTableRow();
					XhtmlTools.AddTextTableRow("", row, "", WapTools.GetText("Operador"), Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall);
					tbContent.Rows.Add(row);
				}
			}
			catch(Exception caught)
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\xhtml\\muggins\\download.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
				Response.Redirect("../error.aspx");	
			}

			_mobile = null;
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
