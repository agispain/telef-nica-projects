using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using KMobile.Catalog.Services;
using web.Tools;

namespace xhtml_v6.Tools
{
	public class getImage : System.Web.UI.Page
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			MobileCaps _mobile = (MobileCaps)Request.Browser;

			string contentName = Request.QueryString["t"];

			ImgDisplayInstructions _imgDisplayInst = new ImgDisplayInstructions(_mobile);
			_imgDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "bullet");
			if (_mobile.MobileDeviceManufacturer == "NOKIA")
				_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue("Url_Galeria_IMG");
			else
				_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue("Url_Galeria2_IMG");		

			int weight = 20000;
			long quality = weight >= 0 ? 100L : 70L;

			EncoderParameters parameters = null;
			ImageCodecInfo jpgCodec = null;
			ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
			for (int i = 0; i < codecs.Length; i++)
				if (codecs[i].MimeType == "image/jpeg")
				{
					jpgCodec = codecs[i];
					break;
				}

			WebRequest webrq = WebRequest.Create(String.Format(_imgDisplayInst.PreviewMaskUrl, contentName.Substring(0, 1), contentName));
			Bitmap bmp = (Bitmap)Bitmap.FromStream(webrq.GetResponse().GetResponseStream());

			parameters = new EncoderParameters();
			parameters.Param = new EncoderParameter[1];
			MemoryStream st = new MemoryStream();
			do
			{
				st = new MemoryStream();
				Response.Clear();
				//save bmp to JPEG format				
				parameters.Param[0] = new EncoderParameter(Encoder.Quality, quality);
			
				bmp.Save(st, jpgCodec, parameters);
				//lower quality
				quality -= 5L;
			} while (st.Length > weight && quality > 30L && weight > 0);
			
			// make byte array the same size as the image
			byte[] imageContent = new Byte[st.Length];

			// rewind the memory stream
			st.Position = 0;

			// load the byte array with the image
			st.Read(imageContent, 0, (int)st.Length);
			Response.ContentType = "image/jpeg";
			Response.BinaryWrite(imageContent);
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
