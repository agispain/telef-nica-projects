using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Mail;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Xml;
using AGInteractive.Business;
using KMobile.BasicTools;
using KMobile.Catalog.Presentation;
using KMobile.Catalog.Services;
using FontSize = System.Web.UI.MobileControls.FontSize;

namespace web.Tools
{
	public struct Especial
	{
		public string name;
		public string filter;
	}

	public class MyPolicy : ICertificatePolicy 
	{
		public bool CheckValidationResult(
			ServicePoint srvPoint
			, X509Certificate certificate
			, WebRequest request
			, int certificateProblem) 
		{
			return true;
		}
	}

	public class WapTools
	{
		private static XmlDocument _xmlDoc;
		
		//private static string SoapUri = "http://195.235.160.209:1280/AdCLProxy/Ads";
		//private static string SoapUri = "http://213.4.216.20:1280/AdCLProxy/Ads";
		private static string TEFUri = "http://213.4.216.20:1280/AdCLProxy/GetAds?";
		private static string newTEFUri = "http://213.4.216.20:1280/AdCLProxy/GetAdvertisements?";
		//private static string newTEFUri2 = "http://80.169.208.62/adc/adc?";
		private static string newTEFUri3 = "http://es-acl.telefonica.com/adc/adc?";
		//private static string TEFUriTest = "http://195.235.160.209:1280/AdCLProxy/GetAds?";
		//private static string TEFUriTest = "http://195.53.117.3:80/AdCLProxy/GetAds?";

		
//		private static string SoapEnvelope = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
//		"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:AdsTarget\">" +
//		"<soapenv:Header/>" +
//		"<soapenv:Body>" + 
//		"<urn:adrequest id=\"img-633402297791557500\" pepid=\"emocion\" adserverid=\"JumpTap\" adultcontrol=\"SAFE\" version=\"1.0\">" +                      
//		"<context>" +                                 
//		"<ua>{2}</ua>" +                                   
//		"<time>{0}</time>" +                    
//		"</context>" +                    
//		"<user>" +                         
//		"<uid>{1}</uid>" +                       
//		"</user>" +            
//		"<adreq id=\"{3}\" slot=\"{4}\" explicitimprnotif=\"true\" explicitinternotif=\"true\" >" +                                   
//		"<adtype>banner</adtype>" +                      
//		"</adreq>" +          
//		"</urn:adrequest>" +
//		"</soapenv:Body>" +
//		"</soapenv:Envelope>";

		public static bool isTestSite(HttpRequest req)
		{ 
			if (req.ServerVariables["SERVER_NAME"].ToUpper().IndexOf(".DEV.")>=0 || req.ServerVariables["SERVER_NAME"].ToUpper().IndexOf("LOCALHOST")>=0)
				return true;
			else
				return false;
		}

//		public static string HttpSOAPRequest(System.Web.TraceContext t, string userAgent, string uid, string spot)
//		{
//			System.Net.ServicePointManager.CertificatePolicy = new MyPolicy();
//			Object[] obj_params = {DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss"), uid, userAgent, cont++.ToString(), spot};
//			string message = String.Format (SoapEnvelope, obj_params);
//			//byte [] message_bytes = new UTF8Encoding().GetBytes (message);
//			byte [] message_bytes = System.Text.Encoding.ASCII.GetBytes(message);
//			Uri soapUrl = new Uri (SoapUri);
//			t.Write(message);
//
//			HttpWebRequest req = (HttpWebRequest)WebRequest.Create(soapUrl);
//			req.ContentType = "text/xml";
//			req.Method = "POST";
//			req.KeepAlive = false;
//			req.Timeout = 5000;
//			//req.Accept = "application/soap+xml, application/dime, multipart/related, text/*";
//			//req.Headers.Add("SOAPAction", SoapAction);
//			req.ContentLength = message_bytes.Length;
//			//req.Credentials = new NetworkCredential("ag.com.spai", "ag.spai");
//			//System.Security.Cryptography.X509Certificates.X509Certificate cert = System.Security.Cryptography.X509Certificates.X509Certificate.CreateFromCertFile(HttpContext.Current.Server.MapPath("~/capeconnect.cer"));
//			//req.ClientCertificates.Add(cert);
//
//			Stream req_str = req.GetRequestStream ();
//			req_str.Write (message_bytes, 0, message_bytes.Length);
//			req_str.Flush();
//			message_bytes = null;
//			req_str.Close();
//			req_str = null;
//			t.Warn("Sending request");
//			try
//			{
//				HttpWebResponse resp = (HttpWebResponse)req.GetResponse ();
//				t.Write("Getting response");
//				req = null;
//				long clen = resp.ContentLength;
//				t.Write("clen: " + clen.ToString());
//				Stream resp_stream = resp.GetResponseStream();
//				StreamReader readStream = new StreamReader (resp_stream, Encoding.ASCII);
//				string receivestream = readStream.ReadToEnd();
//				t.Write(receivestream);
//			}
//			catch (WebException e)
//			{				
//				t.Warn("Error in HttpSOAPRequest: " + e.ToString());
//				if ((HttpWebResponse)(e.Response) != null && e.Status.ToString() == "ProtocolError")
//				{
//					Stream resp = e.Response.GetResponseStream();
//					StreamReader readStream = new StreamReader (resp, Encoding.ASCII);
//					string receivestream = readStream.ReadToEnd ();
//					t.Warn(receivestream);
//					resp.Close();
//					resp = null;
//					readStream.Close();
//					readStream = null;
//					return null;
//				}
//				return  null;
//			}	
//			return null;
//		}

		public static void CallPubTEF(System.Web.TraceContext t, HttpRequest req, string uid, string format, XhtmlTable tbPub, int slot, string mobileType)
		{
			string banner = "";
			string postData = "";

			try
			{
				HttpWebRequest myRequest;
				if (uid == "" || uid == null) uid = "34000000000";
				Object[] obj_params = {req.UserAgent, DateTime.Now.AddHours(6).ToString("yyyy-MM-ddTHH:mm:ss") + ".000", uid, GetAdServerCounter().ToString(), slot};
				//postData = string.Format("pepid=emocion&adserverid=JumpTap&adultcontrol=SAFE&version=1.0&ua={0}&time={1}&uid={2}&id={3}&slot={4}&adtype=banner", obj_params);
				postData = string.Format("pepid=1100&adserverid=Amobee&adultcontrol=SAFE&version=1.0&ua={0}&time={1}&uid={2}&id={3}&slot={4}&adtype=banner", obj_params);
				
				myRequest = (HttpWebRequest)WebRequest.Create(TEFUri + postData);
				t.Warn(TEFUri + postData);
				
				myRequest.Method = "GET";
				myRequest.Timeout = Convert.ToInt32(WapTools.GetXmlValue("JumpTap/Timeout")); //(isTestSite(req)) ? 5000 : Convert.ToInt32(WapTools.GetXmlValue("JumpTap/Timeout"));
				t.Write("Sending request");
				HttpWebResponse myHttpWebResponse= (HttpWebResponse)myRequest.GetResponse();
				t.Write("Getting response");
				Stream streamResponse=myHttpWebResponse.GetResponseStream();
				StreamReader streamRead = new StreamReader( streamResponse );
				banner = streamRead.ReadToEnd();
				t.Write(banner);
				streamRead.Close();
				streamResponse.Close();
				myHttpWebResponse.Close();
				if (Convert.ToBoolean(WapTools.GetXmlValue("JumpTap/SendEmail")))
					WapTools.SendMail(HttpContext.Current, TEFUri + postData, banner);				
			}
			catch(Exception e)
			{
				t.Write(e.ToString()); banner="";
				if (Convert.ToBoolean(WapTools.GetXmlValue("JumpTap/SendEmail")))
					WapTools.SendMail(HttpContext.Current, TEFUri + postData, e.ToString());
			}
			if (banner != "") // && mobileType.ToUpper() != "NOKIA2760") // Not ERROR								
			{
				string link="", img="";
				XmlDocument _xmlBanner = new XmlDocument();	
				_xmlBanner.InnerXml = banner;
				t.Write("Reading link and banner: " + _xmlBanner.InnerXml);
				// link
				try
				{
					link = _xmlBanner.SelectSingleNode("adresponse/adres/interaction/locator").InnerText;
					t.Write("link: " + link);
					img = _xmlBanner.SelectSingleNode("adresponse/adres/adsource/locator").InnerText;
					t.Write("banner: " + img);
				}
				catch{}
				// IMAGE
				if (link != "" && img != "")
				{
					XhtmlTableRow row = new XhtmlTableRow();
					XhtmlTableCell cell = new XhtmlTableCell();
					XhtmlLink lnk = new XhtmlLink();
					lnk.ImageUrl = img;
					lnk.NavigateUrl = link;
					cell.Controls.Add(lnk);
					cell.HorizontalAlign = HorizontalAlign.Center;
					row.Controls.Add(cell);
					tbPub.Rows.Add(row);
					cell = null;
					row = null;
					lnk = null;
				}
			}
		}

		public static void CallNewPubTEF(System.Web.TraceContext t, HttpRequest req, string uid, XhtmlTable tbPub, int slot)
		{
			CallNewPubTEF(t, req, uid, tbPub, slot, true);
		}

		public static void CallNewPubTEF(System.Web.TraceContext t, HttpRequest req, string uid, XhtmlTable tbPub, int slot, bool show)
		{
			string banner = "";
			string postData = "";

			try
			{
				HttpWebRequest myRequest;
				if (uid == "" || uid == null) uid = "0";
				Object[] obj_params = {slot, req.UserAgent, GetAdServerCounter().ToString(), uid};
				postData = string.Format("ch=010&as={0}&ua={1}&ai={2}&ve=1&ui={3}&ac=2&pi=1100&pr=0101", obj_params);
				myRequest = (HttpWebRequest)WebRequest.Create(newTEFUri + postData);
				//t.Warn(newTEFUri + postData);
				
				myRequest.Method = "GET";
				myRequest.Timeout = Convert.ToInt32(WapTools.GetXmlValue("JumpTap/Timeout")); //(isTestSite(req)) ? 5000 : Convert.ToInt32(WapTools.GetXmlValue("JumpTap/Timeout"));
				//t.Write("Sending request");
				HttpWebResponse myHttpWebResponse= (HttpWebResponse)myRequest.GetResponse();
				//t.Write("Getting response");
				Stream streamResponse=myHttpWebResponse.GetResponseStream();
				StreamReader streamRead = new StreamReader( streamResponse );
				banner = streamRead.ReadToEnd();
				//t.Write(banner);
				streamRead.Close();
				streamResponse.Close();
				myHttpWebResponse.Close();
				if (Convert.ToBoolean(WapTools.GetXmlValue("JumpTap/SendEmail")))
					WapTools.SendMail(HttpContext.Current, TEFUri + postData, banner);				
			}
			catch(Exception e)
			{
				//t.Write(e.ToString()); banner="";
				if (Convert.ToBoolean(WapTools.GetXmlValue("JumpTap/SendEmail")))
					WapTools.SendMail(HttpContext.Current, TEFUri + postData, e.ToString());
			}
			if (banner != "" && show) // && mobileType.ToUpper() != "NOKIA2760") // Not ERROR								
			{
				string link="", img="";
				XmlDocument _xmlBanner = new XmlDocument();	
				_xmlBanner.InnerXml = banner;
				//_xmlBanner.InnerXml = "<adresponse version=\"1\" id=\"REQ_13009\"><returnres><returncode>6200</returncode><returnstr>Status OK</returnstr></returnres><ad flight=\"1008590\" campaign=\"1003395\" ad_placement=\"1\" id=\"13009\"><resource ad_presentation=\"0101\"><creative_element type=\"image\"><attribute type=\"locator\">http://tfes-prod.amobee.com/content/468033/13394.gif?adcl_event=impression&amp;adcl_id=13009&amp;adcl_pepid=1100&amp;adcl_adserverid=Amobee&amp;adcl_version=1&amp;adcl_notifyadsrv=false</attribute><interaction type=\"click2wap\"><attribute type=\"URL\"><![CDATA[http://tfes-prod.amobee.com/upsteed/actionpage?pr=1&amp;as=1095&amp;time=1249396209087&amp;h=mozilla%2F5.0&amp;pl=1&amp;u=cfcd208495d565ef66e7dff9f98764da&amp;test=0&amp;n=&amp;a=1008591&adcl_event=interaction&adcl_id=13009&adcl_pepid=1100&adcl_adserverid=Amobee&adcl_version=1&adcl_notifyadsrv=false&adcl_type=click2wap]]></attribute></interaction></creative_element></resource></ad></adresponse>";
				//t.Write("Reading link and banner: " + _xmlBanner.InnerXml);
				// link
				try
				{
					link = _xmlBanner.SelectSingleNode("adresponse/ad/resource/creative_element/interaction/attribute[@type='URL']").InnerText;
					//t.Write("link: " + link);
					img = _xmlBanner.SelectSingleNode("adresponse/ad/resource/creative_element/attribute[@type='locator']").InnerText;
					//t.Write("banner: " + img);
				}
				catch{}
				// IMAGE
				if (link != "" && img != "")
				{
					XhtmlTableRow row = new XhtmlTableRow();
					XhtmlTableCell cell = new XhtmlTableCell();
					XhtmlLink lnk = new XhtmlLink();
					lnk.ImageUrl = img;
					lnk.NavigateUrl = link;
					cell.Controls.Add(lnk);
					cell.HorizontalAlign = HorizontalAlign.Center;
					row.Controls.Add(cell);
					tbPub.Rows.Add(row);
					cell = null;
					row = null;
					lnk = null;
				}
			}
		}

		public static void CallNewPubTEF2(System.Web.TraceContext t, HttpRequest req, string uid, XhtmlTable tbPub, string slot)
		{
			string banner = "";
			string postData = "";

			try
			{
				HttpWebRequest myRequest;
				if (uid == "" || uid == null) uid = "0";
				Object[] obj_params = {slot, req.UserAgent, GetAdServerCounter().ToString(), uid};
				postData = string.Format("ch=010&as={0}&ua={1}&ai={2}&ve=acl1&ui={3}&ac=2&pi=1100&pr=0101&ps=300", obj_params);
				myRequest = (HttpWebRequest)WebRequest.Create(newTEFUri3 + postData);
				//t.Warn(newTEFUri + postData);
				
				myRequest.Method = "GET";
				myRequest.Timeout = Convert.ToInt32(WapTools.GetXmlValue("JumpTap/Timeout")); //(isTestSite(req)) ? 5000 : Convert.ToInt32(WapTools.GetXmlValue("JumpTap/Timeout"));
				//t.Write("Sending request");
				HttpWebResponse myHttpWebResponse= (HttpWebResponse)myRequest.GetResponse();
				//t.Write("Getting response");
				Stream streamResponse=myHttpWebResponse.GetResponseStream();
				StreamReader streamRead = new StreamReader( streamResponse );
				banner = streamRead.ReadToEnd();
				//t.Write(banner);
				streamRead.Close();
				streamResponse.Close();
				myHttpWebResponse.Close();
				if (Convert.ToBoolean(WapTools.GetXmlValue("JumpTap/SendEmail")))
					WapTools.SendMail(HttpContext.Current, newTEFUri3 + postData, banner);				
			}
			catch(Exception e)
			{
				//t.Write(e.ToString()); banner="";
				if (Convert.ToBoolean(WapTools.GetXmlValue("JumpTap/SendEmail")))
					WapTools.SendMail(HttpContext.Current, newTEFUri3 + postData, e.ToString());
			}
			if (banner != "") // && mobileType.ToUpper() != "NOKIA2760") // Not ERROR								
			{
				string link="", img="";
				XmlDocument _xmlBanner = new XmlDocument();	
				_xmlBanner.InnerXml = banner;
				//_xmlBanner.InnerXml = "<adresponse version=\"1\" id=\"REQ_13009\"><returnres><returncode>6200</returncode><returnstr>Status OK</returnstr></returnres><ad flight=\"1008590\" campaign=\"1003395\" ad_placement=\"1\" id=\"13009\"><resource ad_presentation=\"0101\"><creative_element type=\"image\"><attribute type=\"locator\">http://tfes-prod.amobee.com/content/468033/13394.gif?adcl_event=impression&amp;adcl_id=13009&amp;adcl_pepid=1100&amp;adcl_adserverid=Amobee&amp;adcl_version=1&amp;adcl_notifyadsrv=false</attribute><interaction type=\"click2wap\"><attribute type=\"URL\"><![CDATA[http://tfes-prod.amobee.com/upsteed/actionpage?pr=1&amp;as=1095&amp;time=1249396209087&amp;h=mozilla%2F5.0&amp;pl=1&amp;u=cfcd208495d565ef66e7dff9f98764da&amp;test=0&amp;n=&amp;a=1008591&adcl_event=interaction&adcl_id=13009&adcl_pepid=1100&adcl_adserverid=Amobee&adcl_version=1&adcl_notifyadsrv=false&adcl_type=click2wap]]></attribute></interaction></creative_element></resource></ad></adresponse>";
				//t.Write("Reading link and banner: " + _xmlBanner.InnerXml);
				// link
				try
				{
					link = _xmlBanner.SelectSingleNode("adresponse/ad/resource/creative_element/interaction/attribute[@type='URL']").InnerText;
					t.Write("link: " + link);
					img = _xmlBanner.SelectSingleNode("adresponse/ad/resource/creative_element/attribute[@type='locator']").InnerText;
					//t.Write("banner: " + img);
				}
				catch{}
				// IMAGE
				if (link != "" && img != "")
				{
					XhtmlTableRow row = new XhtmlTableRow();
					XhtmlTableCell cell = new XhtmlTableCell();
					XhtmlLink lnk = new XhtmlLink();
					lnk.ImageUrl = img;
					lnk.NavigateUrl = link;
					cell.Controls.Add(lnk);
					cell.HorizontalAlign = HorizontalAlign.Center;
					row.Controls.Add(cell);
					tbPub.Rows.Add(row);
					cell = null;
					row = null;
					lnk = null;
				}
			}
		}

	
		/*public static void CallPub(HttpRequest req, string type, string format, XhtmlTable tbPub)
		{
			string banner = "";
			HttpWebRequest myRequest;
			try
			{
				string postData = string.Format("spot={0}&pub=ts&ver=1.5&u={1}&format={2}&ua={3}", type, req.Headers["TM_user-id"], format, req.UserAgent);
				myRequest = (HttpWebRequest)WebRequest.Create(ConfigurationSettings.AppSettings["UrlPub"] + postData);
				myRequest.Method = "GET";
				myRequest.Timeout = Convert.ToInt32(ConfigurationSettings.AppSettings["TimeOut"]) ;
				HttpWebResponse myHttpWebResponse= (HttpWebResponse)myRequest.GetResponse();
				Stream streamResponse=myHttpWebResponse.GetResponseStream();
				StreamReader streamRead = new StreamReader( streamResponse );
				banner = streamRead.ReadToEnd();
				streamRead.Close();
				streamResponse.Close();
				myHttpWebResponse.Close();
			}
			catch{banner="";}
			
			if (banner != "") // Not ERROR								
			{
				string link="", line1="", line2="", img="";
				XmlDocument _xmlBanner = new XmlDocument();	
				_xmlBanner.InnerXml = banner;
				
				// link
				try
				{
					link = (_xmlBanner.SelectSingleNode("ads/banner/link") != null) ?  _xmlBanner.SelectSingleNode("ads/banner/link").Attributes["href"].Value : "";
					img = (_xmlBanner.SelectSingleNode("ads/banner/img") != null) ?  _xmlBanner.SelectSingleNode("ads/banner/img").Attributes["src"].Value : "";
					line1 = (_xmlBanner.SelectSingleNode("ads/banner/line1") != null) ? _xmlBanner.SelectSingleNode("ads/banner/line1").InnerText : "";
					line2 = (_xmlBanner.SelectSingleNode("ads/banner/line2") != null) ? _xmlBanner.SelectSingleNode("ads/banner/line2").InnerText : "";
				}
				catch{}
				// IMAGE
				if (link != "" && img != "")
				{
					XhtmlTableRow row = new XhtmlTableRow();
					XhtmlTableCell cell = new XhtmlTableCell();
					XhtmlLink lnk = new XhtmlLink();
					lnk.ImageUrl = img;
					lnk.NavigateUrl = link;
					cell.Controls.Add(lnk);
					cell.HorizontalAlign = HorizontalAlign.Center;
					row.Controls.Add(cell);
					tbPub.Rows.Add(row);
					cell = null;
					row = null;
					lnk = null;
				}
				if (link != "" && line1 != "")
				{
					XhtmlTableRow row = new XhtmlTableRow();
					XhtmlTableCell cell = new XhtmlTableCell();
					XhtmlLink lnk = new XhtmlLink();
					lnk.NavigateUrl = link;
					lnk.Text = line1;
					cell.Controls.Add(lnk);
					cell.HorizontalAlign = HorizontalAlign.Center;
					row.Controls.Add(cell);
					tbPub.Rows.Add(row);
					cell = null;
					row = null;
					lnk = null;		
				}
				if (line2 != "")
				{
					XhtmlTableRow row = new XhtmlTableRow();
					XhtmlTableCell cell = new XhtmlTableCell();
					cell.Text = line2;
					cell.HorizontalAlign = HorizontalAlign.Center;
					row.Cells.Add(cell);
					tbPub.Rows.Add(row);
					row = null;
					cell = null;	
				}
			}
		}
*/

		public static XmlNode GetXmlNode( string path )
		{
			string pathXmlFile = HttpContext.Current.Server.MapPath("~/ConfigSite.xml"); // Gets Physical path of the "ConfigSite.xml" on server
			Cache cache = HttpContext.Current.Cache;
		
			try
			{
				_xmlDoc = (XmlDocument)cache[pathXmlFile];
				if (_xmlDoc == null)
				{
					_xmlDoc = new XmlDocument();
					_xmlDoc.Load(pathXmlFile);  // loads "ConfigSite.xml file 
					cache.Add(pathXmlFile, _xmlDoc, new CacheDependency(pathXmlFile), DateTime.Now.AddHours(6),TimeSpan.Zero, CacheItemPriority.High, null);
				}
				XmlNode root =_xmlDoc.DocumentElement;
				return root.SelectSingleNode(path);
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				cache = null;
				pathXmlFile = null;
			}
		}

		public static string GetXmlValue( string path )
		{
			string xmlValue = null;
			try 
			{
				xmlValue = GetXmlNode(path).Attributes["value"].Value;
			}
			catch  
			{
				xmlValue = "";
				//	throw ex;
				//SendMail(HttpContext.Current, ex);
			}
			return xmlValue;
		} 

		public static string GetText( string text )
		{
			return GetXmlValue(String.Format("Texts/data[@name='{0}']", text));
		}

		public static string GetImage( HttpRequest req, string img )
		{
			return String.Format("http://{0}{1}/Images/{2}", req.ServerVariables["SERVER_NAME"], req.ApplicationPath, GetXmlValue(String.Format("Images/data[@name='{0}']", img)));
		} 

		public static string GetImage( HttpRequest req, string img, int width, bool is3g, bool isTouch )
		{
			return GetImageWeb(req, img, width);
		}
		
		public static string GetImage( HttpRequest req, string img, int width, bool is3g )
		{
			if (is3g)
				return GetImage3g(req, img, width);
			else
			{
				string size;
				if (width>=450) width = 315;
				else if (width>=339 && width<450) width = 315; 
				else if (width>=315 && width<339) width = 315; 
				else if (width>=229 && width<315) width = 229; 
				else if (width>=164 && width<229) width = 164; 
				else width = 118;
			
				if (ConfigurationSettings.AppSettings["IsChristmas"] == "true")
				{
					if (width == 315) width = 229;
					size =  "christmas_" + width.ToString();
				}
				else 
					size = width.ToString();
			
				string image = GetXmlValue(String.Format("Images/data[@name='{0}']", img));
				if (image!="")
					return String.Format("http://{0}{1}/Images/{2}/{3}", req.ServerVariables["SERVER_NAME"], req.ApplicationPath, size, image);
				else 
					return "";
			}
		} 

		public static string GetImageWeb( HttpRequest req, string img, int width )
		{
			string image = GetXmlValue(String.Format("Images/data[@name='{0}']", img));
			if (image!="")
			{
				string group = (width > 240) ? "web" : "web16";				
				return String.Format("http://{0}{1}/Images/{2}/{3}", req.ServerVariables["SERVER_NAME"], req.ApplicationPath, group, image);
			}
			else 
				return "";
		} 
/*
		public static string GetImageWeb( HttpRequest req, string img )
		{
			string image = GetXmlValue(String.Format("Images/data[@name='{0}']", img));
			if (image!="")
				return String.Format("http://{0}{1}/Images/web/{2}", req.ServerVariables["SERVER_NAME"], req.ApplicationPath, image);
			else 
				return "";
		} 
*/

		public static string GetImage3g( HttpRequest req, string img, int width )
		{
			string folder = "16";
			if (width>=450) folder = "315";
			else if (width>=315) folder = "315";
			else if (width>=229) folder = "16B";
			
			string image = GetXmlValue(String.Format("Images/data[@name='{0}']", img));
			if (image!="")
				return String.Format("http://{0}{1}/Images/{2}/{3}", req.ServerVariables["SERVER_NAME"], req.ApplicationPath, folder, image);
			else 
				return "";
		} 

		public static string GetDefaultContentType( string contentGroup )
		{
			return GetXmlValue(String.Format("DefaultContentType/data[@name='{0}']", contentGroup));
		}

		public static string GetDefaultContentGroup( string contentType )
		{
			return GetXmlValue(String.Format("DefaultContentGroup/data[@name='{0}']", contentType));
		}

		
		public static void LogUser(HttpRequest req, int idSite, string mobiletype)
		{
			try
			{
				string sUID = req.Headers["TM_user-id"]; 
				if(sUID != null && sUID != "") 
				{
					User u = new User();
					u.Visit(sUID, idSite, mobiletype);
					u = null;
				}
			}
			catch(Exception ex){string kk = ex.Message;}
		}

		public static System.Web.UI.MobileControls.Link BuildLink( string text, string navigateUrl )
		{
			System.Web.UI.MobileControls.Link lnk = new System.Web.UI.MobileControls.Link();
			lnk.Font.Size = FontSize.Small;
			lnk.BreakAfter = true;
			lnk.Alignment = Alignment.Left;
			lnk.Text = text;
			lnk.NavigateUrl = navigateUrl;
			return lnk;
		}

		public static XhtmlLink BuildLink2( string text, string navigateUrl )
		{
			XhtmlLink lnk = new XhtmlLink();
			lnk.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall;
			lnk.Text = text;
			lnk.NavigateUrl = navigateUrl;
			return lnk;
		}

		public static string FindProperty(PropertyCollection colProperty, string name)
		{
			try{return colProperty[name].Value.ToString();}
			catch{return "";}			
		}

		public static void AddPicto( MobileCaps mobile, System.Web.UI.MobileControls.Panel pnl, string imageUrl )
		{
			if (mobile.IsAdvanced || mobile.IsColor)
			{
				System.Web.UI.MobileControls.Image img = new System.Web.UI.MobileControls.Image();
				img.ImageUrl = imageUrl;
				img.BreakAfter = false;
				pnl.Controls.Add(img);
				img = null;
			}
		}
		
//		public static string GetUrlBilling( System.Web.HttpRequest req, int drm, string contentGroup, string contentType, string referer, string version, string contentSet )
//		{
//			string urlBilling; 			
//			urlBilling = String.Format("{0}{1}?e=1&d={2}&ref={3}&ct={4}&cs={5}&c={6}",
//				//WapTools.isTestSite(req) ? WapTools.GetXmlValue("BillingTest/Url") : WapTools.GetXmlValue("Billing/Url"),
//				WapTools.GetXmlValue("Billing/Url"),
//				//WapTools.isTestSite(req) ? WapTools.GetXmlValue("BillingTest/UrlBilling_" + contentType) : WapTools.GetXmlValue("Billing/UrlBilling_" + contentType),
//				WapTools.GetXmlValue("Billing/UrlBilling_" + contentType),
//				drm.ToString(), referer, "{1}", contentSet, "{2}");
//			return urlBilling;
//		}

        public static string GetChargingProfilesUrl()
        {
            string urlGetChargingProfiles;
            urlGetChargingProfiles = String.Format("{0}?cg={1}&id={2}", WapTools.GetXmlValue("Billing/UrlGetChargingProfiles"), "{0}", "{1}");

            return urlGetChargingProfiles;

        }

		public static string GetUrlBilling(string contentGroup, string contentType, string referer, string version, string contentSet, string mobileType, int promo, int sexy)
		{
			string urlBilling; 		
			if (promo > 0)
				urlBilling = String.Format("{0}{1}?ref={2}&ct={3}&cs={4}&c={5}",
					//WapTools.isTestSite(req) ? WapTools.GetXmlValue("BillingTest/Url") : WapTools.GetXmlValue("Billing/Url"),
					WapTools.GetXmlValue("Billing/Url"),
					//WapTools.isTestSite(req) ? WapTools.GetXmlValue("BillingTest/UrlBilling_" + contentType) : WapTools.GetXmlValue("Billing/UrlBilling_" + contentType),
					WapTools.GetXmlValue("Billing/UrlBilling_Promo_" + contentType),
					referer, "{1}", contentSet, "{2}");
			else
				urlBilling = String.Format("{0}{1}?ref={2}&ct={3}&cs={4}&c={5}",
					//WapTools.isTestSite(req) ? WapTools.GetXmlValue("BillingTest/Url") : WapTools.GetXmlValue("Billing/Url"),
					(sexy > 0) ? WapTools.GetXmlValue("Billing/UrlSexy") : WapTools.GetXmlValue("Billing/Url"),
					//WapTools.isTestSite(req) ? WapTools.GetXmlValue("BillingTest/UrlBilling_" + contentType) : WapTools.GetXmlValue("Billing/UrlBilling_" + contentType),
					(sexy > 0) ? WapTools.GetXmlValue("Billing/UrlSexyBilling_" + contentType) : WapTools.GetXmlValue("Billing/UrlBilling_" + contentType),
					referer, "{1}", contentSet, "{2}");
			return urlBilling;
		}
	
		public static string GetUrlBilling(string contentGroup, string contentType, string referer, string version, string contentSet, string mobileType, int promo)
		{
			return GetUrlBilling(contentGroup, contentType, referer, version, contentSet, mobileType, promo, 0);
		}

		public static string GetUrlBilling(string contentGroup, string contentType, string referer, string version, string contentSet, string mobileType)
		{
			return GetUrlBilling(contentGroup, contentType, referer, version, contentSet, mobileType, -1, 0);
		}

		public static string GetBrandedUrlBilling(string contentGroup, string contentType, string referer, string version, string contentSet )
		{
			string urlBilling; 			
			urlBilling = String.Format("{0}{1}?ref={2}&ct={3}&cs={4}&c={5}",
				//WapTools.isTestSite(req) ? WapTools.GetXmlValue("BillingTest/Url") : WapTools.GetXmlValue("Billing/Url"),
				WapTools.GetXmlValue("Billing/Url"),
				//WapTools.isTestSite(req) ? WapTools.GetXmlValue("BillingTest/UrlBilling_Branded_" + contentType) : WapTools.GetXmlValue("Billing/UrlBilling_" + contentType),
				WapTools.GetXmlValue("Billing/UrlBilling_" + contentType),
				referer, "{0}", contentSet, "{1}");
			return urlBilling;
		}

		public static string GetUrlBillingFree( System.Web.HttpRequest req, string contentGroup, string contentType, string referer, string version, string contentSet )
		{
			return String.Format("{0}{1}?ct={2}&ref={3}&cs={4}&c={5}", 
				req.ApplicationPath,
				WapTools.GetXmlValue("Billing/UrlBilling_Free"),
				"{1}", referer, contentSet, "{2}");
		}

		public static string GetUrlMuggin( System.Web.HttpRequest req)
		{
			return String.Format("{0}?c={1}", WapTools.GetXmlValue("Muggins/Url"),"{2}");
			//return String.Format("{0}?t={1}", WapTools.GetXmlValue("Muggins/Url"), "{0}");
		}

		public static string GetUrlBillingClub099( System.Web.HttpRequest req, string contentGroup, string contentType, string referer, string version, string contentSet )
		{
			return String.Format("{0}{1}?ref={2}&ct={3}&cs={4}&c={5}", 
				WapTools.GetXmlValue("Billing/UrlClub099"),
				WapTools.GetXmlValue("Billing/UrlClubBilling"),
				referer, "{1}", contentSet, "{2}");
		}		

		public static string GetUrlBillingClub( System.Web.HttpRequest req, string contentGroup, string contentType, string referer, string version, string contentSet )
		{
			return String.Format("{0}{1}?ref={2}&ct={3}&cs={4}&c={5}", 
				(contentGroup.StartsWith("VIDEO") || contentGroup == "") ? WapTools.GetXmlValue("Billing/UrlClubV") : WapTools.GetXmlValue("Billing/UrlClub"),
				WapTools.GetXmlValue("Billing/UrlClubBilling"),
				referer, "{1}", contentSet, "{2}");
		}		
		
		public static string GetUrlXView( System.Web.HttpRequest req, string contentGroup, string contentType, string referer, string version, string contentSet )
		{
			return String.Format("{0}{1}?ref={2}&ct={3}&cs={4}&c={5}", 
				req.ApplicationPath,
				(contentGroup.StartsWith("VIDEO") || contentGroup == "") ? WapTools.GetXmlValue("Billing/UrlXViewV") : WapTools.GetXmlValue("Billing/UrlXView"),
				referer, "{1}", contentSet, "{2}");
		}

        public static string GetChargingsProfilesUrl()
        {
            string urlGetChargingProfiles;
            urlGetChargingProfiles = String.Format("{0}?cg={1}&id={2}", WapTools.GetXmlValue("Billing/UrlGetChargingProfiles"), "{0}", "{1}");

            return urlGetChargingProfiles;

        }

		public static string GetUrlXView( System.Web.HttpRequest req, string contentGroup, string contentType, string referer, string version, string contentSet, string paramBack)
		{
			return String.Format("{0}{1}?ref={2}&ct={3}&cs={4}&c={5}&{6}", 
				req.ApplicationPath,
				WapTools.GetXmlValue("Billing/UrlXView"),
				referer, contentSet, "{1}", paramBack);
		}

		public static string GetParamBack(HttpRequest req, bool decrypt)
		{
			int index = 1;
			string param = "";
			if( !decrypt && req.QueryString["a1"] != null ) param = String.Format("a0={0}", req.QueryString["a0"]);
			while( req.QueryString[String.Format("a{0}", index)] != null )
			{
				if(decrypt)
				{
					if( param == "" )
						param = String.Format("{0}={1}", req.QueryString[String.Format("a{0}", index)], req.QueryString[String.Format("a{0}", (index + 1))]);
					else
						param = String.Format("{0}&amp;{1}={2}", param, req.QueryString[String.Format("a{0}", index)], req.QueryString[String.Format("a{0}", (index + 1))]);
				}
				else
				{
					if( param == "" )
						param = String.Format("a{0}={1}&amp;a{2}={3}", index, req.QueryString[String.Format("a{0}", index)], index + 1, req.QueryString[String.Format("a{0}", index + 1)]);
					else
						param = String.Format("{0}&amp;a{1}={2}&amp;a{3}={4}", param, index, req.QueryString[String.Format("a{0}", index)], index + 1, req.QueryString[String.Format("a{0}", index + 1)]);
				}
				index = index + 2;
			}
			return param;
		}
		
		public static void SetHeader(HttpContext context)
		{			
			context.Response.Cache.SetNoServerCaching();
			context.Response.Cache.SetExpires( DateTime.Parse("01/01/1970") );
			context.Response.Cache.SetCacheability( HttpCacheability.NoCache );
			context.Response.Cache.SetMaxAge( new TimeSpan(0,0,0));
			context.Response.Cache.SetNoStore();
			context.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
			context.Response.AddHeader("pragma", "no-cache");
		}

		public static string UpdateFooter(MobileCaps mobile, HttpContext context, string urlBackDefault )
		{
			SetHeader(context);

			if( context.Request.QueryString["a1"] != null )
				urlBackDefault = String.Format("{0}{1}", context.Request.ApplicationPath, WapTools.GetXmlValue("Back/CatalogGraphic"));
			try
			{
				if( urlBackDefault != null )
				{
					string param = GetParamBack(context.Request, true);

					return String.Format("{0}?{1}", urlBackDefault, param);
				}
				else
				{
					return "./default.aspx";
				}
			} 
			catch{return "./default.aspx";}
		}

		public static string getPicto(HttpRequest req, string img, MobileCaps _mobile)
		{
			if (_mobile.ScreenPixelsWidth >= 330)
				return GetImage(req, img + "60");
			else if (_mobile.ScreenPixelsWidth >= 240)
				return GetImage(req, img + "40");
			else if (_mobile.ScreenPixelsWidth >= 170)
				return GetImage(req, img + "25");
			else
				return GetImage(req, img + "20");
		}

		public static string getPicto3g(string path, string img, int width)
		{
			if (width >= 229)
				return String.Format("{0}/Images/16B/{1}", path, GetXmlValue(String.Format("Images/data[@name='{0}']", img)));
			else
				return String.Format("{0}/Images/16/{1}", path, GetXmlValue(String.Format("Images/data[@name='{0}']", img)));			
		}

		public static void AddUIDatLog(HttpRequest req, HttpResponse rep, System.Web.TraceContext t)
		{
			string sUID = req.Headers["TM_user-id"];
			if (sUID != null && sUID != "" && req.UserAgent.IndexOf("iPhone") <= 0 && req.UserAgent.IndexOf("droid") <= 0 && req.UserAgent.IndexOf("Windows Phone") <= 0 && req.UserAgent.ToLower().IndexOf("berry") <= 0)
			{					
				t.Write("Adding Content-Type application/vnd.wap.xhtml+xml: No Android/BB/iPhone");
				rep.ContentType =  "application/vnd.wap.xhtml+xml";				
			}
            rep.ContentType = "text/html";
		}
		
		public static bool isCompatibleThemes(MobileCaps mobile)
		{
			try
			{
				bool compatible = false;
				foreach (string m in WapTools.getNodes("ThemesCompatibility"))
				{
					if (mobile.MobileType == m)
					{
						compatible = true;
						break;
					}
				}
				return (compatible); 
			}
			catch {return false;}
		}

		public static bool hasDescription(int idContentSet)
		{
			try
			{
				bool desc = false;
				foreach (string id in WapTools.getNodes("Descriptions"))
				{
					if (id == idContentSet.ToString())
					{
						desc = true;
						break;
					}
				}
				return (desc); 
			}
			catch {return false;}
		}
		public static string PreviewUrl(string contentGroup, MobileCaps _mobile)
		{
			string contentUrl = String.Format("Url_{0}", contentGroup);
			if(contentGroup != "SCHEME")
			{
//				if(_mobile.ScreenPixelsWidth > 210) contentUrl = contentUrl +  "_BIG";
			}
			return contentUrl;
		}
		
		public static string PreviewUrlSmall(string contentGroup, MobileCaps _mobile)
		{
			string contentUrl = String.Format("Url_{0}", contentGroup);
			return contentUrl;
		}

		public static bool noPreview(int idContentSet, string mobiletype)
		{
			try
			{
				if (!WapTools.isSomething(mobiletype, "Touch"))
				{
					bool desc = false;
					foreach (string id in WapTools.getNodes("NoPreviews"))
					{
						if (id == idContentSet.ToString())
						{
							desc = true;
							break;
						}
					}
					return (desc); 
				}
				else 
					return false;
			}
			catch {return false;}
		}

		public static Especial getEspecial (int nbEspecial, string day, MobileCaps mobile)
		{
			Especial esp = new Especial();
			try
			{
				string esp3g = null;
				if (is3G(mobile.MobileType) || nbEspecial == 4) esp3g = "_G";		
				else esp3g = "_X";
			
				esp.name = GetXmlValue(String.Format("Especial{0}{2}/data[@name='{1}']", nbEspecial, day, esp3g));
				try
				{
					esp.filter = GetXmlNode(String.Format("Texts/data[@name='{0}']", esp.name)).Attributes["filter"].Value;
					if (esp.filter=="") esp.filter = "IMG_COLOR";
				} 
				catch{esp.filter="IMG_COLOR";}
				return esp;
			}
			catch
			{
				esp.name = "Especial" + nbEspecial;
				esp.filter = "";
				return esp;
			}			
		}
		public static Especial GetCompatibleEspecial (Especial esp)
		{
			Especial nextEsp = new Especial();
		
			try
			{
				string nextIndex = GetXmlNode(String.Format("Texts/data[@name='{0}']", esp.name)).Attributes["next"].Value;
				try
				{
					if(nextIndex != null && nextIndex != "")
					{	
						nextEsp.name = "Especial" + nextIndex;
						nextEsp.filter = GetXmlNode(String.Format("Texts/data[@name='{0}']", nextEsp.name)).Attributes["filter"].Value;
					}
					if (nextEsp.filter=="" || nextEsp.filter== null) nextEsp.filter = "IMG_COLOR";
				} 
				catch{ nextEsp.filter="IMG_COLOR"; }
				return nextEsp;
			}
			catch
			{
				nextEsp.name = esp.name;
				nextEsp.filter = "IMG_COLOR";
				return nextEsp;
			}	
		}

		public static ArrayList getNodes(string node)
		{
			return getNodes (node, "value"); 
		}

		public static ArrayList getNodes(string node, string param)
		{
			ArrayList h = new ArrayList();
			foreach(XmlNode n in GetXmlNode(node))
			{
				h.Add(n.Attributes[param].Value);
			}
			return h;       
		}

        public static bool isBranded(KMobile.Catalog.Presentation.Content content)
		{
			string copyright = null;
			try
			{
				copyright = content.PropertyCollection["Copyright"].Value.ToString();
				bool branded = false;
				foreach (string id in WapTools.getNodes("Copyrights"))
				{
					if (id == copyright)
					{
						branded = true;
						break;
					}
				}
				return (branded); 
			}
			catch {return false;}
		}

//		public static bool isJunior(MobileCaps mobile)
//		{
//			try
//			{
//				bool junior = false;
//				foreach (string m in WapTools.getNodes("Junior_Mobiles"))
//				{
//					if (mobile.MobileType == m)
//					{
//						junior = true;
//						break;
//					}
//				}
//				return (junior); 
//			}
//			catch {return false;}
//		}

//		public static ContentSet FilterContentset (ContentSet cs)
//		{
//			for (int i = 0; i < cs.Count; i++)
//				if ((cs.ContentCollection[i]).ContentRatings[0].Value>0)
//					cs.ContentCollection.RemoveAt(i);
//			return cs;
//		}

		public static int isAlerta(int IDContentset)
		{
			int id = 0;
			try
			{
				foreach (string c in WapTools.getNodes("Alertas"))
				{
					if (c == IDContentset.ToString())
					{
						id =  Convert.ToInt32(GetXmlNode(String.Format("Alertas/ContentSet[@value='{0}']", c)).Attributes["link"].Value);
						break;
					}
				}
				return (id); 
			}
			catch {return 0;}
		}

		public static bool is3G(string mobiletype)
		{
			bool correct;
			try
			{
			//	xswitch = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]);
				
				correct = false;
				foreach (string m in WapTools.getNodes("Conditions3G/Correct"))
				{
					if (mobiletype == m)
					{
						correct = true;
						break;
					}
				}
				return (correct); 
			}
			catch {return false;}
		}

		public static bool isBig(string mobiletype)
		{
			bool correct;
			try
			{
				correct = false;
				foreach (string m in WapTools.getNodes("BigPictos"))
				{
					if (mobiletype == m)
					{
						correct = true;
						break;
					}
				}
				return (correct); 
			}
			catch {return false;}
		}

		public static bool isSomething(string mobiletype, string smthg)
		{
			bool correct;
			try
			{
				correct = false;
				foreach (string m in WapTools.getNodes(smthg))
				{
					if (mobiletype == m)
					{
						correct = true;
						break;
					}
				}
				return (correct); 
			}
			catch {return false;}
		}

		public static bool isMStore(string mobiletype)
		{
			bool correct;
			try
			{
				correct = true;
				foreach (string m in WapTools.getNodes("NotMStore"))
				{
					if (mobiletype == m)
					{
						correct = false;
						break;
					}
				}
				return (correct); 
			}
			catch {return true;}
		}

		public static int isGallery(int idContentSet)
		{
			int id = 0;
			try
			{
				foreach (string idGallery in WapTools.getNodes("IsGallery", "id"))
				{
					if (idContentSet == Convert.ToInt32(idGallery))
					{
						
						id = Convert.ToInt32(GetXmlValue(String.Format("IsGallery/Gallery[@id='{0}']", idGallery)));							
						break;
					}
				}
				return (id); 
			}
			catch {return 0;}
		}

		public static bool isSexy(int IDContentset)
		{
			try
			{
				if (Convert.ToBoolean(GetXmlValue("Home/SexyUrl")))
					foreach (string c in WapTools.getNodes("Sexy"))
						if (c == IDContentset.ToString())
							return true;
				return false;
			}
			catch {return false;}
		}

		public static string isInstructions(int IDContentset)
		{
			try
			{
				return WapTools.GetXmlNode(String.Format("Flash/ContentSet[@value='{0}']", IDContentset)).Attributes["text"].Value;
			}
			catch {return null;}
		}

		public static int isPurchaserClubV(Customer c)
		{
			try
			{
				CommandCollection cc = c.LoadCommands(CommandStatus.Delivered, DateTime.Today.AddDays(-1), DateTime.Now, new Guid(WapTools.GetXmlValue("DisplayKeyClubV")), null);
				if (cc.Count > 0)
					return cc.Count;
				else
					return -1;				
			}
			catch{return -1;}
		}

		public static int isPurchaserClub099(Customer c)
		{
			try
			{
				CommandCollection cc = c.LoadCommands(CommandStatus.Delivered, DateTime.Today.AddDays(-7), DateTime.Now, new Guid(WapTools.GetXmlValue("DisplayKeyClub099")), null);
				if (cc.Count > 0)
					return cc.Count;
				else
					return -1;				
			}
			catch{return -1;}
		}

		public static bool isPurchaser(Customer c)
		{
			try
			{
				if (c.Alias == "0343500081583261738")
					return false;
				
				CommandCollection cc = c.LoadCommands(CommandStatus.Delivered, DateTime.Now.AddMonths(-6), DateTime.Today.AddDays(-1).AddHours(18), new Guid(WapTools.GetXmlValue("DisplayKey")), null);
				if (cc.Count > 0)
					return true;
				else
					return false;
//				{
//					cc = c.LoadCommands(CommandStatus.Delivered, new DateTime(2007, 3, 1), DateTime.Today.AddDays(-1).AddHours(18), new Guid(WapTools.GetXmlValue("DisplayKeyClub")), null);
//					if (cc.Count > 0)
//						return true;
//					else
//					{
//						cc = c.LoadCommands(CommandStatus.Delivered, new DateTime(2007, 3, 1), DateTime.Today.AddDays(-1).AddHours(18), new Guid(WapTools.GetXmlValue("DisplayKeyAvatars")), null);
//						if (cc.Count > 0)
//							return true;
//						else
//							return false;
//					}		
//				}
			}
			catch{return true;}
		}

		public static bool is2x1(Customer c)
		{
			try
			{
				if (c.Alias == "0345516763289356429") //JB FM:0343500081583261738
				{
					CommandCollection cc = c.LoadCommands(CommandStatus.Delivered, DateTime.Today.AddDays(-1), DateTime.Now, new Guid(WapTools.GetXmlValue("DisplayKey")), null);
					if (cc!= null && cc.Count>0 && cc[cc.Count-1].ServiceId != 363 && cc[cc.Count-1].ServiceId != 364 && cc[cc.Count-1].ServiceId != 365 && cc[cc.Count-1].ServiceId != 366 && cc[cc.Count-1].ServiceId != 431 && cc[cc.Count-1].ServiceId != 445 && cc[cc.Count-1].ServiceId != 446)
						return true;
					return false;
				} 
				else
					return false;
			}
			catch{return false;}
		}

		public static int isPromo(HttpRequest req)
		{
			int promo = -1;
			try
			{
				if (Convert.ToBoolean(GetXmlValue("Promo/Enabled")))
				{
					try
					{
						Customer cst = new Customer(req);
						CommandCollection cc = cst.LoadCommands(CommandStatus.Delivered, DateTime.Now.AddDays(-1), DateTime.Now, new Guid(GetXmlValue("DisplayKey")), null);
						int num = 0;

						if (cc!= null && cc.Count>0 && cc[cc.Count-1].ServiceId != 363 && cc[cc.Count-1].ServiceId != 364 && cc[cc.Count-1].ServiceId != 365 && cc[cc.Count-1].ServiceId != 366)
						{
							foreach (AGInteractive.Business.Command c in cc)
								if (c.ServiceId != 363 && c.ServiceId != 364 && c.ServiceId != 365 && c.ServiceId != 366) 
									num++;
						
							if (num > 0 && num % Convert.ToInt32(GetXmlValue("Promo/Num")) == 0)
								promo = 0;
							else if (num % Convert.ToInt32(GetXmlValue("Promo/Num")) == Convert.ToInt32(GetXmlValue("Promo/Num"))-1)
								promo = 1;					
							cc = null;
							cst = null;
						}
					}
					catch
					{
						return promo;
					}
				}
				return promo;
			}
			catch {return -1;}
		}

		private static int GetAdServerCounter()
		{
			SqlDataReader reader = null;
			Int32 value = -1;
			
			try
			{
				reader = SqlHelper.ExecuteReader(
					Regedit.GetConnectionString( "IMODE" ),
					"getAdserverCounter"
					);

				if (reader.Read())
				{
					value = Convert.ToInt32(reader[0]);
				}
				reader.Close();
				return value;
			}
			catch (Exception caught)
			{
				throw new Exception("Unable to get user's info", caught);
			}
			finally
			{
				if (reader != null) reader.Close();
				reader = null;
			}
		}

		#region Exceptions & Email
		public static void SendMail (HttpContext context, Exception error)
		{
			SendMail(context, error, true); 
		}
		
		public static void SendMail (HttpContext context, string request, string response)
		{
			try
			{
				/*MailMessage mailMessage = new MailMessage();
				mailMessage.From =  "\"Im�genes y Fondos - Request AdServer\" <f.martin@ag.com>";
				mailMessage.To = "f.martin@ag.com"; // adcl_test@yahoo.es; j_arribas_df@hotmail.com"; // arribas@tid.es; amg@tid.es";
				mailMessage.Subject = "Request to ad server";
				mailMessage.BodyFormat = MailFormat.Text;
				mailMessage.BodyEncoding = Encoding.UTF8;
				mailMessage.Body = String.Format("Request: \n\n{0}\n\nResponse: \n\n{1}", request, response);
				mailMessage.Body += SysInfo(context.Request);	
				SmtpMail.SmtpServer = "192.168.0.71";
				SmtpMail.Send(mailMessage);*/

				EnhancedMailMessage mailMessage = new EnhancedMailMessage();
				mailMessage.From =  "\"Im�genes y Fondos\" <f.martin@ag.com>";
				mailMessage.To = "f.martin@ag.com"; // adcl_test@yahoo.es; j_arribas_df@hotmail.com"; // arribas@tid.es; amg@tid.es";
				mailMessage.Subject = "Request to ad server";
				mailMessage.BodyFormat = MailFormat.Text;
				mailMessage.BodyEncoding = Encoding.UTF8;
				mailMessage.Body = String.Format("Request: \n\n{0}\n\nResponse: \n\n{1}", request, response);
				mailMessage.Body += SysInfo(context.Request);	
				SmtpMail.SmtpServer = "localhost"; //"127.0.0.1";
				SmtpMail.Send(mailMessage);
			}
			catch(Exception ex)
			{
				Log.LogError("Site emocion : Unexpected exception while sending mail in emocion\\xhtml", ex);
			}
		}

		public static void SendMail (HttpContext context, Exception error, bool isHandled)
		{
			try
			{
				/*MailMessage mailMessage = new MailMessage();
				mailMessage.From =  "\"Kiwee Telecom\" <kiweetelecom@ag.com>";
				mailMessage.To = "f.martin@ag.com; lsahni@ag.com";
				mailMessage.Subject = "emocion.kiwee.com crashed";
				mailMessage.BodyFormat = MailFormat.Text;
				mailMessage.BodyEncoding = Encoding.UTF8;
				mailMessage.Body = String.Format("{0} exception in {1}\nError : {2}\n", isHandled ? "Handled" : "Unhandled", Environment.MachineName, error.ToString());
				mailMessage.Body += SysInfo(context.Request);	
				mailMessage.Body += AssemblyInfo(error);
				mailMessage.Body += EnhancedStackTrace(error);
				mailMessage.Body += GetASPSettings(context);
				SmtpMail.SmtpServer = "192.168.0.71";
				SmtpMail.Send(mailMessage);*/

				EnhancedMailMessage mailMessage = new EnhancedMailMessage();
				mailMessage.From =  "\"Kiwee Telecom\" <kiweetelecom@ag.com>";
				mailMessage.To = "f.martin@ag.com";
				mailMessage.Subject = "emocion.kiwee.com crashed";
				mailMessage.BodyFormat = MailFormat.Text;
				mailMessage.BodyEncoding = Encoding.UTF8;
				mailMessage.Body = String.Format("{0} exception in {1}\nError : {2}\n", isHandled ? "Handled" : "Unhandled", Environment.MachineName, error.ToString());
				mailMessage.Body += SysInfo(context.Request);	
				mailMessage.Body += AssemblyInfo(error);
				mailMessage.Body += EnhancedStackTrace(error);
				mailMessage.Body += GetASPSettings(context);
				SmtpMail.SmtpServer = "localhost"; 
				SmtpMail.Send(mailMessage);
			}
			catch(Exception ex)
			{
				Log.LogError("Site emocion : Unexpected exception while sending mail in emocion\\xhtml", ex);
			}
		}
		
		public static string GetASPSettings(HttpContext context)
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append("--- ASP.NET Collections --");
			sb.Append(Environment.NewLine);
			sb.Append(Environment.NewLine);

			sb.Append(HttpVarsToString(context.Request.QueryString, "QueryString"));
			sb.Append(HttpVarsToString(context.Request.QueryString, "Form"));
			sb.Append(HttpVarsToString(context.Request.Cookies));
			sb.Append(HttpVarsToString(context.Session));
			sb.Append(HttpVarsToString(context.Cache));
			sb.Append(HttpVarsToString(context.Application));
			sb.Append(HttpVarsToString(context.Request.ServerVariables, "ServerVariables"));
			
			return sb.ToString();
		}

		public static string HttpVarsToString(HttpCookieCollection cookies)
		{
			if(cookies == null) return "";
			if(cookies.Count == 0) return "";
			
			StringBuilder sb = new StringBuilder();
			
			sb.Append("Cookies");
			sb.Append(Environment.NewLine);
			sb.Append(Environment.NewLine);
			try
			{
				for(int i = 0; i < cookies.Count; i++)
					AppendLine(sb, cookies[i].Name.ToString(), cookies[i].Value.ToString());
			}
			catch{ return "";}
			
			sb.Append(Environment.NewLine);
			return sb.ToString();
		}
		
		public static string HttpVarsToString(HttpApplicationState application)
		{
			if(application == null) return "";
			if(application.Count == 0) return "";
			
			StringBuilder sb = new StringBuilder();
			
			sb.Append("Application");
			sb.Append(Environment.NewLine);
			sb.Append(Environment.NewLine);
			try
			{
				for(int i = 0; i < application.Count; i++)
					AppendLine(sb, application.Keys[i].ToString(), application[i].ToString());
			}
			catch{ return "";}
			
			sb.Append(Environment.NewLine);
			return sb.ToString();
		}
		
		public static string HttpVarsToString(Cache cache)
		{
			if(cache == null) return "";
			if(cache.Count == 0) return "";
			
			StringBuilder sb = new StringBuilder();
			
			sb.Append("Cache");
			sb.Append(Environment.NewLine);
			sb.Append(Environment.NewLine);
			try
			{
				foreach(DictionaryEntry de in cache)
					AppendLine(sb, de.Key.ToString(), de.Value.ToString());
			}
			catch{	return "";}
			
			sb.Append(Environment.NewLine);
			return sb.ToString();
		}

		public static string HttpVarsToString(HttpSessionState session)
		{
			if(session == null) return "";
			if(session.Count == 0) return "";
			
			StringBuilder sb = new StringBuilder();
			
			sb.Append("Session");
			sb.Append(Environment.NewLine);
			sb.Append(Environment.NewLine);
			try
			{
				for(int i = 0; i < session.Count; i++)
					AppendLine(sb, session.Keys[i].ToString(), session[i].ToString());
			}
			catch{ return "";}
			
			sb.Append(Environment.NewLine);
			return sb.ToString();
		}

		public static string HttpVarsToString(NameValueCollection nvc, string title)
		{
			if(nvc == null) return "";
			if(! nvc.HasKeys()) return "";
			
			StringBuilder sb = new StringBuilder();
			
			sb.Append(title);
			sb.Append(Environment.NewLine);
			sb.Append(Environment.NewLine);
			try
			{
				for(int i = 0; i < nvc.Count; i++)
					AppendLine(sb, nvc.GetKey(i).ToString(), nvc.Get(i).ToString());
			}
			catch{ return "";}
			
			sb.Append(Environment.NewLine);
			return sb.ToString();
		}
		
		public static string SysInfo(HttpRequest request)
		{	
			StringBuilder sb = new StringBuilder();
			
			try
			{
				sb.AppendFormat("{0, -30}{1}", "URL:	", WebCurrentUrl(request));
				sb.Append(Environment.NewLine);
				sb.AppendFormat("{0, -30}{1}", "User-Agent:	", request.UserAgent.ToString());
				sb.Append(Environment.NewLine);
				sb.AppendFormat("{0, -30}{1}", "Date and Time:	", DateTime.Now.ToString());
				sb.Append(Environment.NewLine);
				sb.AppendFormat("{0, -30}{1}", ".NET RunTime Version:	", Environment.Version.ToString());
				sb.Append(Environment.NewLine);
				sb.AppendFormat("{0, -30}{1}", "Application Domain:	", AppDomain.CurrentDomain.FriendlyName);
				sb.Append(Environment.NewLine);
			}
			catch
			{ return "";}
			sb.Append(Environment.NewLine);
			return sb.ToString();
		}
		
		public static string WebCurrentUrl(HttpRequest request)
		{	
			StringBuilder sb = new StringBuilder();
			
			try
			{
				sb.Append("http://"+request.ServerVariables["server_name"]);
				if(request.ServerVariables["server_port"] != "80")
					sb.Append(":"+request.ServerVariables["server_port"]);
				sb.Append(request.ServerVariables["url"]);
				if(request.ServerVariables["query_string"].Length > 0)
					sb.Append("?"+request.ServerVariables["query_string"]);

			}
			catch{ return "";}
			
			return sb.ToString();
		}
		public static string AssemblyInfo(Exception ex)
		{	
			try
			{
				Assembly a = GetAssemblyFromName(ex.Source);	
				if(a != null)
					return AssemblyDetails(a);
				else 
					return AllAssemblyDetails();
			}
			catch{ return "";}	
		}
		
		public static Assembly GetAssemblyFromName(string  assemblyName)
		{	
			foreach(Assembly a in AppDomain.CurrentDomain.GetAssemblies())
			{
					if(a.GetName().Name == assemblyName)
					return a;
			}
			return null;
		}
		
		public static string AssemblyDetails(Assembly a)
		{	
			StringBuilder sb = new StringBuilder();
			
			try
			{
				sb.AppendFormat("{0, -30}{1}", "Assembly CodeBase:	", a.CodeBase.Replace("file:///", ""));
				sb.Append(Environment.NewLine);
				sb.AppendFormat("{0, -30}{1}", "Assembly Full Name:	", a.FullName);
				sb.Append(Environment.NewLine);
				sb.AppendFormat("{0, -30}{1}", "Assembly Version:	", a.GetName().Version.ToString());
				sb.Append(Environment.NewLine);
				sb.AppendFormat("{0, -30}{1}", "Assembly BuildDate:	", AssemblyBuildDate(a).ToString());
				sb.Append(Environment.NewLine);
			}
			catch{ return "";}
			sb.Append(Environment.NewLine);
			return sb.ToString();
		}

		public static string AllAssemblyDetails()
		{	
			StringBuilder sb = new StringBuilder();
			
			try
			{
				foreach(Assembly a in AppDomain.CurrentDomain.GetAssemblies())
				{
					sb.AppendFormat("{0, -30}{1}", "Assembly CodeBase:	", a.CodeBase.Replace("file:///", ""));
					sb.Append(Environment.NewLine);
					sb.AppendFormat("{0, -30}{1}", "Assembly Full Name:	", a.FullName);
					sb.Append(Environment.NewLine);
					sb.AppendFormat("{0, -30}{1}", "Assembly Version:	", a.GetName().Version.ToString());
					sb.Append(Environment.NewLine);
					sb.AppendFormat("{0, -30}{1}", "Assembly BuildDate:	", AssemblyBuildDate(a).ToString());
					sb.Append(Environment.NewLine);
				}
				
			}
			catch{ return "";}
			sb.Append(Environment.NewLine);
			return sb.ToString();
		}

		public static DateTime AssemblyBuildDate(Assembly a)
		{	
			try
			{
				return File.GetLastWriteTime(a.Location);
			}
			catch{ return DateTime.MaxValue;}
		}
		
		public static string EnhancedStackTrace(Exception ex)
		{
			if(ex == null) return "";
			return EnhancedStackTrace(new StackTrace(ex, true));
		}

		public static string EnhancedStackTrace(StackTrace st)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(Environment.NewLine);
			sb.Append("--- Stack Trace ---");
			sb.Append(Environment.NewLine);
			try
			{
				for(int frame = 0; frame < st.FrameCount; frame++)
				{
					StackFrame sf = st.GetFrame(frame);
					sb.Append(StackFrameToString(sf));
				}
			}
			catch{ return "";}
			sb.Append(Environment.NewLine);
			return sb.ToString();
		}

		public static string StackFrameToString(StackFrame sf)
		{
			StringBuilder sb = new StringBuilder();
			int param = 0;
			MemberInfo mi = sf.GetMethod();
			sb.Append("	");
			sb.Append(mi.DeclaringType.Namespace);
			sb.Append(".");
			sb.Append(mi.DeclaringType.Name);
			sb.Append(".");
			sb.Append(mi.Name);
			sb.Append("(");
			
			foreach(ParameterInfo parameter in sf.GetMethod().GetParameters())
			{
				param += 1;
				if(param > 1) sb.Append(", ");
				sb.Append(parameter.ParameterType.Name +" ");
				sb.Append(parameter.Name);
			}
			
			sb.Append(")");
			sb.Append(Environment.NewLine);

			sb.Append("		");
			if((sf.GetFileName() == null) || (sf.GetFileName().Length == 0))
			{
				sb.Append("(Unknown File)");
				sb.Append(": N ");
				sb.Append(String.Format("{0:#0000}", sf.GetNativeOffset()));
			}
			else
			{
				sb.Append(Path.GetFileName(sf.GetFileName()));
				sb.Append(": line ");
				sb.Append(String.Format("{0:#0000}", sf.GetFileLineNumber()));
				sb.Append(", column ");
				sb.Append(String.Format("{0:#00}", sf.GetFileColumnNumber()));
			}
			sb.Append(Environment.NewLine);
			return sb.ToString();
		}

		public static void AppendLine(StringBuilder sb, string key, string value)
		{
			if(value == null) return;
			sb.Append(String.Format("	{0, -30}{1}", key, value));
			sb.Append(Environment.NewLine);
		}
		#endregion

		#region ContentGroups
		public class xContentGroup
		{
			string name, ct;
			int rows, cols;
			bool free;
			public string Name
			{
				get{ return name; }
				set{ name = value; }
			}
			public string ContentType
			{
				get{ return ct; }
				set{ ct = value; }
			}
			public int  nbRows
			{
				get{ return rows; }
				set{ rows = value; }
			}
			public int nbCols
			{
				get{ return cols; }
				set{ cols = value; }
			}
			public bool Free
			{
				get{ return free; }
				set{ free = value; }
			}
		}

		public class xContentGroupGraphic: xContentGroup
		{
			public xContentGroupGraphic()
			{
				this.nbCols = 2;
				this.nbRows = 4;
				this.Free = false;
			}
		}

		public class xContentGroupSound: xContentGroup
		{
			public xContentGroupSound()
			{
				this.nbCols = 1;
				this.nbRows = 10;
				this.Free = false;
			}
		}

          
		public class IMG : xContentGroupGraphic
		{
			public IMG()
			{
				this.Name = "IMG";
				this.ContentType = "IMG_COLOR";
			}
		}

		public class ANIM : xContentGroupGraphic
		{
			public ANIM()
			{
				this.Name = "ANIM";
				this.ContentType = "ANIM_COLOR";
			}
		}
		public class VIDEO : xContentGroupGraphic
		{
			public VIDEO()
			{
				this.Name = "VIDEO";
				this.ContentType = "VIDEO_DWL";
			}
		}

		public class GAME : xContentGroupGraphic
		{
			public GAME()
			{
				this.Name = "GAME";
				this.ContentType = "GAME_JAVA";
			}
		}

		public class SOUND: xContentGroupGraphic
		{
			public SOUND()
			{
				this.Name = "SOUND";
				this.ContentType = "SOUND_POLY";
			}
		}
		public class SFX: xContentGroupGraphic
		{
			public SFX()
			{
				this.Name = "SFX";
				this.ContentType = "SOUND_FX";
			}
		}
		public class COMPOSITE: xContentGroupGraphic
		{
			public COMPOSITE()
			{
				this.Name = "COMPOSITE";
				this.ContentType = "";
			}
		}
		public class contentgroupFactory
		{
			public xContentGroup Create(string cg)
			{
				switch(cg)
				{
					case "IMG":
					case "IMG_COLOR":
						return new IMG();
					case "ANIM":
					case "ANIM_COLOR":
						return new ANIM();
					case "VIDEO":
					case "VIDEO_DWL":
						return new VIDEO();
					case "GAME":
					case "GAME_JAVA":
						return new GAME();
					case "SOUND":
					case "SOUND_POLY":
						return new SOUND();
					case "SFX":
					case "SOUND_FX":
						return new SFX();
					case "COMPOSITE":
						return new COMPOSITE();
					default:
						return null;
				}
			}
		}

		#endregion
	
		public enum SearchType
		{
			ts_ImagenesFondos_text_top, 
			ts_ImagenesFondos_banner_top, 
			ts_home_banner_top, 
			ts_sexy_18_banner_top,
			ts_ImagenesFondos_masportales_banner_top, 
			ts_e2_0_9_p_banner_top,
			ts_e2_0_9_0_p_banner_top 
		}
	}
}
