using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using KMobile.Catalog.Services;
using web.Tools;

namespace web
{
	/// <summary>
	/// Summary description for muggins.
	/// </summary>
	public class muggins2 : XCatalogBrowsing
	{
		protected XhtmlTable tbHeader, tbTitle, tbPreviews;
		protected XhtmlTableRow rowPreviews, rowPreviews1, rowPreviews2, rowPreviews3, rowPreviews4, rowPreviews5, rowPreviews6, rowPreviews7, rowPreviews8, rowPreviews9, rowPreviews10, rowPreviews11, rowPreviews12;
		public string volver, subir;
		protected int ms;
		protected string text = "", page = "";
		public bool is3g = false;

		private void Page_Load(object sender, System.EventArgs e)
		{
			_mobile = (MobileCaps)Request.Browser;
			try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType));}
			catch{is3g = false;}

			try
			{
				WapTools.SetHeader(this.Context);
				WapTools.AddUIDatLog(Request, Response, this.Trace);				
			} 
			catch{}

			try 
			{
				_contentType = (Request.QueryString["ct"] != null) ? Request.QueryString["ct"] : "";
				_contentGroup = WapTools.GetDefaultContentGroup(_contentType);
				try{_idContentSet = (Request.QueryString["cs"] != null) ? Convert.ToInt32(Request.QueryString["cs"]) : 0;}
				catch{_idContentSet = 0;}
				_displayKey = WapTools.GetXmlValue("DisplayKey");
	
				string suf = (_mobile.ScreenPixelsWidth >= 200) ? "_100" : "_80";
				
				XhtmlLink link1 = new XhtmlLink();
				link1.ImageUrl = String.Format("{0}/Images/muggins/{1}", this.Request.ApplicationPath, "g_alternativa" + suf + ".gif");
                link1.NavigateUrl = "http://emocion.dev.kiwee.com/test/muggins/getChargingProfileAvatar.aspx?c=63523&sex=girl&skin=white&eyes=lbrown&hairStyle=short&hairColor=black&mood=fun&acc=gl_palestina_scarf&glasses=gl_noglasses&shoes=high_heeled_boots&colorShoes=green&clothes=short_dress_stars&colorClothes=brown&underWear=stripedleggins&colorUnderWear=red&trousers=tartan&colorTrousers=white";
				XhtmlTableCell cell = new XhtmlTableCell();
				cell.HorizontalAlign = HorizontalAlign.Center;
				cell.Controls.Add(link1);
				rowPreviews1.Controls.Add(cell);
				
				XhtmlLink link2 = new XhtmlLink();
				link2.ImageUrl = String.Format("{0}/Images/muggins/{1}", this.Request.ApplicationPath, "b_agresivo" + suf + ".gif");
                link2.NavigateUrl = "http://emocion.dev.kiwee.com/test/muggins/getChargingProfileAvatar.aspx?c=64695&sex=boy&skin=white&eyes=lbrown&hairStyle=soldier&hairColor=lbrown&mood=furious&acc=hand_band&glasses=noglasses&shoes=sneakers2&colorShoes=blue&clothes=hoodie&colorClothes=blue&underWear=&colorUnderWear=&trousers=skater_low&colorTrousers=white";
				cell = new XhtmlTableCell();
				cell.HorizontalAlign = HorizontalAlign.Center;
				cell.Controls.Add(link2);
				rowPreviews1.Controls.Add(cell);
				
				XhtmlTools.AddLinkTableRow("LongCell", rowPreviews2, WapTools.GetText("DwldMuggin"), link1.NavigateUrl, Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
				XhtmlTools.AddLinkTableRow("LongCell", rowPreviews2, WapTools.GetText("DwldMuggin"), link2.NavigateUrl, Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
				
				link1 = new XhtmlLink();
				link1.ImageUrl = String.Format("{0}/Images/muggins/{1}", this.Request.ApplicationPath, "g_loba" + suf + ".gif");
                link1.NavigateUrl = "http://emocion.dev.kiwee.com/test/muggins/getChargingProfileAvatar.aspx?c=63523&sex=girl&skin=mulato&eyes=brown&hairStyle=devil&hairColor=blond&mood=angry&acc=lollypop&glasses=gl_sun_glasses_orange&shoes=vampire_shoes&colorShoes=red&clothes=vampire_gown&colorClothes=red&underWear=&colorUnderWear=black&trousers=&colorTrousers=black";
				cell = new XhtmlTableCell();
				cell.HorizontalAlign = HorizontalAlign.Center;
				cell.Controls.Add(link1);
				rowPreviews3.Controls.Add(cell);
				
				link2 = new XhtmlLink();
				link2.ImageUrl = String.Format("{0}/Images/muggins/{1}", this.Request.ApplicationPath, "b_alternativo" + suf + ".gif");
                link2.NavigateUrl = "http://emocion.dev.kiwee.com/test/muggins/getChargingProfileAvatar.aspx?c=63521&sex=boy&skin=black&eyes=blue&hairStyle=dread&hairColor=brown&mood=cool&acc=shoulderbag&glasses=noglasses&shoes=snowboard_boots&colorShoes=blue&clothes=pull-over&colorClothes=blue&underWear=&colorUnderWear=&trousers=baggies&colorTrousers=red";
				cell = new XhtmlTableCell();
				cell.HorizontalAlign = HorizontalAlign.Center;
				cell.Controls.Add(link2);
				rowPreviews3.Controls.Add(cell);

				XhtmlTools.AddLinkTableRow("LongCell", rowPreviews4, WapTools.GetText("DwldMuggin"), link1.NavigateUrl, Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
				XhtmlTools.AddLinkTableRow("LongCell", rowPreviews4, WapTools.GetText("DwldMuggin"), link2.NavigateUrl, Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
				
				link1 = new XhtmlLink();
				link1.ImageUrl = String.Format("{0}/Images/muggins/{1}", this.Request.ApplicationPath, "g_pisci" + suf + ".gif");
                link1.NavigateUrl = "http://emocion.dev.kiwee.com/test/muggins/getChargingProfileAvatar.aspx?c=63758&sex=girl&skin=white&eyes=blue&hairStyle=short&hairColor=brown&mood=happy&acc=&glasses=gl_sun_glasses_dark&shoes=beach_slippers&colorShoes=silver&clothes=short_dress_stars&colorClothes=black&underWear=&colorUnderWear=&trousers=&colorTrousers=";
				cell = new XhtmlTableCell();
				cell.HorizontalAlign = HorizontalAlign.Center;
				cell.Controls.Add(link1);
				rowPreviews5.Controls.Add(cell);
				
				link2 = new XhtmlLink();
				link2.ImageUrl = String.Format("{0}/Images/muggins/{1}", this.Request.ApplicationPath, "b_macarra" + suf + ".gif");
                link2.NavigateUrl = "http://emocion.dev.kiwee.com/test/muggins/getChargingProfileAvatar.aspx?c=63520&sex=boy&skin=mulato&eyes=blue&hairStyle=long&hairColor=lbrown&mood=happy&acc=keys&acc=hand_band&glasses=sunglasses&shoes=sneakers&colorShoes=white&clothes=tank_top&colorClothes=black&underWear=&colorUnderWear=&trousers=pleated&colorTrousers=green";
				cell = new XhtmlTableCell();
				cell.HorizontalAlign = HorizontalAlign.Center;
				cell.Controls.Add(link2);
				rowPreviews5.Controls.Add(cell);

				XhtmlTools.AddLinkTableRow("LongCell", rowPreviews6, WapTools.GetText("DwldMuggin"), link1.NavigateUrl, Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
				XhtmlTools.AddLinkTableRow("LongCell", rowPreviews6, WapTools.GetText("DwldMuggin"), link2.NavigateUrl, Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
				
				link1 = new XhtmlLink();
				link1.ImageUrl = String.Format("{0}/Images/muggins/{1}", this.Request.ApplicationPath, "g_smart" + suf + ".gif");
                link1.NavigateUrl = "http://emocion.dev.kiwee.com/test/muggins/getChargingProfileAvatar.aspx?c=63524&sex=girl&skin=black&eyes=blue&hairStyle=medium&hairColor=blond&mood=standard&acc=bangle&acc=gloves&shoes=high_heels&colorShoes=black&clothes=trench&colorClothes=brown&underWear=stripedleggins&colorUnderWear=white&trousers=tartan&colorTrousers=red&earrings=earrings_purple&necklace=neck_cross";
				cell = new XhtmlTableCell();
				cell.HorizontalAlign = HorizontalAlign.Center;
				cell.Controls.Add(link1);
				rowPreviews7.Controls.Add(cell);
				
				link2 = new XhtmlLink();
				link2.ImageUrl = String.Format("{0}/Images/muggins/{1}", this.Request.ApplicationPath, "g_compras" + suf + ".gif");
                link2.NavigateUrl = "http://emocion.dev.kiwee.com/test/muggins/getChargingProfileAvatar.aspx?c=63525&sex=girl&skin=white&eyes=lbrown&hairStyle=fringe&hairColor=red&mood=sexy&acc=chocolate_box&glasses=gl_noglasses&shoes=vampire_shoes&colorShoes=red&clothes=abrigo_borne&colorClothes=green&underWear=&colorUnderWear=black&trousers=bell_jeans&colorTrousers=white&earrings=earrings_black";
				cell = new XhtmlTableCell();
				cell.HorizontalAlign = HorizontalAlign.Center;
				cell.Controls.Add(link2);
				rowPreviews7.Controls.Add(cell);

				XhtmlTools.AddLinkTableRow("LongCell", rowPreviews8, WapTools.GetText("DwldMuggin"), link1.NavigateUrl, Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
				XhtmlTools.AddLinkTableRow("LongCell", rowPreviews8, WapTools.GetText("DwldMuggin"), link2.NavigateUrl, Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
				
				link1 = new XhtmlLink();
				link1.ImageUrl = String.Format("{0}/Images/muggins/{1}", this.Request.ApplicationPath, "g_disco" + suf + ".gif");
                link1.NavigateUrl = "http://emocion.dev.kiwee.com/test/muggins/getChargingProfileAvatar.aspx?c=63751&sex=girl&skin=white&eyes=lbrown&hairStyle=fringe&hairColor=red&mood=sexy&acc=chocolate_box&glasses=gl_noglasses&shoes=vampire_shoes&colorShoes=red&clothes=abrigo_borne&colorClothes=green&underWear=&colorUnderWear=black&trousers=bell_jeans&colorTrousers=white&earrings=earrings_black";
				cell = new XhtmlTableCell();
				cell.HorizontalAlign = HorizontalAlign.Center;
				cell.Controls.Add(link1);
				rowPreviews9.Controls.Add(cell);
				
				link2 = new XhtmlLink();
				link2.ImageUrl = String.Format("{0}/Images/muggins/{1}", this.Request.ApplicationPath, "b_devil" + suf + ".gif");
                link2.NavigateUrl = "http://emocion.dev.kiwee.com/test/muggins/getChargingProfileAvatar.aspx?c=62116&sex=boy&skin=mulato&eyes=blue&hairStyle=vampire&hairColor=black&mood=devil&acc=earpiece&glasses=glasses&shoes=vampire_shoes&colorShoes=black&clothes=vampire_blouse&colorClothes=black&underWear=&colorUnderWear=&trousers=army&colorTrousers=black";
				cell = new XhtmlTableCell();
				cell.HorizontalAlign = HorizontalAlign.Center;
				cell.Controls.Add(link2);
				rowPreviews9.Controls.Add(cell);

				XhtmlTools.AddLinkTableRow("LongCell", rowPreviews10, WapTools.GetText("DwldMuggin"), link1.NavigateUrl, Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
				XhtmlTools.AddLinkTableRow("LongCell", rowPreviews10, WapTools.GetText("DwldMuggin"), link2.NavigateUrl, Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
	
				link1 = new XhtmlLink();
				link1.ImageUrl = String.Format("{0}/Images/muggins/{1}", this.Request.ApplicationPath, "g_inlove" + suf + ".gif");
                link1.NavigateUrl = "http://emocion.dev.kiwee.com/test/muggins/getChargingProfileAvatar.aspx?c=64121&sex=girl&skin=mulato&eyes=blue&hairStyle=curly&hairColor=brown&mood=in_love&acc=gloves&acc=gl_angel&glasses=gl_noglasses&shoes=ballerina&colorShoes=red&clothes=valentines_tshirt&colorClothes=green&underWear=&colorUnderWear=green&trousers=skinny_jeans&colorTrousers=black";
				cell = new XhtmlTableCell();
				cell.HorizontalAlign = HorizontalAlign.Center;
				cell.Controls.Add(link1);
				rowPreviews11.Controls.Add(cell);
				
				link2 = new XhtmlLink();
				link2.ImageUrl = String.Format("{0}/Images/muggins/{1}", this.Request.ApplicationPath, "b_pijo" + suf + ".gif");
                link2.NavigateUrl = "http://emocion.dev.kiwee.com/test/muggins/getChargingProfileAvatar.aspx?c=64131&sex=boy&skin=black&eyes=green&hairStyle=posh&hairColor=black&mood=happy&acc=laptopbag&acc=watch&glasses=noglasses&shoes=casual&colorShoes=brown&clothes=classic_jacket&colorClothes=black&underWear=&colorUnderWear=&trousers=pleated&colorTrousers=brown";
				cell = new XhtmlTableCell();
				cell.HorizontalAlign = HorizontalAlign.Center;
				cell.Controls.Add(link2);
				rowPreviews11.Controls.Add(cell);

				XhtmlTools.AddLinkTableRow("LongCell", rowPreviews12, WapTools.GetText("DwldMuggin"), link1.NavigateUrl, Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
				XhtmlTools.AddLinkTableRow("LongCell", rowPreviews12, WapTools.GetText("DwldMuggin"), link2.NavigateUrl, Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
				
				//XhtmlTools.AddLinkTable("LongCell", tbPreviews, WapTools.GetText("PersMuggin"), "../xhtml/muggins/catalog.aspx", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall, "");					
				XhtmlTools.AddTextTableRow("", rowPreviews, "", text, Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall);
				
				link1 = null;		
				link2 = null;	
			}
			catch(Exception caught) 
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\web\\muggins.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
				Response.Redirect("./error.aspx");	
			}
			
			#region HEADER
			/*XhtmlImage img = new XhtmlImage();
			img.ImageUrl = WapTools.GetImage(this.Request, "muggin",  _mobile.ScreenPixelsWidth, is3g);
			XhtmlTools.AddImgTable(tbHeader, img); */
			XhtmlTools.AddTextTable(tbHeader, "Crea tu AVATAR - Descarga ILIMITADA", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, true, FontUnit.XXSmall);
		
			#endregion

			#region PICTOS
			if(is3g)
			{
				volver = WapTools.getPicto3g(this.Request.ApplicationPath, "volver", _mobile.ScreenPixelsWidth);
				subir = WapTools.getPicto3g(this.Request.ApplicationPath, "subir", _mobile.ScreenPixelsWidth);
			}
			_mobile = null;

			#endregion
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
