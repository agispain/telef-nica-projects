using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using KMobile.Catalog.Services;
using web.Tools;

namespace web
{
	/// <summary>
	/// Summary description for muggins.
	/// </summary>
	public class avatars : XCatalogBrowsing
	{
		protected XhtmlTable tbHeader, tbTitle, tbPreviews;
		protected XhtmlTableRow rowPreviews, rowPreviews1, rowPreviews2;
		public string volver, subir;
		protected int ms;
		protected string text = "", page = "";
		public bool is3g = false;

		private void Page_Load(object sender, System.EventArgs e)
		{
			_mobile = (MobileCaps)Request.Browser;
			try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType));}
			catch{is3g = false;}

			try
			{
				WapTools.SetHeader(this.Context);
				WapTools.AddUIDatLog(Request, Response, this.Trace);				
			} 
			catch{}

			try 
			{
				_contentType = (Request.QueryString["ct"] != null) ? Request.QueryString["ct"] : "";
				_contentGroup = WapTools.GetDefaultContentGroup(_contentType);
				try{_idContentSet = (Request.QueryString["cs"] != null) ? Convert.ToInt32(Request.QueryString["cs"]) : 0;}
				catch{_idContentSet = 0;}
				_displayKey = WapTools.GetXmlValue("DisplayKey");
	
				XhtmlLink link1 = new XhtmlLink();
				link1.ImageUrl = WapTools.GetImage(this.Request, "winter");
				
				link1.NavigateUrl = String.Format("../xhtml/muggins/catalog.aspx");
				XhtmlTableCell cell = new XhtmlTableCell();
				cell.HorizontalAlign = HorizontalAlign.Center;
				cell.Controls.Add(link1);
				rowPreviews1.Controls.Add(cell);
				//	XhtmlTools.AddImgTableRow(rowPreviews, bmimg1);

//				if (_mobile.ScreenPixelsWidth >= 170)
//				{
//					XhtmlLink link2 = new XhtmlLink();
//					link2.ImageUrl = WapTools.GetImage(this.Request, "girl_big");
//				
//					link2.NavigateUrl = String.Format("./muggins/catalog.aspx");
//					cell = new XhtmlTableCell();
//					cell.HorizontalAlign = HorizontalAlign.Center;
//					cell.Controls.Add(link2);
//					rowPreviews1.Controls.Add(cell);
//					link2 = null;
//				}

                XhtmlTools.AddLinkTableRow("LongCell", rowPreviews2, WapTools.GetText("Muggin"), "http://emocion.dev.kiwee.com/test/muggins/catalog.aspx", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
//				if (_mobile.ScreenPixelsWidth >= 170)
//					XhtmlTools.AddLinkTableRow("", rowPreviews2, WapTools.GetText("Muggin"), "./muggins/catalog.aspx", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
				//if (WapTools.isTestSite(this.Request))
				XhtmlTools.AddLinkTable("LongCell", tbTitle, WapTools.GetText("ExMuggin"), "http://emocion.dev.kiwee.com/testWeb/muggins2.aspx", Color.Empty, Color.Empty, 2, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
				
				text ="<br/>&#161;Digitaliza tu look con los Avatares de Invierno!<br/>" +
					//"15 de Octubre: Llega Halloween... Trick or treat? No te olvides de disfrazar tambi&eacute;n a tu Yo Virtual!";
					//"27 de Junio: Llega el calor y el verano, quita la ropa de abrigo y refresca a tu Yo Virtual!";
					//Se acerca San Valent&iacute;n!! Es el momento de poner tu avatar enamorado, o quiz&aacute;s coraz&oacute;n roto????";
					//"Anima a la roja. V&iacute;stete con los colores de tu selecci&oacute;n favorita: Espa&ntilde;a, Brasil, Italia, Argentina...";
					"16 de Noviembre: Sientes fr&iacute;o? Notas el viento? Y la nieve? Es hora de abrigarse bien, pero no te olvides de tus avatares, porque con la &uacute;ltima colecci&oacute;n de ropa que hemos preparado ell@s tambi&eacute;n podr&aacute;n disfrutar del Invierno! A qu&eacute; esperas?! �PONTE DE MODA Y HAZTE FAMOS@!";
					//"17 de Diciembre: Hace cada vez m&aacute;s fr&iacute;o, pero se acercan las fiestas de navidad y con ellas unos de los momentos m&aacute;s bonitos de todo el a&ntilde;o.<br/>" +
					//"Disfr&aacute;zate de Pap&aacute; Noel o convi&eacute;rtete en un Elfo del Polo! Como siempre, te hemos preparado una nueva colecci&oacute;n de ropa, fondos y accesorios especiales que te ayudar&aacute; a ser original y pasar unas fiestas inolvidables!";
					//"&#161;&#161;Puedes compartir tus nuevos avatares con tus compa&ntilde;eros de clase y competir con ellos para crear el m&aacute;s bonito!! A qu&eacute; esperas?! &#161;PONTE DE MODA Y HAZTE FAMOS@!";
					//"<br/>&#161;Bienvenido al Incre&#237;ble Mundo de los Avatares de Movistar Emoci&#243;n!<br/>" +
					//"<b>11 de Mayo: </b>Nueva colecci&#243;n de ropa y complementos de verano para chicos y chicas!!!<br/>" +
					//"El calor y el verano ya est�n aqu&iacute;: ba�adores, chanclas, nuevo fondos... A qu&eacute; esperas??? Entra y cr&eacute;ate a t&iacute; mismo. PONTE DE MODA Y HAZTE FAMOS@!...";
				//XhtmlTools.AddLinkTable("LongCell", tbPreviews, WapTools.GetText("Muggin") + " - Descarga Ilimitada", "../xhtml/muggins/catalog.aspx", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, false, FontUnit.XXSmall, "");
					
				XhtmlTools.AddTextTableRow("", rowPreviews, "", text, Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall);
				
				link1 = null;							
			}
			catch(Exception caught) 
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\web\\muggins.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
				Response.Redirect("./error.aspx");	
			}
	
			/*XhtmlImage img = new XhtmlImage();
			img.ImageUrl = WapTools.GetImage(this.Request, "muggin",  _mobile.ScreenPixelsWidth, is3g);
			XhtmlTools.AddImgTable(tbHeader, img); */
			XhtmlTools.AddTextTable(tbHeader, "Avatares - Colecci&oacute;n Invierno", Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, true, FontUnit.XXSmall);


			#region PICTOS
			if(is3g)
			{
				volver = WapTools.getPicto3g(this.Request.ApplicationPath, "volver", _mobile.ScreenPixelsWidth);
				subir = WapTools.getPicto3g(this.Request.ApplicationPath, "subir", _mobile.ScreenPixelsWidth);
			}
			_mobile = null;

			#endregion
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
