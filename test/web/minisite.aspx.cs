using System;
using System.Collections;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using KMobile.Catalog.Presentation;
using AGInteractive.Business;
using KMobile.Catalog.Services;
using web.Tools;

namespace web
{
	public class minisite : XCatalogBrowsing  
	{
		private ArrayList _contentCollImg = new ArrayList();
		private ArrayList _contentCollAnim = new ArrayList();
		private ArrayList _contentCollVideo = new ArrayList();
		private ArrayList _contentCollTemas = new ArrayList();
		private ArrayList _contentCollContentSet = new ArrayList();
		//private ArrayList _fondoCollGenera = new ArrayList();
		//private ArrayList _tonosCollGenera = new ArrayList();
		protected XhtmlTable tbLinks, tbEspecial, tbPostales, tbAhora2, tbEnd3, tbThemes, tbPub, tbCanales, tbTopNews, tbImages, tbAnims, tbShops, tbHeader, tbCategorias, tbLinkImage, tbLinkImage2, tbEnd, tbTop, tbNew, tbTitleShop, tbAhora;  
		protected XhtmlTableRow rowPostales, rowEspecial, rowImg, rowTitlesImg, rowAnims, rowTemas, rowVideos, rowTop, rowNew, rowShop1, rowShop2, rowShop3, rowShop4, rowShop5, rowmoreshops, rowTitleShops, rowEspecial2, rowEspecial3;
		protected XhtmlTableCell cellLink, cellImg, cellLink2, cellImg2, cellLink3, cellImg3;
		protected XhtmlTable tbHeader3, tbHeader4, tbHeader2, tbHeaderAhora, tbHeaderDestacados, tbTemas, tbVideos;
		int init = 0, promo = -1; 
		private bool is2x1 = false;
		public string textMarquee, buscar, emocion, back, up, fondo, musica, title = "Im&aacute;genes y Fondos", css = "xhtml.css", picto = "bullet";
		protected string f1, f2, f3, f4, f5, banner_footer, volver, subir;
		//	protected xhtml.Tools.XhtmlTable tbAnims;  
		public bool is3g = false, isWeb = false, showFooter = false, showMarquee = false;
		
		private void Page_Load(object sender, System.EventArgs e)
		{ 
			try
			{
				_mobile = (MobileCaps)Request.Browser;
				try{isWeb = WapTools.isSomething(_mobile.MobileType, "TouchWeb");}
				catch{isWeb = false;}
				if (!isWeb) 
				{
					try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType));}
					catch{is3g = false;}
				} 
				else
				{
					css = "web.css";
				}
				
				try
				{
					WapTools.SetHeader(this.Context);
					//WapTools.LogUser(this.Request, 100, _mobile.MobileType);
					WapTools.AddUIDatLog(Request, Response, this.Trace);				
				} 
				catch{}
	
				if (_mobile.MobileType != null)   
				{	 
					DateTime currentTime = DateTime.Now.AddHours(9);

					if (((currentTime.Month == 12) && (currentTime.Day == 9 || currentTime.Day == 18 || currentTime.Day == 24))
						|| (currentTime.Month == 1 && (currentTime.Day == 1 || currentTime.Day == 5)))
					{
						showMarquee = true;
						Customer customer = new Customer(this.Request);
						is2x1 = WapTools.is2x1(customer);
						Trace.Write("2x1: ", is2x1.ToString());
						textMarquee = WapTools.GetText("Hay2x1");
					} 
					else if (currentTime.Month == 12
						|| (currentTime.Month == 1 && currentTime.Day <= 8))
					{
						showMarquee = true;
						textMarquee = WapTools.GetText("GanaIpad");
					} 
					else 
					{
						showMarquee = false;
					}

					#region PROMO
//					promo = WapTools.isPromo(this.Request);
//					Trace.Warn(promo.ToString());
//					if (promo >= 0)
//					{
//						XhtmlTableRow rowPromo = new XhtmlTableRow();
//						XhtmlTools.AddTextTableRow(rowPromo, WapTools.GetXmlValue("Promo/Message" + promo.ToString()), Color.Empty, Color.Green, 2, HorizontalAlign.Center,  VerticalAlign.Middle, true, FontUnit.XSmall);
//						tbHeaderDestacados.Rows.Add(rowPromo);
//					}
//					else if (Convert.ToBoolean(WapTools.GetXmlValue("Promo/Enabled")))
//					{
//						XhtmlTableRow rowPromo = new XhtmlTableRow();
//						XhtmlTools.AddTextTableRow(rowPromo, WapTools.GetXmlValue("Promo/PromoText"), Color.Empty, Color.Green, 2, HorizontalAlign.Center,  VerticalAlign.Middle, true, FontUnit.XSmall);
//						tbHeaderDestacados.Rows.Add(rowPromo);
//					}
					#endregion

					_displayKey = WapTools.GetXmlValue("DisplayKey");
					_idContentSet = Request.QueryString["id"] != null ? Convert.ToInt32(Request.QueryString["id"]) : Convert.ToInt32(WapTools.GetXmlValue("Home/Composite"));
					//if(_idContentSet != 6447) 
					BrowseContentSetExtended(null, -1, -1);
				
					if( _mobile.IsCompatible("IMG_COLOR") )  
					{ 
						if (WapTools.GetText(_idContentSet.ToString()) != "")
							title = WapTools.GetText(_idContentSet.ToString() );

						#region HEADER
						XhtmlImage img = new XhtmlImage();
						img.ImageUrl = WapTools.GetImage(this.Request, _idContentSet.ToString(),  _mobile.ScreenPixelsWidth, is3g, true);
						if (img.ImageUrl != "")
							XhtmlTools.AddImgTable(tbHeader, img, 1);
						else
							tbHeader.Visible = false;
						
						#endregion
						
						#region IMAGENES
 
						img = new XhtmlImage();
						if(_idContentSet != 6123)	img.ImageUrl = WapTools.GetImageWeb(this.Request, "img_" + _idContentSet.ToString(),  _mobile.ScreenPixelsWidth);
						else img.ImageUrl = "" ;
						if (img.ImageUrl != "")
							XhtmlTools.AddImgTable(tbHeaderDestacados, img, 1);
						else
							img = null;
						try 
						{ 
							init = DateTime.Now.Millisecond;							
							if (_contentCollImg.Count > 0)
							{
								DisplayImages(rowImg, "IMG", "IMG_COLOR", init, _contentCollImg.Count);
								//if (_mobile.IsXHTML)
								DisplayImages(rowTitlesImg, "IMG", "IMG_COLOR", init+2, _contentCollImg.Count);
							}
//							if(_idContentSet == 6447)
//							{
//								ContentSet contentSetTemp = StaticCatalogService.GetContentsByRandomize( _displayKey, 6444, "IMG", "IMG_COLOR", _mobile.MobileType, Convert.ToInt32(WapTools.GetXmlValue("Home/Nb_Previews")) + Convert.ToInt32(WapTools.GetXmlValue("Home/Nb_Links")));
//								DisplayContents(rowImg, rowTitlesImg, contentSetTemp, contentSetTemp.Name, init);
//								contentSetTemp = null;
//							}

						}
						catch{}

						if (_contentCollContentSet.Count > 0)
						{
							try
							{
								DisplayContentSets(tbTop, "IMG", 0, 2, _idContentSet.ToString() );
								//if(_idContentSet == 6123)
								//	XhtmlTools.AddLinkTable("", tbTop, "Disney Halloween", "./linkto.aspx?cg=COMPOSITE&id=10091", Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, picto));
								DisplayContentSets(tbTop, "IMG", 2, -1, _idContentSet.ToString() );
								
							}
							catch{}
						}
						if (_idContentSet == 6032)
							XhtmlTools.AddLinkTable("LongCell", tbTop, "M�s Fondos", "./catalog.aspx?cg=IMG&cs=6032", Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));								
						else if (_idContentSet == 6178)
							XhtmlTools.AddLinkTable("LongCell", tbTop, "M�s Dibujos Animados", "./catalog.aspx?cg=COMPOSITE&cs=3144&ms=6178", Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));								
						#endregion

						#region TEMAS
						if (_contentCollTemas.Count > 0 && _mobile.IsCompatible("BG_SCHEME"))
						{
							img = new XhtmlImage();
							img.ImageUrl = WapTools.GetImageWeb(this.Request, "temas_" + _idContentSet.ToString(),  _mobile.ScreenPixelsWidth);
							if (img.ImageUrl != "")
								XhtmlTools.AddImgTable(tbHeader4, img, 1);
							try 
							{ 
								init = DateTime.Now.Millisecond;							
								DisplayImages(rowTemas, "SCHEME", "BG_SCHEME", init, _contentCollTemas.Count);
							}
							catch{}
						}  
						else
						{
							tbThemes.Visible = false;
							tbHeader4.Visible = false;
						}

						if (_contentCollContentSet.Count > 0)
						{
							try{DisplayContentSets(tbThemes, "SCHEME", 0, -1, _idContentSet.ToString() );}
							catch{}  
						}
						#endregion

						#region ANIMS DESTACADAS
						if (_contentCollAnim.Count > 0 && _mobile.IsCompatible("ANIM_COLOR"))
						{
							img = new XhtmlImage();
							img.ImageUrl = WapTools.GetImageWeb(this.Request, "anim_" + _idContentSet.ToString(),  _mobile.ScreenPixelsWidth);
							if (img.ImageUrl != "")
								XhtmlTools.AddImgTable(tbHeader2, img, 1);
							try 
							{ 
								init = DateTime.Now.Millisecond;							
								DisplayImages(rowAnims, "ANIM", "ANIM_COLOR", init, _contentCollImg.Count);
							}
							catch{}
						}  
						else
						{
							tbAnims.Visible = false;
							tbHeader2.Visible = false;
						}

						if (_contentCollContentSet.Count > 0)
						{
							try{DisplayContentSets(tbAnims, "ANIM", 0, -1, _idContentSet.ToString() );}
							catch{}  
						}
						#endregion

						#region VIDEOS DESTACADAS
						if (_contentCollVideo.Count > 0 && _mobile.IsCompatible("VIDEO_CLIP"))
						{
							img = new XhtmlImage();
							img.ImageUrl = WapTools.GetImageWeb(this.Request, "video_" + _idContentSet.ToString(),  _mobile.ScreenPixelsWidth);
							if (img.ImageUrl != "")
								XhtmlTools.AddImgTable(tbHeader3, img, 1);
							try 
							{ 
								init = DateTime.Now.Millisecond;							
								DisplayImages(rowVideos, "VIDEO_RGT", "VIDEO_CLIP", init, _contentCollImg.Count);
							}
							catch{}
							

							if (_contentCollContentSet.Count > 0)
							{
								try
								{
									DisplayContentSets(tbVideos, "VIDEO_RGT", 0, -1, _idContentSet.ToString() );
									//	XhtmlTools.AddLinkTable("", tbVideos, "Juegos de Halloween", "http://www.iwapserver.com/ms/halloween", Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.NotSet, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, picto));
								}
								catch{}
							}						
						}
						else if(_idContentSet == 6447 && _mobile.IsCompatible("VIDEO_CLIP"))
						{
							img = new XhtmlImage();
							img.ImageUrl = WapTools.GetImage(this.Request, "videos",  _mobile.ScreenPixelsWidth, is3g);
							if (img.ImageUrl != "")
								XhtmlTools.AddImgTable(tbHeader3, img, 1);
							ContentSet contentSetTemp = StaticCatalogService.GetContentsByRandomize( _displayKey, 6446, "VIDEO_RGT", WapTools.GetDefaultContentType("VIDEO_RGT"), _mobile.MobileType, Convert.ToInt32(WapTools.GetXmlValue("Home/Nb_Previews")));
							DisplayContents(rowVideos, null, contentSetTemp, contentSetTemp.Name, init);
							contentSetTemp = null;
						}
						else
						{
							tbVideos.Visible = false;
							tbHeader3.Visible = false;
						}
						#endregion

						#region LINKS
						try
						{
							if (_idContentSet == 6032)
							{
								XhtmlImage header = new XhtmlImage();
								header.ImageUrl = WapTools.GetImage(this.Request, "dedicatorias",  _mobile.ScreenPixelsWidth, is3g);
								XhtmlTools.AddImgTable(tbLinks, header, 2);
								XhtmlTableRow row = new XhtmlTableRow();
								XhtmlTableCell cell = new XhtmlTableCell();
								cell.HorizontalAlign = HorizontalAlign.Center;
								XhtmlLink lnk = new XhtmlLink();
								lnk.ImageUrl = WapTools.GetText("LnkGenera10027");
								lnk.NavigateUrl = WapTools.GetText("LnkGenera10028");
								cell.Controls.Add(lnk);
								row.Cells.Add(cell);
								if (_mobile.ScreenPixelsWidth>=140)
								{
									cell = new XhtmlTableCell();
									cell.HorizontalAlign = HorizontalAlign.Center;
									lnk = new XhtmlLink();
									lnk.ImageUrl = WapTools.GetText("LnkGenera10029");
									lnk.NavigateUrl = WapTools.GetText("LnkGenera10030");
									cell.Controls.Add(lnk);
									row.Cells.Add(cell);
								}
								tbLinks.Rows.Add(row);								
								XhtmlTools.AddLinkTable("", tbLinks, "M�s Fondodedicatorias", String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FONDODEDICATORIAS")), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								header = new XhtmlImage();
								header.ImageUrl = WapTools.GetImage(this.Request, "fondonombres",  _mobile.ScreenPixelsWidth, is3g);
								XhtmlTools.AddImgTable(tbLinks, header, 2);
								row = new XhtmlTableRow();
								cell = new XhtmlTableCell();
								cell.HorizontalAlign = HorizontalAlign.Center;
								lnk = new XhtmlLink();
								lnk.ImageUrl = WapTools.GetText("LnkGenera10023");
								lnk.NavigateUrl = WapTools.GetText("LnkGenera10024");
								cell.Controls.Add(lnk);
								row.Cells.Add(cell);
								if (_mobile.ScreenPixelsWidth>=140)
								{
									cell = new XhtmlTableCell();
									cell.HorizontalAlign = HorizontalAlign.Center;
									lnk = new XhtmlLink();
									lnk.ImageUrl = WapTools.GetText("LnkGenera10025");
									lnk.NavigateUrl = WapTools.GetText("LnkGenera10026");
									cell.Controls.Add(lnk);
									row.Cells.Add(cell);
								}
								tbLinks.Rows.Add(row);
								XhtmlTools.AddLinkTable("", tbLinks, "M�s Fondonombres", String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/FONDONOMBRES")), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							}
							if (_idContentSet == 6032)
							{
								string paramBack = "";
								img = new XhtmlImage();
								img.ImageUrl = WapTools.GetImage(this.Request, is3g ? "descargate_home" : "descargate",  _mobile.ScreenPixelsWidth, is3g);
								XhtmlTools.AddImgTable(tbLinks, img, 2);
								if (_idContentSet != 6190)
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("VIDEO"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/VIDEO"), paramBack), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								else
								{
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("IMG"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/IMG"), paramBack), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								}
								if (_mobile.IsCompatible("ANIM_COLOR"))
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("ANIM"), String.Format("./catalog.aspx?cg=COMPOSITE&cs={0}&{1}", WapTools.GetXmlValue("Home/ANIM"), paramBack), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								if (WapTools.isCompatibleThemes(_mobile))
									XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("Temas"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/TEMAS")), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("AnimaNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/ANIMANOMBRES")), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								if (_mobile.IsCompatible("VIDEO_DWL")) XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("VideoNombres"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEONOMBRES")), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
								if (_mobile.IsCompatible("VIDEO_DWL")) XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("VideoAnimaciones"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", WapTools.GetXmlValue("Home/VIDEOANIMACIONES")), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
							}
//							if (_idContentSet == 6447)
//							{
//								string paramBack = "";
//								img = new XhtmlImage();
//								img.ImageUrl = WapTools.GetImage(this.Request, is3g ? "descargate_home" : "descargate",  _mobile.ScreenPixelsWidth, is3g);
//								XhtmlTools.AddImgTable(tbLinks, img, 2);
//								if (_mobile.IsCompatible("IMG_COLOR"))
//									XhtmlTools.AddLinkTable("", tbImages, "Im�genes San Valent�n" , String.Format("./catalog.aspx?cg=IMG&cs={0}&{1}", "6444", paramBack), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
//								if (_mobile.IsCompatible("ANIM_COLOR"))
//									XhtmlTools.AddLinkTable("", tbLinks, "Animaciones San Valent�n" , String.Format("./catalog.aspx?cg=ANIM&cs={0}&{1}", "6445", paramBack), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
//								if (_mobile.IsCompatible("VIDEO_RGT")) XhtmlTools.AddLinkTable("", tbLinks, "V�deos San Valent�n" , String.Format("./catalog.aspx?cg=VIDEO_RGT&cs={0}&{1}", "6446", paramBack), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
//								//	XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("FondoNombresC"), String.Format("./linkto.aspx?cg=COMPOSITE&id={0}", "10052"), Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
//							}
//							if (_idContentSet == 5897)
//							{
//								XhtmlTools.AddLinkTable("", tbLinks, WapTools.GetText("SFGame"), "./linkto.aspx?cg=COMPOSITE&id=10077", Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XXSmall, WapTools.GetImage(this.Request, "bullet"));
//							}
						}
						catch{} 
						#endregion

						musica = (_mobile.ScreenPixelsWidth > 128) ? "M&uacute;sica y Tonos" : "M&uacute;sica";
						
						#region 3G
						if (is3g)
						{
							if (_idContentSet != 6123) css = "3g.css";
							volver = WapTools.getPicto3g(this.Request.ApplicationPath, "volver", _mobile.ScreenPixelsWidth);
							subir = WapTools.getPicto3g(this.Request.ApplicationPath, "subir", _mobile.ScreenPixelsWidth);		
						}
						#endregion
					}
				}
			}
			catch(Exception caught) 
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\xhtml\\minisite.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
				Response.Redirect("./error.aspx");				
			}
			finally
			{
				_contentCollImg = null;
				_contentCollAnim = null;
				_contentCollContentSet = null;
				//_fondoCollGenera = null;
			}
		}


		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		
		#region Override
        protected override void DisplayContentSet(KMobile.Catalog.Presentation.Content content, System.Web.UI.MobileControls.Panel pnl)
		{
			_contentCollContentSet.Add(content);
		}

        protected override void DisplayImg(KMobile.Catalog.Presentation.Content content, System.Web.UI.MobileControls.Panel pnl)
		{
			if (content.ContentGroup.Name == "IMG") _contentCollImg.Add(content);
			else if (content.ContentGroup.Name == "ANIM") _contentCollAnim.Add(content);
			else if (content.ContentGroup.Name == "SCHEME") _contentCollTemas.Add(content);
			else _contentCollVideo.Add(content);
		}
		#endregion

		#region Display
		public void DisplayImages(TableRow row, string contentGroup, string contentType, int start, int count)
		{

            KMobile.Catalog.Presentation.Content content = null;
			_imgDisplayInst = new ImgDisplayInstructions(_mobile);
			_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(WapTools.PreviewUrl(contentGroup, _mobile));
			_imgDisplayInst.TextDwld = WapTools.GetText("Download");
			_imgDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "Img");
//			if (contentGroup == "IMG")
//				_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, contentGroup, contentType, (is3g) ? HttpUtility.UrlEncode("3g|HOME_" + WapTools.GetText(_idContentSet.ToString())) : HttpUtility.UrlEncode("xhtml|HOME_" + WapTools.GetText(_idContentSet.ToString())), "", "0");
//			else

			/*if (_idContentSet == 6590)
				_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingFree(this.Request, contentGroup, contentType, "FREE_MUNDIAL", "", _idContentSet.ToString());
			else*/

			if (is2x1) 
				_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingFree(this.Request, _contentGroup, _contentType, "2x1", "", _idContentSet.ToString());
			else
				_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(contentGroup, contentType, (is3g) ? HttpUtility.UrlEncode("3g|HOME_" + WapTools.GetText(_idContentSet.ToString())) : HttpUtility.UrlEncode("xhtml|HOME_" + WapTools.GetText(_idContentSet.ToString())) , "", "0", _mobile.MobileType, promo);
			_imgDisplayInst.UrlDwld += "&ms=" + Request.QueryString["id"];

			TableItemStyle tableStyle = new TableItemStyle();
			tableStyle.HorizontalAlign = HorizontalAlign.Center;
			int previews = (_mobile.ScreenPixelsWidth < 140) ? 1 : 2;
                     
			for( int i = start; i < start + previews; i++ )
			{
				if (contentGroup == "IMG")
                    content = (KMobile.Catalog.Presentation.Content)_contentCollImg[(i) % _contentCollImg.Count];
				else if (contentGroup == "ANIM")
                    content = (KMobile.Catalog.Presentation.Content)_contentCollAnim[(i) % _contentCollAnim.Count];
				else if (contentGroup == "SCHEME")
                    content = (KMobile.Catalog.Presentation.Content)_contentCollTemas[(i) % _contentCollTemas.Count];
				else if (contentGroup == "VIDEO" || contentGroup == "VIDEO_RGT")
                    content = (KMobile.Catalog.Presentation.Content)_contentCollVideo[(i) % _contentCollVideo.Count];

				if (content != null)
				{
					XhtmlTableCell tempCell = new XhtmlTableCell();
					ImgDisplay imgDisplay = new ImgDisplay(_imgDisplayInst);
					imgDisplay.Display(tempCell, content);
					imgDisplay = null;
					tempCell.ApplyStyle(tableStyle);
					row.Cells.Add(tempCell);
					tempCell = null;
				}
			}  
			content = null;
			_imgDisplayInst = null;
			tableStyle = null;
		}                     
		public void DisplayContents(TableRow row, TableRow row2, ContentSet contentset, string name, int start)
		{
			_imgDisplayInst = new ImgDisplayInstructions(_mobile);
			_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(WapTools.PreviewUrl(contentset.ContentGroup, _mobile));
			//	_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(String.Format("Url_{0}", contentset.ContentGroup));
			_imgDisplayInst.TextDwld = WapTools.GetText("Download");
			_imgDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "download");
			//_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup), HttpUtility.UrlEncode(String.Format("{0}|HOME_{1}",referer, name)), "", _idContentSet.ToString());
			_imgDisplayInst.UrlDwld = WapTools.GetUrlBilling(contentset.ContentGroup, WapTools.GetDefaultContentType(contentset.ContentGroup),  (is3g) ? HttpUtility.UrlEncode(String.Format("3g|HOME_{0}", name)) : HttpUtility.UrlEncode(String.Format("xhtml|HOME_{0}", name)), "", _idContentSet.ToString(), _mobile.MobileType, promo) + "&p=" + _idContentSet + "&t=" + Server.UrlEncode(name);
                   
			TableItemStyle tableStyle = new TableItemStyle();
			try
			{
				tableStyle.HorizontalAlign = HorizontalAlign.Center; 
				int previews = (_mobile.ScreenPixelsWidth < 140 && contentset.ContentGroup != "ANIM") ? 1 : 2;            
				for( int i = start; i < start + previews; i++ )
				{
                    KMobile.Catalog.Presentation.Content content = contentset.ContentCollection[i % contentset.Count];
					if (content != null)
					{
						XhtmlTableCell tempCell = new XhtmlTableCell();
						//XhtmlTableCell textCell = new XhtmlTableCell();
						ImgDisplay imgDisplay = new ImgDisplay(_imgDisplayInst);
						//imgDisplay.Display(tempCell, textCell, content);
						imgDisplay.Display(tempCell, content);
						imgDisplay = null;
						tempCell.ApplyStyle(tableStyle);
						//textCell.ApplyStyle(tableStyle);
						row.Cells.Add(tempCell); 
						//rowTexts.Cells.Add(textCell);		
						tempCell = null;
					} 
					content = null;
				}
			}
			catch{}
			try
			{
				tableStyle.HorizontalAlign = HorizontalAlign.Center; 
				int previews = (_mobile.ScreenPixelsWidth < 140 && contentset.ContentGroup != "ANIM") ? 1 : 2;            
				for( int i = start + 2; i < start + 2 + previews; i++ )
				{
                    KMobile.Catalog.Presentation.Content content = contentset.ContentCollection[i % contentset.Count];
					if (content != null && row2 != null)
					{
						XhtmlTableCell tempCell = new XhtmlTableCell();
						//XhtmlTableCell textCell = new XhtmlTableCell();
						ImgDisplay imgDisplay = new ImgDisplay(_imgDisplayInst);
						//imgDisplay.Display(tempCell, textCell, content);
						imgDisplay.Display(tempCell, content);
						imgDisplay = null;
						tempCell.ApplyStyle(tableStyle);
						//textCell.ApplyStyle(tableStyle);
						row2.Cells.Add(tempCell); 
						//rowTexts.Cells.Add(textCell);		
						tempCell = null;
					} 
					content = null;
				}
			}
			catch{}
		}
		public void DisplayContentSets(XhtmlTable t, string cg, int rangeInf, int rangeSup, string ms)
		{
			_contentSetDisplayInst = new ContentSetDisplayInstructions(_mobile);
			_contentSetDisplayInst.UrlPicto = WapTools.GetImage(this.Request, picto);
			_contentSetDisplayInst.UrlDwld = "./catalog.aspx?ms=" + ms + "&cs={0}&cg={1}";
			//_contentSetDisplayInst.UrlDwld = "./linkto.aspx?id={0}&cg={1}";
			//_contentSetDisplayInst.UrlDwld = "./catalog.aspx?cg=IMG&ct=IMG_COLOR&cs={0}";
			XhtmlTableCell cell = new XhtmlTableCell();
			cell.ColumnSpan = 2;
				
			XhtmlTableRow row = new XhtmlTableRow();
			if (rangeSup == -1) rangeSup = _contentCollContentSet.Count;
			if (rangeSup > _contentCollContentSet.Count) rangeSup = _contentCollContentSet.Count;
			for( int i = rangeInf; i < rangeSup; i++ )
			{
                KMobile.Catalog.Presentation.Content content = (KMobile.Catalog.Presentation.Content)_contentCollContentSet[i];
				//	if (WapTools.FindProperty(content.PropertyCollection, "CompositeContentGroup") != cg && cg == "IMG" && WapTools.FindProperty(content.PropertyCollection, "CompositeContentGroup")=="COMPOSITE") ;
				//	else 
				if (WapTools.FindProperty(content.PropertyCollection, "CompositeContentGroup") != cg) continue;
				ContentSetDisplay contentSetDisplay = new ContentSetDisplay(_contentSetDisplayInst);
				contentSetDisplay.Display(cell, content, true);			
				contentSetDisplay = null;
				cell.CssClass="LongCell";
				row.Controls.Add(cell);
				cell = new XhtmlTableCell();
				cell.ColumnSpan = 2;
				cell.CssClass="LongCell";
				t.Controls.Add(row);
				row = new XhtmlTableRow();
				content = null;
			}
			//t.Controls.Add(row);
			_contentSetDisplayInst = null;
			cell = null; row = null;
		}
		//
		//		public void InitializeFondonombresGenera(int rangeInf, int rangeSup)
		//		{
		//			for( int i = rangeInf; i <= rangeSup; i++ )
		//			{
		//				XhtmlLink lnk = new XhtmlLink();
		//				lnk.ImageUrl = "http://www.marketingmovil.com/portales/movistar/wap/magic/imgPre/" + WapTools.GetText("FondoGenera" + i.ToString());
		//				lnk.NavigateUrl = "http://www.marketingmovil.com:8080/portal/img/ServletAction?idaccion=2160&" + WapTools.GetText("LnkFondoGenera" + i.ToString()) + "&origen=2158";
		//				_fondoCollGenera.Add(lnk);
		//				lnk = null;
		//			}
		//		}

		//		public void InitializeTonosGenera(int rangeInf, int rangeSup)
		//		{
		//			for( int i = rangeInf; i <= rangeSup; i++ )
		//			{
		//				XhtmlLink lnk = new XhtmlLink();
		//				lnk.Text = WapTools.GetText("TonoGenera" + i.ToString());
		//				lnk.NavigateUrl = "http://www.marketingmovil.com:8080/portal/ZPTonosHumorRealAction?idaccion=3102&" + WapTools.GetText("LnkTonoGenera" + i.ToString()) + "&origen=3101";
		//				_tonosCollGenera.Add(lnk);
		//				lnk = null;
		//			}
		//		}
		//
		//		public void DisplayFondonombresGenera(XhtmlTable tb, int start)
		//		{
		//			XhtmlTableRow row = new XhtmlTableRow();
		//			int previews = (_mobile.ScreenPixelsWidth > 140) ? 2 : 1;
		//			for( int i = start; i < start + previews; i++ )
		//			{
		//				XhtmlLink lnk = (XhtmlLink) _fondoCollGenera[i % _fondoCollGenera.Count];
		//				if(lnk != null)
		//				{
		//					XhtmlTableCell cell = new XhtmlTableCell(); 
		//					cell.HorizontalAlign = HorizontalAlign.Center;
		//					cell.Controls.Add(lnk);
		//					row.Cells.Add(cell);
		//					lnk = null;
		//					cell = null;
		//				}
		//			}
		//			tb.Rows.Add(row);
		//		}
		//
		//		public void DisplayTonosGenera(XhtmlTable tb, int start)
		//		{
		//			XhtmlTableRow row = new XhtmlTableRow();
		//			for( int i = start; i < start + 2; i++ )
		//			{
		//				XhtmlLink lnk = (XhtmlLink) _tonosCollGenera[i % _tonosCollGenera.Count];
		//				if(lnk != null)
		//				{
		//					XhtmlTableCell cell = new XhtmlTableCell(); 
		//					cell.HorizontalAlign = HorizontalAlign.Center;
		//					cell.Controls.Add(lnk);
		//					row.Cells.Add(cell);
		//					lnk = null;
		//					cell = null;
		//				}
		//			}
		//			tb.Rows.Add(row);
	}
	/*	protected string PreviewUrl(string contentGroup)
		{
			string contentUrl = String.Format("Url_{0}", contentGroup);
			if(contentGroup != "SCHEME")
			{
				if(_mobile.ScreenPixelsWidth > 210) contentUrl = contentUrl +  "_BIG";
			}
			return contentUrl;
		}
	*/	
	#endregion
}


