<?xml version="1.0" encoding="UTF-8" ?>
<%@ Page language="c#" Codebehind="default.aspx.cs" AutoEventWireup="false" Inherits="web._default" %>
<%@ Register tagprefix="xhtml" Namespace="web.Tools" Assembly="web" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Imgenes y Fondos</title>
		<% if (isIE) { %>
		<link rel="shortcut icon" href="kiwee.ico"/>
			<% } %>
			<meta charset="utf-8"/>
			<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
			<meta forua="true" http-equiv="Cache-Control" content="no-cache, max-age=0, must-revalidate, proxy-revalidate, s-maxage=0"/>
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
			<link rel="stylesheet" type="text/css" href="./car.css"/>
				<style type="text/css">
		#content { BACKGROUND-IMAGE: url(Images/loading5.gif); POSITION: relative; BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: center top }
		.swipeOption { MARGIN: 20px 0px }
		.wmuGallery { MARGIN: 20px 0px }
		.swipeOption { POSITION: relative; HEIGHT: 35px; OVERFLOW: hidden }
		SPAN.arrow_l { BACKGROUND-IMAGE: url(Images/web/bg_shaddow_left.png); Z-INDEX: 90; POSITION: absolute; PADDING-BOTTOM: 0px; PADDING-LEFT: 1%; WIDTH: 52px; PADDING-RIGHT: 0px; DISPLAY: block; BACKGROUND-REPEAT: repeat-y; BACKGROUND-POSITION: left top; HEIGHT: 100%; TOP: 0px; PADDING-TOP: 2%; LEFT: 0px }
		SPAN.arrow_r { BACKGROUND-IMAGE: url(Images/web/bg_shaddow_right.png); Z-INDEX: 90; POSITION: absolute; TEXT-ALIGN: right; PADDING-BOTTOM: 0px; PADDING-LEFT: 0px; WIDTH: 52px; PADDING-RIGHT: 1%; DISPLAY: block; BACKGROUND-REPEAT: repeat-y; BACKGROUND-POSITION: right top; HEIGHT: 100%; TOP: 0px; RIGHT: 0px; PADDING-TOP: 2% }
		.swipeOptionWrapper { }
		.swipeOption .swipeOptionWrapper article { POSITION: relative; TEXT-ALIGN: center }
		.wmuGallery .wmuGalleryImage { POSITION: relative; TEXT-ALIGN: center }
		.wmuGallery .wmuGalleryImage IMG { WIDTH: auto; MAX-WIDTH: 100%; HEIGHT: auto }
		.wmuGallery .wmuGalleryImage { MARGIN-BOTTOM: 10px }
		.swipeOptionPagination { DISPLAY: none }
		IFRAME#teleifr { MARGIN: 0px auto; DISPLAY: block }
		article SPAN { TEXT-ALIGN: center; FILTER: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3d94f6', endColorstr='#0250cc'); PADDING-BOTTOM: 0px; LINE-HEIGHT: 30px; BACKGROUND-COLOR: #88aad8; PADDING-LEFT: 2%; WIDTH: 80%; PADDING-RIGHT: 2%; DISPLAY: inline-block; FONT-FAMILY: arial; COLOR: #ffffff; FONT-SIZE: 15px; FONT-WEIGHT: bold; TEXT-DECORATION: none; PADDING-TOP: 0px; moz-box-shadow: inset -1px 4px 5px -1px #145299; webkit-box-shadow: inset -1px 4px 5px -1px #145299; box-shadow: inset -1px 4px 5px -1px #145299; moz-border-radius: 6px; webkit-border-radius: 6px; border-radius: 6px; text-shadow: -1px -2px 2px #004a94 }
		article SPAN:active { POSITION: relative; TOP: 1px }
		BODY { PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px }
		DIV { PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px }
		TABLE { PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px }
		#cabecera { BACKGROUND-IMAGE: url(Images/web/cabecera.png); BACKGROUND-REPEAT: no-repeat; HEIGHT: 210px; background-size: 100% 100% }
		#pie { BACKGROUND-IMAGE: url(Images/web/pie.png); BACKGROUND-REPEAT: no-repeat; HEIGHT: 105px; background-size: 100% 100% }
		#pie A { WIDTH: 30%; DISPLAY: block; HEIGHT: 105px }
		IFRAME { MARGIN-TOP: 0px; MARGIN-BOTTOM: 0px }
		TABLE { TEXT-ALIGN: left; PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; WIDTH: 95%; PADDING-RIGHT: 0px; FONT-FAMILY: Tahoma, verdana, sans-serif; FONT-SIZE: x-small; PADDING-TOP: 0px }
		TR { PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px }
		TD { PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px }
		</style>
	</head>
	<body>
		<!--<table width="100%">
			<xhtml:XhtmlTableRow id="especiales1" runat="server" />
			<xhtml:XhtmlTableRow id="especiales2" runat="server" />
		</table>-->
		<div id="cabecera">
			<div align="center" style="PADDING-TOP: 40px">
				<img src="./Images/<%=group%>/destacados.png" alt="destacados"/>
			</div>
			<div id="slidecont">
				<div id="slidebar">
					<div class="cell">
						<a href="#" onclick="dothis('<%=destacadosLnks[0]%>');return false;"><img src="<%=destacados[0]%>" alt="destacados"/></a>
					</div>
					<div class="cell">
						<a href="#" onclick="dothis('<%=destacadosLnks[1]%>');return false;"><img src="<%=destacados[1]%>" alt="destacados"/></a>
					</div>
					<div class="cell">
						<a href="#" onclick="dothis('<%=destacadosLnks[2]%>');return false;"><img src="<%=destacados[2]%>" alt="destacados"/></a>
					</div>
					<div class="cell">
						<a href="#" onclick="dothis('<%=destacadosLnks[3]%>');return false;"><img src="<%=destacados[3]%>" alt="destacados"/></a>
					</div>
					<div class="cell">
						<a href="#" onclick="dothis('<%=destacadosLnks[4]%>');return false;"><img src="<%=destacados[4]%>" alt="destacados"/></a>
					</div>
					<div class="cell">
						<a href="#" onclick="dothis('<%=destacadosLnks[5]%>');return false;"><img src="<%=destacados[5]%>" alt="destacados"/></a>
					</div>
					<div class="cell">
						<a href="#" onclick="dothis('<%=destacadosLnks[6]%>');return false;"><img src="<%=destacados[6]%>" alt="destacados"/></a>
					</div>
					<div class="cell">
						<a href="#" onclick="dothis('<%=destacadosLnks[7]%>');return false;"><img src="<%=destacados[7]%>" alt="destacados"/></a>
					</div>
					<div class="cell">
						<a href="#" onclick="dothis('<%=destacadosLnks[8]%>');return false;"><img src="<%=destacados[8]%>" alt="destacados"/></a>
					</div>
					<div class="cell">
						<a href="#" onclick="dothis('<%=destacadosLnks[9]%>');return false;"><img src="<%=destacados[9]%>" alt="destacados"/></a>
					</div>
					<div class="cell">
						<a href="#" onclick="dothis('<%=destacadosLnks[10]%>');return false;"><img src="<%=destacados[10]%>" alt="destacados"/></a>
					</div>
					<div class="cell">
						<a href="#" onclick="dothis('<%=destacadosLnks[11]%>');return false;"><img src="<%=destacados[11]%>" alt="destacados"/></a>
					</div>
					<div class="cell">
						<a href="#" onclick="dothis('<%=destacadosLnks[12]%>');return false;"><img src="<%=destacados[12]%>" alt="destacados"/></a>
					</div>
					
					
				</div>
			</div>
			<div style="MARGIN-TOP: 50px; DISPLAY: none">
				<h3>Out:</h3>
				<div id="output"></div>
			</div>
			<div class="swipeOption conModule">
				<span class="arrow_l">
					<img src="http://emocion.kiwee.com/web/Images/web/arrows_left_bg.png" alt="destacados"/></span>
				<div class="swipeOptionWrapper">
					<article name="./linkto.aspx?id=7474">
						<span>Categorias</span>
					</article>
					<article name="./linkto.aspx?id=7473">
						<span>Home</span>
					</article>
					<article name="http://emocion.dev.kiwee.com/testWeb/muggins.aspx">
						<span>Avatares</span>
					</article>
					
					<article name="./linkto.aspx?id=20">
						<span>Portales</span>
					</article>
					<article name="./linkto.aspx?cg=COMPOSITE&amp;id=3619&amp;cl=1">
						<span>Todo 0.99</span>
					</article>
					<article name="./linkto.aspx?cg=COMPOSITE&amp;id=5862&amp;cl=1">
						<span>Videos 1.99</span>
					</article>
				</div>
				<span class="arrow_r">
					<img src="http://emocion.kiwee.com/web/Images/web/arrows_right_bg.png" alt="destacados"/></span>
			</div>
		</div>
		<div>
			<img src="./Images/<%=group%>/Publicidad_EmoWeb.gif" alt="publicidad"/>
		</div>
		<xhtml:XhtmlTable id="tbPub" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />
		<div id="content" style="TEXT-ALIGN:center; FONT-SIZE:28px">
			<iframe  frameborder="0" border="0" cellspacing="0" name="ifrm"  id="iframe_content" width="100%"
				scrolling="no"></iframe>
		</div>
		<div>
			<img src="./Images/<%=group%>/Publicidad_EmoWeb.gif" alt="publicidad"/>
		</div>
		<xhtml:XhtmlTable id="tbPub2" CssClass="normal" Runat="server" cellspacing="0" cellpadding="0" />
		<div id="pie"><a href="http://wap.movistar.com" target="_self"></a></div>
		<script src="./pagejavas.js" language="javascript"></script>
		<script src="./carousel.js" language="javascript"></script>
		<style> #slidebar { width: 100%; height: 50px; }
	</style>
	</body>
</html>
