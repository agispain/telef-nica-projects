using System;
using System.Collections;
using System.Configuration;
using System.Web;
using System.Drawing;
using System.Web.UI.WebControls;
using KMobile.Catalog.Services;
using KMobile.Catalog.Presentation;
using web.Tools;

namespace web.club
{
	public class _default : XCatalogBrowsing
	{
		private ArrayList _contentCollImg = new ArrayList(); 
		private ArrayList _contentCollContentSet = new ArrayList();  
		private ArrayList _contentCollTexts = new ArrayList();  
		protected XhtmlTable tbPreviews, tbLinks;
		protected XhtmlTableRow rowPreviews, rowPreviews2, rowPreviews3;
		public string buscar, emocion, atras, up, fondo, back, title =  "Im&aacute;genes y Fondos";
		public string marquee;
		public bool isHome = false, is3g = false, showHeader = false, showFooter = false;
		protected int isSexy = 0;

		private void Page_Load(object sender, System.EventArgs e)
		{  
			try 
			{
				_mobile = (MobileCaps)Request.Browser;
				try{is3g = Convert.ToBoolean(ConfigurationSettings.AppSettings["Switch_3G"]) && (WapTools.is3G(_mobile.MobileType));}
				catch{is3g = false;}

				try
				{
					WapTools.SetHeader(this.Context);
					WapTools.AddUIDatLog(Request, Response, this.Trace);																	
				} 
				catch{}

				if (_mobile.MobileType != null &&  _mobile.IsCompatible("IMG_COLOR"))
				{
					_idContentSet = 7016;
					_displayKey = WapTools.GetXmlValue("DisplayKeyClub"); 
					BrowseContentSetExtended(null, -1, -1, false); 
					marquee = WapTools.GetText("Marquee");
			
					WapTools.LogUser(this.Request, 109, _mobile.MobileType);		

				
					XhtmlImage imgDia = new XhtmlImage();
					imgDia.ImageUrl = WapTools.GetImageWeb(this.Request, "imagendia",  _mobile.ScreenPixelsWidth);					
					imgDia.ImageAlign = ImageAlign.Middle;
					XhtmlTools.AddImgTableRow(rowPreviews, imgDia);
					//XhtmlTools.AddTextTableRow(rowPreviews, WapTools.GetText("DailyImage"), Color.Empty, Color.Empty, 1, HorizontalAlign.Center, VerticalAlign.Middle, true, FontUnit.XSmall);
					DisplayImage(rowPreviews2, rowPreviews3);
					//DisplayContentSets(tbLinks);
					//XhtmlTools.AddLinkTable("IMG", tbLinks, WapTools.GetText("ZonaPromo"), "promo.aspx", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XSmall, String.Format(WapTools.GetXmlValue("Url_PictoClub"), 4));
					
					#region BANNERS
					XhtmlImage img = new XhtmlImage();
					img.ImageUrl = WapTools.GetImageWeb(this.Request, "especiales",  _mobile.ScreenPixelsWidth);
					XhtmlTools.AddImgTable(tbLinks, img);
					img = null;
					
					XhtmlLink lnk = new XhtmlLink();
					lnk.ImageUrl = WapTools.GetImageWeb(this.Request, "sexy",  _mobile.ScreenPixelsWidth); //sexy.gif
					lnk.NavigateUrl = "../linkto.aspx?id=11050";
					XhtmlTools.AddLinkTable(tbLinks, lnk, 1);
					
					lnk = new XhtmlLink();
					lnk.ImageUrl = WapTools.GetImageWeb(this.Request, "banneramor",  _mobile.ScreenPixelsWidth); //banneramor.gif
					lnk.NavigateUrl = "../linkto.aspx?id=11051";
					XhtmlTools.AddLinkTable(tbLinks, lnk, 1);
					
					lnk = new XhtmlLink();
					lnk.ImageUrl = WapTools.GetImageWeb(this.Request, "zonaespecial",  _mobile.ScreenPixelsWidth);
					lnk.NavigateUrl = "../linkto.aspx?id=11052";
					XhtmlTools.AddLinkTable(tbLinks, lnk, 1);
					
					lnk = new XhtmlLink();
					lnk.ImageUrl = WapTools.GetImageWeb(this.Request, "zonapromo",  _mobile.ScreenPixelsWidth);
					lnk.NavigateUrl = "../linkto.aspx?id=11053";
					XhtmlTools.AddLinkTable(tbLinks, lnk, 1);
					#endregion

					XhtmlTools.AddLinkTable("LongCell", tbLinks, WapTools.GetText("MoreCategories"), "catalog.aspx?cs=6664&cg=COMPOSITE", Color.Empty, Color.Empty, 1, HorizontalAlign.Left, VerticalAlign.Middle, true, FontUnit.XSmall, WapTools.GetImage(this.Request, "bullet"));
					             
					#region PICTOS
					fondo = WapTools.GetImage(this.Request, "fondo", _mobile.ScreenPixelsWidth, false);
					buscar = WapTools.getPicto(this.Request, "buscar", _mobile);
					emocion = WapTools.getPicto(this.Request, "emocion", _mobile);
					back = WapTools.getPicto(this.Request, "back", _mobile);
					up = WapTools.getPicto(this.Request, "up", _mobile);
					#endregion

					_mobile = null;			
				}
				else
				{
					tbPreviews.Visible = false;
					XhtmlTableRow row = new XhtmlTableRow();
					XhtmlTools.AddTextTableRow(row, WapTools.GetText("Compatibility2"), Color.Empty, Color.Empty, 2, HorizontalAlign.Left, VerticalAlign.Middle, false, FontUnit.XSmall);
					tbLinks.Rows.Add(row);
					row = null;
				}
			}
			catch(Exception caught) 
			{
				WapTools.SendMail(HttpContext.Current, caught);
				Log.LogError(String.Format("Site emocion : Unexpected exception in emocion\\xhtml\\club\\catalog.aspx - UA : {0} - QueryString : {1}", Request.UserAgent, Request.ServerVariables["QUERY_STRING"]), caught);
				Response.Redirect("../error.aspx");				
			}
		}

		#region Code g�n�r� par le Concepteur Web Form
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN�: Cet appel est requis par le Concepteur Web Form ASP.NET.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		#region Override
        protected override void DisplayContentSet(KMobile.Catalog.Presentation.Content content, System.Web.UI.MobileControls.Panel pnl)
		{
			_contentCollContentSet.Add(content);
		}

        protected override void DisplayImg(KMobile.Catalog.Presentation.Content content, System.Web.UI.MobileControls.Panel pnl)
		{
			if (content.ContentGroup.Name == "IMG") _contentCollImg.Add(content);
		}
		#endregion

		#region Display
		public void DisplayImage(TableRow row, TableRow rowTitle)
		{
            KMobile.Catalog.Presentation.Content content = null;
			_imgDisplayInst = new ImgDisplayInstructions(_mobile);
			_imgDisplayInst.PreviewMaskUrl = WapTools.GetXmlValue(WapTools.PreviewUrl("IMG", _mobile));
			_imgDisplayInst.TextDwld = WapTools.GetText("Download");
			_imgDisplayInst.UrlPicto = WapTools.GetImage(this.Request, "Img");
			//_imgDisplayInst.UrlDwld = WapTools.GetUrlXView(this.Request, "IMG", "IMG_COLOR", (is3g) ? HttpUtility.UrlEncode("CLUB_xHTML|HOME") : HttpUtility.UrlEncode("xhtml|HOME"), "", "0");
			_imgDisplayInst.UrlDwld = WapTools.GetUrlBillingClub(this.Request, "IMG", "IMG_COLOR", HttpUtility.UrlEncode("CLUB_xHTML|HOME"), "", "0");
			_imgDisplayInst.DisplayDescription = false;
						
							               
			TableItemStyle tableStyle = new TableItemStyle();
			tableStyle.HorizontalAlign = HorizontalAlign.Center;

            content = (KMobile.Catalog.Presentation.Content)_contentCollImg[DateTime.Now.Day % _contentCollImg.Count];

			if (content != null)  
			{
				XhtmlTableCell tempCell = new XhtmlTableCell();
				XhtmlTableCell tempCellTitle = new XhtmlTableCell();
				ImgDisplay imgDisplay = new ImgDisplay(_imgDisplayInst);
				imgDisplay.Display(tempCell, tempCellTitle, content);
				imgDisplay = null;
				tempCell.ApplyStyle(tableStyle);
				row.Cells.Add(tempCell);
				tempCellTitle.ApplyStyle(tableStyle);
				rowTitle.Cells.Add(tempCellTitle);
				tempCell = null;
			}
			 
			content = null;
			_imgDisplayInst = null;
			tableStyle = null;
		}   
 
		public void DisplayContentSets(XhtmlTable t)
		{
			_contentSetDisplayInst = new ContentSetDisplayInstructions(_mobile);
			_contentSetDisplayInst.UrlDwld = "./catalog.aspx?cs={0}&cg={1}";
			XhtmlTableCell cell = new XhtmlTableCell();
			XhtmlTableRow row = new XhtmlTableRow();
			for( int i = 0; i < _contentCollContentSet.Count; i++ )
			{
				_contentSetDisplayInst.UrlPicto = String.Format(WapTools.GetXmlValue("Url_PictoClub"), i+1);

                KMobile.Catalog.Presentation.Content content = (KMobile.Catalog.Presentation.Content)_contentCollContentSet[i];
				ContentSetDisplay contentSetDisplay = new ContentSetDisplay(_contentSetDisplayInst);
				contentSetDisplay.Display(cell, content, true);			
				contentSetDisplay = null;
				row.Controls.Add(cell);
				cell = new XhtmlTableCell();
				t.Controls.Add(row);
				row = new XhtmlTableRow();
				content = null;
			}
			//t.Controls.Add(row);
			_contentSetDisplayInst = null;
			cell = null; row = null;
		}
		#endregion       
	}
}
