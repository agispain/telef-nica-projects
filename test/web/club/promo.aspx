<?xml version="1.0" encoding="UTF-8" ?>
<%@ Register tagprefix="xhtml" Namespace="web.Tools" Assembly="web" %>
<%@ Page language="c#" Codebehind="promo.aspx.cs" AutoEventWireup="false" Inherits="web.club.promo" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>
			<%=title%>
		</title>
		<link rel="stylesheet" href="<%=css%>" type="text/css" />
		<meta forua="true" http-equiv="Cache-Control" content="no-cache, max-age=0, must-revalidate, proxy-revalidate, s-maxage=0" />
	</head>
	<body>
		<a id="start" name="start" />
		<Xhtml:XhtmlTable id="tbHeader" Runat="server" CssClass="normal" />
			<Xhtml:XhtmlTable id="tbTitle" Runat="server" CssClass="normal" />
			<Xhtml:XhtmlTable id="tbLinks" Runat="server" CssClass="normal" /><br/>
			
			<hr />
			<table width="100%">
				<tr>
					<td align="center"><a href="./default.aspx">Zona VIP de Fondos</a></td>
				</tr>
			</table>		
		
		
		<% if (is3g) { %>
		<table width="100%" class="up_back" bgcolor="#cccccc">
			<tr>
				<td width="50%" align="left">
					<img src="<%=volver%>" alt=""/><b><a href="./catalog.aspx?cs=6664&cg=COMPOSITE">Categor&iacute;as</a></b>
				</td>
				<td width="50%" align="right">
					<b><a class="right" href="#start">Subir</a></b><img src="<%=subir%>" alt=""/>
				</td>
			</tr>
		</table>			
		<% } %>
	</body>
</html>
