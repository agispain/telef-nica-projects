/* Modernizr 2.0.6 (Custom Build) | MIT & BSD

 * Build: http://www.modernizr.com/download/#-touch-teststyles-prefixes

 */
;
window.Modernizr = function (a, b, c) {
	function y(a, b) {
		return !!~ ("" + a).indexOf(b)
	}

	function x(a, b) {
		return typeof a === b
	}

	function w(a, b) {
		return v(m.join(a + ";") + (b || ""))
	}

	function v(a) {
		j.cssText = a
	}
	var d = "2.0.6",
		e = {},
		f = b.documentElement,
		g = b.head || b.getElementsByTagName("head")[0],
		h = "modernizr",
		i = b.createElement(h),
		j = i.style,
		k, l = Object.prototype.toString,
		m = " -webkit- -moz- -o- -ms- -khtml- ".split(" "),
		n = {},
		o = {},
		p = {},
		q = [],
		r = function (a, c, d, e) {
			var g, i, j, k = b.createElement("div");
			if (parseInt(d, 10)) while (d--) j = b.createElement("div"), j.id = e ? e[d] : h + (d + 1), k.appendChild(j);
			g = ["&shy;", "<style>", a, "</style>"].join(""), k.id = h, k.innerHTML += g, f.appendChild(k), i = c(k, a), k.parentNode.removeChild(k);
			return !!i
		},
		s, t = {}.hasOwnProperty,
		u;
	!x(t, c) && !x(t.call, c) ? u = function (a, b) {
		return t.call(a, b)
	} : u = function (a, b) {
		return b in a && x(a.constructor.prototype[b], c)
	};
	var z = function (c, d) {
			var f = c.join(""),
				g = d.length;
			r(f, function (c, d) {
				var f = b.styleSheets[b.styleSheets.length - 1],
					h = f.cssRules && f.cssRules[0] ? f.cssRules[0].cssText : f.cssText || "",
					i = c.childNodes,
					j = {};
				while (g--) j[i[g].id] = i[g];
				e.touch = "ontouchstart" in a || j.touch.offsetTop === 9
			}, g, d)
		}([, ["@media (", m.join("touch-enabled),("), h, ")", "{#touch{top:9px;position:absolute}}"].join("")], [, "touch"]);
	n.touch = function () {
		return e.touch
	};
	for (var A in n) u(n, A) && (s = A.toLowerCase(), e[s] = n[A](), q.push((e[s] ? "" : "no-") + s));
	v(""), i = k = null, e._version = d, e._prefixes = m, e.testStyles = r;
	return e
}(this, this.document);

/*

 * touchSwipe - jQuery Plugin

 * http://plugins.jquery.com/project/touchSwipe

 * http://labs.skinkers.com/touchSwipe/

 *

 * Copyright (c) 2010 Matt Bryson (www.skinkers.com)

 * Dual licensed under the MIT or GPL Version 2 licenses.

 *

 * $version: 1.2.3

 */

(function (a) {
	a.fn.swipe = function (p) {
		if (!this) {
			return false
		}
		var e = {
			fingers: 1,
			threshold: 75,
			swipe: null,
			swipeLeft: null,
			swipeRight: null,
			swipeUp: null,
			swipeDown: null,
			swipeStatus: null,
			click: null,
			triggerOnTouchEnd: true,
			allowPageScroll: "auto"
		};
		var c = "left";
		var i = "right";
		var o = "up";
		var l = "down";
		var k = "none";
		var d = "horizontal";
		var g = "vertical";
		var n = "auto";
		var b = "start";
		var h = "move";
		var j = "end";
		var f = "cancel";
		var m = "start";
		if (p.allowPageScroll == undefined && (p.swipe != undefined || p.swipeStatus != undefined)) {
			p.allowPageScroll = k
		}
		if (p) {
			a.extend(e, p)
		}
		return this.each(function () {
			var B = a(this);
			var y = null;
			var C = 0;
			var s = {
				x: 0,
				y: 0
			};
			var v = {
				x: 0,
				y: 0
			};
			var E = {
				x: 0,
				y: 0
			};

			function u(G) {
				m = b;
				C = G.touches.length;
				distance = 0;
				direction = null;
				if (C == e.fingers) {
					s.x = v.x = G.touches[0].pageX;
					s.y = v.y = G.touches[0].pageY;
					if (e.swipeStatus) {
						t(G, m)
					}
				} else {
					x(G)
				}
			}

			function D(G) {
				if (m == j || m == f) {
					return
				}
				v.x = G.touches[0].pageX;
				v.y = G.touches[0].pageY;
				direction = q();
				C = G.touches.length;
				m = h;
				A(G, direction);
				if (C == e.fingers) {
					distance = w();
					if (e.swipeStatus) {
						t(G, m, direction, distance)
					}
					if (!e.triggerOnTouchEnd) {
						if (distance >= e.threshold) {
							m = j;
							t(G, m);
							x(G)
						}
					}
				} else {
					m = f;
					t(G, m);
					x(G)
				}
			}

			function F(G) {
				G.preventDefault();
				distance = w();
				direction = q();
				if (e.triggerOnTouchEnd) {
					m = j;
					if (C == e.fingers && v.x != 0) {
						if (distance >= e.threshold) {
							t(G, m);
							x(G)
						} else {
							m = f;
							t(G, m);
							x(G)
						}
					} else {
						m = f;
						t(G, m);
						x(G)
					}
				} else {
					if (m == h) {
						m = f;
						t(G, m);
						x(G)
					}
				}
			}

			function x(G) {
				C = 0;
				s.x = 0;
				s.y = 0;
				v.x = 0;
				v.y = 0;
				E.x = 0;
				E.y = 0
			}

			function t(H, G) {
				if (e.swipeStatus) {
					e.swipeStatus.call(B, H, G, direction || null, distance || 0)
				}
				if (G == f) {
					if (e.click && C == 1 && (isNaN(distance) || distance == 0)) {
						e.click.call(B, H, H.target)
					}
				}
				if (G == j) {
					if (e.swipe) {
						e.swipe.call(B, H, direction, distance)
					}
					switch (direction) {
					case c:
						if (e.swipeLeft) {
							e.swipeLeft.call(B, H, direction, distance)
						}
						break;
					case i:
						if (e.swipeRight) {
							e.swipeRight.call(B, H, direction, distance)
						}
						break;
					case o:
						if (e.swipeUp) {
							e.swipeUp.call(B, H, direction, distance)
						}
						break;
					case l:
						if (e.swipeDown) {
							e.swipeDown.call(B, H, direction, distance)
						}
						break
					}
				}
			}

			function A(G, H) {
				if (e.allowPageScroll == k) {
					G.preventDefault()
				} else {
					var I = e.allowPageScroll == n;
					switch (H) {
					case c:
						if ((e.swipeLeft && I) || (!I && e.allowPageScroll != d)) {
							G.preventDefault()
						}
						break;
					case i:
						if ((e.swipeRight && I) || (!I && e.allowPageScroll != d)) {
							G.preventDefault()
						}
						break;
					case o:
						if ((e.swipeUp && I) || (!I && e.allowPageScroll != g)) {
							G.preventDefault()
						}
						break;
					case l:
						if ((e.swipeDown && I) || (!I && e.allowPageScroll != g)) {
							G.preventDefault()
						}
						break
					}
				}
			}

			function w() {
				return Math.round(Math.sqrt(Math.pow(v.x - s.x, 2) + Math.pow(v.y - s.y, 2)))
			}

			function r() {
				var J = s.x - v.x;
				var I = v.y - s.y;
				var G = Math.atan2(I, J);
				var H = Math.round(G * 180 / Math.PI);
				if (H < 0) {
					H = 360 - Math.abs(H)
				}
				return H
			}

			function q() {
				var G = r();
				if ((G <= 45) && (G >= 0)) {
					return c
				} else {
					if ((G <= 360) && (G >= 315)) {
						return c
					} else {
						if ((G >= 135) && (G <= 225)) {
							return i
						} else {
							if ((G > 45) && (G < 135)) {
								return l
							} else {
								return o
							}
						}
					}
				}
			}
			try {
				this.addEventListener("touchstart", u, false);
				this.addEventListener("touchmove", D, false);
				this.addEventListener("touchend", F, false);
				this.addEventListener("touchcancel", x, false)
			} catch (z) {}
		})
	}
})(jQuery);

/*!

 * jQuery swipeOption v2.1

 *

 * Copyright (c) 2011 Brice Lechatellier

 * http://brice.lechatellier.com/

 *

 * Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php

 */



;
(function ($) {



	$.fn.swipeOption = function (options) {



		/* Default Options

        ================================================== */

		var defaults = {

			animation: 'fade',

			animationDuration: 110,

			slideshow: false,

			slideshowSpeed: 5000,

			slideToStart: 0,

			navigationControl: false,

			paginationControl: true,

			previousText: 'Previous',

			nextText: 'Next',

			touch: true,

			slide: 'article',

			items: 1

		};

		var options = $.extend(defaults, options);



		return this.each(function () {



			/* Variables

            ================================================== */

			var $this = $(this);

			var currentIndex = options.slideToStart;

			var wrapper = $this.find('.swipeOptionWrapper');

			var slides = $this.find(options.slide);

			var slidesCount = slides.length;

			var slideshowTimeout;

			var paginationControl;

			var isAnimating;





			/* Load Slide

            ================================================== */

			var loadSlide = function (index, infinite, touch) {

					if (isAnimating) {

						return false;

					}

					isAnimating = true;

					currentIndex = index;

					var slide = $(slides[index]);

					if (options.animation == 'fade') {

						slides.css({

							position: 'absolute',

							opacity: 0

						});

						slide.css('position', 'relative');

						slide.animate({
							opacity: 1
						}, options.animationDuration, function () {

							isAnimating = false;

						});

					} else if (options.animation == 'slide') {

						if (!infinite) {

							wrapper.animate({
								marginLeft: -$this.width() / options.items * index
							}, options.animationDuration, function () {

								isAnimating = false;

							});

						} else {

							if (index == 0) {

								wrapper.animate({
									marginLeft: -$this.width() / options.items * slidesCount
								}, options.animationDuration, function () {

									wrapper.css('marginLeft', 0);

									isAnimating = false;

								});

							} else {

								if (!touch) {

									wrapper.css('marginLeft', - $this.width() / options.items * slidesCount);

								}

								wrapper.animate({
									marginLeft: -$this.width() / options.items * index
								}, options.animationDuration, function () {

									isAnimating = false;

								});

							}

						}

					}

					if (paginationControl) {

						paginationControl.find('a').each(function (i) {

							if (i == index) {

								$(this).addClass('wmuActive');

							} else {

								$(this).removeClass('wmuActive');

							}

						});



						var xe = ($('.wmuActive').attr("title"));

						var nxe = parseInt(xe) + 2;

						var ey = $('div article:nth-child(' + nxe + ')').attr("name");


						dothis(ey);



					}



					// Trigger Event
					$this.trigger('slideLoaded', index);

				};





			/* Navigation Control

            ================================================== */

			if (options.navigationControl) {

				var prev = $('<a class="swipeOptionPrev">' + options.previousText + '</a>');

				prev.click(function (e) {

					e.preventDefault();

					clearTimeout(slideshowTimeout);

					if (currentIndex == 0) {

						loadSlide(slidesCount - 1, true);

					} else {

						loadSlide(currentIndex - 1);

					}

				});

				$this.append(prev);



				var next = $('<a class="swipeOptionNext">' + options.nextText + '</a>');

				next.click(function (e) {

					e.preventDefault();

					clearTimeout(slideshowTimeout);

					if (currentIndex + 1 == slidesCount) {

						loadSlide(0, true);

					} else {

						loadSlide(currentIndex + 1);

					}

				});

				$this.append(next);

			}





			/* Pagination Control

            ================================================== */



			if (options.paginationControl) {

				paginationControl = $('<ul class="swipeOptionPagination"></ul>');

				$.each(slides, function (i) {

					paginationControl.append('<li><a href="#" title="' + i + '">' + i + '</a></li>');

					paginationControl.find('a:eq(' + i + ')').click(function (e) {

						e.preventDefault();

						clearTimeout(slideshowTimeout);

						loadSlide(i);

					});

				});

				$this.append(paginationControl);

			}





			/* Slideshow

            ================================================== */

			if (options.slideshow) {

				var slideshow = function () {

						if (currentIndex + 1 < slidesCount) {

							loadSlide(currentIndex + 1);

						} else {

							loadSlide(0, true);

						}

						slideshowTimeout = setTimeout(slideshow, options.slideshowSpeed);

					}

				slideshowTimeout = setTimeout(slideshow, options.slideshowSpeed);

			}





			/* Resize Slider

            ================================================== */

			var resize = function () {

					var slide = $(slides[currentIndex]);

					if (options.animation == 'slide') {

						slides.css({

							width: $this.width() / options.items

						});

						wrapper.css({

							marginLeft: -$this.width() / options.items * currentIndex,

							width: $this.width() * slides.length

						});

					}

				};





			/* Touch

            ================================================== */

			var touchSwipe = function (event, phase, direction, distance) {

					clearTimeout(slideshowTimeout);

					if (phase == 'move' && (direction == 'left' || direction == 'right')) {

						if (direction == 'right') {

							if (currentIndex == 0) {

								wrapper.css('marginLeft', (-slidesCount * $this.width() / options.items) + distance);

							} else {

								wrapper.css('marginLeft', (-currentIndex * $this.width() / options.items) + distance);

							}

						} else if (direction == 'left') {

							wrapper.css('marginLeft', (-currentIndex * $this.width() / options.items) - distance);

						}

					} else if (phase == 'cancel') {

						if (direction == 'right' && currentIndex == 0) {

							wrapper.animate({
								marginLeft: -slidesCount * $this.width() / options.items
							}, options.animationDuration);

						} else {

							wrapper.animate({
								marginLeft: -currentIndex * $this.width() / options.items
							}, options.animationDuration);

						}

					} else if (phase == 'end') {

						if (direction == 'right') {

							if (currentIndex == 0) {

								loadSlide(slidesCount - 1, true, true);

							} else {

								loadSlide(currentIndex - 1);

							}

						} else if (direction == 'left') {

							if (currentIndex + 1 == slidesCount) {

								loadSlide(0, true);

							} else {

								loadSlide(currentIndex + 1);

							}

						} else {

							wrapper.animate({
								marginLeft: -currentIndex * $this.width() / options.items
							}, options.animationDuration);

						}

					}

				};

			if (options.touch && options.animation == 'slide') {

				if ($.isFunction($.fn.swipe)) {

					$this.swipe({
						triggerOnTouchEnd: false,
						swipeStatus: touchSwipe,
						allowPageScroll: 'vertical'
					});

				}

			}





			/* Init Slider

            ================================================== */

			var init = function () {

					var slide = $(slides[currentIndex]);

					var img = slide.find('img');

					img.load(function () {

						wrapper.show();

					});

					if (options.animation == 'slide') {

						if (options.items > slidesCount) {

							options.items = slidesCount;

						}

						slides.css('float', 'left');

						slides.each(function (i) {

							var slide = $(this);

							slide.attr('data-index', i);

						});

						for (var i = 0; i < options.items; i++) {

							wrapper.append($(slides[i]).clone());

						}

						slides = $this.find(options.slide);

					}

					resize();



					$this.trigger('hasLoaded');

					loadSlide(currentIndex);

				}

			init();





			/* Bind Events

            ================================================== */

			// Resize
			$(window).resize(resize);



			// Load Slide
			$this.bind('loadSlide', function (e, i) {

				clearTimeout(slideshowTimeout);

				loadSlide(i);

			});



		});

	}



})(jQuery);

$('.conModule1').swipeOption({

	touch: Modernizr.touch,

	animation: 'slide',

	items: 4

});

$('.conModule').swipeOption({

	touch: Modernizr.touch,

	animation: 'slide',

	items: 3

});

function dothis(e) {
	var content_ifr = '<iframe name="ifrm" id="iframe_content" src="' + e + '" width="100%" scrolling="auto" frameborder="0" id ="teleifr"></iframe>';
	$('#content').html(content_ifr).delay(5000);
	jQuery(function () {
		sizeIFrame();
		jQuery("#iframe_content").load(sizeIFrame);
	});
}

function sizeIFrame() {
	var helpFrame = jQuery("#iframe_content");
	var innerDoc = (helpFrame.get(0).contentDocument) ? helpFrame.get(0).contentDocument : helpFrame.get(0).contentWindow.document;
	helpFrame.height(innerDoc.body.scrollHeight + 35);
}

if (getUrlVars()["id"]){
	var content_custom_ifr = '<iframe name="ifrm" id="iframe_content" src="./linkto.aspx?id=' + getUrlVars()["id"] + '&cg=' + getUrlVars()["cg"] + '&cl=' + getUrlVars()["cl"] + '" width="100%" scrolling="auto" frameborder="0" id ="teleifr"></iframe>';
	$('#content').html(content_custom_ifr);
		jQuery(function () {
		sizeIFrame();
		jQuery("#iframe_content").load(sizeIFrame);
	});
	}

function getUrlVars() {
	  var vars = {};
	  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
		  vars[key] = value;
	  });
	  return vars;
	  

}
