using System;
using System.IO;
using System.Net;
using web.Tools;

namespace web
{
	public class dwld : System.Web.UI.Page
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			string type = Request.QueryString["type"];
			string guid = Request.QueryString["guid"];
			string name = Request.QueryString["name"];

			string url = String.Format("http://emocion.kiwee.com/xhtml/{0}/{1}/{2}", type, guid, name);

			try
			{
				Trace.Warn(url);
				HttpWebRequest myRequest = (HttpWebRequest)HttpWebRequest.Create(new Uri(url));
				myRequest.Method = "GET";
				Trace.Warn("GetResponse");
                System.Net.ServicePointManager.CertificatePolicy = new MyPolicy();
				HttpWebResponse myHttpWebResponse= (HttpWebResponse)myRequest.GetResponse();
				Trace.Warn("GetResponse");
				Stream streamResponse = myHttpWebResponse.GetResponseStream();
				StreamReader streamRead = new StreamReader( streamResponse );
				string banner = streamRead.ReadToEnd();
				Trace.Write(banner);
				Response.Clear();
				Response.AppendHeader( "content-disposition","attachment; filename=" + name );

				if (name.EndsWith(".dm"))
					Response.ContentType = "application/vnd.oma.drm.message";
				else if (name.EndsWith(".3gp"))
					Response.ContentType = "video/3gpp";
				else if (name.EndsWith(".jpg"))
					Response.ContentType = "image/jpeg";
				else if (name.EndsWith(".gif"))
					Response.ContentType = "image/gif";
				else if (name.EndsWith(".mp4"))
					Response.ContentType = "video/mp4";		

				//Response.ContentType = "application/download";
				//Response.ContentType = "video/3gpp";
				Response.Flush();
				Response.Write(streamResponse); //.WriteFile( f.Name );
				Response.End();

				streamResponse.Close();
				myHttpWebResponse.Close();
			}
			catch (Exception ex) {Trace.Warn(ex.Message);}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
