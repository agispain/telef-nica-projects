<?xml version="1.0" encoding="UTF-8" ?>
<%@ Register tagprefix="xhtml" Namespace="web.Tools" Assembly="web" %>
<%@ Page language="c#" Codebehind="view.aspx.cs" AutoEventWireup="false" Inherits="web.view" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
          <head>
                    <title>Im&aacute;genes</title>
                    <link rel="stylesheet" href="./xhtml.css" type="text/css" />
                    <meta forua="true" http-equiv="Cache-Control" content="no-cache, max-age=0, must-revalidate, proxy-revalidate, s-maxage=0" />
          </head>
          <body>
                     <a id="start" name="start" />
                    <Xhtml:XhtmlTable id="tbHeader" Runat="server" CssClass="normal" />
                    <Xhtml:XhtmlTable id="tbTitle" Runat="server" CssClass="normal" />
                    <Xhtml:XhtmlTable id="tbPreviews" Runat="server" CssClass="normal">
						<Xhtml:XhtmlTableRow id="rowPreview" Runat="server" />
						<Xhtml:XhtmlTableRow id="rowClub1" Runat="server" />
						<Xhtml:XhtmlTableRow id="rowClub2" Runat="server" />
						<Xhtml:XhtmlTableRow id="rowLinkPPD" Runat="server" />
                    </Xhtml:XhtmlTable>

                    <Xhtml:XhtmlTable id="tbContentset" Runat="server" CssClass="normal"/>
                    <Xhtml:XhtmlTable id="tbLinks" Runat="server" CssClass="normal"/>

                    <hr />
                    <table width="100%">
						<tr>
							<td align="center"><a href="./default.aspx" style="color: #696969">Im&aacute;genes y Fondos</a></td>
						</tr>
					</table>                                     
          </body>
</html>
